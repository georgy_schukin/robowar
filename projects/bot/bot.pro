#-------------------------------------------------
#
# Project created by QtCreator 2014-06-27T11:55:26
#
#-------------------------------------------------

QT       -= core gui

TARGET = bot
TEMPLATE = lib

include (../Local.pri)

INCLUDEPATH += "$$ROBO_HOME/include"

QMAKE_CXXFLAGS += -DROBO_EXPORT

DESTDIR = "$$ROBO_HOME/game/Bots"

SOURCES += \
    ../../src/bots/bot.cpp

HEADERS += \
    ../../include/bots/bot.h
