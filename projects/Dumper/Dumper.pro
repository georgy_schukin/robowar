TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
TARGET = Dumper

include (../Local.pri)

INCLUDEPATH += "$$ROBO_HOME/include"

DESTDIR = "$$ROBO_HOME/game"

unix {    
    LIBS += -lboost_system -lboost_thread -lboost_filesystem -ldl
}

SOURCES += \
    ../../src/dumper/dumper.cpp \
    ../../src/dumper/main.cpp \
    ../../src/engine/model/impl/environment.cpp \
    ../../src/engine/script/scriptlibrary.cpp \
    ../../src/engine/script/dllscript.cpp \
    ../../src/engine/script/localscript.cpp \
    ../../src/engine/visual/localvisualizercontroller.cpp \
    ../../src/engine/game/gameengine.cpp \
    ../../src/engine/game/gamelogic.cpp \
    ../../src/engine/visual/data/visualizerdata.cpp \
    ../../src/engine/game/gamefield.cpp \
    ../../src/engine/game/gamemap.cpp \
    ../../src/engine/visual/data/visualizerupdatedata.cpp \
    ../../src/engine/visual/data/gamefielddata.cpp \
    ../../src/engine/visual/data/robotdata.cpp \
    ../../src/engine/game/objects/robot.cpp \
    ../../src/engine/game/objects/base.cpp \
    ../../src/engine/game/objects/team.cpp \
    ../../src/engine/visual/data/basedata.cpp \
    ../../src/bots/builtin/bot.cpp \
    ../../src/engine/model/impl/robotcontroller.cpp \
    ../../src/engine/visual/data/teamdata.cpp \
    ../../src/engine/model/impl/message.cpp \
    ../../src/bots/builtin/smartbot.cpp

HEADERS += \
    ../../include/bots/builtin/bot.h \
    ../../include/bots/builtin/bots.h \
    ../../include/bots/builtin/smartbot.h \
    ../../include/dumper/dumper.h \
    ../../include/engine/game/gameengine.h \
    ../../include/engine/game/gamefield.h \
    ../../include/engine/game/gamelogic.h \
    ../../include/engine/game/gamemap.h \
    ../../include/engine/game/objects/activegameobject.h \
    ../../include/engine/game/objects/base.h \
    ../../include/engine/game/objects/cell.h \
    ../../include/engine/game/objects/gameobject.h \
    ../../include/engine/game/objects/robot.h \
    ../../include/engine/game/objects/team.h \
    ../../include/engine/model/coord2d.h \
    ../../include/engine/model/impl/baseview.h \
    ../../include/engine/model/impl/environment.h \
    ../../include/engine/model/impl/gameobjectview.h \
    ../../include/engine/model/impl/message.h \
    ../../include/engine/model/impl/robotcontroller.h \
    ../../include/engine/model/impl/robotview.h \
    ../../include/engine/model/interfaces/ibaseview.h \
    ../../include/engine/model/interfaces/ienvironment.h \
    ../../include/engine/model/interfaces/igamecell.h \
    ../../include/engine/model/interfaces/igamefield.h \
    ../../include/engine/model/interfaces/igamefieldview.h \
    ../../include/engine/model/interfaces/igameobject.h \
    ../../include/engine/model/interfaces/igameobjectview.h \
    ../../include/engine/model/interfaces/imessage.h \
    ../../include/engine/model/interfaces/irobotaction.h \
    ../../include/engine/model/interfaces/irobotcontroller.h \
    ../../include/engine/model/interfaces/irobotview.h \
    ../../include/engine/model/interfaces/iteam.h \
    ../../include/engine/model/robotfunction.h \
    ../../include/engine/script/dllscript.h \
    ../../include/engine/script/localscript.h \
    ../../include/engine/script/script.h \
    ../../include/engine/script/scriptlibrary.h \
    ../../include/engine/visual/data/basedata.h \
    ../../include/engine/visual/data/gamefielddata.h \
    ../../include/engine/visual/data/robotdata.h \
    ../../include/engine/visual/data/teamdata.h \
    ../../include/engine/visual/data/visualizerdata.h \
    ../../include/engine/visual/data/visualizerupdatedata.h \
    ../../include/engine/visual/localvisualizercontroller.h \
    ../../include/engine/visual/visualizercontroller.h \
    ../../include/engine/visual/visualizer.h \
    ../../include/robowar.h \
    ../../src/dumper/rapidjson/allocators.h \
    ../../src/dumper/rapidjson/encodings.h \
    ../../src/dumper/rapidjson/filestream.h \
    ../../src/dumper/rapidjson/internal/stack.h \
    ../../src/dumper/rapidjson/internal/strfunc.h \
    ../../src/dumper/rapidjson/rapidjson.h \
    ../../src/dumper/rapidjson/writer.h \
