#pragma once

struct Coord2d { int x, y; };

inline bool operator==(Coord2d a, Coord2d b)
{ return a.x == b.x && a.y == b.y; }

inline bool operator!=(Coord2d a, Coord2d b)
{ return a.x != b.x || a.y != b.y; }

inline Coord2d operator+(Coord2d a, Coord2d b)
{ return {a.x+b.x, a.y+b.y}; }

inline Coord2d operator-(Coord2d a, Coord2d b)
{ return {a.x-b.x, a.y-b.y}; }

inline Coord2d operator+=(Coord2d &a, Coord2d b)
{ return a = a+b; }

inline Coord2d operator-=(Coord2d &a, Coord2d b)
{ return a = a-b; }
