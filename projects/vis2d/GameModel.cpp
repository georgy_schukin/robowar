#include "GameModel.h"

#include <QJsonDocument>
#include <QByteArray>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>
#include <QFile>

GameModel::GameModel(const QString &data_dir)
: data_dir(data_dir)
{
	loaded = false;
	iter_loaded = false;
}

GameModel::~GameModel()
{

}

bool loadDocument(QJsonDocument &result, QString fname)
{
	QFile file(fname);
	if(!file.open(QIODevice::ReadOnly)) return 0;
	char *memory = (char*)file.map(0, file.size());
	if(!memory) return 0;
	result = QJsonDocument::fromJson(QByteArray(memory, file.size()));
	return 1;
}

Coord2d loadCoord2d(const QJsonValueRef &x)
{
	return { x.toArray()[0].toInt(), x.toArray()[1].toInt() };
}

Obj loadObj(const QJsonValueRef &x)
{
	auto o = x.toObject();
	Obj res { loadCoord2d(o["pos"]),
		o["team"].toInt(),
		o["hp"].toInt(), false, {0,0}, false, {0,0} };
	if(o["shoot"].isArray())
	{
		res.shoot = true;
		res.shoot_pos = loadCoord2d(o["shoot"]);
	}
	if(o["debug"].isArray())
	{
		res.debug = true;
		res.debug_pos = loadCoord2d(o["debug"]);
	}
	return res;
}

void GameModel::load()
{
	loaded = false;
	iter_loaded = false;

	QJsonDocument json;
	if(!loadDocument(json, data_dir + "/init.json")) return;

	this->field_width = json.object()["FieldWidth"].toInt();
	this->field_height = json.object()["FieldHeight"].toInt();
	this->iteration_count = 0;
	this->iteration = 0;

	teams.clear();
	for(auto i : json.object()["teams"].toArray())
		teams.push_back(i.toString());

	loaded = true;

	load_last();
	if(iteration_count)
		load_iter(0);
	emit updated();
}

void GameModel::load_last()
{
	if(!loaded) return;
	QJsonDocument json;
	if(!loadDocument(json, data_dir + "/last.json")) return;

	stopped = json.object()["stop"].toBool();
	iteration_count = json.object()["iter"].toInt();

	emit last_updated(iteration_count, stopped);
}

void GameModel::load_iter(int i)
{
	if(!loaded) return;
	iter_loaded = false;
	
	QJsonDocument json;
	if(!loadDocument(json, data_dir + QString("/%1.json").arg(i))) return;

	robots.clear();
	for(auto x : json.object()["robots"].toArray())
		robots.push_back(loadObj(x));

	bases.clear();
	for(auto x : json.object()["bases"].toArray())
		bases.push_back(loadObj(x));

	iter_loaded = true;
	emit iter_updated();
}
