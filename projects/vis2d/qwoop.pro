TEMPLATE = app
TARGET = qwoop
INCLUDEPATH += .

CONFIG += c++11

# Input
HEADERS += GameWindow.h GameModel.h Coord2d.h Projection.h
SOURCES += GameWindow.cpp GameModel.cpp Projection.cpp

QT = core gui widgets opengl
