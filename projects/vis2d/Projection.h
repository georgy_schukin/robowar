#include "Coord2d.h"

class Projection
{
public:
	Projection();

	Coord2d screenToField(Coord2d s);
	Coord2d fieldToScreen(Coord2d f, bool center = false);

	void setScale(int);
	void modifyScale(Coord2d s, int i);
	Coord2d shift;
	int scale;

	struct Cache
	{
		int b;
		int px, py;
		int bx, by;
	} c;
};


/*
scale - 1/2 of size of hexagon side (including border)
Cache structure:
   b - cell border size
   bx = sqrt(2) * scale
   by = scale
   px, py - same as (bx,by), but without border

       0   by        3*by 4*by
      _|___|__________|____|______> y
  0 _|      ___________
     |     /           \
     |    /             \
     |   /               \
     |  /                 \
 bx _| /                   \
     | \                   /
     |  \                 /
     |   \               /
     |    \             /
2*bx_|     \___________/
     |
     v

     x
 */
