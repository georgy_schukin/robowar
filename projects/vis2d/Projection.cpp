#include "Projection.h"
#include <cmath>

Projection::Projection()
{
	setScale(1);
	shift = {0,0};
}

Coord2d Projection::screenToField(Coord2d s) {
	s -= shift;
	int qx = s.x / c.bx;
	int qy = s.y / c.by;
	int dx = s.x % c.bx;
	int dy = s.y % c.by;

	bool q1 = dx*c.by + dy * c.bx >= c.bx * c.by;
	bool q2 = (c.bx-dx)*c.by + dy * c.bx >= c.bx * c.by;

	int x=qx/2,y=qy/3;

	switch(qy % 6)
	{
		case 0:
			if(qx%2 == 0) {
				if(!q1) {x--;y--;}
			} else {
				if(!q2) y--;
			}
			break;
		case 3:
			if(qx%2 == 0) {
				if(q2) x--;
				else y--;
			} else {
				if(!q1) y--;
			}
			break;
		case 4:case 5:
			x = (qx+1)/2-1;
			break;
	}
	return {x, y};
}

Coord2d Projection::fieldToScreen(Coord2d f, bool center) {
	Coord2d res { c.bx*(2*f.x + f.y%2), c.by*3*f.y};
	if(center) res = res + Coord2d {c.bx, c.by*2};
	return res + shift;
}

void Projection::setScale(int scale) {
	c.px = scale*sqrt(3);
	c.py = scale;

	c.b = 0;
	c.bx = sqrt(3) * (scale+c.b);
	c.by = scale+c.b;
}


void Projection::modifyScale(Coord2d, int i) {
	if(scale + i < 3) return;
	if(scale + i > 100) return;
	setScale(scale+i);
}
