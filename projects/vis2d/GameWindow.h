#pragma once

#include "GameModel.h"
#include "Projection.h"

#include <QSpinBox>
#include <QGLWidget>
#include <QDockWidget>
#include <QMainWindow>

struct GameView : public QGLWidget
{
	Q_OBJECT
public:
	explicit GameView(const GameModel &gm, QWidget *parent = 0);
	void glInit();
	void paintGL();

	QColor teamColor(int team);
	void setColor(int team, double d);
	void drawHexagon(Coord2d);
	void drawLine(Coord2d from, Coord2d to, char type);

	void mouseMoveEvent(QMouseEvent *);
	void mousePressEvent(QMouseEvent *);
	void mouseReleaseEvent(QMouseEvent *);

	Projection proj;
	Coord2d hover;

	Coord2d old_mouse_pos;
	bool pressed = false;

signals:
	void cell_hover(int, int);
public slots:
	void data_updated();

protected:
	const GameModel &gm;
};

class GameWindow : public QMainWindow
{
	Q_OBJECT
public:
	explicit GameWindow(GameModel &gm, QWidget *parent = 0);

public slots:
	void toggleDock();
	void toggleFullscreen();
	void test_coords(int, int);

protected:
	GameModel &gm;
	GameView *gameView;
	QDockWidget *dock;
};
