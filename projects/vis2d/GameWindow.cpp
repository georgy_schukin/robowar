#include "GameWindow.h"

#include <QApplication>
#include <QLabel>
#include <QTimer>
#include <QTextStream>
#include <QVBoxLayout>
#include <QShortcut>
#include <QMouseEvent>
#include <iostream>

GameView::GameView(const GameModel &gm, QWidget *parent)
: QGLWidget(parent), gm(gm)
{
	setMouseTracking(true);
}

void GameView::glInit() {
	int nPaneWidth = size().width();
	int nPaneHeight = size().height();

	glViewport( 0, 0, nPaneWidth, nPaneHeight );
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT );

	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(0.0, nPaneWidth, nPaneHeight, 0.0, -1.0, 1.0 );

	glMatrixMode( GL_MODELVIEW );
}

void GameView::paintGL() {
	glLoadIdentity( );
	if(gm.iter_loaded)
	{
		glColor3f(0.3, 0.3, 0.3);

		for(int y = 0; y < gm.field_height; y++) {
			drawHexagon({-1, y});
			drawHexagon({gm.field_width, y});
		}
		for(int x = 0; x < gm.field_width; x++) {
			drawHexagon({x, -1});
			drawHexagon({x, gm.field_height});
		}

		for(auto &r : gm.robots) {
			setColor(r.team, 0.2 + 0.4 * r.health / 5);
			drawHexagon(r.pos);
		}

		for(auto &r : gm.bases) {
			setColor(r.team, 1);
			drawHexagon(r.pos);
		}

		for(auto &r : gm.robots) {
			if(r.shoot)
				drawLine(r.pos, r.shoot_pos, 0);
			if(r.debug)
				drawLine(r.pos, r.debug_pos, 1);
		}
	}

	glFlush();
	swapBuffers();
}

QColor GameView::teamColor(int team)
{
	double l = 255;
	if(team == -1) return QColor(l,l,l);

	switch(team % 7)
	{
	case  0: return QColor(l, 0, 0);
	case  1: return QColor(0, l, 0);
	case  2: return QColor(0, 0, l);
	case  3: return QColor(l, l, 0);
	case  4: return QColor(0, l, l);
	case  5: return QColor(l, 0, l);
	case  6: return QColor(l/2, l/2, l/2);
	}

	return QColor(0,0,0);
}

void GameView::setColor(int team, double d) {
	QColor c = teamColor(team);
	glColor3f(c.redF()*d, c.greenF()*d, c.blueF()*d);
}

void GameView::drawHexagon(Coord2d p) {
	Coord2d base = proj.fieldToScreen(p);

	glPushMatrix();
	glTranslatef(base.x, base.y, 0);

	const int px = proj.c.px, py = proj.c.py;

	glBegin(GL_POLYGON);
	glVertex2i(px, 0);
	glVertex2i(2*px, py);
	glVertex2i(2*px, 3*py);
	glVertex2i(px, 4*py);
	glVertex2i(0, 3*py);
	glVertex2i(0, py);
	glVertex2i(px, 0);
	glEnd();

	glPopMatrix();
}

void GameView::drawLine(Coord2d f, Coord2d t, char type) {
	Coord2d base0 = proj.fieldToScreen(f, true);
	Coord2d base1 = proj.fieldToScreen(t, true);

	glLineWidth(2.5);
	if(type == 0) glColor4f(1,1,0.5,1);
	else          glColor4f(0,0,1,0.25);
	glBegin(GL_LINES);
	glVertex2i(base0.x+1, base0.y+1);
	if(type == 0) glColor4f(1,1,0.5,0);
	else          glColor4f(0,0,1,0.1);
	glVertex2i(base1.x-1, base1.y-1);
	glEnd();
}

void GameView::mouseMoveEvent(QMouseEvent *event) {
	Coord2d pos {event->pos().x(), event->pos().y()};
	if(pressed) {
		proj.shift += pos - old_mouse_pos;
		old_mouse_pos = pos;
		update();
	} else {
		Coord2d h = proj.screenToField(pos);
		if(h != hover) {
			hover = h;
			update();
		}
		cell_hover(h.x, h.y);
	}
}

void GameView::mousePressEvent(QMouseEvent *event)
{
	if(event->buttons() | Qt::LeftButton)
	{
		pressed = 1;
		old_mouse_pos = {event->pos().x(), event->pos().y()};
	}
}

void GameView::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->buttons() | Qt::LeftButton)
		pressed = 0;
}

void GameView::data_updated() {
	update();
}

////

GameWindow::GameWindow(GameModel &gm, QWidget *parent)
: QMainWindow(parent), gm(gm)
{
	setFixedSize(1000, 700);

	gameView = new GameView(gm, this);
	this->setCentralWidget(gameView);

	#define ADD_SHORTCUT(KEY_SEQ, SLOT_) {\
	QShortcut *shortcut = new QShortcut(QKeySequence(KEY_SEQ), this); \
	shortcut->setContext(Qt::ApplicationShortcut); \
	connect(shortcut, SIGNAL(activated()), this, SLOT(SLOT_())); \
	}

	ADD_SHORTCUT(Qt::Key_F10, toggleDock);
	ADD_SHORTCUT(Qt::Key_F11, toggleFullscreen);

	{
	QShortcut *shortcut = new QShortcut(QKeySequence(Qt::Key_F3), this);

	shortcut->setContext(Qt::ApplicationShortcut);
	connect(shortcut, &QShortcut::activated,
		[this](){
			this->gm.load();
		});
	}

	connect(&gm, &GameModel::iter_updated, gameView, &GameView::data_updated);

	connect(gameView, SIGNAL(cell_hover(int,int)), this, SLOT(test_coords(int, int)));


	dock = new QDockWidget(this);
	this->addDockWidget(Qt::RightDockWidgetArea, dock);

	auto dockWidget = new QWidget(this);
	dock->setWidget(dockWidget);
	auto layout = new QVBoxLayout(this);
	dockWidget->setLayout(layout);

	
	auto layout_iter = new QHBoxLayout(this);
	auto widget_iter = new QWidget(this);
	widget_iter->setLayout(layout_iter);
	layout->addWidget(widget_iter);
	auto spinbox = new QSpinBox(this);
	layout_iter->addWidget(spinbox);
	auto last_iter_label = new QLabel(this);
	layout_iter->addWidget(last_iter_label);

	connect(spinbox,
		(void (QSpinBox::*)(int))
		&QSpinBox::valueChanged, [=](int v){
			this->gm.load_iter(v);
		});

	connect(&gm, &GameModel::last_updated,
		[=](int i, bool l) {
			spinbox->setMaximum(i);
			last_iter_label->setText(QString(l?"/ %1":"/ %1+").arg(i));
		});

	auto team_names = new QWidget(this);
	auto team_names_layout = new QVBoxLayout(this);
	layout->addWidget(team_names);
	team_names->setLayout(team_names_layout);

	connect(&gm, &GameModel::updated,
		[=](){
			auto widgets =
				team_names->findChildren<QWidget *>();
			foreach(QWidget *w, widgets)
				delete w;

			QTextStream qts;
			int i = 0;
			for(auto team : this->gm.teams) {
				auto name = new QLabel(this);
				name->setText(team);
				auto color = this->gameView->teamColor(i++);

				QPalette pal;
				pal.setColor(QPalette::Text, color);
				pal.setColor(QPalette::Foreground, color);
				pal.setColor(QPalette::Background, QColor());
				name->setPalette(pal);
				name->setAutoFillBackground(true);
				team_names_layout->addWidget(name);
			}
		});

	gm.load();

	auto timer = new QTimer(this);
	timer->setInterval(100);
	connect(timer, &QTimer::timeout, 
		[=](){
			this->gm.load_last();
			if(this->gm.stopped)
				timer->stop();
		});
	timer->start();
}

void GameWindow::toggleDock()
{
	if(dock->isVisible())
		dock->hide();
	else
		dock->show();
}

void GameWindow::test_coords(int x, int y)
{
	setWindowTitle(QString("%1 %2").arg(x).arg(y));
}

void GameWindow::toggleFullscreen()
{
	if(this->isFullScreen())
		this->showNormal();
	else
		this->showFullScreen();
}

int main(int argc, char **argv)
{
	QApplication app (argc, argv);

	GameModel gm("./Dump");
	GameWindow gw(gm);
	gw.show();

	return app.exec();
}
