#pragma once

#include "Coord2d.h"

#include <QString>
#include <QObject>
#include <vector>

struct Obj
{
	Coord2d pos;
	int team;
	int health;

	bool shoot;
	Coord2d shoot_pos;

	bool debug;
	Coord2d debug_pos;
};

class GameModel : public QObject
{
Q_OBJECT
public:
	GameModel(const QString &data_dir);
	~GameModel();

	bool loaded;
	int field_width;
	int field_height;
	std::vector<QString> teams;
	bool stopped;
	int iteration_count;

	bool iter_loaded;
	int iteration;
	std::vector<Obj> robots;
	std::vector<Obj> bases;

	void load();
	void load_last();
	void load_iter(int);

signals:
	void updated();
	void iter_updated();
	void last_updated(int count, bool stopped);

private:
	const QString data_dir;
};
