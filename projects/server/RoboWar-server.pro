#-------------------------------------------------
#
# Project created by QtCreator 2015-06-09T16:31:16
#
#-------------------------------------------------

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
TARGET = robowar-server

unix {
    LIBS += -lboost_system -lboost_thread -lboost_chrono -lboost_date_time -lboost_program_options
}

SOURCES += main.cpp \    
    server/server.cpp \
    server/connection.cpp \
    server/message.cpp

HEADERS += \    
    server/server.h \
    server/server_config.h \
    server/message.h \
    server/connection.h \
    server/types.h \
    server/session.h
