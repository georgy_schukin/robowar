#include "server/server.h"

int main(int argc, char *argv[]) {

    robowar::ServerConfig conf;
    robowar::Server server(conf);

    server.start();

    return 0;
}
