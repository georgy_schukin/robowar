#pragma once

#include <boost/shared_ptr.hpp>

namespace robowar {

typedef boost::shared_ptr<class Connection> ConnectionPtr;
typedef boost::shared_ptr<class Session> SessionPtr;

class Session {
private:
    ConnectionPtr connection;
};

}
