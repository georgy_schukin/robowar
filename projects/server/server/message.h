#pragma once

#include <boost/property_tree/ptree.hpp>
#include <boost/shared_ptr.hpp>
#include <string>

namespace robowar {

class Message {
public:
    Message(const std::string &str);
    virtual ~Message() {}

    std::string getTag();
    std::string getBody();

private:
    boost::property_tree::ptree data;
};

typedef boost::shared_ptr<Message> MessagePtr;

}
