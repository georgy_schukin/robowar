#pragma once

#include "server/server_config.h"
#include "server/types.h"

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>

namespace robowar {

typedef boost::shared_ptr<class Connection> ConnectionPtr;

class Server {
public:
    Server(const ServerConfig &config);
    ~Server();

public:
    void start();
    void stop();

private:
    void startAccept();
    void handleAccept(ConnectionPtr connection, const boost::system::error_code &error);

private:
    ServerConfig config;

    IOService io_service;
    Acceptor acceptor;
};

}
