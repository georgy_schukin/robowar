#include "server/server.h"
#include "server/connection.h"

#include <boost/bind.hpp>
#include <iostream>

namespace robowar {

Server::Server(const ServerConfig &config) :
    config(config),
    acceptor(io_service, EndPoint(tcp::v4(), config.port)) {
}

Server::~Server() {
    stop();
}

void Server::start() {
    acceptor.listen();
    startAccept();
    io_service.run();
}

void Server::stop() {
    acceptor.close();
    io_service.stop();
}

void Server::startAccept() {
    std::cout << "Accepting..." << std::endl;
    ConnectionPtr connection = Connection::create(io_service);
    acceptor.async_accept(connection->getSocket(),
                          boost::bind(&Server::handleAccept, this, connection, boost::asio::placeholders::error));
}

void Server::handleAccept(ConnectionPtr connection, const boost::system::error_code &error) {
    if (!error) {

    }
    startAccept();
}

}
