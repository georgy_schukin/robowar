#pragma once

#include "server/types.h"

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/array.hpp>
#include <cstddef>

namespace robowar {

typedef boost::shared_ptr<class Connection> ConnectionPtr;
typedef boost::shared_ptr<class Message> MessagePtr;

class Connection : public boost::enable_shared_from_this<Connection> {
public:
    Connection(IOService &io_service);

    static ConnectionPtr create(IOService &io_service);

    Socket& getSocket() {
      return socket;
    }

    MessagePtr read();

private:
    void handleRead(const boost::system::error_code &ec, size_t bytes_transferred);

private:
    Socket socket;
};

}
