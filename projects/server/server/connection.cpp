#include "server/connection.h"
#include "server/message.h"

#include <boost/make_shared.hpp>

namespace robowar {

Connection::Connection(IOService &io_service)
    : socket(io_service) {
}

ConnectionPtr Connection::create(IOService &io_service) {
    return ConnectionPtr(new Connection(io_service));
}

MessagePtr Connection::read() {
    //socket.receive()
}

}
