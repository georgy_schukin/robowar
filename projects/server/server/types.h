#pragma once

#include <boost/asio.hpp>

namespace robowar {

typedef boost::asio::io_service IOService;

using namespace boost::asio::ip;

typedef tcp::socket Socket;
typedef tcp::acceptor Acceptor;
typedef tcp::endpoint EndPoint;

}
