#pragma once

#include "robowar.h"

typedef unsigned int uint;

extern "C"
{
    ROBO_FUNC void execute(robowar::IRobotController *robot);
}
