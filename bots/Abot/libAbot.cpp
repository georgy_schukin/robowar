#include <cstdlib>
#include <iostream>
#include "libAbot.h"

#define HELP 1

using namespace robowar;
using namespace std;

void callHelp (IRobotController *robot);
void listen (IRobotController *robot);

ROBO_FUNC void execute(IRobotController *robot) {
    robot->move(rand() % 7);

    int id = robot->getEnvironment()->getIteration();
    robot->getOwnMemory()->writeValue(0,id);
    robot->getCell();

    for (size_t i = 0; i < robot->getNumOfRobotsInView(); i++) {
        IRobotView *bot = robot->getRobotInView(i);
        if (bot->isEnemy()) {
            robot->shoot(bot);

            callHelp(robot);

            robot->cancelMove();
            break;
        }
    }

    for (size_t i = 0; i < robot->getNumOfBasesInView(); i++) {
        IBaseView *base = robot->getBaseInView(i);
        if (!base->isActive() || base->isEnemy()) {
            robot->shoot(base);
            robot->cancelMove();
            break;
        }
    }

    listen(robot);
}

void callHelp (IRobotController *robot)
{
    IGameCell * cell = robot->getCell();
    int x = cell->getCoord().getX();
    int y = cell->getCoord().getY();
    cout << x << " " << y << endl;
    robot->getOwnMessage()->writeValue(1,HELP);
    robot->getOwnMessage()->writeValue(2,x);
    robot->getOwnMessage()->writeValue(3,y);
    robot->transmit();

}

void listen (IRobotController *robot)
{
    for (uint i = 0;i < robot->getNumOfMessages();++i)
    {
        //cout << "MSG ";
        IMessage * msg = robot->getMessage(i);
        /*for (uint j = 0;j < msg->getSize();++j)
        {
            cout << msg->readValue(j) << " ";
        }
        cout << endl;*/

        switch (msg->readValue(1))
        {
        case HELP:
            //cout << "HELPING" << endl;
            Coord2D coord(msg->readValue(2),msg->readValue(3));
            robot->move(coord);
            break;
        }
    }
}
