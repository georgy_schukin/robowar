#include "robowar.h"
#include <cstdlib>
#include <vector>
#include <list>
#include <iostream>
#include <stdio.h>

#define MAX_NUMBER_OF_DEFENDERS 0   // PDO: #if defined(MAX_NUMBER_OF_DEFENDERS) && (MAX_NUMBER_OF_DEFENDERS>0)
#define NUMBER_OF_VISIBLE_FRIENDS_TO_BECOME_SEARCHER 10
#define NUMBER_OF_VISIBLE_FRIENDS_TO_SKIP_SENDING_MESSAGE 10
#define BECOME_SEARCHER_PROBABILITY 100

#define DEBUG 0

#define NUMBER_OF_SEARCH_LOCATIONS 8

#define MSG_ID                        0
#define MSG_STATUS                    1
#define MSG_BASE_X                    2
#define MSG_BASE_Y                    3
#define MSG_ACTION                    4
#define MSG_CHILD_NUMBER              5
#define MSG_NUMBER_OF_DEFENDERS       6
#define MSG_NUMBER_OF_ENEMIES         7
#define MSG_OFFSET                    8

#define MEM_ID                        0
#define MEM_STATUS                    1
#define MEM_BASE_X                    2
#define MEM_BASE_Y                    3
#define MEM_CHILD_NUMBER              4
#define MEM_SEARCH_LOCATION           5
#define MEM_PREVIOUS_SEARCH_LOCATION  6
#define MEM_PREVIOUS_X                7
#define MEM_PREVIOUS_Y                8

using namespace robowar;
using namespace std;

//-----------------------------------------------------------------------------------------

struct EnemyCoords
{
    int x,y;      // enemy coordinates
};

struct EnemyInfo
{
    IGameObjectView *obj;
    int distance; // to select the nearest
    int isbase;   // higher priority to attack bases
    int re;       // number of retranslations
};

enum StatusType { st_none = 0, st_born, st_searcher, st_defender, st_trooper };
enum MesagetType { mt_none = 0, mt_born, mt_born_answer, mt_shoot, mt_move };

int location_x[8];
int location_y[8];

int min(int a,int b) { return a<b?a:b; }
int max(int a,int b) { return a>b?a:b; }

void fill_locations(int field_width,int field_height,int view_range)
{
    #if (NUMBER_OF_SEARCH_LOCATIONS!=8)
        #error
    #endif
    location_x[0] = min(field_width-1, view_range);
    location_x[1] = min(field_width-1, view_range);
    location_x[2] = max(0, field_width - view_range);
    location_x[3] = max(0, field_width - view_range);
    location_x[4] = min(field_width-1, 3*view_range);
    location_x[5] = min(field_width-1, 3*view_range);
    location_x[6] = max(0, field_width - 3*view_range);
    location_x[7] = max(0, field_width - 3*view_range);

    location_y[0] = min(field_height-1, view_range);
    location_y[1] = max(0, field_height - view_range);
    location_y[2] = min(field_height-1, view_range);
    location_y[3] = max(0, field_height - view_range);
    location_y[4] = min(field_height-1, 3*view_range);
    location_y[5] = max(0, field_height - 3*view_range);
    location_y[6] = min(field_height-1, 3*view_range);
    location_y[7] = max(0, field_height - 3*view_range);
}

int rng()
{
  //static int x = rand(); x = 1664525*x + 1013904223; return x;
  return rand();
}

int Distance(int x1,int y1,int x2,int y2)
{ int d,dx,dy;
  dx = abs(x2-x1);
  dy = abs(y2-y1);
  if (dx==0 || dy==0) return dx+dy;
  if ((x1<x2 && y1%2==0) || (x2<x1 && y1%2!=0)) d = (dy+1)/2; // even right || odd left
  else                                          d = dy/2;     // even left || odd right
  if (d>dx) d = dx;
  if (d>dy) d = dy;
  return dx+dy-d;
}

void PositionMove(int &x,int &y,int direction)
{ switch(direction)
  { case 1: x++; break;
    case 2: y++; if (y%2==0) x++; break;
    case 3: y++; if (y%2!=0) x--; break;
    case 4: x--; break;
    case 5: y--; if (y%2!=0) x--; break;
    case 6: y--; if (y%2==0) x++; break;
  }
}

int choose_random_direction(IRobotController *robot,int a,int b)
{   if (!robot->canMove(a)) return b;
    if (!robot->canMove(b)) return a;
    return rng()%2==0 ? a : b;
}

int DirectionTo(int x1,int y1,int x2,int y2)
{ if (y2>y1)      // going down
  { if (y1%2==0) if (x2>x1) return 2;
                 else       return 3;
    else         if (x2<x1) return 3;
                 else       return 2;
  }
  else if (y2<y1) // going up
  { if (y1%2==0) if (x2>x1) return 6;
                 else       return 5;
    else         if (x2<x1) return 5;
                 else       return 6;
  }
  else            // same y
  { if (x2>x1)      return 1;
    else if (x2<x1) return 4;
  }
  return 0;
}

int DirectionRandomTo(IRobotController *robot,int x1,int y1,int x2,int y2)
{
  int dx=abs(x1-x2);
  int dy=abs(y1-y2);
  if (y2>y1)      // going down
  { if (y1%2==0) if (x2>x1)  // dir = 2;
                 { if (dy>=3*dx) return choose_random_direction(robot,2,3);
                   if (dy<2*dx-1) return choose_random_direction(robot,2,1);
                   return 2;
                 }
                 else       // dir = 3;
                 { if (dy>=4*dx) return choose_random_direction(robot,3,2);
                   if (dy<2*dx) return choose_random_direction(robot,3,4);
                   return 3;
                 }
    else         if (x2<x1) // dir = 3;
                 { if (dy>=3*dx) return choose_random_direction(robot,3,2);
                   if (dy<2*dx-1) return choose_random_direction(robot,3,4);
                   return 3;
                 }
                 else       // dir = 2;
                 { if (dy>=4*dx) return choose_random_direction(robot,2,3);
                   if (dy<2*dx) return choose_random_direction(robot,2,1);
                   return 2;
                 }
  }
  else if (y2<y1) // going up
  { if (y1%2==0) if (x2>x1) // dir = 6;
                 { if (dy>=3*dx) return choose_random_direction(robot,6,5);
                   if (dy<2*dx-1) return choose_random_direction(robot,6,1);
                   return 6;
                 }
                 else       // dir = 5;
                 { if (dy>=4*dx) return choose_random_direction(robot,5,6);
                   if (dy<2*dx) return choose_random_direction(robot,5,4);
                   return 5;
                 }
    else         if (x2<x1) // dir = 5;
                 { if (dy>=3*dx) return choose_random_direction(robot,5,6);
                   if (dy<2*dx-1) return choose_random_direction(robot,5,4);
                   return 5;
                 }
                 else       // dir = 6;
                 { if (dy>=4*dx) return choose_random_direction(robot,6,5);
                   if (dy<2*dx) return choose_random_direction(robot,6,1);
                   return 6;
                 }
  }
  else            // same y
  { if (x2>x1)      return 1;
    else if (x2<x1) return 4;
  }
  return 0;
}

int DirectionRandomFrom(IRobotController *robot,int x1,int y1,int x2,int y2)
{
  int dx=abs(x1-x2);
  int dy=abs(y1-y2);
  if (y2>y1)      // going down
  { if (y1%2==0) if (x2>x1)  // dir = 5;
                 { if (dy>=3*dx) return choose_random_direction(robot,5,6);
                   if (dy<2*dx-1) return choose_random_direction(robot,5,4);
                   return 5;
                 }
                 else       // dir = 6;
                 { if (dy>=4*dx) return choose_random_direction(robot,6,5);
                   if (dy<2*dx) return choose_random_direction(robot,6,1);
                   return 6;
                 }
    else         if (x2<x1) // dir = 6;
                 { if (dy>=3*dx) return choose_random_direction(robot,6,5);
                   if (dy<2*dx-1) return choose_random_direction(robot,6,1);
                   return 6;
                 }
                 else       // dir = 5;
                 { if (dy>=4*dx) return choose_random_direction(robot,5,6);
                   if (dy<2*dx) return choose_random_direction(robot,5,4);
                   return 5;
                 }
  }
  else if (y2<y1) // going up
  { if (y1%2==0) if (x2>x1) // dir = 3;
                 { if (dy>=3*dx) return choose_random_direction(robot,3,2);
                   if (dy<2*dx-1) return choose_random_direction(robot,3,4);
                   return 3;
                 }
                 else       // dir = 2;
                 { if (dy>=4*dx) return choose_random_direction(robot,2,3);
                   if (dy<2*dx) return choose_random_direction(robot,2,1);
                   return 2;
                 }
    else         if (x2<x1) // dir = 2;
                 { if (dy>=3*dx) return choose_random_direction(robot,2,3);
                   if (dy<2*dx-1) return choose_random_direction(robot,2,1);
                   return 2;
                 }
                 else       // dir = 3;
                 { if (dy>=4*dx) return choose_random_direction(robot,3,2);
                   if (dy<2*dx) return choose_random_direction(robot,3,4);
                   return 3;
                 }
  }
  else            // same y
  { if (x2>x1)      return 4;
    else if (x2<x1) return 1;
  }
  return 0;
}

int DirectionFrom(int x1,int y1,int x2,int y2)
{ if (y2>y1)      // going up
  { if (y1%2==0) if (x2>x1) return 5;
                 else       return 6;
    else         if (x2<x1) return 6;
                 else       return 5;
  }
  else if (y2<y1) // going down
  { if (y1%2==0) if (x2>x1) return 3;
                 else       return 2;
    else         if (x2<x1) return 2;
                 else       return 3;
  }
  else            // same y
  { if (x2>x1)      return 4;
    else if (x2<x1) return 1;
  }
  return 0;
}

int DirectionRandom(IRobotController *robot)
{   for (int i=0;i<6;i++)
    {   int dir = 1+rng()%6;
        if (robot->canMove(dir)) return dir;
    }
    return 0;
}

int DirectionRandomRadius(IRobotController *robot,int x,int y,int cx,int cy,int dist)
{ if (Distance(x,y,cx,cy)>dist) return DirectionRandomTo(robot,x,y,cx,cy);
  for (int i=0;i<10;i++)
  { int dir = 1+rng()%6;
    int tx = x;
    int ty = y;
    PositionMove(tx,ty,dir);
    if (Distance(tx,ty,cx,cy)<=dist) return dir;
  }
  return 0;
}

int ShootDecision(EnemyInfo *eiarr,list<int> &enea)
{   if (enea.empty()) return -1;
    list<int>::iterator eit = enea.begin();
    int emin = *eit;
    int emin_distance = eiarr[emin].distance;
    for (eit++; eit != enea.end(); eit++)
    {   int en = *eit;
        if (eiarr[en].isbase && eiarr[en].obj!=NULL) { emin = en; break; }       // bases in priority
        int dis = eiarr[en].distance;
        if (dis < emin_distance && eiarr[en].obj!=NULL) { emin = en; emin_distance = dis; }
    }
    if (eiarr[emin].obj!=NULL) return emin;
    return -1;
}

int MoveDecision(IRobotController *robot,int x,int y,EnemyCoords *ecarr,EnemyInfo *eiarr,list<int> &enea,list<int> &efar,int good_range)
{   if (enea.empty())
    {   if (efar.empty()) return DirectionRandom(robot); // General case of movement ------------------------------------------ // MAYBE TODO: not to go near friends...
        list<int>::iterator eit = efar.begin();
        int emin = *eit;
        int emin_distance = eiarr[emin].distance;
        for (eit++; eit != efar.end(); eit++)
        {   int en = *eit;
            int dis = eiarr[en].distance;
            if (dis < emin_distance) { emin = en; emin_distance = dis; } // get the nearest enemy
        }
        return DirectionRandomTo(robot,x,y,ecarr[emin].x,ecarr[emin].y);                                             // go to it!
    }
    else
    {   list<int>::iterator eit = enea.begin();
        int emin = *eit;
        int emin_distance = eiarr[emin].distance;
        for (eit++; eit != enea.end(); eit++)
        {   int en = *eit;
            int dis = eiarr[en].distance;
            if (dis < emin_distance) { emin = en; emin_distance = dis; } // get the nearest enemy
        }
        if (emin_distance > good_range) return DirectionRandomTo(robot,x,y,ecarr[emin].x,ecarr[emin].y);             // reach the good_range
        if (emin_distance < good_range) return DirectionRandomFrom(robot,x,y,ecarr[emin].x,ecarr[emin].y);
        return 0;
    }
}

int MoveDecisionRadius(IRobotController *robot,int x,int y,EnemyCoords *ecarr,EnemyInfo *eiarr,list<int> &enea,list<int> &efar,int good_range,int cx,int cy,int dist)
{   if (enea.empty())
    {   if (efar.empty()) return DirectionRandomRadius(robot,x,y,cx,cy,dist); // General case of movement ------------------------------------------ // MAYBE TODO: not to go near friends...
        list<int>::iterator eit = efar.begin();
        int emin = *eit;
        int emin_distance = eiarr[emin].distance;
        for (eit++; eit != efar.end(); eit++)
        {   int en = *eit;
            int dis = eiarr[en].distance;
            if (dis < emin_distance) { emin = en; emin_distance = dis; } // get the nearest enemy
        }
        int dir = DirectionRandomTo(robot,x,y,ecarr[emin].x,ecarr[emin].y);                                             // try to go to it!
        int x1 = x, y1 = y;
        PositionMove(x1,y1,dir);
        if (Distance(cx,cy,x1,y1) <= dist) return dir;
        return DirectionRandomRadius(robot,x,y,cx,cy,dist);
    }
    else
    {   list<int>::iterator eit = enea.begin();
        int emin = *eit;
        int emin_distance = eiarr[emin].distance;
        for (eit++; eit != enea.end(); eit++)
        {   int en = *eit;
            int dis = eiarr[en].distance;
            if (dis < emin_distance) { emin = en; emin_distance = dis; } // get the nearest enemy
        }
        int dir = 0;
        if (emin_distance > good_range) dir = DirectionRandomTo(robot,x,y,ecarr[emin].x,ecarr[emin].y);             // reach the good_range
        else if (emin_distance < good_range) dir = DirectionRandomFrom(robot,x,y,ecarr[emin].x,ecarr[emin].y);
        int x1 = x, y1 = y;
        PositionMove(x1,y1,dir);
        if (Distance(cx,cy,x1,y1) <= dist) return dir;
        return 0;
    }
}

/*
bool check_memory(IMessage *m,int offset,int value)
{   int old_value = m->readValue(offset);
    if (old_value!=value)
    {   cout << "Changed ["<<offset<<"]: "<<old_value<<" --> "<<value<<endl<<flush;
        return false;
    }
    return true;
}
*/

extern "C"
ROBO_FUNC void execute(IRobotController *robot) {

    const IEnvironment *env = robot->getEnvironment();
    const IGameFieldView *field = robot->getFieldView();
    IMessage *memory = robot->getOwnMemory();
    int id = memory->readValue(MEM_ID); while (id==0) id = rng();
    int status = memory->readValue(MEM_STATUS); // initially: st_none
    int bx = memory->readValue(MEM_BASE_X);
    int by = memory->readValue(MEM_BASE_Y);
    int child_number = memory->readValue(MEM_CHILD_NUMBER);
    int search_location = memory->readValue(MEM_SEARCH_LOCATION);
    int search_previous_location = memory->readValue(MEM_PREVIOUS_SEARCH_LOCATION);
    int previous_x = memory->readValue(MEM_PREVIOUS_X);
    int previous_y = memory->readValue(MEM_PREVIOUS_Y);
    int action = mt_none;

    const Coord2D &here = robot->getCell()->getCoord();

    if (DEBUG)
    {   if (status==st_none) printf(" New! Robot[%d,%d] id:%d st:%d bx:%d by:%d ch:%d\n",here.getX(),here.getY(),id,status,bx,by,child_number);
        else printf("  Robot[%d,%d] id:%d st:%d bx:%d by:%d ch:%d\n",here.getX(),here.getY(),id,status,bx,by,child_number);
        fflush(stdout);
    }

    int width = env->getFieldWidth();
    int height = env->getFieldHeight();
    int enemies_array_size = height*width/4;
    int number_of_first_searchers = (width+height)/40;

    int good_range_to_enemy;
    if (robot->getHitpoints() > 2) good_range_to_enemy = max(1,env->getShootRange()-2); // preferred distance to enemy
                              else good_range_to_enemy = 1;
    int shoot_range = env->getShootRange();
    int view_range = env->getViewRange();

    EnemyCoords *ecarr = new EnemyCoords [enemies_array_size];
    EnemyInfo * eiarr = new EnemyInfo [enemies_array_size];
    int number_of_enemies = 0;
    list<int> enea, efar;

    int is_born_message = 0;
    int born_message_count = 0;
    int is_born_answer_message = 0;
    int number_of_defenders = 0;
    int number_of_visible_friends = 0;
    int born_answer_number_of_defenders = 0;
    int my_base_visible = 0;

    // watch visible bases
    for (size_t i=0;i<robot->getNumOfBasesInView();i++)
    {
        IBaseView *b = robot->getBaseInView(i);
        const Coord2D &bcoord = b->getCell()->getCoord();
        int x  = bcoord.getX();
        int y  = bcoord.getY();
        int d = b->getDistance();
        if (b->isEnemy() || !b->isActive())
        {
            if (number_of_enemies<enemies_array_size)
            {
                ecarr[number_of_enemies].x = x;
                ecarr[number_of_enemies].y = y;
                eiarr[number_of_enemies].obj = b;
                eiarr[number_of_enemies].distance = d;
                eiarr[number_of_enemies].isbase = 1;
                eiarr[number_of_enemies].re = 0;
                number_of_enemies++;
            }
        }
        else
        {
            if (status==st_none && d==1) { bx = x; by = y; } // mother base
            my_base_visible = 1;
        }
    }

    // watch visible robots
    for (size_t i=0;i<robot->getNumOfRobotsInView();i++)
    {
        IRobotView *r = robot->getRobotInView(i);
        const Coord2D &rcoord = r->getCell()->getCoord();
        int x  = rcoord.getX();
        int y  = rcoord.getY();
        int d = r->getDistance();
        if (r->isEnemy())
        {
            if (number_of_enemies<enemies_array_size)
            {   ecarr[number_of_enemies].x = x;
                ecarr[number_of_enemies].y = y;
                eiarr[number_of_enemies].obj = r;
                eiarr[number_of_enemies].distance = d;
                eiarr[number_of_enemies].isbase = 0;
                eiarr[number_of_enemies].re = 0;
                number_of_enemies++;
            }
        }
        else
            number_of_visible_friends++;
    }
    if (my_base_visible) good_range_to_enemy = 1;

    // watch messages
    for (size_t i=0;i<robot->getNumOfMessages();i++)
    {
        IMessage *msg = robot->getMessage(i);
        int r_id;
        if (DEBUG) r_id = msg->readValue(MSG_ID);
        int r_status = msg->readValue(MSG_STATUS);
        int r_bx = msg->readValue(MSG_BASE_X);
        int r_by = msg->readValue(MSG_BASE_Y);
        int r_action = msg->readValue(MSG_ACTION);
        int r_child_number;
        if (DEBUG) r_child_number = msg->readValue(MSG_CHILD_NUMBER);
        if (DEBUG) born_answer_number_of_defenders = msg->readValue(MSG_NUMBER_OF_DEFENDERS);
        if (r_status==st_defender && r_bx==bx && r_by==by) number_of_defenders++;
        switch(r_action)
        {
            case mt_born:
                if (r_bx==bx && r_by==by)
                {   is_born_message = 1;
                    born_message_count = msg->readValue(MSG_CHILD_NUMBER);
                }
                break;
            case mt_born_answer:
                if (r_bx==bx && r_by==by)
                {   is_born_answer_message = 1;
                    born_answer_number_of_defenders = msg->readValue(MSG_NUMBER_OF_DEFENDERS);
                }
                break;
        }
        int r_ne = msg->readValue(MSG_NUMBER_OF_ENEMIES);
        if (DEBUG) { printf("    Recv id:%d st:%d bx:%d by:%d act:%d ch:%d def:%d en:%d\n",r_id,r_status,r_bx,r_by,r_action,r_child_number,born_answer_number_of_defenders,r_ne); fflush(stdout); }
        int offset = MSG_OFFSET;
        // get enemies from messages
        for (int ie=0; ie<r_ne && number_of_enemies<enemies_array_size; ie++, offset+=4)
        {
            int j;
            int x  = msg->readValue(offset+0);
            int y  = msg->readValue(offset+1);
            for (j=0; j<number_of_enemies; j++)
                if (ecarr[j].x == x && ecarr[j].y == y) break;
            if (j==number_of_enemies && number_of_enemies<enemies_array_size)
            { int b  = msg->readValue(offset+2);
              int re = msg->readValue(offset+3);
              ecarr[number_of_enemies].x = x;
              ecarr[number_of_enemies].y = y;
              eiarr[number_of_enemies].obj = NULL;
              int d = field->getDistance(here,Coord2D(x,y));
              eiarr[number_of_enemies].distance = d;
              eiarr[number_of_enemies].isbase = b;
              eiarr[number_of_enemies].re = re;
              number_of_enemies++;
            }
        }
    }

    if (DEBUG) { cout<<"    mt_born:"<<is_born_message<<" mt_born_answer:"<<is_born_answer_message<<" defenders:"<<number_of_defenders<<" friends:"<<number_of_visible_friends<<endl<<flush; }

    for (int ie=0; ie<number_of_enemies; ie++)
        if (eiarr[ie].distance <= shoot_range)
        {
            if (eiarr[ie].obj!=NULL) enea.push_back(ie);
        }
        else efar.push_back(ie);

    //--------------------------------------------
    int target = -1;
    int direction = 0;
    switch(status)
    {
        case st_none:
        {   if (is_born_message)
            {   child_number = born_message_count+1;
                action = mt_born_answer;
            }
            else
                child_number = 1;
            status = st_born;
            if (DEBUG) { printf("    st_none -> st_born\n"); fflush(stdout); }
            if (robot->canShoot()) target = ShootDecision(eiarr,enea);
            direction = MoveDecisionRadius(robot,here.getX(),here.getY(),ecarr,eiarr,enea,efar,good_range_to_enemy,bx,by,env->getTransmitRange()-1);
            break;
        }
        case st_born:
        {   if (is_born_answer_message) // I'm ready to become someone
            {   if (enea.empty() && child_number<=number_of_first_searchers)
                {   status = st_searcher;
                    if (DEBUG) { printf("    st_born -> st_searcher\n"); fflush(stdout); }
                    fill_locations(width,height,view_range);
                    search_location = rng()%NUMBER_OF_SEARCH_LOCATIONS;
                    search_previous_location = -1;
                    direction = DirectionRandomTo(robot,here.getX(),here.getY(),location_x[search_location],location_y[search_location]);
                }
                else if (MAX_NUMBER_OF_DEFENDERS>0 && born_answer_number_of_defenders<MAX_NUMBER_OF_DEFENDERS)
                {   status = st_defender;
                    if (DEBUG) { printf("    st_born -> st_defender\n"); fflush(stdout); }
                    if (robot->canShoot()) target = ShootDecision(eiarr,enea);
                    direction = MoveDecisionRadius(robot,here.getX(),here.getY(),ecarr,eiarr,enea,efar,good_range_to_enemy,bx,by,env->getTransmitRange()-1);
                }
                else
                {   status = st_trooper;
                    if (DEBUG) { printf("    st_born -> st_trooper\n"); fflush(stdout); }
                    if (robot->canShoot()) target = ShootDecision(eiarr,enea);
                    direction = MoveDecision(robot,here.getX(),here.getY(),ecarr,eiarr,enea,efar,good_range_to_enemy);
                }
            }
            else
            {   action = mt_born;
                if (robot->canShoot()) target = ShootDecision(eiarr,enea);
                direction = MoveDecisionRadius(robot,here.getX(),here.getY(),ecarr,eiarr,enea,efar,good_range_to_enemy,bx,by,env->getTransmitRange()-1);
            }
            break;
        }
        case st_defender:
        {   if (robot->canShoot()) target = ShootDecision(eiarr,enea);
            direction = MoveDecisionRadius(robot,here.getX(),here.getY(),ecarr,eiarr,enea,efar,good_range_to_enemy,bx,by,env->getTransmitRange()-1);
            break;
        }
        case st_searcher:
        {   if (!enea.empty())
            {   status = st_trooper;
                if (DEBUG) { printf("    st_searcher -> st_trooper\n"); fflush(stdout); }
                if (robot->canShoot()) target = ShootDecision(eiarr,enea);
                direction = MoveDecision(robot,here.getX(),here.getY(),ecarr,eiarr,enea,efar,good_range_to_enemy);
            }
            else
            {   fill_locations(width,height,view_range);
                int tx = location_x[search_location];
                int ty = location_y[search_location];
                if (field->getDistance(here,Coord2D(tx,ty))<=1)
                {   int t;
                    for (int i=0;i<10;i++)
                    {   t = rng()%NUMBER_OF_SEARCH_LOCATIONS;
                        if (t!=search_location && t!=search_previous_location) break;
                    }
                    search_previous_location = search_location;
                    search_location = t;
                    direction = DirectionRandomTo(robot,here.getX(),here.getY(),location_x[search_location],location_y[search_location]);
                }
                else if (here.getX()==previous_x && here.getY()==previous_y)
                        direction = DirectionRandom(robot);
                else
                    direction = DirectionRandomTo(robot,here.getX(),here.getY(),tx,ty);
            }
            break;
        }
        case st_trooper:
        default:
        {   if (robot->canShoot()) target = ShootDecision(eiarr,enea);
            if (enea.empty() && efar.empty() && number_of_visible_friends>=NUMBER_OF_VISIBLE_FRIENDS_TO_BECOME_SEARCHER && rng()%BECOME_SEARCHER_PROBABILITY==0)
            {   status = st_searcher;
                if (DEBUG) { printf("    st_trooper -> st_\n"); fflush(stdout); }
                fill_locations(width,height,view_range);
                search_location = rng()%NUMBER_OF_SEARCH_LOCATIONS;
                search_previous_location = -1;
                direction = DirectionRandomTo(robot,here.getX(),here.getY(),location_x[search_location],location_y[search_location]);
            }
            else
                direction = MoveDecision(robot,here.getX(),here.getY(),ecarr,eiarr,enea,efar,good_range_to_enemy);
        }
    }

    //--------------------------------------------
    // do action
    if (target >= 0) robot->shoot(eiarr[target].obj);
    robot->move(direction);
    //--------------------------------------------
    // send message

    if (status!=st_trooper || number_of_visible_friends<NUMBER_OF_VISIBLE_FRIENDS_TO_SKIP_SENDING_MESSAGE || rng()%(number_of_visible_friends/2)!=0)
    {
        int diameter = field->getDistance(Coord2D(0,0),Coord2D(width-1,height-1));
        int max_retransmit = diameter/env->getTransmitRange();     // maximum message retransmit
        int max_message_size = env->getMessageSize();
        IMessage *message = robot->getOwnMessage();
        message->writeValue(MSG_ID,id);
        message->writeValue(MSG_STATUS,status);
        message->writeValue(MSG_BASE_X,bx);
        message->writeValue(MSG_BASE_Y,by);
        message->writeValue(MSG_ACTION,action);
        message->writeValue(MSG_CHILD_NUMBER,child_number);
        message->writeValue(MSG_NUMBER_OF_DEFENDERS,number_of_defenders);
        int actual_number_of_enemies = 0;
        int offset = MSG_OFFSET;
        for (int ie=0; ie<number_of_enemies && offset+4 < max_message_size; ie++)
        {   int re = eiarr[ie].re;
            if (re < max_retransmit)
            {   message->writeValue(offset+0,ecarr[ie].x);
                message->writeValue(offset+1,ecarr[ie].y);
                message->writeValue(offset+2,eiarr[ie].isbase);
                message->writeValue(offset+3,re+1);
                actual_number_of_enemies++;
                offset += 4;
            }
        }
        message->writeValue(MSG_NUMBER_OF_ENEMIES,actual_number_of_enemies);
        robot->transmit(message);
        if (DEBUG) { printf("    Send id:%d st:%d bx:%d by:%d act:%d ch:%d def:%d en:%d\n",id,status,bx,by,action,child_number,number_of_defenders,actual_number_of_enemies); fflush(stdout); }
    }

    delete[] ecarr;
    delete[] eiarr;

    // update memory
    memory->writeValue(MEM_ID,id);
    memory->writeValue(MEM_STATUS,status);
    memory->writeValue(MEM_BASE_X,bx);
    memory->writeValue(MEM_BASE_Y,by);
    memory->writeValue(MEM_CHILD_NUMBER,child_number);
    memory->writeValue(MEM_SEARCH_LOCATION,search_location);
    memory->writeValue(MEM_PREVIOUS_SEARCH_LOCATION,search_previous_location);
    memory->writeValue(MEM_PREVIOUS_X,here.getX());
    memory->writeValue(MEM_PREVIOUS_Y,here.getY());
}
