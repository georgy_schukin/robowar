#-------------------------------------------------
#
# Project created by QtCreator 2014-06-27T11:55:26
#
#-------------------------------------------------

QT       -= core gui

TARGET = bot3
TEMPLATE = lib
CONFIG = c++0x
INCLUDEPATH += "../../include"

QMAKE_CXXFLAGS += -DROBO_EXPORT

DESTDIR = "../../game/Bots"

SOURCES += \
    bot3.cpp

