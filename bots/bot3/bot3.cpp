#include "bots/bot.h"
#include <cstdlib>
#include <vector>
#include <iostream>
#include <algorithm>

int cw(int i)  { return i==6 ? 1 : i+1; }
int ccw(int i) { return i==1 ? 6 : i-1; }

struct C2d {
	int x, y;

	C2d() : x(0), y(0) {}

	C2d(int i) : x(i % 0x10000), y(i / 0x10000) {}

	C2d(int x, int y) : x(x), y(y) {}

	C2d(robowar::Coord2D &c) : x(c.getX()), y(c.getY()) {}

	C2d(robowar::IGameCell *c)
	: x(c->getCoord().getX()), y(c->getCoord().getY())
	{}

	C2d(robowar::IGameObject *c)
	: x(c->getCell()->getCoord().getX()), y(c->getCell()->getCoord().getY())
	{}

	int to_i() { return x + y * 0x10000; };
};

bool operator ==(const C2d &a, const C2d &b) {
	return a.x == b.x && a.y == b.y;
}

bool operator <(const C2d &a, const C2d &b) {
	if(a.x < b.x) return true;
	if(a.x > b.x) return false;
	return a.y < b.y;
}

C2d my;
C2d map_size;
struct Msg;
bool operator<(const Msg &a, const Msg &b);

struct Msg {
    C2d c;
    int t;
    int p;
    Msg() {c=0;t=0;p=0;}
	Msg(C2d c, int t, int p)
	: c(c), t(t), p(p) {}

	bool ok() { return t > 0 && p > 0; }

	void load(robowar::IMessage *msg, size_t offset) {
		Msg m;
		m.c = msg->readValue(offset);
		m.t = msg->readValue(offset+1)-1;
		m.p = msg->readValue(offset+2);
		if(m.ok() && (!ok() || m < *this)) {
			c = m.c;
			t = m.t;
			p = m.p;
		}
	}
	void save(robowar::IMessage *msg, size_t offset) {
		msg->writeValue(offset, c.to_i());
		msg->writeValue(offset+1, t);
		msg->writeValue(offset+2, p);
	}
};

int getDistance(const C2d &cr1, const C2d &cr2);

bool operator<(const Msg &a, const Msg &b)
{
	int dist1 = getDistance(a.c, my) - a.p*3;
	int dist2 = getDistance(b.c, my) - b.p*3;

	#define CMP(A, B) if(A<B) return true; if(B<A) return false;
	CMP(dist1, dist2)
	CMP(a.c, b.c)
	CMP(a.p, b.p)
	CMP(a.t, b.t)
	#undef CMP
	return false;
}

bool operator==(const Msg &a, const Msg &b)
{
	return a.p == b.p && a.t == b.t && a.c == b.c;
}


int rand2(int a, int b) {
	switch(rand()%2) {
		case 0: return a; break;
		case 1: return b; break;
	}
	return 0;
}

int rand3(int a, int b, int c) {
	switch(rand()%3) {
		case 0: return a; break;
		case 1: return b; break;
		case 2: return c; break;
	}
	return 0;
}

int rand4(int a, int b, int c, int d) {
	switch(rand()%4) {
		case 0: return a; break;
		case 1: return b; break;
		case 2: return c; break;
		case 3: return d; break;
	}
	return 0;
}

int getDirection(const C2d a, const C2d b, bool wide = false) {
	if(rand()%2) wide = false;
	if(b.x == a.x && b.y == a.y) return 0;
	
	int d0 = b.y - a.y;
	int d1 = (b.x+(2*b.y%2)+b.y/2) - (a.x+(2*a.y%2)+a.y/2);
	int d2 = (b.x-b.y%2-b.y/2) - (a.x-a.y%2-a.y/2);

	#define R(A) return (wide? rand3(ccw(A),A,cw(A)) : A);
	#define OR(A,B) return (wide? rand4(ccw(A),A,B,cw(B)) : rand2(A,B));

	if(d0 == 0 && d2 > 0) R(1);
	if(d2 == 0 && d0 > 0) R(2);
	if(d1 == 0 && d0 < 0) R(6);
	if(d0 == 0 && d2 < 0) R(4);
	if(d2 == 0 && d0 < 0) R(5);
	if(d1 == 0 && d0 > 0) R(3);

	if(d0 > 0 && d2 > 0) OR(1,2)
	if(d2 < 0 && d1 > 0) OR(2,3)
	if(d1 < 0 && d0 > 0) OR(3,4)
	if(d0 < 0 && d2 < 0) OR(4,5)
	if(d2 > 0 && d1 < 0) OR(5,6)
	if(d1 > 0 && d0 < 0) OR(6,1)

	#undef OR
	return 0;
}

int getDistance(const C2d &cr1, const C2d &cr2) {
	const int ax = cr1.y;
	const int ay = cr1.x - (cr1.y/2 + cr1.y % 2);
	const int az = cr1.x + cr1.y/2;
	const int bx = cr2.y;
	const int by = cr2.x - (cr2.y/2 + cr2.y % 2);
	const int bz = cr2.x + cr2.y/2;

	const int v1 = abs(ax - bx) + abs(ay - by);
	const int v2 = abs(ay - by) + abs(az - bz);
	const int v3 = abs(az - bz) + abs(ax - bx);

	return v2 < v3 ? (v1 < v2 ? v1 : v2) : (v1 < v3 ? v1 : v3);
}

int get_friend_bases(robowar::IRobotController *c) // get num of friends in view range
{
	int res = 0;
	for(unsigned i = 0; i < c->getNumOfBasesInView();i++)
		if(c->getBaseInView(i)->getTeam() == c->getTeam()) res++;
	return res;
}

struct Map {
	int map[8];
	Map() {
		for(int i = 0; i < 8; i++)
			map[i] = 0;
	}
	void load(robowar::IMessage *msg, size_t offset) {
		for(int i = 0; i < 8; i++)
			map[i] |= msg->readValue(offset+i);
	}
	void save(robowar::IMessage *msg, size_t offset) {
		for(int i = 0; i < 8; i++)
			msg->writeValue(offset+i, map[i]);
	}
	bool get(const C2d c) {
		int i = (c.x*16 / map_size.x) + (c.y*16 / map_size.y)*16;
		return map[i/32] & (1 << (i%32));
	}
	void set(const C2d c) {
		int i = (c.x*16 / map_size.x) + (c.y*16 / map_size.y)*16;
		map[i/32] |= 1 << (i%32);
	}
	int size() {
		int res = 0;
		for(int i = 0; i < 8; i++)
			if(map[i] & (1<<i))
				res++;
		return res;
	}
	bool full() {
		return size() == 8*8;
	}
};


extern "C" ROBO_FUNC void execute(robowar::IRobotController *me)
{
	me->cancelTransmit();
	me->move(0);

	my = C2d(me);
	map_size.x = me->getEnvironment()->getFieldWidth();
	map_size.y = me->getEnvironment()->getFieldHeight();

	Msg msg_enemy, msg_base, msg_explore;
	Map map;
	bool idle = true;

	map.load(me->getOwnMemory(), 4);
	msg_enemy.load(me->getOwnMemory(), 100);
	msg_base.load(me->getOwnMemory(), 103);
	msg_explore.load(me->getOwnMemory(), 106);

	int friend_count = 0;
	int enemy_count = 0;
	int enemy_bases = 0;
	int empty_bases = 0;
	std::vector<C2d> friends;

	int fr_b_num = get_friend_bases(me);
	
	for(unsigned i = 0; i < me->getNumOfRobotsInView(); i++) {
		if(me->getRobotInView(i)->isEnemy())
			enemy_count++;
		else
			friend_count++;
	}

	for(unsigned i = 0;i < me->getNumOfBasesInView();i++) {
		if(!me->getBaseInView(i)->isActive())
			empty_bases++;
		else if(me->getBaseInView(i)->isEnemy())
			enemy_bases++;
	}

	if(enemy_count == 0 && enemy_bases == 0)
		map.set(me);

	{
		int mode = me->getOwnMemory()->readValue(0);
		C2d target(me->getOwnMemory()->readValue(1));
		C2d from(me->getOwnMemory()->readValue(2));
		int timer = me->getOwnMemory()->readValue(3);
		switch(mode) {
		case 0:
			if(rand()%15 == 0) {
				mode = 1;
				do{
					target.x = C2d(me).x + rand() % 31 - 15;
					target.y = C2d(me).y + rand() % 31 - 15;
				} while ( target.x < 0 || target.y < 0
				       || target.x >= map_size.x
				       || target.y >= map_size.y
				       || getDistance(target.x, me) < 10);
				from = me;
				timer = 15;
				me->move(getDirection(me, target, true));
				idle = false;
			} else
				me->move(rand()%6 + 1);
			break;
		case  1:
			if(timer <= 0 || target == me
			 || enemy_bases || empty_bases || enemy_count) {
				mode = 2;
				me->move(getDirection(me, from, false));
				timer = 15;
			} else {
				me->move(getDirection(me, target, true));
				timer--;
			}
			break;
		case 2:
			if(timer <= 0 || from == me || friend_count >= 3) {
				mode = 0;
				me->move(rand()%6 + 1);
			} else {
				idle = true;
				me->move(getDirection(me, from, false));
				timer--;
			}
		}

		me->getOwnMemory()->writeValue(0, mode);
		me->getOwnMemory()->writeValue(1, target.to_i());
		me->getOwnMemory()->writeValue(2, from.to_i());
		me->getOwnMemory()->writeValue(3, timer);
	}
	
	int mindist = -1;
	for(size_t i = 0;i < me->getNumOfRobotsInView(); i++) {
		robowar::IRobotView *robo = me->getRobotInView(i);
		int dist = getDistance(me, robo);
		if(robo->isEnemy()) {
			if(mindist != -1 && dist >= mindist)
				continue;
			me->shoot(robo);
			mindist = dist;

			if(idle) {
				idle = false;
				if(friend_count > enemy_count + 1 && dist > 3)
					me->move(getDirection(me, robo, friend_count > 10));
				if(enemy_count > friend_count)
					me->move(getDirection(robo, me));
			}

			msg_enemy = Msg(robo, 30, 100 + enemy_count-friend_count+fr_b_num*2);
			if(enemy_count > friend_count && fr_b_num)
				msg_enemy.p += 5;

		}
	}

	mindist = -1;
	for(unsigned i = 0;i < me->getNumOfBasesInView();i++) {
		robowar::IBaseView *base = me->getBaseInView(i);
		int dist = getDistance(me, base);
		if(base->getTeam() != me->getTeam()) {
			if(mindist != -1 && dist >= mindist)
				continue;
				me->shoot(base);
				mindist = dist;

			if(idle) {
				idle = false;
				if(friend_count > 1)
					me->move(getDirection(me, base));

			}

			msg_base = Msg(base, 10, 100 + 3+enemy_count-friend_count);
		} else if(friend_count >= 3 && dist <= 2)
				me->move(getDirection(base, me, true));
	}

	if(!map.full() && (!msg_explore.ok() || map.get(msg_explore.c))) {
		msg_explore.p = 100 + rand()%3000;
		msg_explore.t = 50;
		do{
			msg_explore.c.x = rand()%map_size.x;
			msg_explore.c.y = rand()%map_size.y;
		}while(map.get(msg_explore.c));
	}

	for(unsigned i = 0; i < me->getNumOfMessages(); i++) {
		msg_enemy.load(me->getMessage(i), 0);
		msg_base.load(me->getMessage(i), 3);
		msg_explore.load(me->getMessage(i), 6);

		map.load(me->getMessage(i),31);
	}

	msg_enemy.save(me->getOwnMessage(), 0);
	msg_enemy.save(me->getOwnMemory(), 100);
	msg_base.save(me->getOwnMessage(), 3);
	msg_base.save(me->getOwnMemory(), 103);
	msg_explore.save(me->getOwnMessage(), 6);
	msg_explore.save(me->getOwnMemory(), 106);

	map.save(me->getOwnMessage(),31);
	map.save(me->getOwnMemory(),4);
	me->transmit();

	if(idle) {
		Msg msg;
		if(msg_base.ok())
			msg = msg_base;
		else if(msg_enemy.ok())
			msg = msg_enemy;
		else
			msg = msg_explore;

		if(msg.ok()) {
			me->move(getDirection(me, msg.c, friend_count >= 4));
			me->getOwnMemory()->writeValue(253, msg.c.x);
			me->getOwnMemory()->writeValue(254, msg.c.y);
			me->getOwnMemory()->writeValue(255, 359);
		} else
			me->getOwnMemory()->writeValue(255, 0);
	}
}
