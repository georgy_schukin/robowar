#pragma once

#include "robowar.h"

#define HELP    1
#define BASE    2
#define COUNTER 7
#define SCOUT   1
#define SOLDIER 2

extern "C" {
    ROBO_FUNC void execute(robowar::IRobotController *robot);
}
