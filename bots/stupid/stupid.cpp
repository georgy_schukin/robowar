#include "stupid.h"
#include <cstdlib>
#include <iostream>
#include <stdio.h>

using std::cout;
using std::endl;
using namespace robowar;

size_t getNumOfFriends(IRobotController *robot) {
    size_t num = 0;
    for (size_t i = 0; i < robot->getNumOfRobotsInView(); i++)
        if (!robot->getRobotInView(i)->isEnemy())
            num++;
    return num;
}

size_t getNumOfEnemies(IRobotController *robot) {
    size_t num = 0;
    for (size_t i = 0; i < robot->getNumOfRobotsInView(); i++)
        if (robot->getRobotInView(i)->isEnemy())
            num++;
    return num;
}


ROBO_FUNC void execute(IRobotController *robot) {
    int role = robot->getOwnMemory()->readValue(0);
    int direction = robot->getOwnMemory()->readValue(1);


    const size_t r_num = robot->getNumOfRobotsInView(); // num of robots in view range
    const size_t b_num = robot->getNumOfBasesInView(); // num of bases in view range

    Coord2D myCoord = robot->getCell()->getCoord();

    size_t i = 0;

    if (!role){
      int number = rand() % 2;
      robot->getOwnMemory()->writeValue(0, (number > 0)? SOLDIER: SCOUT);//(rand() % 2) + 1);//
      direction = (rand() % 6) + 1;
      //куда идти по умолчанию
      robot->getOwnMemory()->writeValue(1, direction);
      //запоминать базы?
    }

    bool exit = false;
    switch (role){
    case (SCOUT):{
        //cout << "SCOUT" << endl;
        IBaseView *base;
        for (i = 0; i < b_num; ++i){
            base = robot->getBaseInView(i);
              if (!base->isActive() || base->isEnemy()) {
                  robot->shoot(base);

                  //послать сообщение о базе
                  IMessage *msg = robot->getOwnMessage();
                  msg->writeValue(0, BASE);
                  msg->writeValue(1, base->getCell()->getCoord().getX());
                  msg->writeValue(2, base->getCell()->getCoord().getY());
                  msg->writeValue(3, COUNTER);
                 // cout << "transmit base" << endl;
                  robot->transmit();
                  if (1 == base->getDistance()){
                      direction = robot->getFieldView()->getDirection(myCoord, base->getCell()->getCoord());
                  } else {
                      direction = robot->getFieldView()->getDirection(myCoord, base->getCell()->getCoord());
                  }
                  exit = true;
                  break;
              } else {
                 direction = robot->getFieldView()->getDirection(base->getCell()->getCoord(), myCoord);
              }
        }
        if (exit){
            break;
        }
        IRobotView *bot;
        for (i = 0; i < r_num; ++i){
          bot = robot->getRobotInView(i);
              if (bot->isEnemy()) {
                      robot->shoot(bot);
                          IMessage *msg = robot->getOwnMessage();
                          msg->writeValue(0, HELP);
                          msg->writeValue(1, bot->getCell()->getCoord().getX());
                          msg->writeValue(2, bot->getCell()->getCoord().getY());
                          msg->writeValue(3, COUNTER);
                         // cout << "transmit help" << endl;
                          robot->transmit();
                      break;
              }
        }
        //смотрит сообщения
        /*IMessage *recvMsg = NULL;
        int msgType;
        int x;
        int y;
        for (i = 0; (i <= 1) && (i < robot->getNumOfMessages()); ++i){
          // cout << "Here" << endl;
          direction = robot->getDirection(myCoord, Coord2D(x, y));
          recvMsg = robot->getMessage(i);
          msgType = recvMsg->readValue(0);
          x = recvMsg->readValue(1);
          y = recvMsg->readValue(2);
          switch (msgType){
          case (BASE):{
             // cout << "HELP: x = " << x << " y = " << y << endl;
              exit = true;
              break;
          }
          case (HELP):{
              if (1 == i){
                  exit = true;
                  break;
              }
              break;
          }
          }
        }
        if ((recvMsg != NULL) && recvMsg->readValue(3)){
              //cout << "Msg: " << recvMsg->readValue(3) << endl;
              direction = robot->getDirection(myCoord, Coord2D(x, y));
              IMessage *msg = robot->getOwnMessage();
              msg->writeValue(0, recvMsg->readValue(0));
              msg->writeValue(1, robot->getCell()->getCoord().getX());
              msg->writeValue(2, robot->getCell()->getCoord().getY());
              msg->writeValue(3, recvMsg->readValue(3) - 1);
              robot->transmit();
        }

        if (exit){
            break;
        }*/

        break;
    }
    case (SOLDIER):{
        //cout << "SOLDIER" << endl;
        IBaseView *base;
        for (i = 0; i < b_num; ++i){
            base = robot->getBaseInView(i);
              if (!base->isActive() || base->isEnemy()) {
                  robot->shoot(base);
                  if (1 == base->getDistance()){
                      direction = robot->getFieldView()->getDirection(myCoord, base->getCell()->getCoord());
                  } else {
                    direction = robot->getFieldView()->getDirection(myCoord, base->getCell()->getCoord());
                  }
                  exit = true;
                  break;
              } else {
                  direction = robot->getFieldView()->getDirection(base->getCell()->getCoord(), myCoord);
              }
        }
        if (exit){
            break;
        }
        IRobotView *bot;
        for (i = 0; i < r_num; ++i){
          bot = robot->getRobotInView(i);
              if (bot->isEnemy()) {
                      robot->shoot(bot);
                      direction = robot->getFieldView()->getDirection(myCoord, bot->getCell()->getCoord());
                          IMessage *msg = robot->getOwnMessage();
                          msg->writeValue(0, HELP);
                          msg->writeValue(1, bot->getCell()->getCoord().getX());
                          msg->writeValue(2, bot->getCell()->getCoord().getY());
                          msg->writeValue(3, COUNTER);
                        //  cout << "transmit" << endl;
                          robot->transmit();
                      break;
              }
        }
        //смотрит сообщения
        IMessage *recvMsg = NULL;
        int msgType;
        int x;
        int y;
        for (i = 0; (i <= 1) && (i < robot->getNumOfMessages()); ++i){
          //  cout << "Here" << endl;
          direction = robot->getFieldView()->getDirection(myCoord, Coord2D(x, y));
          recvMsg = robot->getMessage(i);
          msgType = recvMsg->readValue(0);
          x = recvMsg->readValue(1);
          y = recvMsg->readValue(2);
          switch (msgType){
          case (HELP):{
             // cout << "HELP: x = " << x << " y = " << y << endl;
              exit = true;
              break;
          }
          case (BASE):{
              if (1 == i){
                  exit = true;
                  break;
              }
              break;
          }
          }
        }
           if ((recvMsg != NULL) && recvMsg->readValue(3)){
             // cout << "Msg: " << recvMsg->readValue(3) << endl;
              direction = robot->getFieldView()->getDirection(myCoord, Coord2D(x, y));
              IMessage *msg = robot->getOwnMessage();
              msg->writeValue(0, recvMsg->readValue(0));
              msg->writeValue(1, robot->getCell()->getCoord().getX());
              msg->writeValue(2, robot->getCell()->getCoord().getY());
              msg->writeValue(3, recvMsg->readValue(3) - 1);
              robot->transmit();
          }

          if (exit){
            break;
          }
        }
    }
    int rot = rand() % 2;
    if (!robot->canMove(direction)){
        if (rot){
            for (i = 0; i < 5; ++i){
                if (robot->canMove(((direction + i) % 6) + 1)){
                  direction = ((direction + i) % 6) + 1;
                  break;
                }
            }
        } else {
            for (i = 5; i < 0; --i){
                if (robot->canMove(((direction + i) % 6) + 1)){
                  direction = ((direction + i) % 6) + 1;
                  break;
                }
            }

        }
    }
    robot->move(direction);
    robot->getOwnMemory()->writeValue(1, direction);
    return;
}

