#-------------------------------------------------
#
# Project created by QtCreator 2014-06-27T11:55:26
#
#-------------------------------------------------

QT       -= core gui

TARGET = stupid
TEMPLATE = lib

INCLUDEPATH += "../../include"

QMAKE_CXXFLAGS += -DROBO_EXPORT

DESTDIR = "../../game/Bots"

SOURCES += \
    stupid.cpp

HEADERS += \
    stupid.h
