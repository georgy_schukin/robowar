#include "red_bot.h"

#include <cstdlib>

using namespace robowar;

#define EAST 1
#define NORD_EAST 2
#define NORD_WEST 3
#define WEST 4
#define SOUTH_EAST 6
#define SOUTH_WEST 5




void move_rand(IRobotController *robot){

    int r = rand()%3;
    int go = rand()+100;

    switch(r)
    {
        case 0: {
               if(!robot->move(go%6+1)) {
                   robot->cancelMove();
                   if(!robot->move((go+1)%6+1)){
                       robot->cancelMove();
                       if(!robot->move((go+2)%6+1)){
                           robot->cancelMove();
                           if(!robot->move((go+3)%6+1)){
                               robot->cancelMove();
                               if(!robot->move((go+4)%6+1)){
                                   robot->cancelMove();
                                   if(!robot->move((go+5)%6+1))
                                       robot->cancelMove();
                               }
                            }
                        }
                    }
                }
               break;
            }
        case 1: { robot->move(rand()%7); break;}
        case 2: break;
    };
}





void move_dir(IRobotController *robot, int cx, int cy)
{
    Coord2D cm = robot->getCell()->getCoord();
    if(cm.getX() == cx)
    {
        if(cm.getY() < cy)
        {
             if(robot->move(SOUTH_EAST)) return;
             else
             {
                 robot->cancelMove();
                 if(robot->move(SOUTH_WEST)) return;
             }

        }
        if(cm.getY() > cy)
        {
             if(robot->move(NORD_EAST)) return;
             else
             {
                 robot->cancelMove();
                 if(robot->move(NORD_WEST)) return;
             }

        }
     }
     else if(cm.getX() > cx)
     {
        if(robot->move(WEST)) return;
        else
        { robot->cancelMove();
        if(cm.getY() < cy)
        {
             if(robot->move(SOUTH_EAST)) return;
             else
             {
                 robot->cancelMove();
                 if(robot->move(SOUTH_WEST)) return;
             }

        }
        if(cm.getY() > cy)
        {
             if(robot->move(NORD_EAST)) return;
             else
             {
                 robot->cancelMove();
                 if(robot->move(NORD_WEST)) return;
             }

        }
        }

     }
    else if(cm.getX() < cx)
    {
       if(robot->move(WEST)) return;
       else
       { robot->cancelMove();
       if(cm.getY() < cy)
       {
            if(robot->move(SOUTH_EAST)) return;
            else
            {
                robot->cancelMove();
                if(robot->move(SOUTH_WEST)) return;

            }

       }
       if(cm.getY() > cy)
       {
            if(robot->move(NORD_EAST)) return;
            else
            {
                robot->cancelMove();
                if(robot->move(NORD_WEST)) return;

            }

       }
       }

    }





}




ROBO_FUNC void execute(IRobotController *robot) {


    if(robot->getNumOfMessages()>0)
    {
        IMessage* m = robot->getMessage(0);
        int coordx = m->readValue(0);
        int coordy = m->readValue(1);

        move_dir(robot, coordx, coordy);
    }
    else
        move_rand(robot);




    int count_Enemy = 0;
    int count_Friend = 0;
    int num_enemy = 0;
    for (size_t i = 0; i < robot->getNumOfRobotsInView(); i++) {
        IRobotView *bot = robot->getRobotInView(i);
        if (bot->isEnemy()){
            count_Enemy++;
            num_enemy = i;
        }
        else count_Friend++;
    }

    if(count_Enemy > 0 && count_Enemy >= count_Friend){
       IRobotView *bot = robot->getRobotInView(num_enemy);
       robot->shoot(bot);
       robot->cancelMove();
       IGameCell* c = bot->getCell();
       robot->move(c);

       Coord2D coord = bot->getCell()->getCoord();

       robot->getOwnMessage()->writeValue(0,coord.getX());
       robot->getOwnMessage()->writeValue(1,coord.getY());
       robot->transmit();


    }
    else if(count_Enemy > 0)
    {
        IRobotView *bot = robot->getRobotInView(num_enemy);
        robot->shoot(bot);
        robot->cancelMove();
    }

    for (size_t i = 0; i < robot->getNumOfBasesInView(); i++) {
        IBaseView *base = robot->getBaseInView(i);
        if (!base->isActive() || base->isEnemy()) {
            robot->shoot(base);
            robot->cancelMove();
            break;
        }
    }



}
