#pragma once

#include "robowar.h"

extern "C" {
    ROBO_FUNC void execute(robowar::IRobotController *robot);
}
