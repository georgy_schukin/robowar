#include "bot.h"
#include <cstdlib>

using namespace robowar;

ROBO_FUNC void execute(IRobotController *robot) {
    robot->move(rand() % 7);

    for (size_t i = 0; i < robot->getNumOfRobotsInView(); i++) {
        IRobotView *bot = robot->getRobotInView(i);
        if (bot->isEnemy()) {
            robot->shoot(bot);
            robot->cancelMove();
            break;
        }
    }

    for (size_t i = 0; i < robot->getNumOfBasesInView(); i++) {
        IBaseView *base = robot->getBaseInView(i);
        if (!base->isActive() || base->isEnemy()) {
            robot->shoot(base);
            robot->cancelMove();
            break;
        }
    }
}
