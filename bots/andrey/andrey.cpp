#include "andrey.h"
#include <stdio.h>
#include <cstdlib>

#define DELTA_XP 5
#define RETRANSMIT_TIME 5
#define BASE_INFLUENCE 6
#define ROBOT_INFLUENCE 1
#define MIN_PATH_RATE_TO_SIZE 4
#define CHANCE_TO_STAY 4

using namespace robowar;

int getDistance(Coord2D cr1, Coord2D cr2)
{
	const int ax = cr1.getY();
	const int ay = cr1.getX() - (cr1.getY() / 2 + cr1.getY() % 2);
	const int az = cr1.getX() + cr1.getY() / 2;
	const int bx = cr2.getY();
	const int by = cr2.getX() - (cr2.getY() / 2 + cr2.getY() % 2);
	const int bz = cr2.getX() + cr2.getY() / 2;

	const int v1 = abs(ax - bx) + abs(ay - by);
	const int v2 = abs(ay - by) + abs(az - bz);
	const int v3 = abs(az - bz) + abs(ax - bx);

	return v2 < v3 ? (v1 < v2 ? v1 : v2) : (v1 < v3 ? v1 : v3); // return min from v1, v2, v3
}

size_t getNumOfFriends(IRobotController *robot)
{
	size_t num = 0;
	for (size_t i = 0; i < robot->getNumOfRobotsInView(); i++)
		if (!robot->getRobotInView(i)->isEnemy())
			num++;
	return num;
}

ROBO_FUNC void execute(IRobotController *robot)
{
	IEnvironment *env = robot->getEnvironment();
	Coord2D my_coord = robot->getCell()->getCoord();
	robot->cancelTransmit();

	size_t r_num = robot->getNumOfRobotsInView(); // num of robots in view range
	size_t b_num = robot->getNumOfBasesInView(); // num of bases in view range
	size_t b_en_num = 0; // num of enemies bases in view range
	for (int i = 0; i < b_num; i++)
		if (robot->getBaseInView(i)->isEnemy())
			b_en_num++;

	size_t m_num = robot->getNumOfMessages(); // num of income messages
	size_t fr_num = getNumOfFriends(robot); // num of friends
	size_t en_num = r_num - fr_num; // num of enemies

	if (robot->getOwnMemory()->readValue(0) == 0)
	{
		robot->getOwnMemory()->writeValue(0, 1);
		robot->getOwnMemory()->writeValue(1, rand() % (env->getFieldWidth() - 1));
		robot->getOwnMemory()->writeValue(2, rand() % (env->getFieldHeight() - 1));
	}

	if (robot->getOwnMemory()->readValue(0) == 1)
	{
		if (my_coord.getX() == robot->getOwnMemory()->readValue(1) &&
			my_coord.getY() == robot->getOwnMemory()->readValue(2))
		{
			int x, y;
			int rx = my_coord.getX();
			int ry = my_coord.getY();
			do
			{
				x = rand() % (env->getFieldWidth() - 1);
				y = rand() % (env->getFieldHeight() - 1);
			}
			while (getDistance(Coord2D(x, y), my_coord) < (env->getFieldWidth() + env->getFieldHeight()) / (2 * MIN_PATH_RATE_TO_SIZE));
			robot->getOwnMemory()->writeValue(1, x);
			robot->getOwnMemory()->writeValue(2, y);
		}
	}
	robot->move(Coord2D(robot->getOwnMemory()->readValue(1), robot->getOwnMemory()->readValue(2)));

	for (int i = r_num - 1; i >= 0; i--) // for each robot in view
	{
		IRobotView *rbt = robot->getRobotInView(i);
		if (rbt->isEnemy())  // robot is not in our team
		{
			robot->shoot(rbt);
			IRobotView **r_en = new IRobotView*[en_num];
			IRobotView **r_fr = new IRobotView*[fr_num];
			int en_count = 0, fr_count = 0;
			for (int j = 0; j < r_num; j++)
			{
				IRobotView *r = robot->getRobotInView(j);
				if (r->isEnemy())
					r_en[en_count++] = r;
				else
					r_fr[fr_count++] = r;
			}
			int fr_xp = 0, en_xp = 0;
			bool canshoot;
			for (fr_count = 0; fr_count < fr_num; fr_count++)
			{
				canshoot = false;
				for (en_count = 0; en_count < en_num; en_count++)
					if (getDistance(r_fr[fr_count]->getCell()->getCoord(), r_en[en_count]->getCell()->getCoord()) <= env->getShootRange())
						canshoot = true;
				if (canshoot)
					fr_xp += r_fr[fr_count]->getHitpoints();
			}
			for (en_count = 0; en_count < en_num; en_count++)
				en_xp += r_en[en_count]->getHitpoints();
			if (fr_xp > en_xp + DELTA_XP)
				robot->move(rbt->getCell()); // move to robot
			else
				robot->move(robot->getFieldView()->getDirection(rbt->getCell()->getCoord(), my_coord)); // move away from enemy
			Coord2D rbt_coord = rbt->getCell()->getCoord();
			IMessage *msg = robot->getOwnMessage();
			msg->writeValue(1, rbt_coord.getX()); // transmit robot coords to others
			msg->writeValue(2, rbt_coord.getY());
			msg->writeValue(3, env->getIteration());
			msg->writeValue(4, ROBOT_INFLUENCE * en_num - BASE_INFLUENCE * b_en_num);
			robot->transmit();
		}
	}

	for (size_t i = 0; i < b_num; i++) // for each base in view
	{
		IBaseView *base = robot->getBaseInView(i);
		if (base->isEnemy() || !base->isActive()) // base is not in our team
		{
			robot->shoot(base);
			if (fr_num > 1 && base->isEnemy())
				robot->move(base->getCell()); // move to base
			else
				robot->cancelMove(); // stay in place
			Coord2D base_coord = base->getCell()->getCoord();
			IMessage *msg = robot->getOwnMessage();
			msg->writeValue(1, base_coord.getX()); // transmit base coords to others
			msg->writeValue(2, base_coord.getY());
			msg->writeValue(3, env->getIteration());
			msg->writeValue(4, ROBOT_INFLUENCE * en_num - BASE_INFLUENCE * b_en_num);
			robot->transmit();
		}
	}

	// if is idle and have income messages
	if (!robot->isShooting() && (m_num > 0))
	{
		IMessage *msg = robot->getMessage(0); // first income message
		int tx = msg->readValue(1);
		int ty = msg->readValue(2);
		int iter = msg->readValue(3);
		int dist = getDistance(Coord2D(tx, ty), my_coord);
		int i, ii = 0;
		int val = dist - msg->readValue(4);
		int m_val = val;
		for (i = 1; i < m_num; i++)
		{
			msg = robot->getMessage(i);
			dist = getDistance(my_coord, Coord2D(msg->readValue(1), msg->readValue(2)));
			val = dist + msg->readValue(4);
			if (val < m_val)
			{
				tx = msg->readValue(1);
				ty = msg->readValue(2);
				m_val = val;
				ii = i;
			}
		}
		msg = robot->getMessage(ii);
		iter = msg->readValue(3);
		if (!(b_num - b_en_num))
			if (getDistance(my_coord, Coord2D(robot->getOwnMemory()->readValue(1), robot->getOwnMemory()->readValue(2))) > getDistance(my_coord, Coord2D(tx, ty)))
			{
				robot->move(Coord2D(tx, ty)); // move to target
				//robot->getOwnMemory()->writeValue(1, tx);
				//robot->getOwnMemory()->writeValue(2, ty);
			}
		if (!robot->isTransmitting() && (env->getIteration() - iter < RETRANSMIT_TIME))
			robot->transmit(msg);
	}

	bool isStop = my_coord.getX() == robot->getOwnMemory()->readValue(3) &&
		my_coord.getY() == robot->getOwnMemory()->readValue(4);
	robot->getOwnMemory()->writeValue(3, my_coord.getX());
	robot->getOwnMemory()->writeValue(4, my_coord.getY());

	// move away from own base
	for (int i = 1; i < 7; i++)
	{
		auto cell = robot->getNearCell(i);
		if (cell && cell->isBaseHere() && cell->getObject()->getTeam() == robot->getTeam() && isStop && !en_num)
		{
			int dir = robot->getFieldView()->getDirection(my_coord, cell->getCoord());
			if (!robot->move((dir + 1) % 6))
				robot->move(dir == 1 ? 6 : (dir - 1) % 6);
		}
	}

	// don't stop
	if (isStop && !robot->isShooting() && robot->canShoot() && !(rand() % CHANCE_TO_STAY))
	{
		int count = 0;
		for (int i = 1; i < 7; i++)
			if (robot->canMove(robot->getNearCell(i)))
				count++;
		if (count)
		{
			int *dirs = new int[count];
			int j = 0;
			for (int i = 1; i < 7; i++)
				if (robot->canMove(robot->getNearCell(i)))
					dirs[j++] = i;
			robot->move(dirs[rand() % count]);
		}
	}
}
