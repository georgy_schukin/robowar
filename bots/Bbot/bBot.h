#pragma once

#include "robowar.h"

extern "C" {
    ROBO_FUNC void execute(robowar::IRobotController *robot);
}

//MEMORY POSITION
#define CLASS 0
#define DIR 1
#define DIRX 2
#define DIRY 3
#define DIRD 2

//MEMORY VALUES
#define SCOUT 1
#define REGULAR 2
#define X 40
#define STAGE2 150

//MESSAGES VALUES

#define ENEMY 1
#define BASE 2

#define MAXCOUNT 3

void start(robowar::IRobotController *robot);
void setClass(robowar::IRobotController *robot, int classType);
void lookAndShoot(robowar::IRobotController *robot);
