#ifndef COMMUNICATION_H
#define COMMUNICATION_H
#include "robowar.h"

void addToMsg(robowar::IRobotController *robot, int * msg, int size);
void speak(robowar::IRobotController *robot);
void listen(robowar::IRobotController *robot);
void enemyReact(robowar::IRobotController * robot, robowar::IMessage * msg, int * i);
void baseReact(robowar::IRobotController * robot, robowar::IMessage * msg, int * i);

#endif // COMMUNICATION_H
