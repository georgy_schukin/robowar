#include <iostream>
#include <cstdlib>
#include "bots/bot.h"
#include "bBot.h"
#include "movement.h"
#include "communication.h"

using namespace robowar;
using namespace std;

void addToMsg(IRobotController *robot, int * msg, int size)
{
    int i = 0;
    do
    {
        if (robot->getOwnMessage()->readValue(i) == ENEMY)
            i += 4;
        else if (robot->getOwnMessage()->readValue(i) == BASE)
            i += 4;
    }
    while (robot->getOwnMessage()->readValue(i) != 0);
    for (int j = 0;j < size;++j)
        robot->getOwnMessage()->writeValue(i + j,msg[j]);
}

void speak(IRobotController *robot)
{
    robot->transmit();
}

void listen(IRobotController *robot)
{
    if (robot->getOwnMemory()->readValue(CLASS) == SCOUT)
        return;
    int n = robot->getNumOfMessages();
    for (int i = 0;i < n;++i)
    {
        IMessage * msg = robot->getMessage(i);
        for (int j = 0;j < msg->getSize();++j)
        {
            if (msg->readValue(j) == ENEMY)
                enemyReact(robot,msg,&j);
            else if (msg->readValue(j) == BASE)
                baseReact(robot,msg,&j);
        }
    }
}

void enemyReact(IRobotController * robot,IMessage * msg,int * i)
{
    *i += 1;
    int x = msg->readValue(*i);
    *i += 1;
    int y = msg->readValue(*i);
    *i += 1;
    int count = msg->readValue(*i);
    setDir(robot,x,y);

    if (count < MAXCOUNT)
    {
        int msg2[] = {ENEMY, x, y, ++count};
        addToMsg(robot, msg2, 4);
    }
}

void baseReact(IRobotController * robot,IMessage * msg,int * i)
{
    *i += 1;
    int x = msg->readValue(*i);
    *i += 1;
    int y = msg->readValue(*i);
    *i += 1;
    int count = msg->readValue(*i);
    setDir(robot,x,y);

    if (count < MAXCOUNT)
    {
        int msg2[] = {BASE, x, y, ++count};
        addToMsg(robot, msg2, 4);
    }
}

