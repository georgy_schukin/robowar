#include <iostream>
#include <cstdlib>
#include "robowar.h"
#include "bBot.h"
#include "movement.h"
#include "communication.h"

using namespace robowar;
using namespace std;

ROBO_FUNC void execute(IRobotController *robot) {
    start(robot);
    move(robot);
    lookAndShoot(robot);
    listen(robot);
    speak(robot);
}

void start(IRobotController *robot)
{
    robot->getOwnMessage()->clear();
    int x = robot->getEnvironment()->getFieldHeight() * robot->getEnvironment()->getFieldWidth();
    if (robot->getOwnMemory()->readValue(CLASS) == 0)
    {
//        int i = robot->getEnvironment()->getIteration();
//        if (i < STAGE2)
//        {
            setClass(robot,SCOUT);
            return;
//        }
//        int d = robot->getEnvironment()->getRobotConstructionTime();
//        int n = (i / d) % 10;
//        if (n < 3)
//            setClass(robot,SCOUT);
//        else
//            setClass(robot,REGULAR);
    }
}

void setClass(IRobotController *robot, int classType)
{
    if (classType == SCOUT)
    {
        int i = robot->getEnvironment()->getIteration();
        int d = robot->getEnvironment()->getRobotConstructionTime();
        int n = (i / d) % 7;
        setDir(robot,n);
        robot->getOwnMemory()->writeValue(CLASS,SCOUT);
    }
    else if (classType == REGULAR)
    {
        resetDir(robot);
        robot->getOwnMemory()->writeValue(CLASS,REGULAR);
    }
}

void lookAndShoot(IRobotController *robot)
{
    for (size_t i = 0; i < robot->getNumOfBasesInView(); i++) {
        IBaseView *base = robot->getBaseInView(i);
        if (!base->isActive() || base->isEnemy()) {
            robot->shoot(base);

            int x = base->getCell()->getCoord().getX();
            int y = base->getCell()->getCoord().getY();

            int msg[] = {BASE, x, y, 0};
            addToMsg(robot, msg, 4);

            setClass(robot,REGULAR);
            break;
        }
    }

    for (size_t i = 0; i < robot->getNumOfRobotsInView(); i++) {
        IRobotView *bot = robot->getRobotInView(i);
        if (bot->isEnemy()) {
            robot->shoot(bot);

            int x = bot->getCell()->getCoord().getX();
            int y = bot->getCell()->getCoord().getY();

            int msg[] = {ENEMY, x, y, 0};
            addToMsg(robot, msg, 4);

            setClass(robot,REGULAR);
            break;
        }
    }
}

