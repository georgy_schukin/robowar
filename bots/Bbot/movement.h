#ifndef MOVEMENT_H
#define MOVEMENT_H

#include "robowar.h"

void move(robowar::IRobotController *robot);
void setDir(robowar::IRobotController *robot, int x, int y);
void setDir(robowar::IRobotController *robot, int dir);
void resetDir(robowar::IRobotController *robot);

#endif // MOVEMENT_H
