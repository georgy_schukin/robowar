#include <iostream>
#include <cstdlib>
#include "robowar.h"
#include "bBot.h"
#include "movement.h"

using namespace robowar;
using namespace std;

void move(IRobotController *robot)
{
    int dir = robot->getOwnMemory()->readValue(DIR);
    if (dir > 0)
    {
        if (dir == 1)
        {
            int x = robot->getOwnMemory()->readValue(DIRX);
            int y = robot->getOwnMemory()->readValue(DIRY);
            Coord2D coord(x,y);
            robot->move(coord);
            resetDir(robot);
        }
        else if (dir == 2)
        {
            int d = robot->getOwnMemory()->readValue(DIRD);
            int x = rand() % 10;
            int d2 = 0;
            if (x < 3)
                d2 = -1;
            else if (3 < x && x < 7)
                d2 = 0;
            else if (7 < x && x < 10)
                d2 = 1;
            d += d2;
            if (robot->getNearCell(d) != NULL)
                if (robot->getNearCell(d)->isFree())
                {
                    robot->move(d);
                    return;
                }
                else
                    setClass(robot,REGULAR);
            robot->move(rand() % 7);
        }
    }
    else
    {
        robot->move(rand() % 7);
    }
}

void setDir(IRobotController *robot, int x, int y)
{
    robot->getOwnMemory()->writeValue(DIR,1);
    robot->getOwnMemory()->writeValue(DIRX,x);
    robot->getOwnMemory()->writeValue(DIRY,y);
}

void setDir(IRobotController *robot, int dir)
{
    robot->getOwnMemory()->writeValue(DIR,2);
    robot->getOwnMemory()->writeValue(DIRD,dir);
}

void resetDir(IRobotController *robot)
{
    robot->getOwnMemory()->writeValue(DIR,0);
    robot->getOwnMemory()->writeValue(DIRX,0);
    robot->getOwnMemory()->writeValue(DIRY,0);
}


