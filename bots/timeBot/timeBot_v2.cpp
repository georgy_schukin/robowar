#include "robowar.h"
#include <string.h>
#include <cstdlib>
#include <stdio.h>
#include <math.h>

using namespace robowar;

#define razbros 7
#define pamat 5
#define radiusprobega 0.5
#define koef 500

void MoveRobot(IRobotController *robot,Coord2D goal)
{
    bool flag = 0;

    flag = robot->move(goal);
    if (!flag)
    {
        robot->cancelMove();

        Coord2D coord = robot->getCell()->getCoord();
        float dx = goal.getX() - coord.getX();
        float dy = goal.getY() - coord.getY();

        if (!flag && dx>=0 && dy>=0)
        {
            if (!flag) flag = robot->move(6);
            if (!flag) flag = robot->move(1);
            if (!flag) flag = robot->move(5);
            if (!flag) flag = robot->move(4);
            if (!flag) flag = robot->move(2);
            if (!flag) flag = robot->move(3);
        }

        if (!flag && dx<0 && dy>=0)
        {
            if (!flag) flag = robot->move(5);
            if (!flag) flag = robot->move(6);
            if (!flag) flag = robot->move(4);
            if (!flag) flag = robot->move(1);
            if (!flag) flag = robot->move(3);
            if (!flag) flag = robot->move(2);
        }

        if (!flag && dx<0 && dy<0)
        {
            if (!flag) flag = robot->move(3);
            if (!flag) flag = robot->move(4);
            if (!flag) flag = robot->move(2);
            if (!flag) flag = robot->move(1);
            if (!flag) flag = robot->move(5);
            if (!flag) flag = robot->move(6);
        }

        if (!flag && dx>=0 && dy<0)
        {
            if (!flag) flag = robot->move(2);
            if (!flag) flag = robot->move(1);
            if (!flag) flag = robot->move(3);
            if (!flag) flag = robot->move(4);
            if (!flag) flag = robot->move(6);
            if (!flag) flag = robot->move(5);
        }
    }
}

size_t getNumOfFriends(IRobotController *robot) {
    size_t num = 0;
    for (size_t i = 0; i < robot->getNumOfRobotsInView(); i++)
        if (!robot->getRobotInView(i)->isEnemy())
            num++;
    return num;
}

size_t getNumOfEnemies(IRobotController *robot) {
    size_t num = 0;
    for (size_t i = 0; i < robot->getNumOfRobotsInView(); i++)
        if (robot->getRobotInView(i)->isEnemy())
            num++;
    return num;
}

Coord2D findPoint(int iter, int maxx, int maxy)
{
    int interval = sqrt((float)maxx*maxx + maxy*maxy);
    //int max = 0;
    //int intervalMass[9];
    //int xMass[9];
    //int yMass[9];

    //intervalMass[0] = interval/2;
    //intervalMass[1] = interval/4;
    //intervalMass[2] = interval/3;
    //intervalMass[3] = interval/3;
    //intervalMass[4] = interval/3;
    //intervalMass[5] = interval/3;
    //intervalMass[6] = interval*3.0/4;
    //intervalMass[7] = interval*3.0/4;
    //intervalMass[8] = interval*3.0/4;

    //xMass[0] = 0.5*maxx;
    //xMass[1] = 0.35*maxx;
    //xMass[2] = 0.5*maxx;
    //xMass[3] = 0.65*maxx;
    //xMass[4] = 0.5*maxx;
    //xMass[5] = 0.15*maxx;
    //xMass[6] = 0.15*maxx;
    //xMass[7] = 0.85*maxx;
    //xMass[8] = 0.85*maxx;

    //yMass[0] = 0.5*maxy;
    //yMass[1] = 0.5*maxy;
    //yMass[2] = 0.65*maxy;
    //yMass[3] = 0.5*maxy;
    //yMass[4] = 0.35*maxy;
    //yMass[5] = 0.15*maxy;
    //yMass[6] = 0.85*maxy;
    //yMass[7] = 0.85*maxy;
    //yMass[8] = 0.15*maxy;

    //for (int i = 0; i<9; i++)
    //  max += intervalMass[i];

    //int ostatok = iter%max;

    //int i = 0;
    //bool flag = 1;
    //for (i = 0; flag && i<9; i++)
    //{
    //  ostatok -= intervalMass[i];
    //  if (ostatok <=0 ) flag = false;
    //}

    //Coord2D rez(xMass[i],yMass[i]);

    //float R = 1;
    //float r = 0.6;
    //float h = 0.13;
    //float fi = iter*0.04*84.0/interval;
    //float koeficient = 1.0/0.5;
    //int x = maxx/2 + maxx*koeficient*((R-r)*cos(fi) + h*cos(((R-r)/r) * fi))/2;
    //int y = maxy/2 + maxy*koeficient*((R-r)*sin(fi) - h*sin(((R-r)/r) * fi))/2;
    //Coord2D rez1(x,y);


    float radiusx = maxx/2;
    float radiusy = maxy/2;
    float fi = iter*0.03 * 50.0/((radiusx+radiusy)/2);
    int x = maxx/2 + radiusx*cos(fi);
    int y = maxy/2 + radiusy*sin(fi);
    Coord2D rez2(x,y);
    
    return rez2;
}


void timebot(IRobotController *robot) {


    ///////////////////////////////////////////////////////////////////////////////////////////// POLU4ENIE START INFORMATION
    IEnvironment *env = robot->getEnvironment();
    Coord2D goal;
    int goalType = 0;
    int goalIter = -1;

    bool Plotnost = 0;

    robot->cancelTransmit();

    const size_t r_num = robot->getNumOfRobotsInView();
    const size_t b_num = robot->getNumOfBasesInView();
    const size_t m_num = robot->getNumOfMessages();

    const size_t fr_num = getNumOfFriends(robot);
    const size_t en_num = r_num - fr_num;

    int iter = env->getIteration();
    int maxx = env->getFieldWidth();
    int maxy = env->getFieldHeight(); 


    ///////////////////////////////////////////////////////////////////////////////////////////// ATAKA BOTOV
    for (size_t i = 0;i < r_num;i++) {
        IRobotView *rbt = robot->getRobotInView(i);
        if (rbt->isEnemy()) {
            robot->shoot(rbt);
            if (fr_num > en_num + 1){
                goal = rbt->getCell()->getCoord(); 
                goalType = 1; 
                goalIter = env->getIteration();
                Plotnost = 1;
            }else 
                if (en_num-3 > fr_num)
                {
                    int x = robot->getCell()->getCoord().getX();
                    int y = robot->getCell()->getCoord().getY();
                    int Direction = robot->getDirection(goal, robot->getCell()->getCoord());
                    if (Direction == 1) {x+=1;}
                    if (Direction == 2) {x+=1;y-=1;}
                    if (Direction == 3) {x-=1;y-=1;}
                    if (Direction == 4) {x-=1;}
                    if (Direction == 5) {x-=1;y+=1;}
                    if (Direction == 6) {x+=1;y+=1;}
                    Coord2D buf(x,y);
                    goal = buf;
                    goalType = 1; 
                    goalIter = env->getIteration();
                }

            const Coord2D &rbt_coord = rbt->getCell()->getCoord();
            IMessage *msg = robot->getOwnMessage();
            msg->writeValue(0, rbt_coord.getX());
            msg->writeValue(1, rbt_coord.getY());
            msg->writeValue(2, env->getIteration());
            msg->writeValue(3, 1);
            robot->transmit();
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////// ATAKA BAZ
    for (size_t i = 0;i < b_num;i++) { 
        IBaseView *base = robot->getBaseInView(i);
        if (base->isEnemy() || !base->isActive()) {
            robot->shoot(base);
            if (fr_num > 1){
                goal = base->getCell()->getCoord(); 
                goalType = 2; 
                goalIter = env->getIteration();
                Plotnost = 1;
            }else{
                int x = robot->getCell()->getCoord().getX();
                int y = robot->getCell()->getCoord().getY();
                Coord2D buf(x,y);
                goal = buf;
            }

            const Coord2D &base_coord = base->getCell()->getCoord();
            IMessage *msg = robot->getOwnMessage();
            msg->writeValue(0, base_coord.getX());
            msg->writeValue(1, base_coord.getY());
            msg->writeValue(2, env->getIteration());
            msg->writeValue(3, 2);
            robot->transmit();
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////// MESSAGE

  //  if(!robot->isShooting() && (m_num > 0)) {
  //      IMessage *msg = robot->getMessage(0);
  //      int tx = msg->readValue(0);
  //      int ty = msg->readValue(1);
  //      int iter = msg->readValue(2);
        //int type = msg->readValue(3);

        //Coord2D buf(tx,ty);
        //goal = buf;
        //goalType = type;

  //      if (!robot->isTransmitting() && ((env->getIteration() - iter) < pamat)) {
  //          robot->transmit(msg);
  //      }
  //  }

    for (int i = 0; !robot->isShooting() && goalType==0 && i<m_num; i++)
    {
        IMessage *msg = robot->getMessage(i);
        int tx = msg->readValue(0);
        int ty = msg->readValue(1);
        int iter = msg->readValue(2);
        int type = msg->readValue(3);

        if (type > goalType)
        {
            Coord2D buf(tx,ty);
            goal = buf;
            goalType = type;
        }
        if (type == goalType && iter > goalIter)
        {
            Coord2D buf(tx,ty);
            goal = buf;
            goalType = type;
        }
        
        if (!robot->isTransmitting() && ((env->getIteration() - iter) < pamat))
            robot->transmit(msg);
    }


    ///////////////////////////////////////////////////////////////////////////////////////////// MOVE
    if (goalType==0)
    {
        int faza;
        int gran;
        int gran1;
        if (maxx>65 && maxy>65)
        {
            faza = iter%500;
            gran = 150;
            gran1 = 80;
        }else{
            faza = iter%300;
            gran = 50;
            gran1 = 25;
        }
        if (faza>=gran1 && faza<gran)
        {
            Coord2D rez (maxx/2,maxy/2);
            int bufx = rez.getX() + rand()%(razbros+6+1)-(razbros+6)/2;
            int bufy = rez.getY() + rand()%(razbros+6+1)-(razbros+6)/2;
            Coord2D rez1(bufx,bufy);
            MoveRobot(robot,rez1);
        }
        if (faza>=gran)
        {
            Coord2D rez = findPoint(iter,maxx,maxy);
            int bufx = rez.getX() + rand()%(razbros+1)-razbros/2;
            int bufy = rez.getY() + rand()%(razbros+1)-razbros/2;
            Coord2D rez1(bufx,bufy);
            MoveRobot(robot,rez1);
        }
        if (faza<gran1)
        {
            bool flag = 1;
            for (int i = 0; flag && i<36; i++)
                flag = !robot->move(rand()%6+1);
        }
    }else{
        if (Plotnost)
            robot->move(goal);
        else{
            int x = goal.getX() + rand()%(razbros+1)-razbros/2;
            int y = goal.getY() + rand()%(razbros+1)-razbros/2;
            Coord2D goalRand(x,y);
            MoveRobot(robot,goalRand);
        }
    }
}
