#include "bots/timebot.h"
#include <string.h>
#include <cstdlib>
#include <stdio.h>
#include <conio.h>
#include <math.h>

using namespace robowar;

#define razbros 7

#define radiusprobega 0.5
#define koef 500
#define vremarandomhod 3
#define dolgoHodit 5

#define dedline1 25 //slu4a'ni' beg
#define dedline2 50 //begut v centr
#define dedline3 90 //beg po krugu
#define dedline4 300 //beg po krugu

#define pobeg 5;

void MoveRobot(IRobotController *robot,Coord2D goal,int oldx, int oldy)
{
	bool flag = 0;

	flag = robot->move(goal);

	if (!flag && oldx == robot->getCell()->getCoord().getX() && oldy == robot->getCell()->getCoord().getY())
	{
		bool flag = robot->move(rand()%6+1);
		for (int i = 0; flag && i<50; i++)
			flag = robot->move(rand()%6+1);
	}else{

		if (!flag)
		{
			robot->cancelMove();

			Coord2D coord = robot->getCell()->getCoord();
			float dx = goal.getX() - coord.getX();
			float dy = goal.getY() - coord.getY();

			if (!flag && dx>=0 && dy>=0)
			{
				if (!flag) flag = robot->move(6);
				if (!flag) flag = robot->move(1);
				if (!flag) flag = robot->move(5);
				if (!flag) flag = robot->move(4);
				if (!flag) flag = robot->move(2);
				if (!flag) flag = robot->move(3);
			}

			if (!flag && dx<0 && dy>=0)
			{
				if (!flag) flag = robot->move(5);
				if (!flag) flag = robot->move(6);
				if (!flag) flag = robot->move(4);
				if (!flag) flag = robot->move(1);
				if (!flag) flag = robot->move(3);
				if (!flag) flag = robot->move(2);
			}

			if (!flag && dx<0 && dy<0)
			{
				if (!flag) flag = robot->move(3);
				if (!flag) flag = robot->move(4);
				if (!flag) flag = robot->move(2);
				if (!flag) flag = robot->move(1);
				if (!flag) flag = robot->move(5);
				if (!flag) flag = robot->move(6);
			}

			if (!flag && dx>=0 && dy<0)
			{
				if (!flag) flag = robot->move(2);
				if (!flag) flag = robot->move(1);
				if (!flag) flag = robot->move(3);
				if (!flag) flag = robot->move(4);
				if (!flag) flag = robot->move(6);
				if (!flag) flag = robot->move(5);
			}
		}
	}
}

size_t getNumOfFriends(IRobotController *robot) {
    size_t num = 0;
    for (size_t i = 0; i < robot->getNumOfRobotsInView(); i++)
        if (!robot->getRobotInView(i)->isEnemy())
            num++;
    return num;
}

size_t getNumOfEnemies(IRobotController *robot) {
    size_t num = 0;
    for (size_t i = 0; i < robot->getNumOfRobotsInView(); i++)
        if (robot->getRobotInView(i)->isEnemy())
            num++;
    return num;
}

Coord2D findPoint(int iter, int maxx, int maxy)
{
	int interval = sqrt((float)maxx*maxx + maxy*maxy);
	float radiusx = maxx/2;
	float radiusy = maxy/2;
	float fi = iter*0.03 * 50.0/((radiusx+radiusy)/2);
	int x = maxx/2 + radiusx*cos(fi);
	int y = maxy/2 + radiusy*sin(fi);
	Coord2D rez2(x,y);
	
	return rez2;
}


void execute(IRobotController *robot) {
	///////////////////////////////////////////////////////////////////////////////////////////// POLU4ENIE START INFORMATION
    IEnvironment *env = robot->getEnvironment();
	IMessage *memory = robot->getOwnMemory();
	Coord2D goal;
	int goalType = 0;
	int goalDistance = 0;
	int goalIter = -1;
	int dolgohodit;
	bool Plotnost = 0;

	

    robot->cancelTransmit();

    const size_t r_num = robot->getNumOfRobotsInView();
    const size_t b_num = robot->getNumOfBasesInView();
    const size_t m_num = robot->getNumOfMessages();

    const size_t fr_num = getNumOfFriends(robot);
    const size_t en_num = r_num - fr_num;

	int iter = env->getIteration();
    int maxx = env->getFieldWidth();
    int maxy = env->getFieldHeight(); 

	int pamat;
	if (iter<dedline2) pamat = 3; else pamat = 10;
	int razmer = sqrt((float)maxx*maxx+maxy*maxy);



	int iterDR = memory->readValue(4);
	if (iterDR==0) {iterDR = iter; memory->writeValue(4,iterDR);}
	/*if (iter<5) dolgohodit = 2;
	if (iter>=5 && iter<10) dolgohodit = 4;
	if (iter>=10) dolgohodit = dolgoHodit;*/
	dolgohodit = dolgoHodit;

	int direction = memory->readValue(0);
	int skolkoidet = memory->readValue(1);
	if (skolkoidet == 0)
	{
		skolkoidet = dolgohodit;
		direction = rand()%6+1;
		memory->writeValue(1,skolkoidet);
		memory->writeValue(0,direction);
	}

	int oldx = memory->readValue(2);
	int oldy = memory->readValue(3);
	int DRAP = memory->readValue(5);
	if (DRAP!=0)
		DRAP--;

    ///////////////////////////////////////////////////////////////////////////////////////////// ATAKA BOTOV
    for (size_t i = 0;i < r_num;i++) {
        IRobotView *rbt = robot->getRobotInView(i);
        if (rbt->isEnemy()) {
            robot->shoot(rbt);

			goal = rbt->getCell()->getCoord(); 
			goalType = 1; 
			goalIter = env->getIteration();
			Plotnost = 1;
			/*if (fr_num + 6 < en_num ) {
				DRAP = pobeg;
				memory->writeValue(5,DRAP);
				int Direction = robot->getDirection(goal, robot->getCell()->getCoord());
				memory->writeValue(6,Direction);
			}*/

            const Coord2D &rbt_coord = rbt->getCell()->getCoord();
            IMessage *msg = robot->getOwnMessage();
            msg->writeValue(0, rbt_coord.getX());
            msg->writeValue(1, rbt_coord.getY());
            msg->writeValue(2, env->getIteration());
			msg->writeValue(3, 1);
            robot->transmit();
        }
    }


	///////////////////////////////////////////////////////////////////////////////////////////// ATAKA BAZ
    for (size_t i = 0;i < b_num;i++) { 
        IBaseView *base = robot->getBaseInView(i);

        if (base->isEnemy()) {
            robot->shoot(base);
            if (fr_num > 2){
				goal = base->getCell()->getCoord(); 
				goalType = 2; 
				goalIter = env->getIteration();
				Plotnost = 1;
			}else{
				int x = robot->getCell()->getCoord().getX();
				int y = robot->getCell()->getCoord().getY();
				Coord2D buf(x,y);
				goal = buf;
			}

            const Coord2D &base_coord = base->getCell()->getCoord();
            IMessage *msg = robot->getOwnMessage();
            msg->writeValue(0, base_coord.getX());
            msg->writeValue(1, base_coord.getY());
            msg->writeValue(2, env->getIteration());
			msg->writeValue(3, 2);
            robot->transmit();
        }

		if (!base->isActive()) {
            robot->shoot(base);
			goal = base->getCell()->getCoord(); 
			goalType = 2; 
			goalIter = env->getIteration();
			Plotnost = 1;

            const Coord2D &base_coord = base->getCell()->getCoord();
            IMessage *msg = robot->getOwnMessage();
            msg->writeValue(0, base_coord.getX());
            msg->writeValue(1, base_coord.getY());
            msg->writeValue(2, env->getIteration());
			msg->writeValue(3, 2);
            robot->transmit();
        }
    }


	///////////////////////////////////////////////////////////////////////////////////////////// MESSAGE
	for (int i = 0; /*(iter>dedline1 || fr_num>1) && */!robot->isShooting() && i<m_num; i++)
	{
        IMessage *msg = robot->getMessage(i);
        int tx = msg->readValue(0);
        int ty = msg->readValue(1);
        int iter = msg->readValue(2);
		int type = msg->readValue(3);

		if (!DRAP)
		{
			if (type > goalType)
			{
				Coord2D buf(tx,ty);
				goal = buf;
				goalType = type;
			}
			if (type == goalType && iter > goalIter)
			{
				Coord2D buf(tx,ty);
				goal = buf;
				goalType = type;
			}
		}else{
			if (type == 2 && iter > goalIter)
			{
				Coord2D buf(tx,ty);
				goal = buf;
				goalType = type;
			}
		}

        if (!robot->isTransmitting() && ((env->getIteration() - iter) < pamat))
            robot->transmit(msg);
    }


    ///////////////////////////////////////////////////////////////////////////////////////////// MOVE
	if (goalType==0)
	{
		int faza;
		int gran1;
		int gran2;
		int gran3;
		int gran4;

		faza = iter%dedline4;
		gran1 = dedline1;
		gran2 = dedline2;
		gran3 = dedline3;
		gran4 = dedline4;

		if (iter<gran1)
		{
			robot->move(rand()%7);
		}else{
			if (faza<gran2)
			{
				Coord2D rez (maxx/2,maxy/2);
				int bufx = rez.getX() + rand()%(razbros+6+1)-(razbros+6)/2;
				int bufy = rez.getY() + rand()%(razbros+6+1)-(razbros+6)/2;
				Coord2D rez1(bufx,bufy);
				MoveRobot(robot,rez1,oldx,oldy);
			}
			if (faza>=gran2 && faza<gran3)
			{
				if (oldx == robot->getCell()->getCoord().getX() && oldy == robot->getCell()->getCoord().getY())
				{
					skolkoidet = dolgohodit;
					direction = rand()%6+1;
					memory->writeValue(1,skolkoidet);
					memory->writeValue(0,direction);
				}

				bool flag = !robot->move(direction);
				for (int i=0; flag && i<40; i++)
				{
					skolkoidet = dolgohodit;
					int direction1 = rand()%6+1;
					if (direction1!=direction)
					{
						memory->writeValue(1,skolkoidet);
						memory->writeValue(0,direction1);

						flag = !robot->move(direction1);
					}
				}
				memory->writeValue(1,skolkoidet-1);
			}
			if (faza>=gran3 && faza<gran4)
			{
				Coord2D rez = findPoint(iter,maxx,maxy);
				int bufx = rez.getX() + rand()%(razbros+1)-razbros/2;
				int bufy = rez.getY() + rand()%(razbros+1)-razbros/2;
				Coord2D rez1(bufx,bufy);
				MoveRobot(robot,rez1,oldx,oldy);
			}
		}
	}else{
		if (Plotnost){
			robot->move(goal);
		}else{
			int x = goal.getX() + rand()%(razbros+1)-razbros/2;
			int y = goal.getY() + rand()%(razbros+1)-razbros/2;
			Coord2D goalRand(x,y);
			MoveRobot(robot,goalRand,oldx,oldy);
		}

		if (oldx == robot->getCell()->getCoord().getX() && oldy == robot->getCell()->getCoord().getY())
			MoveRobot(robot,goal,oldx,oldy);
	}

	memory->writeValue(2,robot->getCell()->getCoord().getX());
	memory->writeValue(3,robot->getCell()->getCoord().getX());
}