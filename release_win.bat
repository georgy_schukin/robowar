@echo off

set RELEASE_DIR="RoboWar"
set GAME_DIR=game
set CUR_DIR="%cd%"
set DOC_DIR=doc\\interface
set VERSION=%1

if "%VERSION%"=="" (
	set DEST=robowar_win32
) else (
	set DEST=robowar_v%VERSION%_win32
)

rd /s /q %RELEASE_DIR%
md %RELEASE_DIR%

copy %GAME_DIR%\\RoboWar2D.exe %RELEASE_DIR% > nul
copy %GAME_DIR%\\game.txt %RELEASE_DIR% > nul
copy %GAME_DIR%\\config.txt %RELEASE_DIR% > nul
robocopy %GAME_DIR%\\Bots %RELEASE_DIR%\\Bots\\ /s > nul

cd %DOC_DIR%
rd /s /q html
rd /s /q latex
doxygen interface.dox > nul
cd latex
call make.bat > nul
cd %CUR_DIR%
copy %DOC_DIR%\\latex\\refman.pdf %RELEASE_DIR%\\ReferenceGuide.pdf > nul

windeployqt %RELEASE_DIR% > nul

winrar a -afzip -m5 %DEST%.zip %RELEASE_DIR%

rd /s /q %RELEASE_DIR%


