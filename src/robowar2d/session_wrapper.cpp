#include "session_wrapper.h"
#include "engine/game/game_engine.h"
#include "engine/game/game_session.h"
#include "engine/state/states.h"

#include <chrono>

namespace robowar2d {

SessionWrapper::SessionWrapper(robowar::GameEngine *engine) :
    engine(engine)
{
    session = engine->newSession();
    session->addListener(this);
}

SessionWrapper::~SessionWrapper() {    
    if (session) {
        engine->deleteSession(session);
    }
}

void SessionWrapper::loadFromFile(const std::string &filename) {
    session->loadGameFromFile(filename);
}

void SessionWrapper::start() {
    session->run();    
}

void SessionWrapper::stop() {
    session->stop();
}

void SessionWrapper::handleInit(const robowar::GameInitState &init) {    
    emit gameStarted(init);
}

void SessionWrapper::handleUpdate(const robowar::GameState &state) {
    emit gameUpdated(state);
}

void SessionWrapper::handleFinish() {

}

}
