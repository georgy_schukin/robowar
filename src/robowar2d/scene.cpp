#include "scene.h"
#include "items/game_field.h"
#include "items/base_item.h"
#include "items/robot_item.h"
#include "engine/state/states.h"

#include <QGraphicsView>
#include <QGraphicsScene>
#include <cassert>
#include <cmath>
#include <set>

namespace robowar2d {

Scene::Scene(QGraphicsView *view) :
    view(view)
{
    scene = std::make_unique<QGraphicsScene>();    
    view->setScene(scene.get());
    cell_size = 40.0f;
}

Scene::~Scene() {
}

void Scene::init(const robowar::GameInitState &state) {
    initField(state);
    initTeams(state);
    view->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
}

void Scene::update(const robowar::GameState &state) {
    updateBases(state);
    updateRobots(state);
}

void Scene::initField(const robowar::GameInitState &state) {
    game_field = std::make_unique<GameField>(scene.get(), state.getCells(), cell_size*0.5f);
}

void Scene::initTeams(const robowar::GameInitState &state) {
    const std::vector<QColor> colors = {Qt::red, Qt::green, Qt::yellow, Qt::blue, Qt::magenta, Qt::cyan,
                         Qt::darkRed, Qt::darkGreen, Qt::darkYellow, Qt::darkBlue, Qt::darkMagenta, Qt::darkCyan};
    int pos = 0;
    for (const auto &team: state.getTeams()) {
        team_colors[team.getId()] = colors[pos % colors.size()];
        pos++;
    }
    for (const auto &team: state.getTeams()) {
        team_names[team.getId()] = QString(team.getName().c_str());
    }
}

void Scene::updateBases(const robowar::GameState &state) {
    for (const auto &data: state.getBases()) {
        auto *base = getBase(data.getId());
        base->update(data);
    }
}

void Scene::updateRobots(const robowar::GameState &state) {    
    for (const auto &p: robots) {
        p.second->removeFromScene();
    }
    for (const auto &data: state.getRobots()) {
        auto *robot = getRobot(data.getId());
        robot->addToScene();
        robot->update(data);
    }
}

Item* Scene::getRobot(int id) {
    auto it = robots.find(id);
    if (it == robots.end()) {
        auto robot = createRobot();
        robots[id] = robot;
        return robot.get();
    }
    return it->second.get();
}

Item* Scene::getBase(int id) {
    auto it = bases.find(id);
    if (it == bases.end()) {
        auto base = createBase();
        bases[id] = base;
        return base.get();
    }
    return it->second.get();
}

ItemPtr Scene::createBase() {
    return std::make_shared<BaseItem>(scene.get(), this, (cell_size/std::sqrt(2))*0.9f);
}

ItemPtr Scene::createRobot() {
    return std::make_shared<RobotItem>(scene.get(), this, cell_size*0.5f);
}

QColor Scene::getTeamColor(int team_id) const {
    static const QColor neutral_color(Qt::white);
    auto it = team_colors.find(team_id);
    return (it != team_colors.end() ? it->second : neutral_color);
}

QString Scene::getTeamName(int team_id) const {
    auto it = team_names.find(team_id);
    return (it != team_names.end() ? it->second : "None");
}

QPointF Scene::getPoint(const robowar::Coord &coord) const {
    assert (game_field);
    return game_field->getPoint(coord);
}

std::vector<int> Scene::getTeamIds() const {
    std::vector<int> result;
    for (const auto &p: team_names) {
        result.push_back(p.first);
    }
    return result;
}

}
