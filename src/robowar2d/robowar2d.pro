#-------------------------------------------------
#
# Project created by QtCreator 2017-06-17T12:12:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
CONFIG += c++14
TARGET = RoboWar2D

DESTDIR = "../../game"

exists(../Local.pri) {
    include(../Local.pri)
}

SOURCES += main.cpp\
        mainwindow.cpp \
    scene.cpp \
    items/game_field.cpp \
    session_wrapper.cpp \
    my_graphics_view.cpp \
    items/base_item.cpp \
    items/robot_item.cpp \
    items/item.cpp \
    items/active_item.cpp \
    player.cpp

HEADERS  += mainwindow.h \
    scene.h \
    items/game_field.h \
    session_wrapper.h \
    my_graphics_view.h \
    items/base_item.h \
    items/environment.h \
    items/robot_item.h \
    items/item.h \
    items/active_item.h \
    player.h

FORMS    += mainwindow.ui

include(../RoboWar_lib.pri)
include(../version.pri)

VERSION = $${VERSION_MAJOR}.$${VERSION_MINOR}.$${VERSION_BUILD}

DEFINES += VERSION_STR=\\\"$$VERSION\\\"


