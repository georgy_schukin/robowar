#pragma once

#include <QGraphicsView>

class MyGraphicsView : public QGraphicsView {
    Q_OBJECT
public:
    MyGraphicsView(QWidget *parent=0);

    void zoomIn(const double &factor);
    void zoomOut(const double &factor);
    void discardZoom();

protected:
    virtual void wheelEvent(QWheelEvent *event);
    //virtual void mousePressEvent(QMouseEvent *event);
    //virtual void mouseReleaseEvent(QMouseEvent *event);
};
