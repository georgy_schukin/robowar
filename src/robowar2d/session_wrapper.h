#pragma once

#include "engine/state/game_listener.h"

#include <QObject>
#include <QMetaType>

namespace robowar {
    class GameEngine;
    class GameSession;    
    class GameInitState;
    class GameState;
}

namespace robowar2d {

class SessionWrapper : public QObject, public robowar::GameListener {
    Q_OBJECT
public:
    SessionWrapper(robowar::GameEngine *engine);
    ~SessionWrapper();

    void loadFromFile(const std::string &filename);

    void start();
    void stop();

signals:
    void gameStarted(const robowar::GameInitState &state);
    void gameUpdated(const robowar::GameState &state);

private:
    void handleInit(const robowar::GameInitState &init);
    void handleUpdate(const robowar::GameState &state);
    void handleFinish();

private:
    robowar::GameEngine *engine;
    robowar::GameSession *session;            
};

}
