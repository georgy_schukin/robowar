#pragma once

#include "items/environment.h"

#include <memory>
#include <vector>
#include <map>

class QGraphicsView;
class QGraphicsScene;

namespace robowar {
    class GameState;
    class GameInitState;    
}

namespace robowar2d {

class GameField;
class Item;
typedef std::shared_ptr<Item> ItemPtr;

class Scene : public Environment {
public:
    Scene(QGraphicsView *view);
    virtual ~Scene();

    void init(const robowar::GameInitState &state);
    void update(const robowar::GameState &state);

    virtual QColor getTeamColor(int team_id) const;
    virtual QString getTeamName(int team_id) const;
    virtual QPointF getPoint(const robowar::Coord &coord) const;

    std::vector<int> getTeamIds() const;

private:
    void initField(const robowar::GameInitState &state);
    void initTeams(const robowar::GameInitState &state);

    void updateBases(const robowar::GameState &state);
    void updateRobots(const robowar::GameState &state);

    Item* getRobot(int id);
    Item* getBase(int id);

    ItemPtr createBase();
    ItemPtr createRobot();

private:
    QGraphicsView *view;
    std::unique_ptr<QGraphicsScene> scene;
    std::unique_ptr<GameField> game_field;
    std::map<int, QColor> team_colors;
    std::map<int, QString> team_names;
    std::map<int, ItemPtr> bases;
    std::map<int, ItemPtr> robots;
    float cell_size;
};

}
