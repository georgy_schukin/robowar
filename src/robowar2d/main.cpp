#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);    
    a.setOrganizationName("Supercomputer Software Department");
    a.setOrganizationDomain("ssd.sscc.ru");
    a.setApplicationName("RoboWar");
    a.setApplicationVersion(VERSION_STR);

    MainWindow w;
    w.show();

    return a.exec();
}
