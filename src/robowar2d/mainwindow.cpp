#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "session_wrapper.h"
#include "scene.h"
#include "player.h"
#include "engine/game/game_engine.h"
#include "engine/state/states.h"

#include <QMessageBox>
#include <QSettings>
#include <QFileDialog>
#include <QDir>
#include <QSlider>
#include <QLabel>

namespace {
    static const QString LAST_OPEN_DIR = "last_open_dir";
    static const QString LAST_SAVE_DIR = "last_save_dir";

    QString getOption(const QString &key) {
        QSettings settings;
        return settings.value(key).toString();
    }

    void setOption(const QString &key, const QString &value) {
        QSettings settings;
        settings.setValue(key, value);
    }

    QString getAppName() {
        return QString("%1 v%2").arg(qApp->applicationName()).arg(qApp->applicationVersion());
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    session(nullptr)
{
    qRegisterMetaType<robowar::GameInitState>();
    qRegisterMetaType<robowar::GameState>();

    ui->setupUi(this);
    ui->graphicsView->setMouseTracking(true);
    ui->graphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
    ui->graphicsView->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);

    frame_slider = createFrameSlider();
    fps_slider = createFPSSlider();

    auto *fps_label = new QLabel("FPS:", this);
    fps_label->setMargin(5);

    ui->toolBar->addWidget(frame_slider);
    ui->toolBar->addWidget(fps_label);
    ui->toolBar->addWidget(fps_slider);

    stat_label = new QLabel(this);
    stat_label->setTextFormat(Qt::RichText);
    stat_label->setStyleSheet("padding: 0px 0px 5px 5px");
    ui->statusBar->addWidget(stat_label);

    engine = std::make_unique<robowar::GameEngine>();

    setWindowTitle(getAppName());
}

MainWindow::~MainWindow() {
    delete ui;
}

QSlider* MainWindow::createFrameSlider() {
    auto *slider = new QSlider(Qt::Horizontal, this);
    slider->setMinimum(0);
    slider->setMaximum(10);
    connect(slider, &QSlider::sliderMoved, this, &MainWindow::changeFrame);
    return slider;
}

QSlider* MainWindow::createFPSSlider() {
    auto *slider = new QSlider(Qt::Horizontal, this);
    slider->setMinimum(1);
    slider->setMaximum(100);
    slider->setMaximumWidth(100);
    return slider;
}

void MainWindow::onGameStarted(const robowar::GameInitState &state) {
    if (scene) {
        scene->init(state);
    }
}

void MainWindow::onGameUpdateReceived(const robowar::GameState &state) {
    if (player) {
        player->newFrame(state);
    }
}

void MainWindow::onGameStateChanged(const robowar::GameState &state) {
    if (scene) {
        scene->update(state);
    }
    updateStatus(state);
    const auto iter = state.getIteration();
    const auto num_of_iters = player->getNumOfFrames();
    frame_slider->setMaximum(num_of_iters - 1);
    frame_slider->setValue(iter);
    setWindowTitle(QString("%1: iteration %2 of %3").arg(getAppName()).arg(iter).arg(num_of_iters));
}

void MainWindow::on_actionOpen_triggered() {
    const QString name_filter = "Text files (*.txt);; All files (*.*)";
    const QString last_dir = getOption(LAST_OPEN_DIR);
    const QString filename = QFileDialog::getOpenFileName(this, "Open game file", last_dir, name_filter);
    if (!filename.isNull()) {
        loadGameFromFile(filename);
        setOption(LAST_OPEN_DIR, QDir().absoluteFilePath(filename));
        last_filename = filename;
    }
}

void MainWindow::on_actionRestart_triggered() {
    if (!last_filename.isEmpty()) {
        loadGameFromFile(last_filename);
    }
}

void MainWindow::on_actionStop_triggered() {
    if (session) {
        session->stop();
    }
}

void MainWindow::loadGameFromFile(const QString &filename) {
    try {
        scene = std::make_unique<robowar2d::Scene>(ui->graphicsView);
        player = newPlayer();
        session = newSession();
        session->loadFromFile(filename.toStdString());
        session->start();
        player->start();
    }
    catch (const std::exception &e) {
        QMessageBox::critical(this, "Error", QString(e.what()));
    }
}

std::unique_ptr<robowar2d::SessionWrapper> MainWindow::newSession() {
    auto session = std::make_unique<robowar2d::SessionWrapper>(engine.get());
    connect(session.get(), &robowar2d::SessionWrapper::gameStarted, this, &MainWindow::onGameStarted);
    connect(session.get(), &robowar2d::SessionWrapper::gameUpdated, this, &MainWindow::onGameUpdateReceived);
    return session;
}

std::unique_ptr<robowar2d::Player> MainWindow::newPlayer() {
    auto player = std::make_unique<robowar2d::Player>();
    connect(player.get(), &robowar2d::Player::frameChanged, this, &MainWindow::onGameStateChanged);
    connect(fps_slider, &QSlider::sliderMoved, player.get(), &robowar2d::Player::setFPS);
    return player;
}

void MainWindow::on_actionExit_triggered() {
    qApp->exit();
}

void MainWindow::on_actionAbout_triggered() {
    QMessageBox about(this);
    about.setWindowTitle("About RoboWar");
    about.setIcon(QMessageBox::Information);
    about.setStandardButtons(QMessageBox::Ok);
    about.setTextFormat(Qt::RichText);
    about.setText(QString("RoboWar v%1<br><br>").arg(qApp->applicationVersion()) +
                  QString("Programmed by Georgy Schukin<br>") +
                  QString("<a href=\"mailto:schukin@ssd.sscc.ru\">schukin@ssd.sscc.ru</a><br><br>") +
                  QString("<a href=\"http://ssd.sscc.ru/robowar\">RoboWar site</a>"));
    about.exec();
}

void MainWindow::on_actionPlay_triggered() {
    if (player) {
        player->start();
    }
}

void MainWindow::on_actionPause_triggered() {
    if (player) {
        player->pause();
    }
}

void MainWindow::on_actionNext_triggered() {
    if (player) {
        player->next();
    }
}

void MainWindow::on_actionPrev_triggered() {
    if (player) {
        player->prev();
    }
}

void MainWindow::changeFrame(int index) {
    if (player) {
        player->setCurrentFrame(index);
    }
}

void MainWindow::updateStatus(const robowar::GameState &state) {
    QString text = "<table><tr>";
    int p = 0;
    const auto team_ids = scene->getTeamIds();
    for (const int team_id: team_ids) {
        const auto team_name = scene->getTeamName(team_id);
        const auto team_color = scene->getTeamColor(team_id);
        const auto r_num = getNumOfRobotsForTeam(state, team_id);
        const auto b_num = getNumOfBasesForTeam(state, team_id);
        text += QString("<td style=\"%1\">%2 : robots: %3, bases: %4</td>").
                arg(QString("background-color: %1; padding: 3").arg(team_color.name(QColor::HexRgb))).
                arg(team_name).
                arg(r_num).
                arg(b_num);
        p++;
        if (p % 3 == 0 && p < (int)team_ids.size()) {
            text += "</tr><tr>";
        }
    }
    text += "</tr></table>";
    stat_label->setText(text);
}

int MainWindow::getNumOfRobotsForTeam(const robowar::GameState &state, int team_id) {
    int num = 0;
    for (const auto &robot: state.getRobots()) {
        if (robot.getTeamId() == team_id) {
            num++;
        }
    }
    return num;
}

int MainWindow::getNumOfBasesForTeam(const robowar::GameState &state, int team_id) {
    int num = 0;
    for (const auto &base: state.getBases()) {
        if (base.getTeamId() == team_id) {
            num++;
        }
    }
    return num;
}

