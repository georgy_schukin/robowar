#pragma once

#include "engine/state/game_state.h"

#include <vector>
#include <mutex>
#include <atomic>
#include <memory>
#include <QObject>

class QTimer;

namespace robowar2d {

class Player : public QObject {
    Q_OBJECT
public:
    Player();
    ~Player();

    int getNumOfFrames() const;

signals:
    void frameChanged(const robowar::GameState &state);

public slots:
    void setDelay(int msec);
    void setFPS(int fps);
    void setCurrentFrame(int index);
    void start();
    void pause();
    void next();
    void prev();

    void newFrame(const robowar::GameState &state);

private:
    void emitFrame();

    int getCurrentFrameIndex() const;
    void setCurrentFrameIndex(int index);
    void shiftCurrentFrameIndex(int shift);

    const robowar::GameState& getFrame(int index) const;

private:
    std::vector<robowar::GameState> frames;
    std::atomic<bool> is_playing;
    int current_index;
    int delay_msec;
    mutable std::mutex mutex;
    std::unique_ptr<QTimer> timer;
};

}
