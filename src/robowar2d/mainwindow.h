#pragma once

#include "engine/state/game_state.h"
#include "engine/state/game_init_state.h"

#include <QMainWindow>
#include <QString>
#include <QLabel>
#include <memory>

namespace Ui {
    class MainWindow;
}

namespace robowar {
    class GameEngine;
}

namespace robowar2d {
    class SessionWrapper;
    class Player;
    class Scene;
}

class QSlider;

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();    

public slots:
    void onGameStarted(const robowar::GameInitState &state);
    void onGameUpdateReceived(const robowar::GameState &state);
    void onGameStateChanged(const robowar::GameState &state);

private slots:
    void on_actionOpen_triggered();
    void on_actionExit_triggered();
    void on_actionAbout_triggered();
    void on_actionRestart_triggered();
    void on_actionStop_triggered();

    void on_actionPlay_triggered();
    void on_actionPause_triggered();
    void on_actionNext_triggered();
    void on_actionPrev_triggered();

    void changeFrame(int index);    

private:
    void loadGameFromFile(const QString &filename);
    std::unique_ptr<robowar2d::SessionWrapper> newSession();
    std::unique_ptr<robowar2d::Player> newPlayer();

    QSlider* createFrameSlider();
    QSlider* createFPSSlider();

    void updateStatus(const robowar::GameState &state);
    int getNumOfRobotsForTeam(const robowar::GameState &state, int team_id);
    int getNumOfBasesForTeam(const robowar::GameState &state, int team_id);

private:
    Ui::MainWindow *ui;
    std::unique_ptr<robowar::GameEngine> engine;
    std::unique_ptr<robowar2d::SessionWrapper> session;
    std::unique_ptr<robowar2d::Scene> scene;
    std::unique_ptr<robowar2d::Player> player;

    QSlider *frame_slider;
    QSlider *fps_slider;
    QLabel *stat_label;

    QString last_filename;
};

Q_DECLARE_METATYPE(robowar::GameInitState)
Q_DECLARE_METATYPE(robowar::GameState)
