#include "base_item.h"
#include "environment.h"
#include "engine/state/states.h"

#include <QGraphicsItem>
#include <QGraphicsScene>

namespace robowar2d {

BaseItem::BaseItem(QGraphicsScene *scene, Environment *env, float size) :
    ActiveItem(scene, env),
    size(size) {
}

QGraphicsItem* BaseItem::createMainItem() {
    QPen pen(Qt::black);
    pen.setWidth(2);
    return getScene()->addRect(QRectF(-size/2, -size/2, size, size), pen, QBrush(Qt::white));
}

QString BaseItem::getTooltip(const robowar::ActiveGameObjectState &data) const {
    auto result = Item::getTooltip(data);
    auto *bd = dynamic_cast<const robowar::BaseState*>(&data);
    if (bd) {
        result += "\n";
        result += QString("Attack team: %1").arg(getEnvironment()->getTeamName(bd->getAttackTeamId()));
    }
    return result;
}

}
