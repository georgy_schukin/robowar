#pragma once

#include "engine/game/coords.h"

#include <QColor>
#include <QPointF>
#include <QString>

namespace robowar2d {

class Environment {
public:
    virtual ~Environment() {}

    virtual QColor getTeamColor(int team_id) const = 0;
    virtual QString getTeamName(int team_id) const = 0;
    virtual QPointF getPoint(const robowar::Coord &coord) const = 0;    
};

}
