#pragma once

#include "active_item.h"

class QGraphicsPolygonItem;

namespace robowar2d {

class RobotItem : public ActiveItem {
public:
    RobotItem(QGraphicsScene *scene, Environment *env, float size);                

    virtual void update(const robowar::ActiveGameObjectState &data);

protected:
    virtual QGraphicsItem* createItem();
    virtual QGraphicsItem* createMainItem();
    virtual QString getTooltip(const robowar::ActiveGameObjectState &data) const;    

private:    
    float size;
    QGraphicsPolygonItem *bullet;
};

}
