#pragma once

#include "active_item.h"
#include "engine/state/base_state.h"

namespace robowar2d {

class BaseItem : public ActiveItem {
public:
    BaseItem(QGraphicsScene *scene, Environment *env, float size);        

protected:
    virtual QGraphicsItem* createMainItem();
    virtual QString getTooltip(const robowar::ActiveGameObjectState &data) const;

private:    
    float size;        
};

}
