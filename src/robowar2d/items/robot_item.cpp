#include "robot_item.h"
#include "environment.h"
#include "engine/state/states.h"

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsPolygonItem>
#include <QVector2D>

namespace robowar2d {

RobotItem::RobotItem(QGraphicsScene *scene, Environment *env, float size) :
    ActiveItem(scene, env),
    size(size) {
}

void RobotItem::update(const robowar::ActiveGameObjectState &data) {
    ActiveItem::update(data);
    const auto *rd = dynamic_cast<const robowar::RobotState*>(&data);
    if (rd) {
        bullet->setVisible(rd->isShooting());
        if (rd->isShooting()) {
            const QPointF start(0, 0);
            const QPointF end = getEnvironment()->getPoint(rd->getShootCoord()) - getEnvironment()->getPoint(rd->getCoord());
            QVector2D ortho_v = QVector2D(end.y(), -end.x()).normalized();
            QPointF ortho(ortho_v.x(), ortho_v.y());
            const float arrow_width = 3.0f;
            QVector<QPointF> points = {start + arrow_width*ortho, end, start - arrow_width*ortho};
            bullet->setPolygon(QPolygonF(points));
            bullet->setBrush(QBrush(getEnvironment()->getTeamColor(rd->getTeamId())));
        }
    }
}

QGraphicsItem* RobotItem::createItem() {
    QGraphicsItem *itm = ActiveItem::createItem();
    bullet = new QGraphicsPolygonItem(itm);
    bullet->setPen(QPen(Qt::black));
    return itm;
}

QGraphicsItem* RobotItem::createMainItem() {
    return getScene()->addEllipse(QRectF(-size/2, -size/2, size, size));
}

QString RobotItem::getTooltip(const robowar::ActiveGameObjectState &data) const {
    auto result = Item::getTooltip(data);
    const auto *rd = dynamic_cast<const robowar::RobotState*>(&data);
    if (rd) {
        result += "\n";        
        if (rd->isMoving()) {
            result += QString("Move coord: %1\n").arg(rd->getMoveCoord().toString().c_str());
        }        
        if (rd->isShooting()) {
            result += QString("Shoot coord: %1\n").arg(rd->getShootCoord().toString().c_str());
        }
        result += QString("Is transmitting: %1\n").arg(rd->isTransmitting() ? "yes" : "no");
        result += QString("View distance: %1\n").arg(rd->getViewDistance());
        result += QString("Shoot distance: %1\n").arg(rd->getShootDistance());
        result += QString("Transmit distance: %1").arg(rd->getTransmitDistance());
    }
    return result;
}

}
