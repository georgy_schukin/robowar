#include "item.h"
#include "environment.h"
#include "engine/state/active_game_object_state.h"

#include <QGraphicsItem>
#include <QGraphicsScene>

namespace robowar2d {

Item::Item(QGraphicsScene *scene, Environment *env) :
    scene(scene), env(env), item(nullptr) {
}

Item::~Item() {
    removeFromScene();
    if (item) {
        delete item;
    }
}

void Item::addToScene() {
    if (item && (getScene() != item->scene())) {
        getScene()->addItem(item);
    }
}

void Item::removeFromScene() {
    if (item && (getScene() == item->scene())) {
        getScene()->removeItem(item);
    }
}

void Item::update(const robowar::ActiveGameObjectState &data) {
    if (!getItem()) {
        setItem(createItem());
    }    
    getItem()->setPos(getEnvironment()->getPoint(data.getCoord()));
    getItem()->setToolTip(getTooltip(data));
    auto *shape_item = dynamic_cast<QAbstractGraphicsShapeItem*>(getItem());
    if (shape_item) {
        shape_item->setBrush(QBrush(getEnvironment()->getTeamColor(data.getTeamId())));
    }
}

QString Item::getTooltip(const robowar::ActiveGameObjectState &data) const {
    QString result = "";
    result += QString("Id: %1\n").arg(data.getId());
    result += QString("Team: %1\n").arg(getEnvironment()->getTeamName(data.getTeamId()));
    result += QString("Coord: %1\n").arg(data.getCoord().toString().c_str());
    result += QString("Hitpoints: %1\n").arg(data.getHitpoints());
    result += QString("Max hitpoints: %1\n").arg(data.getMaxHitpoints());
    result += QString("Damage: %1\n").arg(data.getDamage());
    result += QString("Cooldown: %1\n").arg(data.getCooldown());
    result += QString("Max cooldown: %1").arg(data.getMaxCooldown());
    return result;
}

}
