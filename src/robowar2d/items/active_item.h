#pragma once

#include "item.h"

#include <QPointF>

class QGraphicsRectItem;

namespace robowar2d {

class ActiveItem : public Item {
public:
    ActiveItem(QGraphicsScene *scene, Environment *env);

    virtual void update(const robowar::ActiveGameObjectState &data);

protected:
    virtual QGraphicsItem* createItem();
    virtual QGraphicsItem* createMainItem() = 0;

    void updateLifeBar(const robowar::ActiveGameObjectState &data);
    void updateCooldownBar(const robowar::ActiveGameObjectState &data);

    virtual QPointF getLifeBarPoint() const;
    virtual QPointF getCooldownBarPoint() const;
    virtual float getBarWidth() const;
    virtual float getBarHeight() const;

    QGraphicsItem* getMainItem() const {
        return main_item;
    }

private:
    QGraphicsItem *main_item;
    QGraphicsRectItem *lifebar;
    QGraphicsRectItem *cooldown;
};

}
