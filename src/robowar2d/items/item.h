#pragma once

#include <QString>

class QGraphicsItem;
class QGraphicsScene;

namespace robowar {
    class ActiveGameObjectState;
}

namespace robowar2d {

class Environment;

class Item {
public:
    Item(QGraphicsScene *scene, Environment *env);
    virtual ~Item();

    virtual void update(const robowar::ActiveGameObjectState &data);

    void addToScene();
    void removeFromScene();

protected:
    QGraphicsScene* getScene() {
        return scene;
    }

    Environment* getEnvironment() const {
        return env;
    }

    void setItem(QGraphicsItem *item) {
        this->item = item;
    }

    QGraphicsItem* getItem() const {
        return item;
    }

    virtual QGraphicsItem* createItem() = 0;
    virtual QString getTooltip(const robowar::ActiveGameObjectState &data) const;

private:
    QGraphicsScene *scene;
    Environment *env;
    QGraphicsItem *item;
};

}
