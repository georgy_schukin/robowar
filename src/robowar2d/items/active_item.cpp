#include "active_item.h"
#include "environment.h"
#include "engine/state/states.h"

#include <QGraphicsItem>
#include <QGraphicsScene>

namespace robowar2d {

ActiveItem::ActiveItem(QGraphicsScene *scene, Environment *env) :
    Item(scene, env) {
}

void ActiveItem::update(const robowar::ActiveGameObjectState &data) {
    Item::update(data);
    updateLifeBar(data);
    updateCooldownBar(data);
}

void ActiveItem::updateLifeBar(const robowar::ActiveGameObjectState &data) {
    const float hp = float(data.getHitpoints())/data.getMaxHitpoints();
    const auto hp_width = getBarWidth()*hp;
    const auto hp_height = getBarHeight();
    lifebar->setRect(QRectF(getLifeBarPoint(), QSizeF(hp_width, hp_height)));
    static const QBrush high_hp(Qt::green);
    static const QBrush middle_hp(Qt::yellow);
    static const QBrush low_hp(Qt::red);
    if (hp > 0.7) {
        lifebar->setBrush(high_hp);
    } else if (hp > 0.3) {
        lifebar->setBrush(middle_hp);
    } else {
        lifebar->setBrush(low_hp);
    }
}

void ActiveItem::updateCooldownBar(const robowar::ActiveGameObjectState &data) {
    const float cd = float(data.getCooldown())/data.getMaxCooldown();
    const auto cd_width = getBarWidth()*cd;
    const auto cd_height = getBarHeight();
    cooldown->setRect(QRectF(getCooldownBarPoint(), QSizeF(cd_width, cd_height)));
    cooldown->setVisible(data.getCooldown() > 0);
}

QGraphicsItem* ActiveItem::createItem() {
    main_item = createMainItem();
    lifebar = new QGraphicsRectItem(main_item);
    lifebar->setPen(QPen(Qt::black));
    cooldown = new QGraphicsRectItem(main_item);
    cooldown->setPen(QPen(Qt::black));
    cooldown->setBrush(QBrush(Qt::white));
    //lifebar->setFlag(QGraphicsItem::ItemIgnoresTransformations);
    //cooldown->setFlag(QGraphicsItem::ItemIgnoresTransformations);
    return main_item;
}

QPointF ActiveItem::getLifeBarPoint() const {
    const auto tl = getMainItem()->boundingRect().topLeft();
    return QPointF(tl.x(), tl.y() - getBarHeight()*2 - 4.0f);
}

QPointF ActiveItem::getCooldownBarPoint() const {
    const auto tl = getMainItem()->boundingRect().topLeft();
    return QPointF(tl.x(), tl.y() - getBarHeight() - 2.0f);
}

float ActiveItem::getBarWidth() const {
    return getMainItem()->boundingRect().width();
}

float ActiveItem::getBarHeight() const {
    return 5.0f;
}

}
