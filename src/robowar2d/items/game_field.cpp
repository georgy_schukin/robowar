#include "game_field.h"

#include <QGraphicsScene>
#include <QVector>
#include <QGraphicsPolygonItem>
#include <QPolygonF>
#include <algorithm>
#include <cmath>

namespace robowar2d {

namespace {
    QVector<QPointF> getHexVertices(float hex_radius) {
        QVector<QPointF> vertices;
        const float PI = 3.14159;
        for (size_t i = 0; i < 360; i += 60) {
            vertices.push_back(QPointF(hex_radius*std::sin(float(i)*PI/180.0f),
                                       hex_radius*std::cos(float(i)*PI/180.0f)));
        }
        return vertices;
    }
}

GameField::GameField(QGraphicsScene *scene, const std::vector<robowar::Coord> &coords, float hex_radius) :
    scene(scene), coords(coords), hex_radius(hex_radius)
{
    initPoints();
    addCells();
}

GameField::~GameField() {
    for (auto *item: items) {
        scene->removeItem(item);
        delete item;
    }
}

void GameField::initPoints() {
    for (const auto &coord: coords) {
        points.push_back(coordToPoint(coord, getHexRadius()*1.15f));
    }
}

void GameField::addCells() {
    QPolygonF hex(getHexVertices(getHexRadius()));
    QPen pen(QColor(78, 99, 93));
    QBrush brush(QColor(179, 226, 214));
    int pos = 0;
    for (const auto &point: points) {
        auto *cell = scene->addPolygon(hex, pen, brush);
        cell->setPos(point);
        cell->setToolTip(coords[pos].toString().c_str());
        items.push_back(cell);
        pos++;
    }
}

QPointF GameField::coordToPoint(const robowar::Coord &coord, float size) const {
    return QPointF(size * std::sqrt(3.0f) * (coord.getCol() + coord.getRow()*0.5f),
                   size * 1.5f * coord.getRow());
}

QPointF GameField::getPoint(const robowar::Coord &coord) const {
    auto it = std::find(coords.begin(), coords.end(), coord);
    if (it == coords.end()) {
        return QPointF(0, 0);
    }
    return points.at(std::distance(coords.begin(), it));
}

}
