#pragma once

#include "engine/game/coords.h"

#include <vector>
#include <QPointF>

class QGraphicsScene;
class QGraphicsItem;

namespace robowar2d {

class GameField {
public:
    GameField(QGraphicsScene *scene, const std::vector<robowar::Coord> &coords, float hex_radius=10.0f);
    ~GameField();

    QPointF getPoint(const robowar::Coord &coord) const;

    float getHexRadius() const {
        return hex_radius;
    }

private:
    void initPoints();
    void addCells();
    QPointF coordToPoint(const robowar::Coord &coord, float size) const;

private:
    QGraphicsScene *scene;
    std::vector<robowar::Coord> coords;
    float hex_radius;
    std::vector<QPointF> points;
    std::vector<QGraphicsItem*> items;
};

}
