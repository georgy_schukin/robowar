#include "player.h"

#include <QTimer>

namespace robowar2d {

Player::Player() :
    is_playing(false), current_index(0), delay_msec(500)
{
    timer = std::make_unique<QTimer>(this);
    timer->setTimerType(Qt::PreciseTimer);
    timer->setInterval(delay_msec);
    connect(timer.get(), &QTimer::timeout, this, &Player::next);
}

Player::~Player() {
    is_playing = false;
    timer->stop();
}

void Player::setDelay(int msec) {
    delay_msec = msec;
    timer->setInterval(delay_msec);
}

void Player::setFPS(int fps) {
    setDelay(std::max(1, 1000/fps));
}

void Player::setCurrentFrame(int index) {
    setCurrentFrameIndex(index);
    emitFrame();
}

void Player::start() {    
    is_playing = true;
    setCurrentFrameIndex(0);
    emitFrame();
    timer->start();
}

void Player::pause() {
    if (is_playing) {
        is_playing = false;
        timer->stop();
    } else {
        is_playing = true;
        timer->start();
    }
}

void Player::next() {
    shiftCurrentFrameIndex(1);
    emitFrame();
}

void Player::prev() {
    shiftCurrentFrameIndex(-1);
    emitFrame();
}

void Player::emitFrame() {
    emit frameChanged(getFrame(getCurrentFrameIndex()));
}

int Player::getCurrentFrameIndex() const {
    std::unique_lock<std::mutex> lock(mutex);
    return current_index;
}

void Player::setCurrentFrameIndex(int index) {
    std::unique_lock<std::mutex> lock(mutex);
    current_index = std::max(0, std::min((int)frames.size() - 1, index));
}

void Player::shiftCurrentFrameIndex(int shift) {
    setCurrentFrameIndex(getCurrentFrameIndex() + shift);
}

const robowar::GameState& Player::getFrame(int index) const {
    std::unique_lock<std::mutex> lock(mutex);
    static const robowar::GameState empty;
    if (index < 0 || index >= (int)frames.size()) {
        return empty;
    }
    return frames.at(index);
}

void Player::newFrame(const robowar::GameState &state) {
    std::unique_lock<std::mutex> lock(mutex);
    frames.push_back(state);
}

int Player::getNumOfFrames() const {
    std::unique_lock<std::mutex> lock(mutex);
    return frames.size();
}

}
