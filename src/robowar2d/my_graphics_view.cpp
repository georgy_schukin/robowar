#include "my_graphics_view.h"

#include <QWheelEvent>

MyGraphicsView::MyGraphicsView(QWidget *parent) :
    QGraphicsView(parent) {
}

void MyGraphicsView::zoomIn(const double &factor) {
    scale(factor, factor);
}

void MyGraphicsView::zoomOut(const double &factor) {
    scale(1.0/factor, 1.0/factor);
}

void MyGraphicsView::discardZoom() {
    setTransform(QTransform());
}

void MyGraphicsView::wheelEvent(QWheelEvent *event) {
    const auto degrees = event->angleDelta().y() / (8*15);
    if (degrees > 0) {
        zoomIn(1.5);
    } else if (degrees < 0) {
        zoomOut(1.5);
    }
    QGraphicsView::wheelEvent(event);
}

/*void MyGraphicsView::mousePressEvent(QMouseEvent *event) {
    setDragMode(ScrollHandDrag);
    QGraphicsView::mousePressEvent(event);
}

void MyGraphicsView::mouseReleaseEvent(QMouseEvent *event) {
    setDragMode(NoDrag);
    QGraphicsView::mouseReleaseEvent(event);
}*/
