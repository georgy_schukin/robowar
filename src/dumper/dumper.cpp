#include "dumper/dumper.h"

#include <sstream>
#include <boost/filesystem.hpp>
#include "rapidjson/writer.h"
#include "rapidjson/filestream.h"

using namespace rapidjson;

#define INT(NAME,DATA) do { \
	writer.String(NAME); \
	writer.Int(DATA); \
	} while(0)

#define INT_PAIR(NAME, DATA1, DATA2) do{ \
	writer.String(NAME); \
	writer.StartArray(); \
	writer.Int(DATA1); \
	writer.Int(DATA2); \
	writer.EndArray(); \
	} while(0)

robovis::Dumper::Dumper()
{
	dump_directory = "./Dump";
	iteration = 0;
	boost::filesystem::remove_all(dump_directory);
	boost::filesystem::create_directory(dump_directory);
}

void robovis::Dumper::init(const VisualizerData &data, const Environment &env)
{
	FILE *file = fopen((dump_directory + "/init.json").c_str(),"w");
	FileStream stream(file);
	Writer<FileStream> writer(stream);

	writer.StartObject();
	#define INT_(NAME) \
		writer.String(#NAME);writer.Int(env.get##NAME());
	INT_(FieldWidth);
	INT_(FieldHeight);
	INT_(MessageSize);
	INT_(MemorySize);
	INT_(RobotHitpoints);
	INT_(BaseHitpoints);
	INT_(ReloadTime);
	INT_(RobotConstructionTime);
	INT_(ShootRange);
	INT_(ViewRange);
	INT_(TransmitRange);
	INT_(Iteration);
	INT_(Timeout);
	#undef INT_

	writer.String("teams");
	writer.StartArray();
	for(size_t i = 0; i < data.teams.size(); i++)
		writer.String(data.teams[i].name.c_str());
	writer.EndArray();

	writer.EndObject();
	fclose(file);

}

void robovis::Dumper::update(const VisualizerUpdateData &data)
{
	std::stringstream fname;
	fname << dump_directory << "/" << iteration << ".json";
	FILE *file = fopen(fname.str().c_str(),"w");
	FileStream stream(file);
	Writer<FileStream> writer(stream);

	writer.StartObject();

	writer.String("robots");
	writer.StartArray();
	for(size_t i = 0; i < data.robots.size(); i++)
	{
		const robovis::RobotData &x = data.robots[i];
		writer.StartObject();
		//INT("id", x.id);
		INT_PAIR("pos", x.x, x.y);
		if(x.is_shooting)
			INT_PAIR("shoot", x.shoot_x, x.shoot_y);
		INT("team", x.team);
		INT("hp", x.hitpoints);
		if(x.cooldown)
			INT("cooldown", x.cooldown);

		if(x.debug.size())
			INT_PAIR("debug", x.debug[0], x.debug[1]);

		writer.EndObject();
	}
	writer.EndArray();

	writer.String("bases");
	writer.StartArray();
	for(size_t i = 0; i < data.bases.size(); i++)
	{
		const robovis::BaseData &x = data.bases[i];
		writer.StartObject();
		//INT("id", x.id);
		INT_PAIR("pos", x.x, x.y);
		INT("team", x.team);
		INT("hp", x.hitpoints);
		if(x.cooldown)
			INT("cooldown", x.cooldown);

		writer.EndObject();
	}
	writer.EndArray();

	writer.EndObject();
	fclose(file);

	last(false);

	iteration++;
}

void robovis::Dumper::finalize()
{
	last(true);
}

void robovis::Dumper::last(bool last)
{
	const std::string old_file = dump_directory + "/last.json";
	const std::string new_file = dump_directory + "/last.json.new";

	FILE *file = fopen(new_file.c_str(),"w");
	FileStream stream(file);
	Writer<FileStream> writer(stream);
	writer.StartObject();
	writer.String("iter");
	writer.Int(iteration-last);
	writer.String("stop");
	writer.Bool(last);
	writer.EndObject();
	fclose(file);
	rename(new_file.c_str(), old_file.c_str());
}
