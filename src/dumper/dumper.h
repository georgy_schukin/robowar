#include <engine/visual/visualizer.h>

namespace robovis
{

class Dumper : public Visualizer {
public:
	Dumper();
	void init(const VisualizerData &data, const Environment &env);
	void update(const VisualizerUpdateData &data);
	void finalize();
	~Dumper() {}
protected:
	void last(bool last);
	int iteration;
	std::string dump_directory;
};

}
