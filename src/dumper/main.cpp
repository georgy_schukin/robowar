#include "engine/game/gameengine.h"
#include "engine/visual/localvisualizercontroller.h"
#include "dumper/dumper.h"
#include <iostream>

int main() {
	srand(time(NULL));
	robovis::Dumper dumper;
	robovis::LocalVisualizerController vis_controller(&dumper);
	robowar::GameEngine game_engine;
	game_engine.setVisualizerController(&vis_controller);
	game_engine.loadEnvironment("config.txt");
	game_engine.loadGame("game.txt");
	game_engine.start(false);

	game_engine.wait();

	game_engine.shutdown();
	dumper.finalize();
	return 0;
}
