TEMPLATE = subdirs

#CONFIG += ordered

SUBDIRS = robowar \
    robowar2d \
    robowar3d

robowar3d.depends = robowar
robowar2d.depends = robowar
