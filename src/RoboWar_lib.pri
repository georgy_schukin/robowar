win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../robowar/release/ -lrobowar
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../robowar/debug/ -lrobowar
else:unix: LIBS += -L$$OUT_PWD/../robowar/ -lrobowar

INCLUDEPATH += $$PWD/robowar
DEPENDPATH += $$PWD/robowar

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../robowar/release/librobowar.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../robowar/debug/librobowar.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../robowar/release/robowar.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../robowar/debug/robowar.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../robowar/librobowar.a

unix {
    LIBS += -ldl -langelscript -lpthread
}

win32 {
    LIBS += -langelscript
}
