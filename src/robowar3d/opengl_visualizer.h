#pragma once

#include "engine/state/game_listener.h"

#include <functional>

namespace robowar3d {

class OpenGLVisualizer : public robowar::GameListener {
public:    
    virtual ~OpenGLVisualizer() {}

    virtual void reshape(const int &width, const int &height) = 0;
    virtual void mouseMove(const int &x, const int &y, const int &button, const bool &pressed) = 0;
    virtual void keyPressed(unsigned char key, const int &x, const int &y) = 0;
    virtual void draw() = 0;
    virtual void idle() = 0;
    virtual void initGL() = 0;
    virtual void clearGL() = 0;

    /*void setUpdateCallback(UpdateFunc func) {
        this->update_func = func;
    }

    void postUpdate() {
        if (update_func)
            update_func();
    }*/

public:
    //typedef std::function<void(void)> UpdateFunc;

private:
    //UpdateFunc update_func; // callback to call redraw when data is updated
};

}
