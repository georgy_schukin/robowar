#pragma once

#include "mygl/mygl.h"
#include "mygl/texture_storage.h"
#include "config3d.h"
#include "legacy/types.h"
#include "legacy/animation_list.h"
#include "sound/sound_engine.h"
#include "sound/sound_library.h"
#include "opengl_visualizer.h"
#include "engine/state/robot_state.h"
#include "engine/state/base_state.h"
#include "engine/state/team_state.h"

#include <vector>
#include <map>
#include <set>
#include <mutex>
#include <condition_variable>
#include <memory>

namespace robowar3d {

class GameField;

class RoboWar3DLegacy : public OpenGLVisualizer {
public:
    RoboWar3DLegacy(const Config3D &conf);
    virtual ~RoboWar3DLegacy();

    virtual void handleInit(const robowar::GameInitState &init);
    virtual void handleUpdate(const robowar::GameState &state);
    virtual void handleFinish();

    virtual void reshape(const int &width, const int &height);
    virtual void mouseMove(const int &x, const int &y, const int &button, const bool &pressed);
    virtual void keyPressed(unsigned char key, const int &x, const int &y);
    virtual void draw();
    virtual void idle();

    virtual void initGL();
    virtual void clearGL();    

protected:
    enum KeyBinding {
        KB_HELP = 0,
        KB_ANIM_DOWN,
        KB_ANIM_UP,
        KB_MINIMAP,
        KB_SCORE,
        KB_INFO,
        KB_MUSIC,
        KB_SOUND
    };

private:    
    void initDisplayLists();
    void clearDisplayLists();

    void initVertexBuffers();
    void clearVertexBuffers();

    void initTextures();

    void initLights();
    void initMaterials();

    void initScene(const robowar::GameInitState &state);
    void initTeams(const robowar::GameInitState &state);

    const glm::vec3& getTeamColor(int team_id);

    //void initTerrain(GLuint data_vbo, GLuint indices_vbo);
    //void drawTerrain(GLuint data_vbo, GLuint indices_vbo, GLuint texture);

    //void initSkyBox(GLuint data_vbo, GLuint indices_vbo);
    //void drawSkyBox(GLuint data_vbo, GLuint indices_vbo, const std::vector<GLuint> &cubemap);

    //void initLava(GLuint data_vbo, GLuint indices_vbo);
    //void drawLava(GLuint data_vbo, GLuint indices_vbo, GLuint texture);

    void initWorld();
    void drawWorld();

    void drawRobots();
    void drawBases();
    void drawBullets();

    //void initLifeBars(const GLfloat &robot_radius, const GLfloat &base_radius, GLuint data_vbo);
    //void initCooldownBars(const GLfloat &robot_radius, const GLfloat &base_radius, GLuint data_vbo);

    //void drawRobotBars(GLuint life_vbo, GLuint cooldown_vbo);
    //void drawBaseBars(GLuint life_vbo, GLuint cooldown_vbo);

    //void initMinimap();
    //void drawMinimap();

    //void drawScore();
    //void drawInfo();
    //void drawHelp();
    //void drawHUD();

    //void initAnimation();
    //void processAnimation();
    //void waitForAnimationComplete();

    void initRobotCoords();
    void updateRobotCoords();

    void initGLView();

    //void detectEvents(std::vector<std::vector<size_t> > &events);
    //void playSounds();
    void postUpdate();

private:
    std::unique_ptr<GameField> game_field;

    //mygl::Camera3D camera;

    std::vector<robowar::RobotState> robots;
    std::vector<robowar::BaseState> bases;
    std::vector<robowar::TeamState> teams;    

    Config3D config;

    Rect3D world; // 3D scene cube

    std::vector<glm::vec3> robot_coords; // curr positions of robots (they are moving)

    glm::vec3 eye; // position of camera in the world
    glm::vec3 look; // where camera is looking    

    std::vector<GLuint> display_lists; // display list for fast drawing
    std::vector<GLsizei> display_lists_num; // num of display lists
    std::vector<GLuint> vertex_buffers;

    mygl::TextureStorage textures;

    std::vector<GLubyte> minimap;

    std::mutex data_mutex, anim_mutex;
    std::condition_variable anim_cond;

    AnimationList bullets, moves;

    size_t robot_lifebar_size, robot_cooldown_size;
    size_t base_lifebar_size, base_cooldown_size;

    size_t window_width, window_height;

    bool gen_world;

    int curr_iteration;

    std::vector<bool> team_is_defeated;
    //std::vector<int> base_owners;

    GLfloat hex_radius;

    std::map<int, unsigned char> key_bindings;

    robowar3d::SoundEngine sound_engine;
    robowar3d::SoundLibrary sounds;
    robowar3d::SoundPtr music;

    std::vector<GLuint> skybox_tex_ids;
};

}
