#include "robowar3d_legacy.h"
#include "legacy/noise_generator.h"
#include "legacy/components/game_field.h"
#include "engine/state/game_init_state.h"
#include "engine/state/game_state.h"

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/component_wise.hpp>
#include <cstdio>
#include <algorithm>

namespace robowar3d {

const GLfloat BULLET_COLOR[] = {1.0f, 1.0f, 1.0f};

const std::string MUSIC_FILE = "Music/theme.mp3";
const std::string SOUNDS_CONFIG = "Sounds/sounds.txt";
const std::string SOUNDS_PREFIX = "Sounds/";

enum DisplayListName { // names of display lists
    DL_BASE = 0,
    DL_ROBOT,
    DL_HEX2D,
    NUM_OF_DISPLAY_LISTS
};

enum TextureName { // names of textures        
    T_ROCK = 0,
    T_CRACKED,    
    T_LAVA,
    T_SMOKE,
    T_FIRE,    
    T_SKYBOX_UP,
    T_SKYBOX_FRONT,
    T_SKYBOX_LEFT,
    T_SKYBOX_BACK,
    T_SKYBOX_RIGHT,
    T_SKYBOX_DOWN,
    NUM_OF_TEXTURES
};

enum VertexBufferName { // VBOs
    VBO_TERRAIN_DATA = 0,
    VBO_TERRAIN_INDEX,
    VBO_FIELD_DATA,
    VBO_FIELD_INDEX,
    VBO_SKYBOX_DATA,
    VBO_SKYBOX_INDEX,
    VBO_LAVA_DATA,
    VBO_LAVA_INDEX,
    VBO_PARTICLE_DATA,
    VBO_LIFE_DATA,
    VBO_COOLDOWN_DATA,
    NUM_OF_VBO
};

int bases_rot_angle = 0;
int lava_move_step = 0;

RoboWar3DLegacy::RoboWar3DLegacy(const Config3D &conf) : config(conf) {
    eye = glm::vec3(10.0f, 5.0f, 10.0f);
    look = glm::vec3(0.0f, 0.0f, 0.0f);

    gen_world = false;    
    hex_radius = 1.0f;

    key_bindings[KB_HELP] = 'h';
    key_bindings[KB_ANIM_DOWN] = 'a';
    key_bindings[KB_ANIM_UP] = 'd';
    key_bindings[KB_MINIMAP] = 'w';
    key_bindings[KB_SCORE] = 's';
    key_bindings[KB_INFO] = 'z';
    key_bindings[KB_MUSIC] = 'x';
    key_bindings[KB_SOUND] = 'c';

    //music = sound_engine.getSound(MUSIC_FILE, true);

    //sounds.load(&sound_engine, SOUNDS_CONFIG, SOUNDS_PREFIX);
}

RoboWar3DLegacy::~RoboWar3DLegacy() {
}

void RoboWar3DLegacy::initDisplayLists() {
    display_lists_num.resize(NUM_OF_DISPLAY_LISTS);
    display_lists.resize(NUM_OF_DISPLAY_LISTS);    

    display_lists_num[DL_BASE] = 1;
    display_lists[DL_BASE] = glGenLists(display_lists_num[DL_BASE]);
    glNewList(display_lists[DL_BASE], GL_COMPILE);
    glutSolidTeapot(hex_radius); // draw base (as teapot)
    glEndList();

    display_lists_num[DL_ROBOT] = 1;
    display_lists[DL_ROBOT] = glGenLists(display_lists_num[DL_ROBOT]);    
    glNewList(display_lists[DL_ROBOT], GL_COMPILE);
    glutSolidSphere(hex_radius*0.5f, 32, 32); // draw robot (as sphere)
    glEndList();
}

void RoboWar3DLegacy::clearDisplayLists() {
    for (size_t i = 0; i < display_lists.size(); i++) {
        if (glIsList(display_lists[i])) {
            glDeleteLists(display_lists[i], display_lists_num[i]);
        }
    }
    display_lists.clear();
    display_lists_num.clear();
}

void RoboWar3DLegacy::initVertexBuffers() {
    vertex_buffers.resize(NUM_OF_VBO);
    glGenBuffers(NUM_OF_VBO, &vertex_buffers[0]);
}

void RoboWar3DLegacy::clearVertexBuffers() {
    glDeleteBuffers(NUM_OF_VBO, &vertex_buffers[0]);
    vertex_buffers.clear();
}

void RoboWar3DLegacy::initTextures() {
    textures.alloc(NUM_OF_TEXTURES);
    textures.load(T_ROCK, "Textures/rock.jpg");
    textures.load(T_CRACKED, "Textures/cracked.jpg");
    textures.load(T_LAVA, "Textures/lava.jpg");
    textures.load(T_SKYBOX_UP, "Textures/skybox/skybox_up.jpg");
    textures.load(T_SKYBOX_LEFT, "Textures/skybox/skybox_left.jpg");
    textures.load(T_SKYBOX_FRONT, "Textures/skybox/skybox_front.jpg");
    textures.load(T_SKYBOX_RIGHT, "Textures/skybox/skybox_right.jpg");
    textures.load(T_SKYBOX_BACK, "Textures/skybox/skybox_back.jpg");
    textures.load(T_SKYBOX_DOWN, "Textures/skybox/skybox_down.jpg");

    skybox_tex_ids.resize(6);
    for (size_t i = 0; i < 6; i++)
        skybox_tex_ids[i] = textures[T_SKYBOX_UP + i].getId();

    //robowar3d::NoiseGenerator noise_gen(128, 128);

    //noise_gen.genPerlin(7, 0.7f, true);
    //textures[T_SMOKE].loadFromMemory(128, 128, &(noise_gen.getNoise()[0]));

    /*n_gen.GenPerlin(3, 1.0f, true);
    textures[T_FIRE].loadFromMemory(128, 128, &(n_gen.noise[0]));*/
}

void RoboWar3DLegacy::initGL() {
	glShadeModel(GL_SMOOTH);

	glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);
	glEnable(GL_LIGHT0);

    glEnable(GL_POLYGON_SMOOTH);
    glEnable(GL_LINE_SMOOTH);

	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);	
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);	
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

    //glFrontFace(GL_CCW);

    glClearColor(0.0f, 0.0f, 0.05f, 1.0f);

    initTextures();
    initVertexBuffers();
    initMaterials();
}

void RoboWar3DLegacy::clearGL() {
    clearVertexBuffers();
    clearDisplayLists();
    if (game_field) {
        game_field->clearGL();
    }
}

void RoboWar3DLegacy::initLights() {
    const GLfloat position[] = {0.0f, world.max_y/2.0f, 0.0f, 0.0f};
    const GLfloat diffuse[] = {0.3f, 0.3f, 0.3f, 0.0f};
    const GLfloat specular[] = {0.95f, 0.95f, 0.95f, 0.0f};

    glLightfv(GL_LIGHT0, GL_POSITION, position);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
}

void RoboWar3DLegacy::initMaterials() {
    const GLfloat m_spec[] = {1.0f, 1.0f, 1.0f, 1.0f};
    const GLfloat m_emis[] = {0.1f, 0.1f, 0.1f, 1.0f};
    const GLfloat m_shine = 10;
	
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, m_spec);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, m_emis);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, m_shine);
}

void RoboWar3DLegacy::initGLView() {
    glViewport(0, 0, window_width, window_height);
    glMatrixMode(GL_PROJECTION);
       glLoadIdentity();
       gluPerspective(60, GLfloat(window_width)/GLfloat(window_height), 0.5f, 2.5*world.max_x);
    glMatrixMode(GL_MODELVIEW);
       glLoadIdentity();
       gluLookAt(eye.x, eye.y, eye.z, look.x, look.y, look.z, 0.0f, 1.0f, 0.0f);
    glMatrixMode(GL_MODELVIEW);
}

void RoboWar3DLegacy::initScene(const robowar::GameInitState &state) {
    game_field = std::make_unique<GameField>(state.getCells(), hex_radius);
    const auto min = game_field->getMinPoint();
    const auto max = game_field->getMaxPoint();
    const auto min_dim = glm::compMin(min);
    const auto max_dim = glm::compMax(max);
    world.Set(2.0f*min.x, 2.0f*max.x, 2.0f*min_dim, 2.0f*max_dim, 2.0f*min.z, 2.0f*max.z);
    eye = glm::vec3(max.x, world.max_y*0.3f, max.z);
    look = glm::vec3(0.0f, 0.0f, 0.0f);
}

void RoboWar3DLegacy::initTeams(const robowar::GameInitState &state) {
    teams = state.getTeams();
    /*team_is_defeated.resize(teams.size());
    for (size_t i = 0; i < team_is_defeated.size(); i++) {
        team_is_defeated[i] = false;
    }*/
}

const glm::vec3& RoboWar3DLegacy::getTeamColor(int team_id) {
    static const std::vector<glm::vec3> team_colors = {{1.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f},
            {1.0f, 0.0f, 1.0f}, {0.0f, 1.0f, 1.0f}, {1.0f, 0.6f, 0.0f}, {1.0f, 0.8f, 0.6f},
            {0.0f, 0.5f, 0.9f}, {1.0f, 0.5f, 0.7f}, {0.5f, 0.7f, 0.7f}}; // colors of teams
    static const glm::vec3 neutral_color(1.0f, 1.0f, 1.0f);
    if (team_id < 0) {
        return neutral_color;
    }
    auto it = std::find_if(teams.begin(), teams.end(),
                           [team_id](const robowar::TeamState &team) {return team.getId() == team_id;});
    if (it == teams.end()) {
        return neutral_color;
    }
    return team_colors.at(std::distance(teams.begin(), it) % team_colors.size());
}

void RoboWar3DLegacy::initWorld() {
    if (game_field) {
        game_field->initGL();
    }
    //initTerrain(vertex_buffers[VBO_TERRAIN_DATA], vertex_buffers[VBO_TERRAIN_INDEX]);
    //initGameField(hex_radius*0.85f, hex_radius*0.3f, vertex_buffers[VBO_FIELD_DATA], vertex_buffers[VBO_FIELD_INDEX]);
    //initSkyBox(vertex_buffers[VBO_SKYBOX_DATA], vertex_buffers[VBO_SKYBOX_INDEX]);
    //initLava(vertex_buffers[VBO_LAVA_DATA], vertex_buffers[VBO_LAVA_INDEX]);
}

void RoboWar3DLegacy::drawWorld() {    
    /*glDisable(GL_LIGHTING);
        drawSkyBox(vertex_buffers[VBO_SKYBOX_DATA], vertex_buffers[VBO_SKYBOX_INDEX], skybox_tex_ids);
    glEnable(GL_LIGHTING);

    const GLfloat lava_step = GLfloat(lava_move_step)/500.0f; // lava move step

    glMatrixMode(GL_TEXTURE); // draw lava
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(lava_step, lava_step, 0.0f);
        drawLava(vertex_buffers[VBO_LAVA_DATA], vertex_buffers[VBO_LAVA_INDEX], textures[T_LAVA].getId());
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);

    drawTerrain(vertex_buffers[VBO_TERRAIN_DATA], vertex_buffers[VBO_TERRAIN_INDEX], textures[T_ROCK].getId());
    //drawGameField(vertex_buffers[VBO_FIELD_DATA], vertex_buffers[VBO_FIELD_INDEX], textures[T_CRACKED].getId());
    */
    if (game_field) {
        game_field->draw();
    }
}

void RoboWar3DLegacy::drawRobots() {
    /*glMatrixMode(GL_MODELVIEW);
    size_t i = 0;
    for (const auto &robot: robots) {
        if(robot.getHitpoints() == 0)
            continue;

        const GLfloat scale = GLfloat(robot.hitpoints + 1)/GLfloat(environment.getRobotHitpoints() + 1);
        const glm::vec3 robot_color = getTeamColor(robot.team)*scale;
        const glm::vec3 &p = robot_coords[i];
		glPushMatrix(); // draw robot		
            glTranslatef(p.x, p.y, p.z);
            glColor3fv(&robot_color[0]);
            glCallList(display_lists[DL_ROBOT] + robot.hitpoints);
        glPopMatrix();
        i++;
    }*/
}

void RoboWar3DLegacy::drawBases() {    
    glMatrixMode(GL_MODELVIEW);

    for (const auto &base: bases) {
        //const GLfloat scale = GLfloat(base.hitpoints + 1)/GLfloat(environment.getBaseHitpoints() + 1);
        const auto base_color = getTeamColor(base.getTeamId());
        const auto base_coord = game_field->getPoint(base.getCoord());

        glColor3fv(&base_color[0]);
		glPushMatrix();			
            glTranslatef(base_coord.x, base_coord.y + hex_radius*0.8f, base_coord.z); // draw base
            if (base.getTeamId() != -1) { // base is active
                glRotatef((GLfloat)bases_rot_angle, 0.0f, 1.0f, 0.0f);
            }
            glCallList(display_lists[DL_BASE]);
		glPopMatrix();
    }
}

void RoboWar3DLegacy::drawBullets() {
    /*std::unique_lock<std::mutex> lock(anim_mutex);

    const GLboolean is_light = glIsEnabled(GL_LIGHTING);
    if (is_light == GL_TRUE)
        glDisable(GL_LIGHTING);

	glMatrixMode(GL_MODELVIEW);
    glColor3fv(BULLET_COLOR);
    glLineWidth(3.0f);
    glBegin(GL_LINES);
    for(AnimationList::const_iterator it = bullets.begin(); it != bullets.end(); it++) {
        const glm::vec3 &p = ((MoveAnimation*)it->get())->getCurrPos();
        const glm::vec3 v = ((MoveAnimation*)it->get())->getMoveDirNormalized()*hex_radius*0.5f;
        glVertex3f(p.x, p.y, p.z);
        glVertex3f(p.x + v.x, p.y + v.y, p.z + v.z);
	}
    glEnd();

    if (is_light == GL_TRUE)
        glEnable(GL_LIGHTING);
        */
}

void RoboWar3DLegacy::reshape(const int &width, const int &height) {
    window_width = width;
    window_height = height;
    initGLView();
    initLights();
}

void RoboWar3DLegacy::draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    

    {
        std::unique_lock<std::mutex> lock(data_mutex);
        if (gen_world) {
            initDisplayLists();
            initWorld();
            //initLifeBars(hex_radius*0.65f, hex_radius*1.5f, vertex_buffers[VBO_LIFE_DATA]);
            //initCooldownBars(hex_radius*0.65f, hex_radius*1.5f, vertex_buffers[VBO_COOLDOWN_DATA]);
            initGLView();
            initLights();
            gen_world = false;
        }
    }

    //glEnable(GL_LIGHTING);
    //glEnable(GL_TEXTURE_2D);

    drawWorld();

    glEnable(GL_LIGHTING);

    drawRobots();
    drawBases();
    drawBullets();

    glDisable(GL_LIGHTING);
    //glDisable(GL_TEXTURE_2D);

    //drawRobotBars(vertex_buffers[VBO_LIFE_DATA], vertex_buffers[VBO_COOLDOWN_DATA]);
    //drawBaseBars(vertex_buffers[VBO_LIFE_DATA], vertex_buffers[VBO_COOLDOWN_DATA]);

    //drawHUD();
}

void RoboWar3DLegacy::mouseMove(const int &x, const int &y, const int &button, const bool &pressed) {
    static int mouse_pos_x = 0, mouse_pos_y = 0;
    static bool was_pressed = false;

    if (pressed && !was_pressed) {
       mouse_pos_x = x;
       mouse_pos_y = y;
       was_pressed = true;
    } else if (!pressed) {
       was_pressed = false;
    }

    bool change = false;
    const auto dx = float(mouse_pos_x - x);
    const auto dy = float(mouse_pos_y - y);
    if ((button == 0) && pressed) {
        glm::vec3 look_direction = look - eye;
        const float angle = glm::radians(180.0f*dx/window_width);
        look_direction = glm::rotate(look_direction, angle, glm::vec3(0.0f, 1.0f, 0.0f));
        look = eye + look_direction;
        const float step = world.max_x*dy/window_height;
        const glm::vec3 dir = glm::normalize(glm::vec3(look_direction.x, 0.0f, look_direction.y))*step;
        eye += dir;
        look += dir;
        change = true;
    }
    if ((button == 1) && pressed) {
        eye.y += world.max_y*dy/window_height;
        const float angle = glm::radians(180.0f*dx/window_width);
        eye = glm::rotate(eye, angle, glm::vec3(0.0f, 1.0f, 0.0f));
        change = true;
    }

    mouse_pos_x = x;
    mouse_pos_y = y;

    if(change) {
       initGLView();
    }
}

void RoboWar3DLegacy::keyPressed(unsigned char key, const int &x, const int &y) {
    /*key = tolower(key);
    if (key == key_bindings[KB_ANIM_DOWN]) {
        if(config.frames_per_anim > 1) config.frames_per_anim--;
    }
    else if (key == key_bindings[KB_ANIM_UP]) {
        config.frames_per_anim++;
    }
    else if (key == key_bindings[KB_MINIMAP]) {
        config.show_minimap = (config.show_minimap + 1) % 3;
        postUpdate();
    }
    else if (key == key_bindings[KB_SCORE]) {
        config.show_score = (config.show_score + 1) % 3;
        postUpdate();
    }
    else if (key == key_bindings[KB_INFO]) {
        config.show_info = !config.show_info;
        postUpdate();
    }
    else if (key == key_bindings[KB_HELP]) {
        config.show_help = !config.show_help;
        postUpdate();
    } else if (key == key_bindings[KB_MUSIC]) {
        config.enable_music = !config.enable_music;        
        if (music.get()) {
            if (config.enable_music)
                music->play();
            else
                music->pause();
        }
    } else if (key == key_bindings[KB_SOUND]) {
        config.enable_sound = !config.enable_sound;
    }*/
}

void RoboWar3DLegacy::initRobotCoords() {
    robot_coords.resize(robots.size());
    for (size_t i = 0; i < robots.size(); i++) {        
        //const glm::vec3 &p = field_points[robots[i].cell_index];
        //robot_coords[i] = glm::vec3(p.x, p.y, hex_radius*0.5f);
	}
}

void RoboWar3DLegacy::updateRobotCoords() {
    std::unique_lock<std::mutex> lock(anim_mutex);

    for(AnimationList::const_iterator it = moves.begin();it != moves.end();it++) {
        //RobotMoveAnimation *m = (RobotMoveAnimation*)(it->get());
        //robot_coords[m->getRobotIndex()] = m->getCurrPos();
    }
}

/*void RoboWar3DLegacy::initAnimation() {
    std::unique_lock<std::mutex> lock(anim_mutex);

    const int fpa = config.frames_per_anim;
    const int fpa_half = fpa/2;

    for (size_t i = 0; i < robots.size(); i++) {
        const robovis::RobotData &robot = robots[i];
        if (robot.is_shooting) {
            const glm::vec3 &beg_point = robot_coords[i];
            const glm::vec3 &end_point = field_points[robot.shoot_cell_index];
            const glm::vec3 move((end_point.x - beg_point.x)/fpa, (end_point.y - beg_point.y)/fpa, 0.0f);
            bullets.push_back(AnimationPtr(new MoveAnimation(0, fpa_half, beg_point, move)));
		}	
        if (robot.is_moving) {
            const glm::vec3 &beg_point = robot_coords[i];
            const glm::vec3 &end_point = field_points[robot.move_cell_index];
            const glm::vec3 move((end_point.x - beg_point.x)/fpa, (end_point.y - beg_point.y)/fpa, 0.0f);
            moves.push_back(AnimationPtr(new RobotMoveAnimation(fpa_half, fpa, beg_point, move, i)));
        }
    }
}

void RoboWar3DLegacy::processAnimation() {
    std::unique_lock<std::mutex> lock(anim_mutex);

    bullets.process();
    moves.process();

    if ((bullets.empty() && moves.empty())) {
        anim_cond.notify_all();
    }
}

void RoboWar3DLegacy::waitForAnimationComplete() {
    std::unique_lock<std::mutex> lock(anim_mutex);
    while (!(bullets.empty() && moves.empty())) {
        anim_cond.wait(lock);
    }
}*/

void RoboWar3DLegacy::idle() {
    //processAnimation();

    {
        std::unique_lock<std::mutex> lock(data_mutex);
        updateRobotCoords();
    }

    bases_rot_angle = (bases_rot_angle + 1) % 360;
    lava_move_step = (lava_move_step + 1) % 500;

    postUpdate();
}

void RoboWar3DLegacy::handleInit(const robowar::GameInitState &init) {
    {
        std::unique_lock<std::mutex> lock(data_mutex);
        initScene(init);
        initTeams(init);

        gen_world = true; // generate all visual static data
    }

    //update(data);

    /*if (config.enable_music && music.get())
        music->play();*/    
}

void RoboWar3DLegacy::handleUpdate(const robowar::GameState &state) {
    //waitForAnimationComplete();
    {
        std::unique_lock<std::mutex> lock(data_mutex);
        robots = state.getRobots();
        bases = state.getBases();
        curr_iteration = state.getIteration();

        initRobotCoords();
        //initAnimation();
        //initMinimap();

        /*if (config.enable_sound)
            playSounds();*/
    }
}

void RoboWar3DLegacy::handleFinish() {

}

void RoboWar3DLegacy::postUpdate() {
    glutPostRedisplay();
}

}
