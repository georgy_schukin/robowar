#include "config3d.h"

#include <iostream>
#include <fstream>

namespace robowar3d {

Config3D::Config3D() {
    setDefaults();
}

Config3D::Config3D(const std::string &filename) {
    setDefaults();
    load(filename);
}

void Config3D::setDefaults() {
    frames_per_anim = 2;
    refresh_delay = 0;
    enable_sound = true;
    enable_music = true;
    show_minimap = SM_NO_FRAME;
    show_score = SM_WITH_FRAME;
    show_info = true;
    show_help = false;
}

int Config3D::load(const std::string &filename) {
    std::fstream f_in(filename.c_str());

    if (!f_in.is_open()) {
        std::cerr << "Config3D error: failed to open " << filename << std::endl;
        return -1;
    }

    while (!f_in.eof()) {
        std::string tag;
        f_in >> tag;

        if (tag == "") continue;
        if ((tag[0] == '%') || (tag[0] == '#')) { // skip comment
            std::string temp;
            std::getline(f_in, temp);
            continue;
        }

        int value;
        f_in >> value;

        if (tag == "show_particles") show_particles = value;
        else if (tag == "show_minimap") show_minimap = static_cast<ShowMode>(value);
        else if (tag == "show_score") show_score = static_cast<ShowMode>(value);
        else if (tag == "show_info") show_info = (value != 0);
        else if (tag == "frames_per_anim") frames_per_anim = value;
        //else if (tag == "refresh_delay") refresh_delay = value;
        else if (tag == "enable_music") enable_music = value;
        else if (tag == "enable_sound") enable_sound = value;
        else {
            std::cerr << "Config3D error: invalid or unknown tag: " << tag << std::endl;
        }
    }

    f_in.close();
    return 0;
}

}
