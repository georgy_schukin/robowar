#include "mygl/mygl.h"
#include "mygl/shader_library.h"
#include "engine/game/game_engine.h"
#include "engine/game/game_session.h"
#include "robowar3d_legacy.h"
#include "opengl_visualizer.h"

#include <memory>
#include <iostream>

std::unique_ptr<robowar::GameEngine> game_engine;
std::unique_ptr<robowar3d::OpenGLVisualizer> visualizer;
robowar::GameSession *current_session = nullptr;

const std::string CONFIG3D_DEFAULT_FILENAME = "config3D.txt";
//const std::string GAME_ENVIRONMENT_DEFAULT_FILENAME = "config.txt";
const std::string GAME_DEFAULT_FILENAME = "game.txt";
//const std::string SHADERS_PREFIX = "Shaders/";

int mouse_button_pressed = 0;

void initGame(const std::string &game_file) {
    //mygl::ShaderLibrary::getInstance()->setFilePrefix(SHADERS_PREFIX);

    try {
        robowar3d::Config3D config(CONFIG3D_DEFAULT_FILENAME);
        visualizer = std::make_unique<robowar3d::RoboWar3DLegacy>(config);
        //visualizer->setUpdateCallback((robovis::OpenGLVisualizer::UpdateFunc)glutPostRedisplay);
        visualizer->initGL();

        game_engine = std::make_unique<robowar::GameEngine>();
        current_session = game_engine->newSession();
        current_session->addListener(visualizer.get());
        current_session->loadGameFromFile(game_file);
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        exit(-1);
    }
}

void startGame() {
    if (current_session) {
        current_session->run();
    }
}

void reshape(int width, int height) {
    visualizer->reshape(width, height);
}

void mouse(int button, int state, int x, int y) {
    mouse_button_pressed = (button == GLUT_LEFT_BUTTON ? 0 : 1);
    bool pressed = (state == GLUT_DOWN);
    visualizer->mouseMove(x, y, mouse_button_pressed, pressed);
}

void mouseMove(int x, int y) {
    visualizer->mouseMove(x, y, mouse_button_pressed, true);
}

void keyboard(unsigned char key, int x, int y) {
    visualizer->keyPressed(key, x, y);
    if (key == 27) { // Esc key
        exit(0);
    }
}

void draw() {
    visualizer->draw();
    glutSwapBuffers();
}

void idle() {    
    visualizer->idle();
}

void onExit() {
    game_engine->shutdown();
    visualizer->clearGL();    
}

int main(int argc, char *argv[]) {
    glutInit(&argc, argv);

    //glutInitContextVersion(3, 3);
    //glutInitContextFlags(GLUT_FORWARD_COMPATIBLE | GLUT_DEBUG);
    //glutInitContextProfile(GLUT_CORE_PROFILE);

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glutInitWindowSize(1024, 768);
    glutCreateWindow("RoboWar");

    //glutGameModeString("1366x768:32@75"); //screen settings
    //glutEnterGameMode(); // set full screen

    glutReshapeFunc(reshape); // set callbacks
    glutMouseFunc(mouse);
    glutMotionFunc(mouseMove);
    glutKeyboardFunc(keyboard);
    glutDisplayFunc(draw);
    glutIdleFunc(idle);
    glutCloseFunc(onExit);

    GLenum err;
    glewExperimental = GL_TRUE;
    if ((err = glewInit()) != GLEW_OK) {
        std::cerr << "GLEW init ERROR : " << glewGetErrorString(err) << std::endl;
        exit(-1);
    }    

    srand((size_t)time(0));

    const std::string game_file = (argc > 1) ? std::string(argv[1]) : GAME_DEFAULT_FILENAME;

    initGame(game_file);
    startGame(); // start game thread

    glutMainLoop(); // start visualization

    return 0;
}



