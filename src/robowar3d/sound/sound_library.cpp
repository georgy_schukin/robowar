#include "sound_library.h"

#include <fstream>
#include <iostream>
#include <cstdlib>

namespace robowar3d {

const std::string GROUP_TAG = "group";
const std::string EVENT_TAG = "event";

const std::string SHOOT_EVENT = "shoot";
const std::string MOVE_EVENT = "move";
const std::string TRANSMIT_EVENT = "transmit";
const std::string DEATH_EVENT = "death";
const std::string DEFEAT_EVENT = "defeat";
const std::string VICTORY_EVENT = "victory";
const std::string BASE_CAPTURE_EVENT = "base_capture";

const std::string DEFAULT_GROUP = "default";

int SoundLibrary::load(SoundEngine *engine, const std::string &filename, const std::string &prefix) {
    std::ifstream f_in(filename.c_str());
    if (!f_in.is_open()) {
        std::cerr << "Sound library error: can't open config file " << filename << std::endl;
        return -1;
    }

    std::string current_group = DEFAULT_GROUP;

    while(!f_in.eof()) {
        std::string tag;
        f_in >> tag;

        if (tag == "") {
            continue;
        }
        else if ((tag[0] == '%') || (tag[0] == '#')) { // skip comments
            std::string temp;
            std::getline(f_in, temp);
            continue;
        }
        else if (tag == GROUP_TAG) {
            f_in >> current_group;
        }
        else if (tag == EVENT_TAG) {
            std::string event_type_str, sound_file;
            f_in >> event_type_str >> sound_file;
            const SoundLibrary::EventType event_type = getEventType(event_type_str);
            if (event_type != SoundLibrary::ET_UNKNOWN) {
                SoundPtr sound = engine->getSound(prefix + sound_file);
                if (sound.get())
                    sounds[current_group][event_type].push_back(sound);
            }
        }
    }

    f_in.close();
    return 0;
}

void SoundLibrary::playRandomSound(const std::string &group, const EventType &event) {
    SoundLibraryMap::iterator it = sounds.find(group);
    if ((it == sounds.end()) || (it->second[event].empty())) {
        it = sounds.find(DEFAULT_GROUP); // search in default group
    }
    if ((it != sounds.end()) && !it->second[event].empty()) {
        const std::vector<SoundPtr> &snd = it->second[event];
        SoundPtr sound = snd[rand() % snd.size()];
        if (sound.get())
            sound->play();
    }
}

SoundLibrary::EventType SoundLibrary::getEventType(const std::string &type) {
    if (type == SHOOT_EVENT) return SoundLibrary::ET_SHOOT;
    else if (type == MOVE_EVENT) return SoundLibrary::ET_MOVE;
    else if (type == TRANSMIT_EVENT) return SoundLibrary::ET_TRANSMIT;
    else if (type == DEFEAT_EVENT) return SoundLibrary::ET_DEFEAT;
    else if (type == DEATH_EVENT) return SoundLibrary::ET_DEATH;
    else if (type == VICTORY_EVENT) return SoundLibrary::ET_VICTORY;
    else if (type == BASE_CAPTURE_EVENT) return SoundLibrary::ET_BASE_CAPTURE;
    else return SoundLibrary::ET_UNKNOWN;
}

}
