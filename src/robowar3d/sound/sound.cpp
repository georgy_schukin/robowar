#include "sound.h"

#include <iostream>

namespace robowar3d {

void Sound::play() {
    if (!engine || !source)
        return;
    engine->play2D(source);
}

}
