#pragma once

#include "sound.h"

namespace robowar3d {

/**
 * Looped sound. Plays in loops. Call play() to start or resume, pause() to pause.
 */
class LoopedSound : public Sound {
public:
    LoopedSound() : sound(0) {}
    ~LoopedSound();

    virtual void play();
    virtual void pause();

protected:
    friend class SoundEngine;
    LoopedSound(irrklang::ISoundEngine *engn, irrklang::ISoundSource *src) : Sound(engn, src), sound(0) {}

protected:
    irrklang::ISound *sound;
};

}
