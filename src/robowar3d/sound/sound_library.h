#pragma once

#include "sound_engine.h"

#include <string>
#include <map>
#include <vector>

namespace robowar3d {

class SoundLibrary {
public:
    enum EventType {
        ET_UNKNOWN = 0,
        ET_MOVE,
        ET_SHOOT,
        ET_TRANSMIT,
        ET_BASE_CAPTURE,
        ET_DEATH,
        ET_DEFEAT,
        ET_VICTORY,
        NUM_OF_EVENTS
    };

public:
    SoundLibrary() {}
    ~SoundLibrary() {}

    int load(SoundEngine *engine, const std::string &filename, const std::string &prefix = "");
    void playRandomSound(const std::string &group, const EventType &event);

private:
    EventType getEventType(const std::string &type);

private:
    typedef std::map<int, std::vector<SoundPtr> > SoundMap; // sound type -> sounds
    typedef std::map<std::string, SoundMap> SoundLibraryMap; // group -> group sounds

private:
    SoundLibraryMap sounds;
};

}
