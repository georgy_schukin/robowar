#pragma once

#include <irrKlang/irrKlang.h>
#include <memory>

namespace robowar3d {

/**
 * Simple sound. Plays only once. Call play() each time to play.
 */
class Sound {
public:
    Sound() : engine(0), source(0) {}
    virtual ~Sound() {}

    virtual void play();
    virtual void pause() {}

protected:
    friend class SoundEngine;
    Sound(irrklang::ISoundEngine *engn, irrklang::ISoundSource *src) : engine(engn), source(src) {}

protected:
    irrklang::ISoundEngine *engine;
    irrklang::ISoundSource *source;
};

typedef std::shared_ptr<Sound> SoundPtr;

}
