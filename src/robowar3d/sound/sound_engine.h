#pragma once

#include "sound/sound.h"

#include <irrKlang/irrKlang.h>
#include <string>

namespace robowar3d {

/**
 * Engine to play sounds and music. Uses irrKlang library.
 */
class SoundEngine {
public:
    SoundEngine();
    ~SoundEngine();

    SoundPtr getSound(const std::string &file, const bool &looped = false);

private:
    irrklang::ISoundEngine *engine;
};

}
