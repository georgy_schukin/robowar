#include "sound_engine.h"
#include "looped_sound.h"

#include <iostream>

namespace robowar3d {

SoundEngine::SoundEngine() : engine(0) {
    engine = irrklang::createIrrKlangDevice();
    if (!engine) {
        std::cerr << "Error: failed to open audio device" << std::endl;
    }
}

SoundEngine::~SoundEngine() {
    if (engine) {
        engine->stopAllSounds();
        engine->removeAllSoundSources();
        engine->drop();
        engine = 0;
    }
}

SoundPtr SoundEngine::getSound(const std::string &file, const bool &looped) {
    if (!engine)
        return SoundPtr();
    irrklang::ISoundSource *source = engine->getSoundSource(file.c_str());
    if (!source) {
        std::cerr << "Error: failed to open sound file " << file << std::endl;
        return SoundPtr();
    }
    return SoundPtr(looped ? new LoopedSound(engine, source) : new Sound(engine, source));
}

}
