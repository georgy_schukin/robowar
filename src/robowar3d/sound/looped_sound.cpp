#include "looped_sound.h"

#include <iostream>

namespace robowar3d {

LoopedSound::~LoopedSound() {
    if (sound) {
        sound->stop();
        sound->drop();
        sound = 0;
    }
}

void LoopedSound::play() {
    if (!engine || !source)
        return;
    if (!sound)
        sound = engine->play2D(source, true, true, true);
    if (sound)
        sound->setIsPaused(false);
}

void LoopedSound::pause() {
    if (sound)
        sound->setIsPaused(true);
}

}
