#pragma once

#include "engine/game/coords.h"
#include "glm/glm.hpp"
#include "mygl/mygl.h"

#include <vector>

namespace robowar3d {

class GameField {
public:
    GameField(const std::vector<robowar::Coord> &coords, float radius=1.0f);
    ~GameField() {}

    // Extent by X.
    int getWidth() const;

    // Extent by Y.
    int getHeight() const;

    void initGL();
    void clearGL();
    void draw();

    const glm::vec3& getPoint(const robowar::Coord &coord) const;
    glm::vec3 getMinPoint() const;
    glm::vec3 getMaxPoint() const;

private:
    void init();

    glm::vec3 coordToPoint(const robowar::Coord &coord, float size=1.0f);

private:    
    std::vector<robowar::Coord> coords;
    float hex_radius;
    std::vector<glm::vec3> points; // centers of game field hexagons
    int field_width;
    int field_height;
    GLuint vbo[2];
    GLsizei num_of_vertices;
    GLsizei num_of_indices;
};

}
