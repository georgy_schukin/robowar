#include "game_field.h"
#include "glm/gtc/constants.hpp"
#include "mygl/mygl.h"

#include <algorithm>

namespace robowar3d {

namespace {
    std::vector<glm::vec3> getHexVertices(float hex_radius) {
        std::vector<glm::vec3> vertices;
        for (size_t i = 0; i < 360; i += 60) {
            vertices.push_back(glm::vec3(hex_radius*glm::sin(glm::radians(float(i))),
                                         0.0f,
                                         hex_radius*glm::cos(glm::radians(float(i)))));
        }
        return vertices;
    }

    const GLfloat CELL_COLOR[] = {0.0f, 0.0f, 0.95f};
    const GLfloat CELL_BORDER_COLOR[] = {0.3f, 0.0f, 0.6f};

    //const size_t VERTICES_PER_FACE = 6; // hex - 6 points
    //const size_t VERTICES_PER_CELL = VERTICES_PER_FACE*2; // 6 top, 6 bottom
    //const size_t INDICES_PER_CELL = (VERTICES_PER_FACE - 2 + 2*VERTICES_PER_FACE)*3; // triangles: 4 on top, 12 on sides
}

GameField::GameField(const std::vector<robowar::Coord> &coords, float radius) :
    coords(coords), hex_radius(radius)
{
    init();
}

int GameField::getWidth() const {
    return field_width;
}

int GameField::getHeight() const {
    return field_height;
}

void GameField::init() {
    glm::vec3 average;
    for (const auto &coord: coords) {
        const auto point = coordToPoint(coord, hex_radius*1.1);
        points.push_back(point);
        average += point;
    }
    average /= coords.size();
    for (auto &point: points) {
        point -= average;
    }
}

void GameField::initGL() {
    const auto hex_vertices = getHexVertices(hex_radius);
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<GLuint> indices;

    for (const auto &point: points) {
        const auto start_index = vertices.size();
        vertices.push_back(point); // cell center
        for (const auto &hex_vertex: hex_vertices) {
            vertices.push_back(point + hex_vertex); // cell border points
        }
        for (size_t i = 0; i < hex_vertices.size() + 1; i++) {
            normals.push_back(glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f))); // add up normals for all points
        }

        // Setup indices for 6 triangles.
        for (size_t i = 0; i < hex_vertices.size(); i++) {
            indices.push_back(start_index);
            indices.push_back(start_index + i + 1);
            indices.push_back(start_index + (i - 1 + 6) % 6 + 1);
        }
    }

    num_of_vertices = vertices.size();
    num_of_indices = indices.size();

    glGenBuffers(2, &vbo[0]);
    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // bind data buffer
    glBufferData(GL_ARRAY_BUFFER, (vertices.size() + normals.size())*sizeof(glm::vec3), NULL, GL_STATIC_DRAW); // alloc buffer
    glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size()*sizeof(glm::vec3), &vertices[0]); // alloc buffer
    glBufferSubData(GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec3), normals.size()*sizeof(glm::vec3), &normals[0]); // alloc buffer

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[1]); // bind index buffer
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(GLuint), &indices[0], GL_STATIC_DRAW); // load index array

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void GameField::clearGL() {
    glDeleteBuffers(2, &vbo[0]);
}

void GameField::draw() {
    /*glEnable(GL_TEXTURE_GEN_S); // set tex coord auto generation
    glEnable(GL_TEXTURE_GEN_T);

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    const GLfloat scale = 0.05f;

    const GLfloat param_s[4] = {scale, 0.0f, scale, 0.0f};
    const GLfloat param_t[4] = {0.0f, scale, scale, 0.0f};

    glTexGenfv(GL_S, GL_OBJECT_PLANE, param_s);
    glTexGenfv(GL_T, GL_OBJECT_PLANE, param_t);
    glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    glTexGenf(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);*/

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);

    glColor3fv(CELL_COLOR);

    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // bind data buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[1]); // bind index buffer

    glVertexPointer(3, GL_FLOAT, 0, 0);
    glNormalPointer(GL_FLOAT, 0, (void*)(num_of_vertices*sizeof(glm::vec3)));

    glMatrixMode(GL_MODELVIEW);
    glDrawElements(GL_TRIANGLES, num_of_indices, GL_UNSIGNED_INT, 0); // draw cells

    glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
}

const glm::vec3& GameField::getPoint(const robowar::Coord &coord) const {
    static const glm::vec3 empty(0.0f, 0.0f, 0.0f);
    const auto it = std::find(coords.begin(), coords.end(), coord);
    if (it == coords.end()) {
        return empty;
    }
    auto index = std::distance(coords.begin(), it);
    return points.at(index);
}

glm::vec3 GameField::getMinPoint() const {
    glm::vec3 min = points.front();
    for (const auto &point: points) {
        min.x = glm::min(min.x, point.x);
        min.y = glm::min(min.y, point.y);
        min.z = glm::min(min.z, point.z);
    }
    return min;
}

glm::vec3 GameField::getMaxPoint() const {
    glm::vec3 max = points.front();
    for (const auto &point: points) {
        max.x = glm::max(max.x, point.x);
        max.y = glm::max(max.y, point.y);
        max.z = glm::max(max.z, point.z);
    }
    return max;
}

glm::vec3 GameField::coordToPoint(const robowar::Coord &coord, float size) {
    return glm::vec3(size * glm::sqrt(3.0f) * (coord.getCol() + coord.getRow()*0.5f),
                     0.0f,
                     size * 1.5f * coord.getRow());
}

}
