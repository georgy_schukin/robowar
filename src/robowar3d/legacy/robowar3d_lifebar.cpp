#include "robowar3d_legacy.h"

namespace robowar3d {

const size_t LIFE_SCALE = 2; // scale when showing life bar
const size_t COOLDOWN_SCALE = 2; // scale when showing cooldown bar

const GLfloat COOLDOWN_COLOR[] = {0.8f, 0.8f, 0.8f};

/*void initCircleArray(const size_t &num, const GLfloat &radius, const size_t &start, std::vector<glm::vec3> &data) {
    const GLfloat step = (num > 1) ? mymath::DOUBLE_PI/(num - 1) : mymath::DOUBLE_PI;
    for (size_t i = 0; i < num; i++) {
        data[start + i] = glm::vec3(radius*cos(i*step), radius*sin(i*step), 0.0f);
    }
}

void RoboWar3DLegacy::initLifeBars(const GLfloat &robot_radius, const GLfloat &base_radius, GLuint data_vbo) {
    robot_lifebar_size = environment.getRobotHitpoints()*LIFE_SCALE + 1;
    base_lifebar_size = environment.getBaseHitpoints()*LIFE_SCALE + 1;

    std::vector<glm::vec3> robot_data(robot_lifebar_size);
    std::vector<glm::vec3> base_data(base_lifebar_size);

    initCircleArray(robot_lifebar_size, robot_radius, 0, robot_data);
    initCircleArray(base_lifebar_size, base_radius, 0, base_data);

    glBindBuffer(GL_ARRAY_BUFFER, data_vbo); // bind data buffer
    glBufferData(GL_ARRAY_BUFFER, (robot_lifebar_size + base_lifebar_size)*sizeof(glm::vec3), NULL, GL_STATIC_DRAW); // alloc
    glBufferSubData(GL_ARRAY_BUFFER, 0, robot_lifebar_size*sizeof(glm::vec3), &robot_data[0]);
    glBufferSubData(GL_ARRAY_BUFFER, robot_lifebar_size*sizeof(glm::vec3), base_lifebar_size*sizeof(glm::vec3), &base_data[0]);
    glBindBuffer(GL_ARRAY_BUFFER, 0);    
}

void RoboWar3DLegacy::initCooldownBars(const GLfloat &robot_radius, const GLfloat &base_radius, GLuint data_vbo) {
    robot_cooldown_size = environment.getReloadTime()*COOLDOWN_SCALE + 2;
    base_cooldown_size = environment.getRobotConstructionTime()*COOLDOWN_SCALE + 2;

    std::vector<glm::vec3> robot_data(robot_cooldown_size, glm::vec3(0.0f));
    std::vector<glm::vec3> base_data(base_cooldown_size, glm::vec3(0.0f));

    initCircleArray(robot_cooldown_size - 1, robot_radius, 1, robot_data);
    initCircleArray(base_cooldown_size - 1, base_radius, 1, base_data);

    glBindBuffer(GL_ARRAY_BUFFER, data_vbo); // bind data buffer
    glBufferData(GL_ARRAY_BUFFER, (robot_cooldown_size + base_cooldown_size)*sizeof(glm::vec3), NULL, GL_STATIC_DRAW); // alloc
    glBufferSubData(GL_ARRAY_BUFFER, 0, robot_cooldown_size*sizeof(glm::vec3), &robot_data[0]);
    glBufferSubData(GL_ARRAY_BUFFER, robot_cooldown_size*sizeof(glm::vec3), base_cooldown_size*sizeof(glm::vec3), &base_data[0]);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void RoboWar3DLegacy::drawRobotBars(GLuint life_vbo, GLuint cooldown_vbo) {
    glMatrixMode(GL_MODELVIEW);*/

/*    glLineWidth(1.0f);
    for (const robovis::RobotData &robot: robots) {
        if(robot.hitpoints == 0) continue;

        const GLfloat scale = GLfloat(robot.hitpoints + 1)/GLfloat(environment.getRobotHitpoints());
        const GLfloat *robot_color = COLORS[robot.team];

        const glm::vec3 &p = field_points[robot.cell_index];
        glPushMatrix(); // draw robot hex
        glTranslatef(p.x, p.y, p.z + HEX_RADIUS*0.31);
            glColor3f(robot_color[0]*scale, robot_color[1]*scale, robot_color[2]*scale);
            glCallList(display_lists[DL_HEX2D]);
        glPopMatrix();
    }*/

   /* glEnableClientState(GL_VERTEX_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, cooldown_vbo);
    glVertexPointer(3, GL_FLOAT, 0, (GLvoid*)0);

    const GLfloat bar_height = hex_radius*0.75f;

    size_t ind = 0;
    glColor3fv(COOLDOWN_COLOR);
    for (const robovis::RobotData &robot: robots) {
        if(robot.hitpoints == 0)
            continue;
        if(robot.cooldown > 0) {
            const glm::vec3 &p = robot_coords[ind++];
            glPushMatrix(); // draw robot info
                glTranslatef(p.x, p.y, bar_height);
                glDrawArrays(GL_TRIANGLE_FAN, 0, robot.cooldown*COOLDOWN_SCALE + 2); // draw cooldown
            glPopMatrix();
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, life_vbo);
    glVertexPointer(3, GL_FLOAT, 0, 0);

    ind = 0;
    glLineWidth(1.5f);
    for (const robovis::RobotData &robot: robots) {
        if(robot.hitpoints == 0)
            continue;
        const glm::vec3 robot_color = getTeamColor(robot.team)*0.8f;
        const glm::vec3 &p = robot_coords[ind++];
        glColor3fv(&robot_color[0]);
        glPushMatrix(); // draw robot info
            glTranslatef(p.x, p.y, bar_height);
            glDrawArrays(GL_LINE_STRIP, 0, robot.hitpoints*LIFE_SCALE + 1); // draw life bar
        glPopMatrix();
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer
    glDisableClientState(GL_VERTEX_ARRAY);
}

void RoboWar3DLegacy::drawBaseBars(GLuint life_vbo, GLuint cooldown_vbo) {
    glMatrixMode(GL_MODELVIEW);

    glEnableClientState(GL_VERTEX_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, cooldown_vbo);
    glVertexPointer(3, GL_FLOAT, 0, (GLvoid*)(robot_cooldown_size*sizeof(glm::vec3)));

    const GLfloat bar_height = hex_radius*1.5f;

    glColor3fv(COOLDOWN_COLOR);
    for (const robovis::BaseData &base: bases) {
        if(base.cooldown > 0) {
            const glm::vec3 &p = field_points[base.cell_index];
            glPushMatrix(); // draw base
                glTranslatef(p.x, p.y, bar_height);
                glDrawArrays(GL_TRIANGLE_FAN, 0, base.cooldown*COOLDOWN_SCALE + 2); // draw cooldown
            glPopMatrix();
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, life_vbo);
    glVertexPointer(3, GL_FLOAT, 0, (GLvoid*)(robot_lifebar_size*sizeof(glm::vec3)));

    glLineWidth(2.0f);
    for (const robovis::BaseData &base: bases) {
        const glm::vec3 base_color = getTeamColor(base.team)*0.8f;
        const glm::vec3 &p = field_points[base.cell_index];
        glColor3fv(&base_color[0]);
        glPushMatrix();
            glTranslatef(p.x, p.y, bar_height);
            glDrawArrays(GL_LINE_STRIP, 0, base.hitpoints*LIFE_SCALE + 1); // draw life bar
        glPopMatrix();
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer
    glDisableClientState(GL_VERTEX_ARRAY);
}*/
}
