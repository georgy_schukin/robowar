#pragma once

#include "mygl/mygl.h"

#include <vector>
#include <list>
#include <cstddef>
#include <cmath>
#include <memory>

namespace robowar3d {

class Animation {
public:
	Animation() : end_frame(0), curr_frame(0) {}
    Animation(const size_t &start, const size_t &end) : start_frame(start), end_frame(end), curr_frame(0) {}

    bool isAtStart() const {
        return (curr_frame == start_frame);
    }

    bool isActive() const {
        return ((curr_frame >= start_frame) && (curr_frame < end_frame));
    }

    bool isEnded() const {
        return (curr_frame >= end_frame);
    }

    size_t getCurrFrame() const {
        return curr_frame;
    }

    virtual void process();

protected:
    size_t start_frame;
    size_t end_frame;
    size_t curr_frame;
};

typedef std::shared_ptr<Animation> AnimationPtr;

class MoveAnimation : public Animation {
public:
    MoveAnimation() {}
    MoveAnimation(const size_t &start, const size_t &end, const glm::vec3 &cp, const glm::vec3 &mv) :
        Animation(start, end), curr_pos(cp), move(mv) {
        move_norm = glm::normalize(move);
    }

    const glm::vec3& getCurrPos() const {
        return curr_pos;
    }

    const glm::vec3& getMoveDir() const {
        return move;
    }

    const glm::vec3& getMoveDirNormalized() const {
        return move_norm;
    }

    virtual void process();

protected:
    glm::vec3 curr_pos;
    glm::vec3 move;
    glm::vec3 move_norm;
};

/*class BulletAnimation : public MoveAnimation {
public:
	float angle;
	int team;

public:
	BulletAnimation() {}
    BulletAnimation(const size_t &start, const size_t &end, const glm::vec3 &cp, const glm::vec3 &mv) :
        MoveAnimation(start, end, cp, mv) {
        //glm::vec3 v(ep.x - cp.x, ep.y - cp.y, ep.z - cp.z);
        const GLfloat l = sqrt(mv.x*mv.x + mv.y*mv.y);
		angle = acos(float(mv.x/l))*(180.0f/PI); // angle with X
		if(mv.y > 0) angle = -angle;
	}
};*/

class RobotMoveAnimation : public MoveAnimation {
public:
    RobotMoveAnimation() {}
    RobotMoveAnimation(const size_t &start, const size_t &end, const glm::vec3 &cp, const glm::vec3 &mv, const size_t &ind) :
        MoveAnimation(start, end, cp, mv), index(ind) {}

    size_t getRobotIndex() const {
        return index;
    }

protected:
    size_t index;
};

/*class HitAnimation : public Animation {
public:
    size_t index;
    std::vector<glm::vec3> positions;
    std::vector<glm::vec3> velocities;

public:
	HitAnimation() {}
	HitAnimation(int i, int t, int sf, int ef) : 
		index(i), team(t), Animation(sf, ef), death(false) {}

    void Generate(const glm::vec3& pos, int p_num, float max_rad, float max_len);

    virtual void process();
};*/

/*class ParticleAnimation : public Animation // smoke/fire animation
{
public:	
    glm::vec3 curr_pos;
    glm::vec3 move;
	float color;
	float size;
	float transparency;
	float size_dec, trnp_dec;


public:
	ParticleAnimation() {}
    ParticleAnimation(const glm::vec3& cp, const glm::vec3& mv, float clr, float sz, float trnp, int sf, int ef) :
		curr_pos(cp), move(mv), color(clr), size(sz), transparency(trnp), Animation(sf, ef) 
		{
			size_dec = 0.5f/(end_frame - start_frame);
			trnp_dec = -0.9f/(end_frame - start_frame);
		}

    virtual void process();
};*/

}
