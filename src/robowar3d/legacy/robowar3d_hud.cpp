#include "robowar3d_legacy.h"

#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>

namespace robowar3d {

namespace {
    std::string toString(const unsigned char &c) {
        return std::string(1, c);
    }
}

const GLfloat INFO_COLOR[] = {0.85f, 0.85f, 0.85f};
const GLfloat SCREEN_COLOR[] = {0.05f, 0.1f, 0.15f, 0.6f};
const GLfloat SCREEN_BORDER_COLOR[] = {1.0f, 1.0f, 1.0f, 1.0f};
const GLfloat SCREEN_BORDER_WIDTH = 2.0f;

void* SCORE_FONT = GLUT_BITMAP_HELVETICA_12;
void* INFO_FONT = GLUT_BITMAP_HELVETICA_10;
void* HELP_FONT = GLUT_BITMAP_HELVETICA_12;

/*void drawScreen(const int &x1, const int &y1, const int &x2, const int &y2, const GLfloat &transp = SCREEN_COLOR[3]) {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glColor4f(SCREEN_COLOR[0], SCREEN_COLOR[1], SCREEN_COLOR[2], transp);
    glBegin(GL_QUADS);
        glVertex2i(x1, y1);
        glVertex2i(x1, y2);
        glVertex2i(x2, y2);
        glVertex2i(x2, y1);
    glEnd();

    glColor4fv(SCREEN_BORDER_COLOR);
    glLineWidth(SCREEN_BORDER_WIDTH);
    glBegin(GL_LINE_LOOP);
        glVertex2i(x1, y1);
        glVertex2i(x1, y2);
        glVertex2i(x2, y2);
        glVertex2i(x2, y1);
    glEnd();

    glDisable(GL_BLEND);
}

void drawString(int x, int y, const std::string &s, void* font) {
    glWindowPos2i(x, y);
    for (size_t i = 0; i < s.size(); i++) {
        glutBitmapCharacter(font, s[i]);
    }
}

int getStringLength(const std::string &s, void* font) {
    return glutBitmapLength(font, (const unsigned char*)s.c_str());
}

int getFontHeight(void *font) {
    return glutBitmapHeight(font);
}

void getDimensions(const std::vector<std::string> &text, const size_t &inter, void *font, int &width, int &height) {
    height = getFontHeight(font)*text.size() + inter*(text.size() - 1);
    width = 0;
    for (const std::string &str: text) {
        const int len = getStringLength(str, font);
        if (width < len)
            width = len;
    }
}

size_t getMaxTeamNameLength(const std::vector<robovis::TeamData> &teams) {
    size_t max_length = 0;
    for (const robovis::TeamData &team: teams) {
        const size_t len = team.name.size();
        if (len > max_length)
            max_length = len;
    }
    return max_length;
}

void getScore(const std::vector<robovis::TeamData> &teams, std::vector<std::string> &score) {
    const size_t max_length = getMaxTeamNameLength(teams);
    for (const robovis::TeamData &team: teams) {
        std::ostringstream out;
        out << std::left << std::setw(max_length + 2) << team.name + std::string(": ");
        if ((team.num_of_robots == 0) && (team.num_of_bases == 0)) {
            out << "Terminated";
        } else {
            out << "Robots: " << std::left << std::setw(5) << team.num_of_robots <<
                   " Bases: " << std::left << std::setw(5) << team.num_of_bases;
        }
        score.push_back(out.str());
    }
}

void RoboWar3DLegacy::initMinimap() {
    minimap.resize(field_width*field_height*4);
    for (size_t i = 0; i < minimap.size(); i++) {
        minimap[i] = 0;
    }

    for (const robovis::RobotData &robot: robots) {
        if(robot.hitpoints > 0) {
            const glm::vec3 color = getTeamColor(robot.team)*0.6f;
            const size_t ind = 4*((field_height - robot.y - 1)*field_width + robot.x); // invert minimap
            minimap[ind] = (GLubyte)(color.r*255);
            minimap[ind + 1] = (GLubyte)(color.g*255);
            minimap[ind + 2] = (GLubyte)(color.b*255);
            minimap[ind + 3] = (GLubyte)(255);
        }
    }

    for (const robovis::BaseData &base: bases) {
        const glm::vec3 color = getTeamColor(base.team);
        const size_t ind = 4*((field_height - base.y - 1)*field_width + base.x); // invert minimap
        minimap[ind] = (GLubyte)(color.r*255);
        minimap[ind + 1] = (GLubyte)(color.g*255);
        minimap[ind + 2] = (GLubyte)(color.b*255);
        minimap[ind + 3] = (GLubyte)(255);
    }
}

void RoboWar3DLegacy::drawMinimap() {
    if (minimap.empty())
        return;

    const GLfloat max = (field_width > field_height) ? (GLfloat)field_width : (GLfloat)field_height; // max field dimension size
    const GLfloat map_area_size = GLfloat(window_height*0.25f); // minimap area size
    const GLfloat scale = (max > map_area_size ? 1.0f : map_area_size / max); // scale if minimap is too small

    if(config.show_minimap > 1) {
        drawScreen(0, 0, (int)(field_width*scale) + 8, (int)(field_height*scale) + 8);
    }

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glPixelZoom(scale, scale);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glWindowPos2i(4, 4);
    glDrawPixels(field_width, field_height, GL_RGBA, GL_UNSIGNED_BYTE, &minimap[0]);

    glPixelZoom(1, 1);
    glDisable(GL_BLEND);
}

void RoboWar3DLegacy::drawScore() {    
    static void *font = SCORE_FONT;
    static const size_t font_height = getFontHeight(font);
    static const size_t inter = font_height/4;

    std::vector<std::string> score;
    getScore(teams, score);

    int width, height;
    getDimensions(score, inter, font, width, height);

    if(config.show_score > 1) {
        drawScreen(window_width - width - 20, window_height - height - font_height - 10, window_width, window_height);
    }

    for (size_t i = 0; i < score.size(); i++) {
        const glm::vec3 &color = getTeamColor(i);
        glColor3fv(&color[0]);
        drawString(window_width - width - 10, window_height - (i + 1)*(font_height + inter) - 6, score[i], font);
    }
}

void RoboWar3DLegacy::drawInfo() {    
    std::vector<std::string> info;    
    info.push_back(std::string("Iteration: ") + std::to_string(curr_iteration));
    info.push_back(std::string("Frames per animation: ") + std::to_string(config.frames_per_anim));
    info.push_back(std::string("Music: ") + (config.enable_music ? std::string("on") : std::string("off")));
    info.push_back(std::string("Sound: ") + (config.enable_sound ? std::string("on") : std::string("off")));
    info.push_back(""); // separator
    info.push_back("RoboWar v1.1");    
    info.push_back(std::string("Press ") + toString(key_bindings[KB_HELP]) + std::string(" for help"));
    info.push_back("Programmed by Georgy Schukin");

    static void *font = INFO_FONT;
    static const size_t font_height = getFontHeight(font);
    static const size_t inter = font_height/4;

    int width, height;
    getDimensions(info, inter, font, width, height);

    glColor3fv(INFO_COLOR);
    for (size_t i = 0; i < info.size(); i++) {
        drawString(window_width - width - 10, i*(font_height + inter) + 10, info[info.size() - i - 1], font);
    }
}

void RoboWar3DLegacy::drawHelp() {
    static std::vector<std::string> help;    

    static void *font = HELP_FONT;
    static const size_t font_height = getFontHeight(font);
    static const size_t inter = font_height/4;
    static int width = 0, height = 0;
    static bool inited = false;

    if(!inited) {        
        help.push_back(toString(key_bindings[KB_HELP]) + std::string(" - turn help screen on/off"));
        help.push_back(toString(key_bindings[KB_ANIM_DOWN]) + std::string(" - decrease frames per animation"));
        help.push_back(toString(key_bindings[KB_ANIM_UP]) + std::string(" - increase frames per animation"));
        help.push_back(toString(key_bindings[KB_MINIMAP]) + std::string(" - change minimap mode"));
        help.push_back(toString(key_bindings[KB_SCORE]) + std::string("- change score table mode"));
        help.push_back(toString(key_bindings[KB_INFO]) + std::string(" - switch info on/off"));
        help.push_back(toString(key_bindings[KB_MUSIC]) + std::string(" - turn music on/off"));
        help.push_back(toString(key_bindings[KB_SOUND]) + std::string(" - turn sound on/off"));
        help.push_back("Left mouse button - move/rotate camera left/right");
        help.push_back("Right mouse button - move/rotate camera up/down");
        help.push_back("Esc - exit");        

        getDimensions(help, inter, font, width, height);

        inited = true;
    }    

    drawScreen(0, window_height - height - font_height - 10, width + 20, window_height, 0.8f);
    for (size_t i = 0; i < help.size(); i++) {
        drawString(10, window_height - (i + 1)*(font_height + inter) - 6, help[i], font);
    }
}

void RoboWar3DLegacy::drawHUD() {
    glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_DEPTH_TEST);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix(); // push matrix for 3d
    glLoadIdentity();
    gluOrtho2D(0, window_width, 0, window_height); // set 2D

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glPointSize(1.0f);    

    if(config.show_minimap > 0)
        drawMinimap();
    if(config.show_score > 0)
        drawScore();
    if(config.show_info)
        drawInfo();
    if(config.show_help)
        drawHelp();

    glMatrixMode(GL_PROJECTION); // return 3d
    glPopMatrix();           
    glMatrixMode(GL_MODELVIEW); // return old modelview
    glPopMatrix();

    glEnable(GL_DEPTH_TEST);
    glPopAttrib();
}*/

}
