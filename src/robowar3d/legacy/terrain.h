#pragma once

#include "mygl/mygl.h"
#include "robowar3d/legacy/types.h"

namespace robowar3d {

void generateTerrain(const size_t &num_x, const size_t &num_y, const Rect3D &world);
void drawTerrain(const Rect3D &world);
}
