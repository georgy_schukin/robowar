#include "legacy/animation_list.h"

namespace robowar3d {

void AnimationList::process() {
    for(auto it = begin(); it != end();) {
        if ((*it)->isEnded())
            it = erase(it);
        else it++;
    }
    for(auto it = begin(); it != end(); it++) {
        (*it)->process();
    }
}

}
