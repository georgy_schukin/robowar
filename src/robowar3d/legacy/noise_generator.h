#pragma once

#include "mygl/mygl.h"

#include <vector>
#include <cstddef>

namespace robowar3d {

class NoiseGenerator {
public:
    NoiseGenerator();
    NoiseGenerator(size_t sx, size_t sy);
	~NoiseGenerator() {}

    const std::vector<float>& getNoise() const {
        return noise;
    }

    size_t getSizeX() const {
        return size_x;
    }

    size_t getSizeY() const {
        return size_y;
    }

    void init(size_t sx, size_t sy);
    void genPerlin(int oct_num, float persistence, bool circle=false); // gen perlin noise

protected:
    GLfloat interpolate(const GLfloat &a, const GLfloat &b, const GLfloat &x);
    //float Noise(int x, int y);
    std::vector<GLfloat> getSmoothNoise(int oct, const std::vector<GLfloat> &base); // generate smooth noise for octave
    //float InterpolatedNoise(float x, float y);

protected:
    std::vector<GLfloat> noise;
    size_t size_x;
    size_t size_y;
};

}
