#include "terrain_generator.h"

#include <cstdlib>
#include <algorithm>

namespace robowar3d {

TerrainGenerator::TerrainGenerator(const size_t &sx, const size_t &sy, const glm::vec2 &orig, const glm::vec2 &stp) :
    size_x(sx), size_y(sy), origin(orig), step(stp) {
    heights.resize(size_x*size_y);
}

void TerrainGenerator::genFault(const size_t &iter_num, const GLfloat &max_height) {
    const GLfloat min_height = max_height/iter_num;

    for (size_t iter = 0; iter < iter_num; iter++) { // for each iteration
        const GLfloat height = max_height + GLfloat(iter/iter_num)*(min_height - max_height);

        const GLfloat x1 = origin.x + (GLfloat(rand() % 10000)/10000.0f)*step.x*size_x; // generate two random points
        const GLfloat x2 = origin.x + (GLfloat(rand() % 10000)/10000.0f)*step.x*size_x;

        const GLfloat y1 = origin.y - (GLfloat(rand() % 10000)/10000.0f)*step.y*size_y;
        const GLfloat y2 = origin.y - (GLfloat(rand() % 10000)/10000.0f)*step.y*size_y;
	
        const GLfloat a = (y2 - y1);
        const GLfloat b = -(x2 - x1);
        const GLfloat c = -x1*(y2 - y1) + y1*(x2 - x1);

        for (size_t i = 0; i < size_y; i++) { // for each point in terrain
            const GLfloat y = origin.y - i*step.y;
            for (size_t j = 0; j < size_x; j++) {
                const GLfloat x = origin.x + j*step.x;
                GLfloat& h = heights[i*size_x + j];
				if (a*x + b*y - c > 0) {
					h += height; // add height
                } else {
					h -= height; // substract height
				}
			}
		}
	}
}

void TerrainGenerator::genFaultCircle(const size_t &iter_num, const GLfloat &max_height, const GLfloat &min_rad) {
    const GLfloat min_height = GLfloat(max_height/iter_num);

    const GLfloat min_r2 = min_rad*min_rad;
    //const GLfloat max_r2 = max_rad*max_rad;

    //const GLfloat coeff = mymath::PI/(max_r2 - min_r2);
    const GLfloat coeff_x = 0.0001f*step.x*size_x;
    const GLfloat coeff_y = 0.0001f*step.y*size_y;

    for (size_t iter = 0; iter < iter_num; iter++) { // for each iter
        const GLfloat height = max_height + GLfloat(iter/iter_num)*(min_height - max_height);

        const GLfloat x1 = origin.x + GLfloat(rand() % 10000)*coeff_x; // generate two random points
        const GLfloat x2 = origin.x + GLfloat(rand() % 10000)*coeff_x;

        const GLfloat y1 = origin.y - GLfloat(rand() % 10000)*coeff_y;
        const GLfloat y2 = origin.y - GLfloat(rand() % 10000)*coeff_y;
	
        const GLfloat a = (y2 - y1);
        const GLfloat b = -(x2 - x1);
        const GLfloat c = -x1*(y2 - y1) + y1*(x2 - x1);

        for (size_t i = 0; i < size_y; i++) { // for each point in terrain
            const GLfloat y = origin.y - i*step.y;
            for (size_t j = 0; j < size_x; j++) {
                const GLfloat x = origin.x + j*step.x;
                const GLfloat r2 = (x*x + y*y); // get radius

                GLfloat &h = heights[i*size_x + j];
                //GLfloat hgt = 0;
                /*if(r2 < min_r2) { // we are inside ring
					hgt = height*0.15f;
				}
                else if(r2 > max_r2) { // we are outside of ring
					hgt = height*0.15f;
				}
                else { // we are in ring
                    hgt = height*mymath::sin((r2 - min_r2)*coeff) + height*0.15f; // big change with max in ring center
                }*/

                //const GLfloat hgt = height*mymath::sin((r2 - min_r2)*coeff) + height*0.15f;
                if (r2 > min_r2) {
                    //const GLfloat hgt = height*mymath::sin((r2 - min_r2)) + height*0.15f;
                    if (a*x + b*y - c > 0) {
                    /*if (r2 < min_r2) {
                        const GLfloat hgt = height*mymath::sin((r2 - min_r2)*coeff) + height*0.15f;
                        h += glm::smoothstep(height*0.01f, height, hgt);
                    }
                    else*/ h += height; // add height
                    } else {
                        h -= height; // substract height
                    }
                } else {
                    //h = max_height*glm::sin(r2);
                    h = -0.1f;
                }
			}
		}
	}
}

void TerrainGenerator::normalize() { // lower or upper terrain to ground level
    const GLfloat h_min = *std::min_element(heights.begin(), heights.end());
    move(-h_min);
}

void TerrainGenerator::inverse() { // lower or upper terrain to ground level
    for (GLfloat &h: heights) {
        h = fabs(h);
    }
}

void TerrainGenerator::move(const GLfloat &height) {
    for (GLfloat &h: heights) {
        h += height;
    }
}

void TerrainGenerator::smooth(const GLfloat &coeff) {
    const GLfloat c2 = (1.0f - coeff)/8.0f;

    for (size_t i = 1; i < size_y - 1; i++)
    for (size_t j = 1; j < size_x - 1; j++) {
		heights[i*size_x + j] = coeff*heights[i*size_x + j] + c2*(heights[(i - 1)*size_x + j] + heights[(i + 1)*size_x + j] + heights[i*size_x + j - 1] +
			heights[i*size_x + j + 1] + heights[(i - 1)*size_x + j - 1] + heights[(i - 1)*size_x + j + 1] + 
			heights[(i + 1)*size_x + j - 1] + heights[(i + 1)*size_x + j + 1]);
	}
}

void TerrainGenerator::makeFlat(const size_t &start_x, const size_t &start_y, const size_t &end_x, const size_t &end_y, const GLfloat &value) {
    for (size_t i = start_y; i <= end_y; i++)
        for (size_t j = start_x; j <= end_x; j++) {
            heights[i*size_x + j] = value;
        }
}

void TerrainGenerator::getSmoothNormals(std::vector<glm::vec3> &normals) {
    std::vector<glm::vec3> face_normals;
    getFaceNormals(face_normals);

    normals.resize(size_x*size_y);

    const size_t faces_x = size_x - 1;
    const size_t faces_y = size_y - 1;
    for (size_t i = 1; i < size_y - 1; i++) // center points
        for (size_t j = 1; j < size_x - 1; j++) {
            normals[i*size_x + j] = glm::normalize(face_normals[i*faces_x + j] + face_normals[i*faces_x + j - 1] +
                    face_normals[(i - 1)*faces_x + j] + face_normals[(i - 1)*faces_x + j - 1]);
        }

    for (size_t j = 1; j < size_x - 1; j++) { // top and bottom rows
        normals[j] = glm::normalize(face_normals[j] + face_normals[j - 1]);
        normals[(size_y - 1)*size_x + j] = glm::normalize(face_normals[(faces_y - 1)*faces_x + j] + face_normals[(faces_y - 1)*faces_x + j - 1]);
    }

    for (size_t i = 1; i < size_y - 1; i++) { // left and right columns
        normals[i*size_x] = glm::normalize(face_normals[i*faces_x] + face_normals[(i - 1)*faces_x]);
        normals[i*size_x + size_x - 1] = glm::normalize(face_normals[i*faces_x + faces_x - 2] + face_normals[(i - 1)*faces_x + faces_x - 2]);
    }

    normals[0] = glm::normalize(face_normals[0]); // corners
    normals[size_x - 1] = glm::normalize(face_normals[faces_x - 1]);
    normals[(size_y - 1)*size_x] = glm::normalize(face_normals[(faces_y - 1)*faces_x]);
    normals[(size_y - 1)*size_x + size_x - 1] = glm::normalize(face_normals[(faces_y - 1)*faces_x + faces_x - 1]);
}

void TerrainGenerator::getFaceNormals(std::vector<glm::vec3> &face_normals) {
    face_normals.resize((size_x - 1)*(size_y - 1));
    for (size_t i = 0; i < size_y - 1; i++)
        for (size_t j = 0; j < size_x - 1; j++) {
            face_normals[i*(size_x - 1) + j] = glm::cross(glm::vec3(0, -step.y, heights[(i + 1)*size_x + j] - heights[i*size_x + j]),
                                                     glm::vec3(step.x, 0, heights[i*size_x + j + 1] - heights[i*size_x + j]));
        }
}

void TerrainGenerator::getVertices(std::vector<glm::vec3> &vertices) {
    vertices.resize(size_x*size_y);
    for (size_t i = 0; i < size_y; i++)
        for (size_t j = 0; j < size_x; j++) {
            vertices[i*size_x + j] = glm::vec3(origin.x + j*step.x, origin.y - i*step.y, heights[i*size_x + j]);
        }
}

glm::vec3 TerrainGenerator::getVertex(const size_t &x, const size_t &y) {
    return glm::vec3(origin.x + x*step.x, origin.y - y*step.y, heights[y*size_x + x]);
}

}
