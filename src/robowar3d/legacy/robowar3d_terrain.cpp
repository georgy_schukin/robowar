#include "robowar3d_legacy.h"
#include "terrain_generator.h"

namespace robowar3d {

const size_t TERRAIN_SIZE_X = 201;
const size_t TERRAIN_SIZE_Y = 201;
const size_t NUM_OF_TERRAIN_VERTICES = TERRAIN_SIZE_X*TERRAIN_SIZE_Y;
const size_t NUM_OF_TERRAIN_INDICES = (TERRAIN_SIZE_X - 1)*(TERRAIN_SIZE_Y - 1)*6; // 2 triangles (6 indices) for each quad

/*void RoboWar3DLegacy::initTerrain(GLuint data_vbo, GLuint indices_vbo) {
    const size_t width = TERRAIN_SIZE_X;
    const size_t height = TERRAIN_SIZE_Y;

    const glm::vec3 &beg = field_points[0]; // left top point
    const glm::vec3 &end = field_points[field_points.size() - 1]; // right bottom point

    const GLfloat max = std::max(fabs(end.x - beg.x + 8*hex_radius), fabs(beg.y - end.y - 8*hex_radius));
    const GLfloat quad_sz = (world.max_x - world.min_x)/(width - 1);
    const glm::vec2 origin(world.min_x, world.max_y);

    robowar3d::TerrainGenerator tgen(width, height, origin, glm::vec2(quad_sz, quad_sz));
    tgen.genFaultCircle(200, quad_sz*0.3f, (max*0.48f)*sqrt(2.0f)); // generate terrain by Fault alg
    tgen.smooth(0.8f);            

    std::vector<glm::vec3> terrain_vertices(NUM_OF_TERRAIN_VERTICES); // array of vertices and normals
    std::vector<glm::vec3> terrain_normals(NUM_OF_TERRAIN_VERTICES); // array of vertices and normals
    std::vector<glm::vec2> terrain_uvs(NUM_OF_TERRAIN_VERTICES, glm::vec2(0.0f, 0.0f)); //terrain texture coords
    std::vector<GLushort> terrain_indices(NUM_OF_TERRAIN_INDICES);

    tgen.getVertices(terrain_vertices);
    tgen.getSmoothNormals(terrain_normals);    

    const GLfloat scale = 5.0f/world.max_x;
    for (size_t i = 0; i < height; i++)
    for (size_t j = 0; j < width; j++) {
        const size_t ind = i*width + j;
        if (j > 0) {
            terrain_uvs[ind].x = terrain_uvs[ind - 1].x + scale*quad_sz;//scale*glm::length(terrain_vertices[ind] - terrain_vertices[ind - 1]);
        }
        if (i > 0) {
            terrain_uvs[ind].y = terrain_uvs[ind - width].y + scale*quad_sz;//scale*glm::length(terrain_vertices[ind] - terrain_vertices[ind - width]);
        }
    }

    size_t ind2 = 0;
    for (size_t i = 0; i < height - 1; i++) // init indexes
    for (size_t j = 0; j < width - 1; j++) {
        const size_t ind = i*width + j;
        // first triangle
        terrain_indices[ind2++] = GLushort(ind); // [i, j] point
        terrain_indices[ind2++] = GLushort(ind + 1); // [i, j + 1] point
        terrain_indices[ind2++] = GLushort(ind + width); // [i + 1, j] point

        // second triangle
        terrain_indices[ind2++] = GLushort(ind + 1); // [i, j + 1] point
        terrain_indices[ind2++] = GLushort(ind + width + 1); // [i + 1, j + 1] point
        terrain_indices[ind2++] = GLushort(ind + width); // [i + 1, j] point
    }

    glBindBuffer(GL_ARRAY_BUFFER, data_vbo); // bind data buffer
    glBufferData(GL_ARRAY_BUFFER, NUM_OF_TERRAIN_VERTICES*(2*sizeof(glm::vec3) + sizeof(glm::vec2)), NULL, GL_STATIC_DRAW); // alloc buffer
    glBufferSubData(GL_ARRAY_BUFFER, 0, NUM_OF_TERRAIN_VERTICES*sizeof(glm::vec3), &terrain_vertices[0]); // load vertices
    glBufferSubData(GL_ARRAY_BUFFER, NUM_OF_TERRAIN_VERTICES*sizeof(glm::vec3), NUM_OF_TERRAIN_VERTICES*sizeof(glm::vec3), &terrain_normals[0]); // load normals
    glBufferSubData(GL_ARRAY_BUFFER, NUM_OF_TERRAIN_VERTICES*2*sizeof(glm::vec3), NUM_OF_TERRAIN_VERTICES*sizeof(glm::vec2), &terrain_uvs[0]); // load texture coords

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_vbo); // bind index buffer
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, NUM_OF_TERRAIN_INDICES*sizeof(GLushort), &terrain_indices[0], GL_STATIC_DRAW); // load index array

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void RoboWar3DLegacy::drawTerrain(GLuint data_vbo, GLuint indices_vbo, GLuint texture) {    
    //glEnable(GL_TEXTURE_GEN_S); // set tex coord auto generation
    //glEnable(GL_TEXTURE_GEN_T);

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);*/

/*    const GLfloat scale = 5.0f/world.max_x;

    const GLfloat param_s[4] = {scale, 0.0f, 0.0f, 0.0f};
    const GLfloat param_t[4] = {0.0f, scale, 0.0f, 0.0f};

    glTexGenfv(GL_S, GL_OBJECT_PLANE, param_s);
    glTexGenfv(GL_T, GL_OBJECT_PLANE, param_t);
    glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    glTexGenf(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);*/

    /*glColor3f(0.9f, 0.9f, 0.9f); // set base under color for terrain

    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glBindBuffer(GL_ARRAY_BUFFER, data_vbo); // bind data buffer
    glVertexPointer(3, GL_FLOAT, 0, 0);
    glNormalPointer(GL_FLOAT, 0, (GLvoid*)(NUM_OF_TERRAIN_VERTICES*sizeof(glm::vec3)));
    glTexCoordPointer(2, GL_FLOAT, 0, (GLvoid*)(NUM_OF_TERRAIN_VERTICES*2*sizeof(glm::vec3)));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_vbo); // bind index buffer
    glDrawElements(GL_TRIANGLES, NUM_OF_TERRAIN_INDICES, GL_UNSIGNED_SHORT, 0); // draw terrain

    glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    //glDisable(GL_TEXTURE_GEN_S);
    //glDisable(GL_TEXTURE_GEN_T);
}*/

}
