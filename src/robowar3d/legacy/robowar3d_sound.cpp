#include "robowar3d_legacy.h"

#include <cstdlib>
#include <algorithm>

namespace robowar3d {

const size_t EVENT_FREQ = 10;

/*void RoboWar3DLegacy::detectEvents(std::vector<std::vector<size_t> > &events) {
    events.resize(teams.size(), std::vector<size_t>(SoundLibrary::NUM_OF_EVENTS, 0));

    for (const robovis::RobotData &robot: robots) {
        if (robot.is_shooting) {
            events[robot.team][SoundLibrary::ET_SHOOT]++;
        }
        if (robot.is_moving) {
            events[robot.team][SoundLibrary::ET_MOVE]++;
        }
        if (robot.is_transmitting) {
            events[robot.team][SoundLibrary::ET_TRANSMIT]++;
        }
        if (robot.damage >= robot.hitpoints) { // robot is dead
            events[robot.team][SoundLibrary::ET_DEATH]++;
        }
    }

    int winner = -1;
    bool victory = false;
    for (size_t i = 0; i < teams.size(); i++) {
        const robovis::TeamData &team = teams[i];
        if ((team.num_of_bases == 0) && (team.num_of_robots == 0)) { // team is defeated
            if (!team_is_defeated[i]) { // not defeated yet
                events[i][SoundLibrary::ET_DEFEAT]++;
                team_is_defeated[i] = true; // set defeat flag
            }
        } else { // detect victory
            if (winner == -1) {
                winner = i;
                victory = true;
            } else {
                victory = false;
            }
        }
    }
    if (victory) {
        events[winner][SoundLibrary::ET_VICTORY]++;
    }
}

void RoboWar3DLegacy::playSounds() {
    std::vector<std::vector<size_t> > events;
    detectEvents(events);

    std::vector<size_t> total(SoundLibrary::NUM_OF_EVENTS, 0);
    for (size_t e = 0; e < total.size(); e++) {
        for (size_t i = 0; i < events.size(); i++)
            total[e] += events[i][e];
    }

    for (size_t e = 0; e < SoundLibrary::NUM_OF_EVENTS; e++) {
        for (size_t i = 0; i < teams.size(); i++) {
            const size_t &num_of_events = events[i][e];
            if (num_of_events > 0) {
                const size_t freq = ((e == SoundLibrary::ET_DEFEAT) || (e == SoundLibrary::ET_VICTORY)) ? 1 : EVENT_FREQ*total[e];
                if (rand() % freq == 0) {
                    sounds.playRandomSound(teams[i].script_name, (SoundLibrary::EventType)e); // play always
                }
            }
        }
    }
}
*/
}
