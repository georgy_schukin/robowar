#pragma once

#include "animation.h"

#include <list>

namespace robowar3d {

class AnimationList : public std::list<AnimationPtr> {
public:
    AnimationList() {}
    ~AnimationList() {}

    void process();
};

}
