#pragma once

#include "mygl/mygl.h"
#include "robowar3d/legacy/types.h"

namespace robowar3d {

class WorldData {
public:
    Rect3D world_rect;
    GLfloat hex_radius;
};

}
