#pragma once

#include "mygl/mygl.h"
#include <vector>
#include <cstddef>

namespace robowar3d {

/**
 * Generates terrain as height map.
 */
class TerrainGenerator  {
protected:
    std::vector<GLfloat> heights; // use only heights for terrain grid
    size_t size_x; // num of points by x
    size_t size_y; // num of points by y
    glm::vec2 origin; // top left point
    glm::vec2 step; // steps by X and Y

public:
    TerrainGenerator(const size_t &sx, const size_t &sy, const glm::vec2 &orig, const glm::vec2 &stp);
	~TerrainGenerator() {}

    const std::vector<GLfloat>& getHeights() const {
        return heights;
    }

    void genFault(const size_t &iter_num, const GLfloat &max_height); // gen with Fault algorithm
    void genFaultCircle(const size_t &iter_num, const GLfloat &max_height, const GLfloat &min_rad); // gen circle terrain with Fault algorithm
    void normalize(); // lower to min height
    void inverse(); // conv negative height to positive
    void move(const GLfloat &height); // conv negative height to positive
    void smooth(const GLfloat &coeff);
    void makeFlat(const size_t &start_x, const size_t &start_y, const size_t &end_x, const size_t &end_y, const GLfloat &value);

    size_t getSizeX() const {
        return size_x;
    }

    size_t getSizeY() const {
        return size_y;
    }

    void getSmoothNormals(std::vector<glm::vec3> &normals);
    void getFaceNormals(std::vector<glm::vec3> &face_normals);

    void getVertices(std::vector<glm::vec3> &vertices);
    glm::vec3 getVertex(const size_t &x, const size_t &y);

    /*float StepX() const {return step_x;}
	float StepY() const {return step_y;}
    const Point2D& Origin() const {return begin;}*/
};

}
