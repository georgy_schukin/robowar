#pragma once

#include <vector>
#include <cmath>

using namespace std;

class Point2D // 2D point
{
public:
	float x;
	float y;	

public:
	Point2D() : x(0), y(0) {}
	Point2D(float xx, float yy) : x(xx), y(yy) {}			
	Point2D(const Point2D& p) : x(p.x), y(p.y) {}			

	void Set(float xx, float yy)
	{
		x = xx;
		y = yy;	
	}
	void Set(const Point2D& p)
	{
		x = p.x;
		y = p.y;
	}
};

class Vector2D : public Point2D // 2D vector
{
public:
	Vector2D() {}
	Vector2D(float xx, float yy) : Point2D(xx, yy) {}			
	Vector2D(const Point2D& p) : Point2D(p.x, p.y) {}
	Vector2D(const Vector2D& v) : Point2D(v.x, v.y) {}	

	/*void Set(const Vector2D& v)
	{
		x = v.x;
		y = v.y;
	}*/

	float Length() const {return sqrt(x*x + y*y);}
};

class Point3D // 3D point
{
public:
	float x;
	float y;
	float z;

public:
	Point3D() : x(0), y(0), z(0) {}
	Point3D(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {}			
	Point3D(const Point3D& p) : x(p.x), y(p.y), z(p.z) {}			

	void Set(float xx, float yy, float zz)
	{
		x = xx;
		y = yy;
		z = zz;
	}
	void Set(const Point3D& p)
	{
		x = p.x;
		y = p.y;
		z = p.z;
	}
};

class Vector3D : public Point3D // 3D vector
{
public:
	Vector3D() {}
	Vector3D(float xx, float yy, float zz) : Point3D(xx, yy, zz) {}			
	Vector3D(const Point3D& p) : Point3D(p) {}		
	Vector3D(const Vector3D& v) : Point3D(v.x, v.y, v.z) {}		

	/*void Set(const Vector3D& v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
	}*/

	float Length() const {return sqrt(x*x + y*y + z*z);}
};

class Rect2D // 2d rectangle
{
public:
	float left;
	float right;
	float top;
	float bottom;

public:
	Rect2D() : left(0), right(0), top(0), bottom(0) {}

	void Set(float l ,float r, float t, float b)
	{
		left = l;
		right = r;
		top = t;
		bottom = b;
	}
};

struct Rect3D // 3D rectangle
{
	float min_x;
	float max_x;
	float min_y;
	float max_y;
	float min_z;
	float max_z;
		Rect3D()
		{
			Set(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
		}
		void Update(Point3D& point)
		{
			if(min_x > point.x) min_x = point.x;
			if(max_x < point.x) max_x = point.x;
			if(min_y > point.y) min_y = point.y;
			if(max_y < point.y) max_y = point.y;
			if(min_z > point.z) min_z = point.z;
			if(max_z < point.z) max_z = point.z;
		}
		void Set(float min_xx, float max_xx, float min_yy, float max_yy, float min_zz, float max_zz)
		{
			min_x = min_xx;
			max_x = max_xx;
			min_y = min_yy;
			max_y = max_yy;
			min_z = min_zz;
			max_z = max_zz;
		}
};



