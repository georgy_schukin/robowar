#include "robowar3d_legacy.h"

namespace robowar3d {

const size_t NUM_OF_SKYBOX_FACE_VERTICES = 4; // quad - 4 vertices
const size_t NUM_OF_SKYBOX_VERTICES = 24; // 6 sides, 4 vertices each
const size_t NUM_OF_SKYBOX_INDICES = 6; // 6 indices for 6 sides

/*void RoboWar3DLegacy::initSkyBox(GLuint data_vbo, GLuint indices_vbo) {
    const glm::vec3 points[] = {glm::vec3(world.min_x, world.max_y, world.max_z), // top points
                                glm::vec3(world.max_x, world.max_y, world.max_z),
                                glm::vec3(world.max_x, world.min_y, world.max_z),
                                glm::vec3(world.min_x, world.min_y, world.max_z),
                                glm::vec3(world.min_x, world.max_y, world.min_z),
                                glm::vec3(world.max_x, world.max_y, world.min_z), // bottom points
                                glm::vec3(world.max_x, world.min_y, world.min_z),
                                glm::vec3(world.min_x, world.min_y, world.min_z)};

    const glm::vec3 vertices[] = {points[2], points[1], points[0], points[3],
                                  points[0], points[1], points[5], points[4],
                                  points[1], points[2], points[6], points[5],
                                  points[2], points[3], points[7], points[6],
                                  points[3], points[0], points[4], points[7],
                                  points[6], points[5], points[4], points[7]};

    const GLfloat uvs[] = {0.0f, 0.0f,
                           1.0f, 0.0f,
                           1.0f, 1.0f,
                           0.0f, 1.0f}; // tex coords equal for all sides

    const GLubyte indices[] = {0, 1, 2,
                               0, 2, 3};


    glBindBuffer(GL_ARRAY_BUFFER, data_vbo); // bind data buffer
    glBufferData(GL_ARRAY_BUFFER, NUM_OF_SKYBOX_VERTICES*sizeof(glm::vec3) + NUM_OF_SKYBOX_FACE_VERTICES*2*sizeof(GLfloat), NULL, GL_STATIC_DRAW); // alloc buffer
    glBufferSubData(GL_ARRAY_BUFFER, 0, NUM_OF_SKYBOX_VERTICES*sizeof(glm::vec3), &vertices[0]); // load vertices
    glBufferSubData(GL_ARRAY_BUFFER, NUM_OF_SKYBOX_VERTICES*sizeof(glm::vec3), NUM_OF_SKYBOX_FACE_VERTICES*2*sizeof(GLfloat), uvs); // load tex coords

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_vbo); // bind index buffer
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, NUM_OF_SKYBOX_INDICES*sizeof(GLubyte), indices, GL_STATIC_DRAW); // load index array

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void RoboWar3DLegacy::drawSkyBox(GLuint data_vbo, GLuint indices_vbo, const std::vector<GLuint> &cubemap) {
    glDisable(GL_DEPTH_TEST);
    glColor3f(1.0f, 1.0f, 1.0f);

    glEnableClientState(GL_VERTEX_ARRAY);    
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glBindBuffer(GL_ARRAY_BUFFER, data_vbo); // bind data buffer        
    glTexCoordPointer(2, GL_FLOAT, 0, (GLvoid*)(NUM_OF_SKYBOX_VERTICES*sizeof(glm::vec3)));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_vbo); // bind index buffer
    for (size_t i = 0; i < 6; i++) {
        glVertexPointer(3, GL_FLOAT, 0, (GLvoid*)(i*NUM_OF_SKYBOX_FACE_VERTICES*sizeof(glm::vec3)));
        glBindTexture(GL_TEXTURE_2D, cubemap[i]); // bind next cubemap face texture
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        glDrawElements(GL_TRIANGLES, NUM_OF_SKYBOX_INDICES, GL_UNSIGNED_BYTE, 0); // draw skybox face quad
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glDisableClientState(GL_VERTEX_ARRAY);    
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    glEnable(GL_DEPTH_TEST);
}

const size_t NUM_OF_LAVA_VERTICES = 4; // lava quad
const size_t NUM_OF_LAVA_INDICES = 6; // 2 triangles

void RoboWar3DLegacy::initLava(GLuint data_vbo, GLuint indices_vbo) {
    const GLfloat vertices[] = {world.min_x, world.min_y, 0.0f,
                                world.min_x, world.max_y, 0.0f,
                                world.max_x, world.max_y, 0.0f,
                                world.max_x, world.min_y, 0.0f};

    const GLfloat normals[] = {0.0f, 0.0f, 1.0f,
                              0.0f, 0.0f, 1.0f,
                              0.0f, 0.0f, 1.0f,
                              0.0f, 0.0f, 1.0f}; // all up

    const GLfloat uvs[] = {0.0f, 0.0f,
                           0.0f, 15.0f,
                           15.0f, 15.0f,
                           15.0f, 0.0f};

    const GLubyte indices[] = {0, 1, 2,
                               0, 2, 3};

    glBindBuffer(GL_ARRAY_BUFFER, data_vbo); // bind data buffer
    glBufferData(GL_ARRAY_BUFFER, NUM_OF_LAVA_VERTICES*8*sizeof(GLfloat), NULL, GL_STATIC_DRAW); // alloc buffer
    glBufferSubData(GL_ARRAY_BUFFER, 0, NUM_OF_LAVA_VERTICES*3*sizeof(GLfloat), vertices); // load vertices
    glBufferSubData(GL_ARRAY_BUFFER, NUM_OF_LAVA_VERTICES*3*sizeof(GLfloat), NUM_OF_LAVA_VERTICES*3*sizeof(GLfloat), normals); // load normals
    glBufferSubData(GL_ARRAY_BUFFER, NUM_OF_LAVA_VERTICES*6*sizeof(GLfloat), NUM_OF_LAVA_VERTICES*2*sizeof(GLfloat), uvs); // load tex coords

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_vbo); // bind index buffer
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, NUM_OF_LAVA_INDICES*sizeof(GLubyte), indices, GL_STATIC_DRAW); // load index array

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void RoboWar3DLegacy::drawLava(GLuint data_vbo, GLuint indices_vbo, GLuint texture) {    
    glColor3f(1.0f, 1.0f, 1.0f);

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glBindBuffer(GL_ARRAY_BUFFER, data_vbo); // bind data buffer
    glVertexPointer(3, GL_FLOAT, 0, 0);
    glNormalPointer(GL_FLOAT, 0, (GLvoid*)(NUM_OF_LAVA_VERTICES*3*sizeof(GLfloat)));
    glTexCoordPointer(2, GL_FLOAT, 0, (GLvoid*)(NUM_OF_LAVA_VERTICES*6*sizeof(GLfloat)));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_vbo); // bind index buffer
    glDrawElements(GL_TRIANGLES, NUM_OF_LAVA_INDICES, GL_UNSIGNED_BYTE, 0); // draw lava

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);    
}*/

}
