#include "noise_generator.h"

#include <cmath>
#include <cstdlib>

namespace robowar3d {

namespace {
    const float PI = 3.1415926535897f;
}

NoiseGenerator::NoiseGenerator() :
    size_x(0), size_y(0) {
}

NoiseGenerator::NoiseGenerator(size_t sx, size_t sy) {
    init(sx, sy);
}

void NoiseGenerator::init(size_t sx, size_t sy) {
    size_x = sx;
    size_y = sy;
	noise.resize(size_x*size_y);
}

/*float NoiseGenerator::Noise(int x, int y) // gen random noise
{
    const int nn = x + y*57;		
	const int n = (nn << 13)^nn;
return (1.0f - float((n * (n*n*15731 + 789221) + 1376312589) & 0x7fffffff)/1073741824.0f);
}*/

GLfloat NoiseGenerator::interpolate(const GLfloat &a, const GLfloat &b, const GLfloat &x) {
    const GLfloat f = (1.0f - cos(x*PI))*0.5f;
    return a*(1.0f - f) + b*f;
}
 
/*float NoiseGenerator::SmoothedNoise(int x, int y)
{
    const float corners = (Noise(x - 1, y - 1) + Noise(x + 1, y - 1) + Noise(x - 1, y + 1) + Noise(x + 1, y + 1))/16.0f;
    const float sides = (Noise(x - 1, y) + Noise(x + 1, y) + Noise(x, y - 1) + Noise(x, y + 1))/8.0f;
    const float center = Noise(x, y)/4.0f;	
return corners + sides + center;
}

float NoiseGenerator::InterpolatedNoise(float x, float y)
{	
	const float v1 = SmoothedNoise(int(x), int(y));
	const float v2 = SmoothedNoise(int(x) + 1, int(y));
	const float v3 = SmoothedNoise(int(x), int(y) + 1);
	const float v4 = SmoothedNoise(int(x) + 1, int(y) + 1);

	const float i1 = Interpolate(v1, v2, x - int(x));
	const float i2 = Interpolate(v3, v4, x - int(x));
return Interpolate(i1, i2, y - int(y));
}*/

std::vector<GLfloat> NoiseGenerator::getSmoothNoise(int oct, const std::vector<GLfloat> &base) {
   std::vector<GLfloat> result(size_x*size_y);
   const size_t period = 1 << oct; // calculates 2 ^ k
   const GLfloat freq = 1.0f/period;

   for (size_t i = 0; i < size_y; i++) {
      const size_t i0 = size_t(i/period)*period; //calculate the horizontal sampling indices
      const size_t i1 = (i0 + period) % size_y; //wrap around
      const GLfloat hor = (i - i0)*freq;
 
      for (size_t j = 0; j < size_x; j++) {
         const size_t j0 = size_t(j/period)*period; //calculate the vertical sampling indices
         const size_t j1 = (j0 + period) % size_x; //wrap around
         const GLfloat vert = (j - j0)*freq;
          
         const GLfloat top = interpolate(base[i0*size_x + j0], base[i1*size_x + j0], hor); //blend the top two corners
         const GLfloat bottom = interpolate(base[i0*size_x + j1], base[i1*size_x + j1], hor); //blend the bottom two corners
         result[i*size_x + j] = interpolate(top, bottom, vert); //final blend
      }
   }
   return result;
}

void NoiseGenerator::genPerlin(int oct_num, float persistence, bool circle) {
    std::vector<GLfloat> base_noise(size_x*size_y);

    const size_t size = size_x*size_y;

    if (!circle) {
        for (size_t i = 0; i < size; i++) { // gen random noise
            base_noise[i] = GLfloat(rand() % 10000)*0.0001f; // from 0 to 1
		}
    } else { // gen circle chaped noise
        const size_t r2_max = size_x*size_x/4; // max rad
        for (size_t i = 0; i < size_y; i++) // gen random circle noise
        for (size_t j = 0; j < size_x; j++) { // gen random circle noise
            const size_t x = size_x/2 - j;
            const size_t y = size_y/2 - i;
            const size_t r2 = x*x + y*y; // dist to center
			base_noise[i*size_x + j] = (r2 <= r2_max) ? GLfloat(rand() % 10000)*0.0001f : 0.0f;
		}
	}

    for (size_t i = 0; i < size; i++) { // init noise
		noise[i] = 0.0f;
	}

    GLfloat amplitude = 1.0f;
    GLfloat total_amp = 0.0f;   
    for (int oct = oct_num - 1; oct >= 0; oct--) { // for each octave
		amplitude *= persistence;
		total_amp += amplitude;		
        const auto smooth = getSmoothNoise(oct, base_noise); // gen smooth noise for octave
        for (size_t i = 0; i < size; i++) { // add octave to final res
			noise[i] += smooth[i]*amplitude;
		}
	}

    const GLfloat coeff = 1.0f/total_amp;
    for (size_t i = 0; i < size; i++) { // normalize
         noise[i] *= coeff; 
	}
}

}
