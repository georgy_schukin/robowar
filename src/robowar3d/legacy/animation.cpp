#include "animation.h"

namespace robowar3d {

void Animation::process() {
    if (!isEnded())
        curr_frame++;
}

void MoveAnimation::process() {
    if (isActive() && !isEnded()) {
        curr_pos += move;
    }
    Animation::process();
}

/*void HitAnimation::process() {
    if (curr_frame >= start_frame) {
        for (unsigned int i = 0;i < particles.size();i++) {
            particles[i].x += p_moves[i].x;
            particles[i].y += p_moves[i].y;
            particles[i].z += p_moves[i].z;
            p_rads[i] *= 0.8f; // decrease radius
        }
    }
    curr_frame++;
}

void ParticleAnimation::process() {
    //if(p.curr_frame >= p.start_frame) // always start with 0 - may skip check
    {
        curr_pos.x += move.x;
        curr_pos.y += move.y;
        curr_pos.z += move.z;
        size += size_dec;
        transparency += trnp_dec;
    }
    curr_frame++;
}

void HitAnimation::Generate(const Point3D& pos, int p_num, float max_rad, float max_len)
{
	for(int i = 0;i < p_num;i++)
	{
        const float l = max_len*(1 - (float(rand() % 4)/8.0f))/float(end_frame - start_frame);
		const float teta = (float(rand() % p_num)/float(p_num))*PI;
		const float phi = (float(i)/float(p_num))*2*PI;

		particles.push_back(pos);
        p_moves.push_back(Point3D(l*mymath::sin(teta)*mymath::cos(phi),
                                  l*mymath::sin(teta)*mymath::sin(phi),
                                  l*mymath::cos(teta)));
		p_rads.push_back(max_rad*float(rand() % 5)*0.2f);
	}
}*/

}
