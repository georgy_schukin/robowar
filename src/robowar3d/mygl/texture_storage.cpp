#include "mygl/texture_storage.h"
#include <vector>

namespace mygl {

void TextureStorage::alloc(const size_t &size) {
    texture_ids.resize(size);
    glGenTextures(size, &texture_ids[0]);
    for (size_t i = 0; i < size; i++) {
        textures.push_back(Texture(texture_ids[i]));
    }
}

void TextureStorage::clear() {
    textures.clear();
    glDeleteTextures(texture_ids.size(), &texture_ids[0]);
    texture_ids.clear();
}

void TextureStorage::load(const size_t &num, const char *filename) {
    textures[num].loadFromFile(filename);
}

}
