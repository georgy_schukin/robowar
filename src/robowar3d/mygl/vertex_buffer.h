#pragma once

#include "mygl/mygl.h"

namespace mygl {

class VertexBuffer {
protected:
    GLuint buffer_id;
    GLenum target;
    GLsizei num_of_elements;
    GLsizei element_size;

public:
    VertexBuffer(const GLenum &trgt);
    ~VertexBuffer();

    GLuint getId() const {
        return buffer_id;
    }        

    GLsizei getNumOfElements() const {
        return num_of_elements;
    }

    GLsizei getElementSize() const {
        return element_size;
    }        

    void bind();
    void unbind();

    void loadData(GLsizei num_of_elems, GLsizei elem_size, GLvoid *data, GLenum usage);
};

}
