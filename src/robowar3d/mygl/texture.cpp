#include "mygl/texture.h"
#include <SOIL/SOIL.h>
#include <iostream>

namespace mygl {

Texture::Texture(const GLuint &id, const GLenum &trgt) :
    texture_id(id), target(trgt), width(0), height(0), min_filter(GL_LINEAR), mag_filter(GL_LINEAR) {
    //glGenTextures(1, &texture_id);
}

Texture::~Texture() {
    //if (glIsTexture(texture_id) == GL_TRUE)
      //  glDeleteTextures(1, &texture_id);
}

int Texture::loadFromFile(const char *filename) {
    unsigned char *image = SOIL_load_image(filename, &width, &height, 0, SOIL_LOAD_RGBA);

    if (!image) {
        std::cerr << "Error: can't open texture file " << filename << std::endl;
        return -1;
    }

    glBindTexture(target, texture_id);
    glTexImage2D(target, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(target);
    glBindTexture(target, 0);

    SOIL_free_image_data(image);
    return 0;
}

int Texture::loadCubemapFromFiles(const char **files, const size_t &files_num) {
    if (files_num < 6) {
        std::cerr << "Error: at least 6 files required for cubemap" << std::endl;
        return -1;
    }

    static GLenum cubemap_targets[] = {GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                                       GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
                                       GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                                       GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                                       GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                                       GL_TEXTURE_CUBE_MAP_NEGATIVE_Z};

    glBindTexture(target, texture_id);
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(target, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    for (size_t i = 0; i < 6; i++) {
        unsigned char *image = SOIL_load_image(files[i], &width, &height, 0, SOIL_LOAD_RGBA);
        if (!image) {
            std::cerr << "Error: can't open cubemap file " << files[i] << std::endl;
            glBindTexture(target, 0);
            return -1;
        }
        glTexImage2D(cubemap_targets[i], 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
        SOIL_free_image_data(image);
    }
    glBindTexture(target, 0);
    return 0;
}

void Texture::loadFromMemory(const GLuint &width, const GLuint &height, const GLfloat *data) {
    this->width = width;
    this->height = height;

    glBindTexture(target, texture_id);
    glTexImage2D(target, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(target);
    glBindTexture(target, 0);
}

void Texture::bind() {
    glBindTexture(target, texture_id);
}

void Texture::unbind() {
    glBindTexture(target, 0);
}

}
