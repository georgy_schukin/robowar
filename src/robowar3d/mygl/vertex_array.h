#pragma once

#include "mygl/mygl.h"

namespace mygl {

class VertexArray {
protected:
    GLuint array_id;

public:
    VertexArray();
    ~VertexArray();

    GLuint getId() const {
        return array_id;
    }

    void bind();
    void unbind();
};

}
