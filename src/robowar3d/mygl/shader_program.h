#pragma once

#include "mygl/mygl.h"
#include <list>
#include <string>

namespace mygl {

class ShaderProgram {
private:
    GLuint program_id;
    std::list<GLuint> shaders;

public:
    ShaderProgram();
    ~ShaderProgram();

    GLuint getId() const {
        return program_id;
    }

    void attachShader(GLuint shader);
    void attachShader(const std::string &file, GLenum type);
    void detachShader(GLuint shader);

    int link();
    bool isLinked();

    void use();

    void setUniformMat4(const std::string &var, const glm::mat4 &matrix);
    void setVertexAttribMat4(const std::string &var, GLuint start = 0, GLuint divisor = 0);
    void bindAttribLocation(const std::string &attrib, GLuint index);
    GLint getAttribLocation(const std::string &attrib);
};

}
