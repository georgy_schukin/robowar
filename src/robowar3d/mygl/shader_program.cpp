#include "mygl/shader_program.h"
#include "mygl/shader_library.h"
#include <vector>
#include <string>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>

namespace mygl {

ShaderProgram::ShaderProgram() {
    program_id = glCreateProgram();
}

ShaderProgram::~ShaderProgram() {
    for (std::list<GLuint>::iterator it = shaders.begin(); it != shaders.end(); it++) {
        glDetachShader(program_id, *it);
    }
    if (glIsProgram(program_id == GL_TRUE))
        glDeleteProgram(program_id);
}

void ShaderProgram::attachShader(GLuint shader) {
    glAttachShader(program_id, shader);
    shaders.push_back(shader);
}

void ShaderProgram::attachShader(const std::string &file, GLenum type) {
    ShaderLibrary *lib = ShaderLibrary::getInstance();
    ShaderPtr shader = lib->getShader(file, type);
    if (shader.get()) {
        attachShader(shader->getId());
    }
}

void ShaderProgram::detachShader(GLuint shader) {
   for (std::list<GLuint>::iterator it = shaders.begin(); it != shaders.end();) {
       if (*it == shader) {
           glDetachShader(program_id, shader);
           shaders.erase(it);
           break;
       } else {
           it++;
       }
   }
}

int ShaderProgram::link() {
    glLinkProgram(program_id);

    if (!isLinked()) {
        GLint max_length = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &max_length);
        GLint log_length = 0;
        std::vector<GLchar> info_log(max_length);
        glGetProgramInfoLog(program_id, max_length, &log_length, &info_log[0]);
        std::cerr << "Shader program compilation error:\n" << std::string(&info_log[0], log_length) << std::endl;
        return -1;
    }
    return 0;
}

bool ShaderProgram::isLinked() {
    GLint is_linked = 0;
    glGetProgramiv(program_id, GL_LINK_STATUS, &is_linked);
    return (is_linked == GL_TRUE);
}

void ShaderProgram::use() {
    glUseProgram(program_id);
}

void ShaderProgram::setUniformMat4(const std::string &var, const glm::mat4 &matrix) {
    const auto location = glGetUniformLocation(program_id, var.c_str());
    if (location == -1) {
        std::cerr << "Shader program error: can't find uniform " << var << std::endl;
    } else {
        glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
    }
}

void ShaderProgram::bindAttribLocation(const std::string &attrib, GLuint index) {
    glBindAttribLocation(program_id, index, attrib.c_str());
}

GLint ShaderProgram::getAttribLocation(const std::string &attrib) {
    return glGetAttribLocation(program_id, attrib.c_str());
}

void ShaderProgram::setVertexAttribMat4(const std::string &var, GLuint start, GLuint divisor) {
    const auto location = getAttribLocation(var);
    if (location == -1) {
        std::cerr << "Shader program error: can't find vertex attrib " << var << std::endl;
    } else {
        for (GLuint i = 0; i < 4; i++) {
            const GLuint index = location + i;
            glEnableVertexAttribArray(index);
            glVertexAttribPointer(index, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)(start + i*sizeof(glm::vec4)));
            if (divisor > 0)
                glVertexAttribDivisor(index, divisor);
        }
    }
}

}
