#pragma once

#include "mygl/model_data.h"

namespace mygl {

/**
 * Loads model data from .obj files.
 */
class OBJLoader {
public:
    static int load(const char *filename, ModelData &data);
};

}
