#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#ifdef	_WIN32
    #include <GL/wglew.h>
#else
    #include <GL/glxew.h>
#endif

//#include <GLFW/glfw3.h>

#include <GL/freeglut.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
