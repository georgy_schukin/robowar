#pragma once

#include "mygl/shader.h"
#include <string>
#include <map>

namespace mygl {

class ShaderLibrary {
private:
    typedef std::map<std::string, ShaderPtr> ShaderMap;

private:
    std::string prefix;
    ShaderMap shaders;

public:
    ShaderLibrary() : prefix("") {}
    ~ShaderLibrary() {}

    void setFilePrefix(const std::string &pref) {
        this->prefix = pref;
    }

    ShaderPtr getShader(const std::string &file, GLenum type);

    static ShaderLibrary* getInstance();
};

}
