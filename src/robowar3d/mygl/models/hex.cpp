#include "mygl/models/hex.h"

#include <glm/gtc/constants.hpp>

namespace mygl {

Hex::Hex(const GLfloat &rad) : radius(rad),
    vertices_vbo(GL_ARRAY_BUFFER), indices_vbo(GL_ELEMENT_ARRAY_BUFFER) {
    for (size_t i = 0; i < 360; i += 60) {
        points.push_back(glm::vec3(radius*glm::sin(glm::radians(float(i))), 0.0f,
                                   radius*glm::cos(glm::radians(float(i)))));
    }
}

void Hex::bindVertices() {
    vertices_vbo.bind();
    vertices_vbo.loadData(points.size(), sizeof(glm::vec3), &points[0], GL_STATIC_DRAW);
}

void Hex::bindIndices() {
    GLubyte indices_data[12] = {0, 1, 2,
                                0, 2, 3,
                                0, 3, 4,
                                0, 4, 5};
    indices_vbo.bind();
    indices_vbo.loadData(12, sizeof(GLubyte), &indices_data[0], GL_STATIC_DRAW);
}

}

