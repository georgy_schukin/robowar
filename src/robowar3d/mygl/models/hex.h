#pragma once

#include "mygl/vertex_buffer.h"
#include <vector>

namespace mygl {

class Hex {
protected:
    GLfloat radius;
    std::vector<glm::vec3> points;
    mygl::VertexBuffer vertices_vbo;
    mygl::VertexBuffer indices_vbo;

public:
    Hex(const GLfloat &rad = 1.0f);
    ~Hex() {}

    const GLfloat& getRadius() const {
        return radius;
    }

    const std::vector<glm::vec3>& getPoints() const {
        return points;
    }

    size_t getNumOfIndices() const {
        return indices_vbo.getNumOfElements();
    }

    void bindVertices();
    void bindIndices();
};

}
