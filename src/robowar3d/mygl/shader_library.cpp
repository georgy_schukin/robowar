#include "mygl/shader_library.h"

namespace mygl {

ShaderPtr ShaderLibrary::getShader(const std::string &file, GLenum type) {
    ShaderMap::iterator it = shaders.find(file);
    if (it == shaders.end()) {
        ShaderPtr shader(new Shader(type));
        const std::string path = prefix + file;
        if (shader->loadFromFile(path) != -1) {
            shaders[file] = shader;
            return shader;
        } else {
            return ShaderPtr();
        }
    } else {
        return it->second;
    }
}

ShaderLibrary* ShaderLibrary::getInstance() {
    static ShaderLibrary shader_library;
    return &shader_library;
}

}
