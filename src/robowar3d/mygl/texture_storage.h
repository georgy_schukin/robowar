#pragma once

#include "mygl/texture.h"
#include <vector>

namespace mygl {

class TextureStorage {
private:
    std::vector<Texture> textures;
    std::vector<GLuint> texture_ids;
    GLint min_filter;
    GLint mag_filter;

public:
    TextureStorage() {}
    TextureStorage(const size_t &size) {
        alloc(size);
    }
    ~TextureStorage() {
        clear();
    }

    void alloc(const size_t &size);
    void clear();

    Texture& operator[](const size_t &num) {
        return textures[num];
    }

    const Texture& operator[](const size_t &num) const {
        return textures[num];
    }

    void setMinFilter(GLint filter) {
        min_filter = filter;
    }

    void setMagFilter(GLint filter) {
        mag_filter = filter;
    }

    void load(const size_t &num, const char *filename);
};

}
