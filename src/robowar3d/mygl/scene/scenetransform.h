#pragma once

#include "mygl/mygl.h"

namespace mygl {

class SceneTransform {
public:
    virtual const glm::mat4& getProjectionMatrix() const = 0;
    virtual const glm::mat4& getViewMatrix() const = 0;
    virtual const glm::mat4& getModelMatrix() const = 0;
    virtual const glm::mat4& getModelViewProjMatrix() const = 0;
    virtual const glm::mat4& getModelViewMatrix() const = 0;
};

}
