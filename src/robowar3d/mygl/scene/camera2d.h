#pragma once

#include "mygl/scene/camera.h"

namespace mygl {

class Camera2D : public Camera {
protected:
    glm::vec3 camera_position;

public:
    Camera2D() {}
    ~Camera2D() {}

    void setViewport(const GLsizei &width, const GLsizei &height);
    void setCamera(const glm::vec3 &camera);
    void moveCamera(const glm::vec3 &move);
};

}

