#include "mygl/scene/camera2d.h"
#include <glm/gtc/matrix_transform.hpp>

namespace mygl {

void Camera2D::setViewport(const GLsizei &width, const GLsizei &height) {
    Camera::setViewport(width, height);
    projection_matrix = glm::ortho(0, width, height, 0, -1, 1);
    updateProjection();
}

void Camera2D::setCamera(const glm::vec3 &camera) {
    camera_position = camera;
    //view_matrix = glm::lookAt(camera_position, glm::vec3(0, 0, 0), glm::vec3(0, 0, 1));
    updateModelView();
}

void Camera2D::moveCamera(const glm::vec3 &move) {
    camera_position += move;
    //view_matrix = glm::translate(view_matrix, move);
    //view_matrix = glm::lookAt(camera_position, glm::vec3(0, 0, 0), glm::vec3(0, 0, 1));
    updateModelView();
}

}

