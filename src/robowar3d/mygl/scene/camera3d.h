#pragma once

#include "mygl/scene/camera.h"

namespace mygl {

class Camera3D : public Camera {
protected:
    glm::vec3 camera_position;
    glm::vec3 camera_direction;
    glm::vec3 camera_up;

    glm::vec3 world_start;
    glm::vec3 world_end;

public:
    Camera3D() {}
    ~Camera3D() {}

    void setViewport(const GLsizei &width, const GLsizei &height);
    void setCamera(const glm::vec3 &camera, const glm::vec3 &lookat, const glm::vec3 &up = glm::vec3(0, 1, 0));
    void moveCamera(const glm::vec3 &move);
};

}
