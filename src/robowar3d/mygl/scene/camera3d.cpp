#include "mygl/scene/camera3d.h"
#include <glm/gtc/matrix_transform.hpp>

namespace mygl {

void Camera3D::setViewport(const GLsizei &width, const GLsizei &height) {
    Camera::setViewport(width, height);
    projection_matrix = glm::perspective(glm::radians(60.0f), (float)width / (float)height, 0.1f, 100.f);
    updateProjection();
}

void Camera3D::setCamera(const glm::vec3 &camera, const glm::vec3 &lookat, const glm::vec3 &up) {
    camera_position = camera;
    camera_direction = lookat - camera_position;
    camera_up = up;
    view_matrix = glm::lookAt(camera_position, camera_position + camera_direction, camera_up);
    updateModelView();
}

void Camera3D::moveCamera(const glm::vec3 &move) {
    camera_position += move;
    //view_matrix = glm::translate(view_matrix, move);
    view_matrix = glm::lookAt(camera_position, camera_position + camera_direction, camera_up);
    updateModelView();
}

}

