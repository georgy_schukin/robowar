#include "mygl/scene/camera.h"

namespace mygl {

Camera::Camera(const bool &compat_mode) : opengl_compatibility_mode(compat_mode) {
    model_matrix = glm::mat4(1.0f);
    view_matrix = glm::mat4(1.0f);
    projection_matrix = glm::mat4(1.0f);
}

void Camera::setViewport(const GLsizei &width, const GLsizei &height) {
    viewport_width = width;
    viewport_height = height;
    glViewport(0, 0, viewport_width, viewport_height);
}

void Camera::updateModelView() {
    mv_matrix = view_matrix * model_matrix;
    mvp_matrix = projection_matrix * mv_matrix;

    if (opengl_compatibility_mode) {
        glMatrixMode(GL_MODELVIEW);
        glLoadMatrixf(&mv_matrix[0][0]);
    }
}

void Camera::updateProjection() {
    mvp_matrix = projection_matrix * mv_matrix;

    if (opengl_compatibility_mode) {
        glMatrixMode(GL_PROJECTION);
        glLoadMatrixf(&projection_matrix[0][0]);
    }
}

}
