#pragma once

#include "mygl/scene/scenetransform.h"

namespace mygl {

class Camera : public SceneTransform {
protected:
    glm::mat4 projection_matrix; // matrices
    glm::mat4 model_matrix;
    glm::mat4 view_matrix;
    glm::mat4 mv_matrix; // view*model
    glm::mat4 mvp_matrix; // projection*view*model

    GLsizei viewport_width;
    GLsizei viewport_height;

    bool opengl_compatibility_mode;

protected:
    void updateModelView();
    void updateProjection();

public:
    Camera(const bool &compat_mode = false);
    ~Camera() {}

    const glm::mat4& getProjectionMatrix() const {
        return projection_matrix;
    }

    const glm::mat4& getViewMatrix() const {
        return view_matrix;
    }

    const glm::mat4& getModelMatrix() const {
        return model_matrix;
    }

    const glm::mat4& getModelViewProjMatrix() const {
        return mvp_matrix;
    }

    const glm::mat4& getModelViewMatrix() const {
        return mv_matrix;
    }

    const GLsizei& getViewportWidth() const {
        return viewport_width;
    }

    const GLsizei& getViewportHeight() const {
        return viewport_height;
    }

    virtual void setViewport(const GLsizei &width, const GLsizei &height);
    //void setCamera(const glm::vec3 &camera, const glm::vec3 &lookat, const glm::vec3 &up = glm::vec3(0, 1, 0));
    //void moveCamera(const glm::vec3 &move);
};

}
