#include "mygl/vertex_array.h"

namespace mygl {

VertexArray::VertexArray() {
    glGenVertexArrays(1, &array_id);
}

VertexArray::~VertexArray() {
    if (glIsVertexArray(array_id) == GL_TRUE)
        glDeleteVertexArrays(1, &array_id);
}

void VertexArray::bind() {
    glBindVertexArray(array_id);
}

void VertexArray::unbind() {
    glBindVertexArray(0);
}

}
