#include "shader.h"

#include <fstream>
#include <iostream>
#include <streambuf>
#include <vector>

namespace mygl {

Shader::Shader(GLenum type) : shader_type(type) {
    shader_id = glCreateShader(shader_type);
}

Shader::~Shader() {
    if (glIsShader(shader_id) == GL_TRUE)
        glDeleteShader(shader_id);
}

int Shader::loadFromFile(const std::string &filename) {
    std::string shader_str;
    if (getFileContents(filename, shader_str) == -1) {
        return -1;
    }

    const GLchar *source = shader_str.c_str();
    const GLint source_length = shader_str.length();
    glShaderSource(shader_id, 1, &source, &source_length);

    glCompileShader(shader_id);

    if (!isCompiled()) {
        GLint max_length = 0;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &max_length);
        GLint log_length;
        std::vector<GLchar> info_log(max_length);
        glGetShaderInfoLog(shader_id, max_length, &log_length, &info_log[0]);
        std::cerr << "Shader " << filename << " compilation error: " << std::string(&info_log[0], log_length) << std::endl;
        return -1;
    }

    return 0;
}

bool Shader::isCompiled() {
    GLint is_compiled = 0;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &is_compiled);
    return (is_compiled == GL_TRUE);
}

int Shader::getFileContents(const std::string &filename, std::string &data) {
    std::ifstream f_in(filename.c_str());

    if (!f_in.is_open()) {
        std::cerr << "Shader error: failed to open " << filename << std::endl;
        return -1;
    }

    data.assign((std::istreambuf_iterator<char>(f_in)),
                std::istreambuf_iterator<char>());

    f_in.close();
    return 0;
}

}
