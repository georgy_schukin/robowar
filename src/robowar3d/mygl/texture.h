#pragma once

#include "mygl/mygl.h"

namespace mygl {

/**
 * 2D texture.
 */
class Texture {
private:
	GLuint texture_id;	
    GLenum target;
    GLsizei width;
    GLsizei height;
    GLint min_filter;
    GLint mag_filter;
    bool mipmaps;

protected:
    void loadTexData(GLenum target, GLvoid *data);

public:
    Texture(const GLuint &id, const GLenum &trgt = GL_TEXTURE_2D);
    ~Texture();

    void setTarget(GLenum target) {
        this->target = target;
    }

    int loadFromFile(const char *filename);
    int loadCubemapFromFiles(const char **files, const size_t &files_num = 6);

    void loadFromMemory(const GLuint &width, const GLuint &height, const GLfloat *data);    

    void bind();
    void unbind();

    GLuint getId() const {
        return texture_id;
    }

    GLuint getWidth() const {
        return width;
    }

    GLuint getHeight() const {
        return height;
    }
};

}
