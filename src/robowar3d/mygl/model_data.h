#pragma once

#include "mygl/mygl.h"
#include <vector>

namespace myql {

class ModelData {
public:
    std::vector<glm::vec3> vertices; // array of vertices
    std::vector<glm::vec3> normals; // array of normals
    std::vector<glm::vec2> texture_coords; // array of texture UVs
    std::vector<glm::uvec3> faces; // array of faces: each face - 3 vertex indices

public:
    ModelData() {}
    ~ModelData() {}
};

}
