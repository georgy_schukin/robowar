#include "mygl/vertex_buffer.h"

namespace mygl {

VertexBuffer::VertexBuffer(const GLenum &trgt) : target(trgt), num_of_elements(0), element_size(0) {
    glGenBuffers(1, &buffer_id);
}

VertexBuffer::~VertexBuffer() {
    if (glIsBuffer(buffer_id) == GL_TRUE)
        glDeleteBuffers(1, &buffer_id);
}

void VertexBuffer::bind() {
    glBindBuffer(target, buffer_id);
}

void VertexBuffer::unbind() {
    glBindBuffer(target, 0);
}

void VertexBuffer::loadData(GLsizei num_of_elems, GLsizei elem_size, GLvoid *data, GLenum usage) {
    this->num_of_elements = num_of_elems;
    this->element_size = elem_size;
    GLsizeiptr size = num_of_elements*element_size;
    glBufferData(target, size, data, usage);
}

}
