#pragma once

#include "mygl/mygl.h"

#include <string>
#include <memory>

namespace mygl {

class Shader {
public:
    Shader(GLenum type);
    Shader(const Shader &s) : shader_type(s.shader_type), shader_id(s.shader_id) {}
    ~Shader();

    GLenum getType () const {
        return shader_type;
    }

    GLuint getId() const {
        return shader_id;
    }

    int loadFromFile(const std::string &filename);

    bool isCompiled();

private:
    int getFileContents(const std::string &filename, std::string &data);

private:
    GLenum shader_type;
    GLuint shader_id;
};

typedef std::shared_ptr<Shader> ShaderPtr;

}
