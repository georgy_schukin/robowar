#pragma once

#include <string>

namespace robowar3d {

/**
 * Config for 3D visualizer.
 */
class Config3D {
public:
    enum ShowMode {
        SM_NONE = 0,
        SM_NO_FRAME,
        SM_WITH_FRAME
    };

public:
    Config3D();
    Config3D(const std::string &filename);
    ~Config3D() {}

    int load(const std::string &filename);
    void setDefaults();

public:
    int frames_per_anim; // num of frames per animation (between iterations)
    int refresh_delay; // delay between animation frames (msec)

    bool enable_sound; // play sounds
    bool enable_music; // play music

    ShowMode show_minimap; // show minimap mode
    ShowMode show_score; // score show mode

    bool show_info; // show info
    bool show_help; // show onscreen help

    bool show_particles; // show particle effects (smoke/fire)
};

}
