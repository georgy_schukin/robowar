TEMPLATE = app
CONFIG += console
CONFIG += c++14
CONFIG -= app_bundle
CONFIG -= qt
TARGET = RoboWar3D

exists(../Local.pri) {
    include(../Local.pri)
}

DESTDIR = "../../game"

QMAKE_CXXFLAGS_RELEASE += -O2

unix {    
    LIBS += -lGL -lGLU -lglut -lGLEW -lSOIL    
    LIBS += -lIrrKlang    
    LIBS += -lpthread    
}

win32 {
    LIBS += -lSOIL -lfreeglut -lglew32 -lopengl32 -lglu32
    LIBS += -lirrKlang    
}

SOURCES += \    
    mygl/models/hex.cpp \
    mygl/scene/camera.cpp \
    mygl/scene/camera2d.cpp \
    mygl/scene/camera3d.cpp \
    mygl/shader.cpp \
    mygl/shader_library.cpp \
    mygl/shader_program.cpp \
    mygl/texture.cpp \
    mygl/texture_storage.cpp \
    mygl/vertex_array.cpp \
    mygl/vertex_buffer.cpp \
    legacy/animation.cpp \
    legacy/animation_list.cpp \
    legacy/noise_generator.cpp \
    legacy/robowar3d_hud.cpp \
    legacy/robowar3d_lifebar.cpp \
    legacy/robowar3d_skybox.cpp \
    legacy/robowar3d_sound.cpp \
    legacy/robowar3d_terrain.cpp \
    legacy/terrain_generator.cpp \
    sound/looped_sound.cpp \
    sound/sound.cpp \
    sound/sound_engine.cpp \
    sound/sound_library.cpp \
    config3d.cpp \
    main.cpp \   
    legacy/components/game_field.cpp \
    robowar3d_legacy.cpp

HEADERS += \   
    mygl/importers/obj_loader.h \
    mygl/models/cube.h \
    mygl/models/hex.h \
    mygl/scene/camera.h \
    mygl/scene/camera2d.h \
    mygl/scene/camera3d.h \
    mygl/scene/scenetransform.h \
    mygl/model.h \
    mygl/model_data.h \
    mygl/mygl.h \
    mygl/shader.h \
    mygl/shader_library.h \
    mygl/shader_program.h \
    mygl/texture.h \
    mygl/texture_storage.h \
    mygl/vertex_array.h \
    mygl/vertex_buffer.h \
    legacy/animation.h \
    legacy/animation_list.h \
    legacy/noise_generator.h \
    legacy/terrain.h \
    legacy/terrain_generator.h \
    legacy/types.h \
    legacy/world_data.h \
    sound/looped_sound.h \
    sound/sound.h \
    sound/sound_engine.h \
    sound/sound_library.h \
    config3d.h \    
    legacy/components/game_field.h \
    opengl_visualizer.h \
    robowar3d_legacy.h

include(../RoboWar_lib.pri)
