#include "rectangular_game_map.h"

#include <cmath>

namespace robowar {

RectangularGameMap::RectangularGameMap(int width, int height) :
    width(width), height(height)
{
    for (int row = 0; row < height; row++) {
        int offset = row / 2;
        for (int col = -offset; col < width - offset; col++) {
            addCell(Coord(col, row));
        }
    }
}

int RectangularGameMap::getWidth() const {
    return width;
}

int RectangularGameMap::getHeight() const {
    return height;
}

}
