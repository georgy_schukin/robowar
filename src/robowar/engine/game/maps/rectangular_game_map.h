#pragma once

#include "engine/game/game_map.h"

namespace robowar {

class RectangularGameMap : public GameMap {
public:
    RectangularGameMap(int width, int height);

    int getWidth() const;
    int getHeight() const;

private:
    int width;
    int height;
};

}
