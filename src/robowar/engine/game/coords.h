#pragma once

#include "direction.h"

#include <vector>
#include <string>

namespace robowar {

class Coord;

class CubeCoord {
public:
    CubeCoord();
    CubeCoord(int x, int y, int z);

    int getX() const;
    int getY() const;
    int getZ() const;

    Coord toHex() const;

    CubeCoord plus(const CubeCoord &cc) const;

    int distanceTo(const CubeCoord &cc) const;

    std::vector<CubeCoord> getCoordsInRange(int distance) const;

private:
    int x, y, z;
};

/**
 * @brief Coord class.
 *
 * Represents coordinate of a hex cell on a game map as (column, row) or (x, y).
 */
class Coord {
public:
    /// Default constructor (components are set to zeroes).
    Coord();
    /// Constructor.
    Coord(int col, int row);
    Coord(const Coord &coord);

    ~Coord() {}

    bool operator<(const Coord &coord) const;
    bool operator==(const Coord &coord) const;

    /// Get column (x).
    int getCol() const;
    /// Get row (y).
    int getRow() const;        

    /// Get x (column).
    int getX() const;
    /// Get y (row).
    int getY() const;

    CubeCoord toCube() const;

    /// Get sum of this coord and given coord.
    Coord plus(const Coord &coord) const;
    /// Get neighbor coord obtained by moving from this coord in given direction.
    Coord atDirection(const Direction &dir) const;

    /// Get distance from this coord to given coord.
    int distanceTo(const Coord &dest) const;
    /// Get approximate direction from this coord to given coord.
    Direction directionTo(const Coord &dest) const;

    std::vector<Coord> getCoordsInRange(int distance) const;

    std::string toString() const;

private:
    int row, col;
};

}
