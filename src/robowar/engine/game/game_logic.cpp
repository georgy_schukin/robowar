#include "game_logic.h"
#include "game_world.h"
#include "game_map.h"
#include "objects.h"
#include "engine/script/implementation.h"
#include "engine/script/script.h"

#include <algorithm>
#include <cassert>
#include <iostream>

namespace robowar {

GameLogic::GameLogic(GameWorld *world) :
    world(world) {
}

void GameLogic::doIteration() {
    spawnRobots();    
    processRobotViews();
    runRobotScripts();    
    processRobotShootings();    
    processRobotTransmissions();    
    processRobotMovements();    
}

void GameLogic::doUpdate() {
    updateRobots();
    updateBases();
}

void GameLogic::spawnRobots() {
    for (auto *base: world->getBases()) {
        if (base->isReadyToConstruct()) {
            world->spawnRobot(base);
        }
    }
}

void GameLogic::processRobotViews() {
    for (auto *robot: world->getRobots()) {
        robot->clearView();
        const auto cells = world->getMap()->getCellsInRange(robot->getCoord(), robot->getViewDistance());
        for (auto *cell: cells) {
            if (cell->getCoord() == robot->getCoord()) {
                continue;
            }
            if (cell->isRobotHere()) {
                robot->addRobotInView(cell->getRobot());
            } else if (cell->isBaseHere()) {
                robot->addBaseInView(cell->getBase());
            }
        }
    }
}

void GameLogic::runRobotScripts() {
    for (auto *robot: world->getRobots()) {
        if (robot->isDead()) {
            continue;
        }
        robot->prepareRun();
        auto *script = robot->getTeam()->getScript();        
        ScriptRobotController controller(robot, world->getMap());
        script->run(&controller);
    }
}

void GameLogic::processRobotShootings() {           
    std::vector<GameBase*> failed_captures;
    for (auto *robot: world->getRobots()) {
        robot->clearDamage();
    }
    for (auto *base: world->getBases()) {
        base->clearDamage();
        base->clearAttack();
    }
    for (auto *robot: world->getRobots()) {        
        if (robot->isShooting()) {
            const auto *shoot_cell = world->getMap()->getCell(robot->getShootCoord());
            if (shoot_cell->isRobotHere()) {
                auto *shoot_robot = shoot_cell->getRobot();
                shoot_robot->addDamage(1);
            } else if (shoot_cell->isBaseHere()) {
                auto *shoot_base = shoot_cell->getBase();
                shoot_base->addDamage(1);
                if (!shoot_base->getAttackTeam()) { // base wasn't attacked yet
                    shoot_base->setAttackTeam(robot->getTeam());
                } else if (shoot_base->getAttackTeam() != robot->getTeam()) { // different attackers
                    failed_captures.push_back(shoot_base); // capture failed
                }
            }
        }
    }
    for (auto *base: failed_captures) {
        base->setAttackTeam(nullptr);
    }
}

void GameLogic::processRobotTransmissions() {    
    for (auto *robot: world->getRobots()) {
        robot->clearIncomingMessages();
    }
    for (auto *robot: world->getRobots()) {
        if (!robot->isTransmitting()) {
            continue;
        }
        const auto cells = world->getMap()->getCellsInRange(robot->getCoord(), robot->getTransmitDistance());
        for (auto *cell: cells) {
            if (cell->getCoord() == robot->getCoord()) {
                continue;
            }
            if (cell->isRobotHere()) {
                auto *other_robot = cell->getRobot();
                if (!robot->isEnemyWith(other_robot)) {
                    other_robot->addIncomingMessage(robot->getOwnMessage());
                }
            }
        }
    }
}

void GameLogic::processRobotMovements() {
    typedef std::vector<GameRobot*> RobotList;
    typedef std::map<Coord, RobotList> RobotMovementMap;
    RobotMovementMap robot_movements;
    for (auto *robot: world->getRobots()) {
        if (robot->isDead()) {
            continue;
        }                
        robot_movements[robot->getNextCoord()].push_back(robot);
    }
    bool is_ok = true;
    do {        
        std::vector<Coord> conflicts;
        for (const auto &p: robot_movements) {
            // More than one robot on the same cell.
            if (p.second.size() > 1) {
                conflicts.push_back(p.first);
            }
        }
        // Resolve conflicts - cancel conflicting robots movements.
        for (const auto &coord: conflicts) {
            const RobotList robots = robot_movements.at(coord);
            robot_movements.erase(coord);
            for (auto *robot: robots) {
                robot->cancelMove();
                robot_movements[robot->getNextCoord()].push_back(robot);
            }
        }
        is_ok = conflicts.empty();        
    } while (!is_ok);
}

void GameLogic::updateRobots() {
    std::vector<GameRobot*> dead_robots;
    for (auto *robot: world->getRobots()) {
        if (robot->isDead()) {
            dead_robots.push_back(robot);
        }
    }
    for (auto *robot: dead_robots) {
        world->removeRobot(robot);
    }
    for (auto *robot: world->getRobots()) {        
        if (robot->isMoving()) {
            world->moveRobot(robot, robot->getMoveCoord());
        }
        robot->updatePostIteration();
    }    
}

void GameLogic::updateBases() {
    for (auto *base: world->getBases()) {        
        base->updatePostIteration();
    }
}

}
