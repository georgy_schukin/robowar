#pragma once

#include <string>
#include <vector>

/// RoboWar namespace.
namespace robowar {

/**
 * @brief Direction of movement.
 *
 * Direction of movement from a (pointy topped) hex cell.
 */
enum Direction {    
    /// No movement
    DIR_NONE = 0,
    /// North-east
    DIR_NORTH_EAST,
    /// East
    DIR_EAST,
    /// South-east
    DIR_SOUTH_EAST,
    /// South-west
    DIR_SOUTH_WEST,
    /// West
    DIR_WEST,
    /// North-west
    DIR_NORTH_WEST
};

Direction directionFromString(const std::string &dir_str);
const std::vector<Direction>& getAroundDirections();

}
