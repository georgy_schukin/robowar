#pragma once

#include <memory>
#include <vector>
#include <map>

namespace robowar {

class GameListener;
class GameSession;

class GameEngine {
public:
    GameEngine();
    ~GameEngine();

    GameSession* newSession();
    void deleteSession(GameSession *session);

    void shutdown();

private:    
    void gameThreadFunc();

private:
    typedef std::shared_ptr<GameSession> SessionPtr;

private:    
    std::map<int, SessionPtr> sessions;
    int last_session_id;
};

}
