#include "game_engine.h"
#include "engine/game/game_session.h"

#include <iostream>
#include <fstream>
#include <string>

namespace robowar {

GameEngine::GameEngine() :
    last_session_id(0) {
}

GameEngine::~GameEngine() {
    shutdown();
}

GameSession* GameEngine::newSession() {
    const int session_id = last_session_id++;
    SessionPtr session = std::make_shared<GameSession>(session_id);
    sessions[session_id] = session;
    return session.get();
}

void GameEngine::deleteSession(GameSession *session) {
    if (session) {
        session->stop();
        sessions.erase(session->getId());
    }
}

void GameEngine::shutdown() {
    for (const auto &p: sessions) {
        p.second->stop();
    }
    sessions.clear();
}

}
