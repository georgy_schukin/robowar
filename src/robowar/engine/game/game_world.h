#pragma once

#include "engine/game/coords.h"

#include <vector>
#include <map>
#include <memory>

namespace robowar {

class GameMap;
class GameRobot;
class GameBase;
class Script;
class Team;
class RobotConfig;
class BaseConfig;
class GameCell;

typedef std::shared_ptr<GameMap> GameMapPtr;
typedef std::shared_ptr<BaseConfig> BaseConfigPtr;

class GameWorld {
public:
    GameWorld();
    ~GameWorld();

    void setMap(const GameMapPtr &map);
    GameMap* getMap() const;

    Team* addTeam(const std::string &name, Script *script);
    GameRobot* addRobot(Team *team, const Coord &coord);
    GameBase* addBase(Team *team, const Coord &coord);
    GameBase* addNeutralBase(const Coord &coord);

    void spawnRobot(GameBase *producer);
    void moveRobot(GameRobot *robot, const Coord &coord);

    void removeRobot(GameRobot *robot);
    void removeBase(GameBase *base);

    //void setTeamForBase(GameBase *base, Team *team);

    int getNumOfRobots() const;
    int getNumOfBases() const;
    int getNumOfTeams() const;

    GameRobot* getRobot(int index);
    GameBase* getBase(int index);
    Team* getTeam(int index);

    GameRobot* getRobotById(int id);
    GameBase* getBaseById(int id);
    Team* getTeamById(int id);

    const std::vector<GameRobot*>& getRobots() const;
    const std::vector<GameBase*>& getBases() const;
    const std::vector<Team*>& getTeams() const;

    void generateRandom(int num_of_bases, int num_of_neutral_bases);

    Team* getWinner() const;

    int getNumOfRobotsForTeam(int team_id) const;

    void setDefaultBaseConfig(const BaseConfigPtr &config);
    BaseConfig* getDefaultBaseConfig() const;

private:
    Coord getRandomFreeBaseCoord(int free_range, int border_range=2);
    bool isOutOfRangeFromOtherBases(const Coord &coord, int range) const;       
    bool isAwayFromBorder(const Coord &coord, int range) const;

private:
    typedef std::shared_ptr<GameRobot> GameRobotPtr;
    typedef std::shared_ptr<GameBase> GameBasePtr;
    typedef std::shared_ptr<Team> TeamPtr;

private:
    GameMapPtr map;

    std::map<int, GameRobotPtr> robots;
    std::map<int, GameBasePtr> bases;
    std::map<int, TeamPtr> teams;

    std::vector<GameRobot*> robots_v;
    std::vector<GameBase*> bases_v;
    std::vector<Team*> teams_v;

    int last_robot_id;
    int last_base_id;
    int last_team_id;    

    BaseConfigPtr default_base_config;
};

}
