#include "game_session.h"
#include "game_logic.h"
#include "game_world.h"
#include "game_map.h"
#include "engine/state/game_init_state.h"
#include "engine/state/game_state.h"
#include "engine/state/game_listener.h"
#include "engine/script/script_library.h"
#include "engine/script/scripts.h"
#include "engine/game/maps/rectangular_game_map.h"
#include "engine/game/conf/robot_config.h"
#include "engine/game/conf/base_config.h"
#include "engine/game/objects.h"
#include "bots/builtin/bots.h"

#include <fstream>
#include <iostream>

namespace robowar {

GameSession::GameSession(int id) :
    id(id), is_running(false)
{
    script_library = std::make_unique<ScriptLibrary>();
    registerBuiltInBots();
}

GameSession::~GameSession() {
    stop();    
}

int GameSession::getId() const {
    return id;
}

void GameSession::addListener(GameListener *listener) {
    listeners.push_back(listener);
}

void GameSession::loadGameFromFile(const std::string &filename) {
    auto world = loadWorldFromFile(filename);
    setWorld(world);
}

GameWorldPtr GameSession::loadWorldFromFile(const std::string &filename) {
    std::ifstream f_in(filename.c_str());
    if (!f_in.is_open()) {
        throw std::runtime_error("Failed to open game file " + filename);
    }
    auto world = std::make_shared<GameWorld>();
    auto robot_config = std::make_shared<RobotConfig>();
    auto base_config = std::make_shared<BaseConfig>();
    std::string tag;
    while(!f_in.eof()) {
        tag = "";
        f_in >> tag;
        if (tag.empty()) {
            continue;
        } else if ((tag[0] == '%') || (tag[0] == '#')) { // skip comments
            std::string temp;
            std::getline(f_in, temp);
            continue;
        } else if (tag == "config") {
            std::string filename;
            f_in >> filename;
            robot_config = std::make_shared<RobotConfig>(filename);
            base_config = std::make_shared<BaseConfig>(filename);
            world->setDefaultBaseConfig(base_config);
            log(std::string("Loading config ") + filename);
        } else if (tag == "team") { // add team
            std::string team_name, script_path;
            f_in >> team_name >> script_path;
            auto *script = script_library->addScript(script_path);
            auto *team = world->addTeam(team_name, script);
            team->setRobotConfig(robot_config);
            team->setBaseConfig(base_config);
            log(std::string("Adding team ") + team_name + " (" + script_path + ")");
        } else if (tag == "rmap") { // random map
            size_t map_width, map_height, num_of_bases, num_of_neutral_bases;
            f_in >> map_width >> map_height >> num_of_bases >> num_of_neutral_bases;
            auto map = std::make_shared<RectangularGameMap>(map_width, map_height);
            world->setMap(map);
            world->generateRandom(num_of_bases, num_of_neutral_bases);
            log(std::string("Setting map ") + std::to_string(map_width) + "x" + std::to_string(map_height));
        } else {
            throw std::runtime_error("Invalid or unknown tag: " + tag);
        }
    }
    f_in.close();
    return world;
}

void GameSession::setWorld(const GameWorldPtr &world) {
    this->world = world;    
}

void GameSession::run() {
    if (!world) {
        return;
    }
    if (!is_running) {
        is_running = true;
        exec_thread = std::thread(&GameSession::threadFunction, this);
    }
}

void GameSession::stop() {
    if (is_running) {
        is_running = false;
        exec_thread.join();
    }
}

void GameSession::threadFunction() {    
    auto game_logic = std::make_unique<GameLogic>(world.get());
    int iteration = 0;
    postInit();
    while (is_running && !gameOver()) {
        game_logic->doIteration();        
        postUpdate(iteration);        
        game_logic->doUpdate();        
        //std::cout << iteration << std::endl;
        iteration++;
    }
    //std::cout << "done" << std::endl;
    postUpdate(iteration);
    postFinish();
}

bool GameSession::gameOver() const {           
    return (world->getWinner() != nullptr);
}

void GameSession::postInit() {
    GameInitState init_state(world.get());
    for (auto *listener: listeners) {
        listener->handleInit(init_state);
    }
}

void GameSession::postUpdate(int iteration) {
    GameState state(world.get(), iteration);
    for (auto *listener: listeners) {
        listener->handleUpdate(state);
    }
}

void GameSession::postFinish() {
    for (auto *listener: listeners) {
        listener->handleFinish();
    }
}

void GameSession::registerBuiltInBots() {
    script_library->addScript("bot", LocalScript::newInstance(bots::bot));
    script_library->addScript("smartbot", LocalScript::newInstance(bots::smartbot));
}

void GameSession::log(const std::string &str) {
    std::cout << str << std::endl;
}

}
