#include "robot_config.h"

#include <fstream>

namespace robowar {

RobotConfig::RobotConfig() {
    setDefaults();
}

RobotConfig::RobotConfig(const std::string &filename) {
    setDefaults();
    loadFromFile(filename);
}

void RobotConfig::setDefaults() {
    setViewDistance(5);
    setShootDistance(5);
    setTransmitDistance(10);
    setWeaponCooldown(5);
    setMaxHitpoints(5);
    setMemorySize(256);
    setMessageSize(64);
}

void RobotConfig::setViewDistance(int distance) {
    view_distance = distance;
}

void RobotConfig::setShootDistance(int distance) {
    shoot_distance = distance;
}

void RobotConfig::setTransmitDistance(int distance) {
    transmit_distance = distance;
}

void RobotConfig::setWeaponCooldown(int cooldown) {
    weapon_cooldown = cooldown;
}

void RobotConfig::setMaxHitpoints(int hitpoints) {
    max_hitpoints = hitpoints;
}

void RobotConfig::setMemorySize(int size) {
    memory_size = size;
}

void RobotConfig::setMessageSize(int size) {
    message_size = size;
}

int RobotConfig::getViewDistance() const {
    return view_distance;
}

int RobotConfig::getShootDistance() const {
    return shoot_distance;
}

int RobotConfig::getTransmitDistance() const {
    return transmit_distance;
}

int RobotConfig::getWeaponCooldown() const {
    return weapon_cooldown;
}

int RobotConfig::getMaxHitpoints() const {
    return max_hitpoints;
}

int RobotConfig::getMemorySize() const {
    return memory_size;
}

int RobotConfig::getMessageSize() const {
    return message_size;
}

void RobotConfig::loadFromFile(const std::string &filename) {
    std::ifstream f_in(filename.c_str());
    if (!f_in.is_open()) {
        throw std::runtime_error("Failed to open file " + filename);
    }
    std::string tag;
    int value;
    while (!f_in.eof()) {
        f_in >> tag >> value;
        if (tag.empty()) {
            continue;
        }
        if (tag == "robot_hitpoints") {
            setMaxHitpoints(value);
        } else if (tag == "robot_weapon_cooldown") {
            setWeaponCooldown(value);
        } else if (tag == "robot_memory_size") {
            setMemorySize(value);
        } else if (tag == "robot_message_size") {
            setMessageSize(value);
        } else if (tag == "robot_shoot_distance") {
            setShootDistance(value);
        } else if (tag == "robot_view_distance") {
            setViewDistance(value);
        } else if (tag == "robot_transmit_distance") {
            setTransmitDistance(value);
        }
    }
}

}
