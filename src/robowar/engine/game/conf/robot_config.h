#pragma once

#include <string>

namespace robowar {

class RobotConfig {
public:
    RobotConfig();
    RobotConfig(const std::string &filename);
    ~RobotConfig() {}

    void setViewDistance(int distance);
    void setShootDistance(int distance);
    void setTransmitDistance(int distance);
    void setWeaponCooldown(int cooldown);
    void setMaxHitpoints(int hitpoints);
    void setMemorySize(int size);
    void setMessageSize(int size);

    int getViewDistance() const;
    int getShootDistance() const;
    int getTransmitDistance() const;
    int getWeaponCooldown() const;
    int getMaxHitpoints() const;
    int getMemorySize() const;
    int getMessageSize() const;

    void loadFromFile(const std::string &filename);

private:
    void setDefaults();

private:
    int view_distance;
    int shoot_distance;
    int transmit_distance;
    int weapon_cooldown;
    int max_hitpoints;
    int memory_size;
    int message_size;
};

}
