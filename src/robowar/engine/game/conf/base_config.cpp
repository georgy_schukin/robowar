#include "base_config.h"

#include <fstream>

namespace robowar {

BaseConfig::BaseConfig() {
    setDefaults();
}

BaseConfig::BaseConfig(const std::string &filename) {
    setDefaults();
    loadFromFile(filename);
}

void BaseConfig::setDefaults() {
    setConstructionCooldown(5);
    setMaxHitpoints(10);
}

void BaseConfig::setConstructionCooldown(int cooldown) {
    construction_cooldown = cooldown;
}

void BaseConfig::setMaxHitpoints(int hitpoints) {
    max_hitpoints = hitpoints;
}

int BaseConfig::getConstructionCooldown() const {
    return construction_cooldown;
}

int BaseConfig::getMaxHitpoints() const {
    return max_hitpoints;
}

void BaseConfig::loadFromFile(const std::string &filename) {
    std::ifstream f_in(filename.c_str());
    if (!f_in.is_open()) {
        throw std::runtime_error("Failed to open file " + filename);
    }
    std::string tag;
    int value;
    while (!f_in.eof()) {
        f_in >> tag >> value;
        if (tag.empty()) {
            continue;
        }
        if (tag == "base_hitpoints") {
            setMaxHitpoints(value);
        } else if (tag == "base_construction_cooldown") {
            setConstructionCooldown(value);
        }
    }
}

}
