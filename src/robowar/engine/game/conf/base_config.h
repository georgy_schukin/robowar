#pragma once

#include <string>

namespace robowar {

class BaseConfig {
public:
    BaseConfig();
    BaseConfig(const std::string &filename);
    ~BaseConfig() {}

    void setConstructionCooldown(int cooldown);
    void setMaxHitpoints(int hitpoints);

    int getConstructionCooldown() const;
    int getMaxHitpoints() const;

    void loadFromFile(const std::string &filename);

private:
    void setDefaults();

private:
    int construction_cooldown;
    int max_hitpoints;
};

}
