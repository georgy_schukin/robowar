#pragma once

namespace robowar {

class GameWorld;

class GameLogic {
public:
    GameLogic(GameWorld *world);
    ~GameLogic() {}    

    /// Prepare and run robots.
    void doIteration();

    /// Update robots after run.
    void doUpdate();

protected:
    void spawnRobots();    
    void processRobotViews();
    void runRobotScripts();
    void processRobotShootings();
    void processRobotTransmissions();
    void processRobotMovements();
    void updateRobots();
    void updateBases();

private:
    GameWorld *world;
};

}
