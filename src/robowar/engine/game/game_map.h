#pragma once

#include "coords.h"

#include <map>
#include <memory>
#include <vector>

namespace robowar {

class GameCell;
class GameObject;

/**
 * Game map: field of cells.
 */
class GameMap {
public:
    GameMap() {}
    virtual ~GameMap() {}

    void addCell(const Coord &coord);

    void removeCell(const Coord &coord);

    GameCell* getCell(const Coord &coord) const;

    bool hasCell(const Coord &coord) const;

    void setObject(GameObject *object, const Coord &coord);
    void moveObject(GameObject *object, const Coord &new_coord);
    void removeObject(GameObject *object);

    std::vector<Coord> getCellCoords() const;

    GameCell* getEmptyCellAround(const Coord &coord);

    std::vector<GameCell*> getCellsInRange(const Coord &coord, int range);    

    int getNumOfCells() const;

    GameCell* getRandomFreeCell();
    bool hasAllCells(const Coord &coord, int range) const;

private:
    std::map<Coord, std::shared_ptr<GameCell>> cells;
};

typedef std::shared_ptr<GameMap> GameMapPtr;

}
