#pragma once

#include "coords.h"

#include <string>
#include <memory>
#include <thread>
#include <vector>
#include <atomic>

namespace robowar {

class GameListener;
class GameWorld;
class ScriptLibrary;

typedef std::shared_ptr<GameWorld> GameWorldPtr;

class GameSession {
public:
    GameSession(int id);
    ~GameSession();

    int getId() const;

    void loadGameFromFile(const std::string &filename);

    void addListener(GameListener *listener);

    void setWorld(const GameWorldPtr &world);

    void run();
    void stop();        

private:
    GameWorldPtr loadWorldFromFile(const std::string &filename);

    void threadFunction();

    void postInit();
    void postUpdate(int iteration);
    void postFinish();

    void registerBuiltInBots();

    void log(const std::string &str);

    bool gameOver() const;

private:
    int id;
    GameWorldPtr world;
    std::unique_ptr<ScriptLibrary> script_library;    
    std::vector<GameListener*> listeners;
    std::thread exec_thread;
    std::atomic<bool> is_running;
};

}
