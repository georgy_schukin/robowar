#include "coords.h"

#include <cmath>
#include <map>
#include <algorithm>
#include <sstream>

namespace robowar {

namespace {
    const std::map<Direction, Coord> hex_displacements = {
        {DIR_NONE, Coord(0, 0)},
        {DIR_NORTH_EAST, Coord(1, -1)},
        {DIR_EAST, Coord(1, 0)},
        {DIR_SOUTH_EAST, Coord(0, 1)},
        {DIR_SOUTH_WEST, Coord(-1, 1)},
        {DIR_WEST, Coord(-1, 0)},
        {DIR_NORTH_WEST, Coord(0, -1)}
    };
}

CubeCoord::CubeCoord() :
    x(0), y(0), z(0) {
}

CubeCoord::CubeCoord(int x, int y, int z) :
    x(x), y(y), z(z) {
}

int CubeCoord::getX() const {
    return x;
}

int CubeCoord::getY() const {
    return y;
}

int CubeCoord::getZ() const {
    return z;
}

Coord CubeCoord::toHex() const {
    return Coord(x, z);
}

CubeCoord CubeCoord::plus(const CubeCoord &cc) const {
    return CubeCoord(x + cc.getX(), y + cc.getY(), z + cc.getZ());
}

int CubeCoord::distanceTo(const CubeCoord &cc) const {
    const int dx = std::abs(x - cc.getX());
    const int dy = std::abs(y - cc.getY());
    const int dz = std::abs(z - cc.getZ());
    return std::max(dx, std::max(dy, dz));
}

std::vector<CubeCoord> CubeCoord::getCoordsInRange(int distance) const {
    std::vector<CubeCoord> coords;
    for (int dx = -distance; dx <= distance; dx++) {
        for (int dy = std::max(-distance, -dx - distance); dy <= std::min(distance, -dx + distance); dy++) {
            coords.push_back(plus(CubeCoord(dx, dy, -dx - dy)));
        }
    }
    return coords;
}

Coord::Coord() :
    row(0), col(0) {
}

Coord::Coord(int col, int row) :
    row(row), col(col) {
}

Coord::Coord(const Coord &coord) :
    row(coord.row), col(coord.col) {
}

bool Coord::operator<(const Coord &coord) const {
    if (row < coord.getRow()) {
        return true;
    } else if (row == coord.getRow()) {
        return col < coord.getCol();
    }
    return false;
}

bool Coord::operator==(const Coord &coord) const {
    return (row == coord.getRow()) && (col == coord.getCol());
}

int Coord::getRow() const {
    return row;
}

int Coord::getCol() const {
    return col;
}

int Coord::getX() const {
    return getCol();
}

int Coord::getY() const {
    return getRow();
}

CubeCoord Coord::toCube() const {
    return CubeCoord(col, -col - row, row);
}

Coord Coord::plus(const Coord &coord) const {
    return Coord(col + coord.getCol(), row + coord.getRow());
}

Coord Coord::atDirection(const Direction &dir) const {
    return plus(hex_displacements.at(dir));
}

int Coord::distanceTo(const Coord &dest) const {
    return toCube().distanceTo(dest.toCube());
}

Direction Coord::directionTo(const Coord &dest) const {
    int min_dist = distanceTo(dest);
    auto direction = DIR_NONE;
    for (auto dir: getAroundDirections()) {
        const int dist = atDirection(dir).distanceTo(dest);
        if (dist < min_dist) {
            min_dist = dist;
            direction = dir;
        }
    }
    return direction;
}

std::vector<Coord> Coord::getCoordsInRange(int distance) const {
    std::vector<Coord> coords;
    for (const auto &cube_coord: toCube().getCoordsInRange(distance)) {
        coords.push_back(cube_coord.toHex());
    }
    return coords;
}

std::string Coord::toString() const {
    std::ostringstream out;
    out << "(" << getX() << ", " << getY() << ")";
    return out.str();
}

}
