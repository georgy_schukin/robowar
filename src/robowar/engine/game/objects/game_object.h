#pragma once

#include "engine/game/coords.h"

namespace robowar {

class Team;

/**
 * Object on the game map.
 */
class GameObject {
public:
    enum GameObjectType {
        ROBOT = 0,
        BASE,
        RESOURCE
    };

public:            
    GameObject(int id);

    virtual ~GameObject() {}

    void setCoord(const Coord &coord);
    void setTeam(Team *team);

    int getId() const;
    const Coord& getCoord() const;
    Team* getTeam() const;

    virtual GameObjectType getType() const = 0;

    bool hasTeam() const;
    bool isEnemyWith(GameObject *object) const;

    bool coordIsInRange(const Coord &coord, int range) const;

private:
    int object_id; // unique identificator
    Coord coord;
    Team *team;
};

}
