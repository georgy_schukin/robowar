#include "game_robot.h"
#include "engine/game/conf/robot_config.h"
#include "engine/script/implementation.h"

namespace robowar {

GameRobot::GameRobot(int id, const RobotConfig *config) :
    ActiveGameObject(id),
    config(config)
{
    init();
}

void GameRobot::init() {
    setHitpoints(getMaxHitpoints());
    setDamage(0);
    setCooldown(0);
    own_message = std::make_shared<ScriptMessage>(getMessageSize());
    memory = std::make_shared<ScriptMessage>(getMemorySize());
}

int GameRobot::getViewDistance() const {
    return config->getViewDistance();
}

int GameRobot::getShootDistance() const {
    return config->getShootDistance();
}

int GameRobot::getTransmitDistance() const {
    return config->getTransmitDistance();
}

int GameRobot::getMaxHitpoints() const {
    return config->getMaxHitpoints();
}

int GameRobot::getMaxCooldown() const {
    return config->getWeaponCooldown();
}

int GameRobot::getMemorySize() const {
    return config->getMemorySize();
}

int GameRobot::getMessageSize() const {
    return config->getMessageSize();
}

void GameRobot::setMoveCoord(const Coord &coord) {
    move_coord = coord;
    is_moving = canMoveAt(coord);
}

void GameRobot::setShootCoord(const Coord &coord) {
    shoot_coord = coord;
    is_shooting = canShootAt(coord);
}

void GameRobot::transmit() {
    is_transmitting = true;
}

void GameRobot::cancelMove() {
    is_moving = false;
}

void GameRobot::cancelShoot() {
    is_shooting = false;
}

void GameRobot::cancelTransmit() {
    is_transmitting = false;
}

bool GameRobot::canShoot() const {
    return (getCooldown() == 0);
}

bool GameRobot::canMoveAt(const Coord &coord) const {
    return (getCoord().distanceTo(coord) == 1);
}

bool GameRobot::canShootAt(const Coord &coord) const {    
    return canShoot() && coordIsInRange(coord, getShootDistance());
}

bool GameRobot::isShooting() const {
    return is_shooting;
}

bool GameRobot::isMoving() const {
    return is_moving;
}

bool GameRobot::isTransmitting() const {
    return is_transmitting;
}

const Coord& GameRobot::getShootCoord() const {
    return shoot_coord;
}

const Coord& GameRobot::getMoveCoord() const {
    return move_coord;
}

const Coord& GameRobot::getNextCoord() const {
    return (isMoving() ? getMoveCoord() : getCoord());
}

int GameRobot::getNumOfRobotsInView() const {
    return robots_in_view.size();
}

int GameRobot::getNumOfBasesInView() const {
    return bases_in_view.size();
}

GameRobot* GameRobot::getRobotInView(int index) {
    return (index < getNumOfRobotsInView() ? robots_in_view.at(index) : nullptr);
}

GameBase* GameRobot::getBaseInView(int index) {
    return (index < getNumOfBasesInView() ? bases_in_view.at(index) : nullptr);
}

void GameRobot::addRobotInView(GameRobot *robot) {
    robots_in_view.push_back(robot);
}

void GameRobot::addBaseInView(GameBase *base) {
    bases_in_view.push_back(base);
}

void GameRobot::clearView() {
    robots_in_view.clear();
    bases_in_view.clear();
}

int GameRobot::getNumOfIncomingMessages() const {
    return incoming_messages.size();
}

Message* GameRobot::getIncomingMessage(int index) {
    return (index < getNumOfIncomingMessages() ? incoming_messages.at(index).get() : nullptr);
}

void GameRobot::addIncomingMessage(Message *msg) {
    auto incoming_msg = std::make_shared<ScriptMessage>();
    incoming_msg->copy(msg);
    incoming_messages.push_back(incoming_msg);
}

void GameRobot::clearIncomingMessages() {
    incoming_messages.clear();
}

Message *GameRobot::getOwnMessage() const {
    return own_message.get();
}

Message *GameRobot::getMemory() const {
    return memory.get();
}

int GameRobot::getDistanceTo(const Coord &coord) const {
    return getCoord().distanceTo(coord);
}

void GameRobot::prepareRun() {
    cancelMove();
    cancelShoot();
    cancelTransmit();        
}

void GameRobot::updatePostIteration() {
    takeDamage(getDamage());
    if (isShooting()) {
        setCooldown(getMaxCooldown());
    } else if (getCooldown() > 0) {
        setCooldown(getCooldown() - 1);
    }
    cancelMove();
    cancelShoot();
    cancelTransmit();
    clearDamage();
}

}
