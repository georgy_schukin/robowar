#pragma once

#include "active_game_object.h"

namespace robowar {

class BaseConfig;

/**
 * Base on the game map.
 */
class GameBase : public ActiveGameObject {
public:    
    GameBase(int id, const BaseConfig *config);

    virtual GameObjectType getType() const {
        return BASE;
    }

    void setConfig(const BaseConfig *config);

    bool isActive() const;
    bool isReadyToConstruct() const;    

    virtual int getMaxHitpoints() const;
    virtual int getMaxCooldown() const;

    void setAttackTeam(Team *team);
    Team* getAttackTeam() const;

    void clearAttack();

    void updatePostIteration();

protected:
    void init();

private:
    const BaseConfig *config;
    Team *attack_team;
};

}
