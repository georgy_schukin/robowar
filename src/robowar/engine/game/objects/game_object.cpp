#include "game_object.h"
#include "team.h"

namespace robowar {

GameObject::GameObject(int id) :
    object_id(id), team(nullptr) {
}

void GameObject::setCoord(const Coord &coord) {
    this->coord = coord;
}

void GameObject::setTeam(Team *team) {
    this->team = team;
}

int GameObject::getId() const {
    return object_id;
}

const Coord& GameObject::getCoord() const {
    return coord;
}

Team* GameObject::getTeam() const {
    return team;
}

bool GameObject::hasTeam() const {
    return (getTeam() != nullptr);
}

bool GameObject::isEnemyWith(GameObject *object) const {
    if (!hasTeam() || !object->hasTeam()) {
        return false;
    }
    return (getTeam()->getId() != object->getTeam()->getId());
}

bool GameObject::coordIsInRange(const Coord &coord, int range) const {
    return (getCoord().distanceTo(coord) <= range);
}

}
