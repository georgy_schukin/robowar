#pragma once

#include <string>
#include <memory>

namespace robowar {

class Script;
class RobotConfig;
class BaseConfig;

typedef std::shared_ptr<RobotConfig> RobotConfigPtr;
typedef std::shared_ptr<BaseConfig> BaseConfigPtr;

/**
 * Robot team.
 */
class Team {
public:    
    Team(int id, const std::string &name, Script *script);
    ~Team() {}

    void setName(const std::string &name);
    void setScript(Script *script);

    void setRobotConfig(const RobotConfigPtr &config);
    void setBaseConfig(const BaseConfigPtr &config);

    int getId() const;
    const std::string& getName() const;
    Script* getScript() const;

    const RobotConfig* getRobotConfig() const;
    const BaseConfig* getBaseConfig() const;

    static BaseConfigPtr getDefaultBaseConfig();

private:
    int id;
    std::string name;
    Script *script;
    RobotConfigPtr robot_config;
    BaseConfigPtr base_config;
};

}
