#include "active_game_object.h"

namespace robowar {

ActiveGameObject::ActiveGameObject(int id) :
    GameObject(id),
    hitpoints(0), damage(0), cooldown(0) {
}

void ActiveGameObject::setHitpoints(int hitpoints) {
    this->hitpoints = hitpoints;
}

void ActiveGameObject::setDamage(int damage) {
    this->damage = damage;
}

void ActiveGameObject::setCooldown(int cooldown) {
    this->cooldown = cooldown;
}

void ActiveGameObject::addDamage(int change) {    
    damage += change;
}

void ActiveGameObject::takeDamage(int damage) {
    hitpoints = std::max(0, std::min(hitpoints - damage, getMaxHitpoints()));
}

void ActiveGameObject::clearDamage() {
    setDamage(0);
}

int ActiveGameObject::getHitpoints() const {
    return hitpoints;
}

int ActiveGameObject::getDamage() const {
    return damage;
}

int ActiveGameObject::getCooldown() const {
    return cooldown;
}

bool ActiveGameObject::isDead() const {
    return (getHitpoints() <= 0) || (getDamage() >= getHitpoints());
}

}
