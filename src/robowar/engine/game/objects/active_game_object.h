#pragma once

#include "game_object.h"

namespace robowar {

/**
 * Active game object (can be damaged, has cooldown).
 */
class ActiveGameObject : public GameObject {    
public:    
    ActiveGameObject(int id);

    void setHitpoints(int hitpoints);
    void setDamage(int damage);
    void setCooldown(int cooldown);

    void addDamage(int change);
    void takeDamage(int damage);
    void clearDamage();

    int getHitpoints() const;
    int getDamage() const;
    int getCooldown() const;        

    virtual bool isDead() const;

    virtual int getMaxHitpoints() const = 0;
    virtual int getMaxCooldown() const = 0;

private:
    int hitpoints;
    int damage;
    int cooldown;
};

}
