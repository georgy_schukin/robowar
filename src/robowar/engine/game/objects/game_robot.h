#pragma once

#include "active_game_object.h"

#include <memory>
#include <vector>

namespace robowar {

class RobotConfig;
class Message;
class GameBase;

typedef std::shared_ptr<Message> MessagePtr;

/**
 * Robot on the game map.
 */
class GameRobot : public ActiveGameObject {
public:    
    GameRobot(int id, const RobotConfig *config);

    virtual GameObjectType getType() const {
        return ROBOT;
    }

    int getViewDistance() const;
    int getShootDistance() const;
    int getTransmitDistance() const;

    virtual int getMaxHitpoints() const;
    virtual int getMaxCooldown() const;

    int getMemorySize() const;
    int getMessageSize() const;

    void setMoveCoord(const Coord &coord);
    void setShootCoord(const Coord &coord);
    void transmit();

    void cancelMove();
    void cancelShoot();
    void cancelTransmit();

    bool canShoot() const;
    bool canMoveAt(const Coord &coord) const;
    bool canShootAt(const Coord &coord) const;

    bool isMoving() const;
    bool isShooting() const;
    bool isTransmitting() const;

    const Coord& getShootCoord() const;
    const Coord& getMoveCoord() const;

    int getNumOfRobotsInView() const;
    int getNumOfBasesInView() const;

    GameRobot* getRobotInView(int index);
    GameBase* getBaseInView(int index);

    void addRobotInView(GameRobot *robot);
    void addBaseInView(GameBase *base);

    void clearView();

    int getNumOfIncomingMessages() const;
    Message* getIncomingMessage(int index);
    void addIncomingMessage(Message *msg);

    void clearIncomingMessages();    

    Message* getOwnMessage() const;
    Message* getMemory() const;        

    void prepareRun();
    void updatePostIteration();

    const Coord& getNextCoord() const;

protected:
    void init();
    int getDistanceTo(const Coord &coord) const;

private:
    const RobotConfig *config;
    bool is_moving;
    bool is_shooting;
    bool is_transmitting;
    Coord move_coord;
    Coord shoot_coord;

    MessagePtr own_message;
    MessagePtr memory;
    std::vector<MessagePtr> incoming_messages;
    std::vector<GameRobot*> robots_in_view;
    std::vector<GameBase*> bases_in_view;
};

}
