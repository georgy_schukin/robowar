#include "game_cell.h"
#include "game_object.h"
#include "game_robot.h"
#include "game_base.h"

namespace robowar {

GameCell::GameCell():
    cell_type(NORMAL), object(nullptr) {
}

GameCell::GameCell(const Coord &coord, const CellType &type) :
    coord(coord), cell_type(type), object(nullptr) {
}

void GameCell::setCoord(const Coord &coord) {
    this->coord = coord;
}

void GameCell::setCellType(const CellType &type) {
    this->cell_type = type;
}

void GameCell::setObject(GameObject *object) {
    this->object = object;
    if (object) {
        object->setCoord(coord);
    }
}

void GameCell::setEmpty() {
    setObject(nullptr);
}

const Coord& GameCell::getCoord() const {
    return coord;
}

GameCell::CellType GameCell::getCellType() const {
    return cell_type;
}

GameObject* GameCell::getObject() const {
    return object;
}

bool GameCell::isRobotHere() const {
    return hasObjectOfType(GameObject::ROBOT);
}

bool GameCell::isBaseHere() const {
    return hasObjectOfType(GameObject::BASE);
}

bool GameCell::hasObjectOfType(int type) const {
    auto *obj = getObject();
    if (!obj) {
        return false;
    }
    return (obj->getType() == (GameObject::GameObjectType)type);
}

bool GameCell::isEmpty() const {
    return (getObject() == nullptr);
}

GameRobot* GameCell::getRobot() const {
    return getObject() ? dynamic_cast<GameRobot*>(getObject()) : nullptr;
}

GameBase* GameCell::getBase() const {
    return getObject() ? dynamic_cast<GameBase*>(getObject()) : nullptr;
}

}
