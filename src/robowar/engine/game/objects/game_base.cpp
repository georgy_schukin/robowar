#include "game_base.h"
#include "team.h"
#include "engine/game/conf/base_config.h"

namespace robowar {

GameBase::GameBase(int id, const BaseConfig *config) :
    ActiveGameObject(id),
    config(config), attack_team(nullptr)
{
    init();
}

void GameBase::init() {
    setHitpoints(getMaxHitpoints());
    setCooldown(0);
    clearDamage();
    clearAttack();
}

void GameBase::setConfig(const BaseConfig *config) {
    this->config = config;
    init();
}

bool GameBase::isActive() const {
    return hasTeam();
}

bool GameBase::isReadyToConstruct() const {
    return isActive() && (getCooldown() == 0);
}

int GameBase::getMaxHitpoints() const {
    return config ? config->getMaxHitpoints() : 0;
}

int GameBase::getMaxCooldown() const {
    return config ? config->getConstructionCooldown() : 0;
}

void GameBase::setAttackTeam(Team *team) {
    attack_team = team;
}

Team* GameBase::getAttackTeam() const {
    return attack_team;
}

void GameBase::clearAttack() {
    setAttackTeam(nullptr);
}

void GameBase::updatePostIteration() {    
    if (!getAttackTeam()) {
        clearDamage();
    }
    takeDamage(getDamage());
    if (isDead() && getAttackTeam()) {
        // Base is captured - transfer it to the attacking team.        
        setTeam(getAttackTeam());
        setConfig(getAttackTeam()->getBaseConfig());
    } else if (isActive()) {
        if (isReadyToConstruct()) {
            setCooldown(getMaxCooldown());
        } else if (getCooldown() > 0) {
            setCooldown(getCooldown() - 1);
        }
    }
    clearDamage();
    clearAttack();
}

}
