#include "team.h"
#include "engine/script/script.h"
#include "engine/game/conf/robot_config.h"
#include "engine/game/conf/base_config.h"

namespace robowar {

Team::Team(int id, const std::string &name, Script *script) :
    id(id), name(name), script(script)
{
    robot_config = std::make_shared<RobotConfig>();
    base_config = std::make_shared<BaseConfig>();
}

void Team::setName(const std::string &name) {
    this->name = name;
}

void Team::setScript(Script *script) {
    this->script = script;
}

void Team::setRobotConfig(const RobotConfigPtr &config) {
    robot_config = config;
}

void Team::setBaseConfig(const BaseConfigPtr &config) {
    base_config = config;
}

int Team::getId() const {
    return id;
}

const std::string& Team::getName() const {
    return name;
}

Script* Team::getScript() const {
    return script;
}

const RobotConfig* Team::getRobotConfig() const {
    return robot_config.get();
}

const BaseConfig* Team::getBaseConfig() const {
    return base_config.get();
}

BaseConfigPtr Team::getDefaultBaseConfig() {
    static BaseConfigPtr config = std::make_shared<BaseConfig>();
    return config;
}

}

