#pragma once

#include "engine/game/coords.h"

namespace robowar {

class GameRobot;
class GameBase;
class GameObject;

/**
 * Cell of game field.
 */
class GameCell {
public:
    enum CellType {
        NORMAL = 0,
        ROCK,
        LAVA
    };

public:
    GameCell();
    GameCell(const Coord &coord, const CellType &type=NORMAL);
    ~GameCell() {}

    void setCoord(const Coord &coord);
    void setCellType(const CellType &type);
    void setObject(GameObject *object);
    void setEmpty();

    const Coord& getCoord() const;
    CellType getCellType() const;
    GameObject *getObject() const;

    bool isRobotHere() const;
    bool isBaseHere() const;
    bool isEmpty() const;

    GameRobot* getRobot() const;
    GameBase* getBase() const;

private:
    bool hasObjectOfType(int type) const;

private:    
    Coord coord;
    CellType cell_type;
    GameObject *object;
};

}
