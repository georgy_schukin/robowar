#include "game_world.h"
#include "objects.h"
#include "conf/robot_config.h"
#include "conf/base_config.h"
#include "game_map.h"

#include <algorithm>
#include <cstdlib>
#include <ctime>

namespace robowar {

namespace {
    template <class Key, class Value>
    Value* getItemByKey(const typename std::map<Key, std::shared_ptr<Value>> &m, Key key) {
        auto it = m.find(key);
        return (it != m.end() ? it->second.get() : nullptr);
    }    

    template <class Value>
    Value* getItemByIndex(const typename std::vector<Value*> &v, int index) {
        return (index < (int)v.size() ? v.at(index) : nullptr);
    }
}


GameWorld::GameWorld() :
    last_robot_id(0), last_base_id(0), last_team_id(0)
{
    default_base_config = Team::getDefaultBaseConfig();
}

GameWorld::~GameWorld() {
}

void GameWorld::setMap(const GameMapPtr &map) {
    this->map = map;
}

GameMap* GameWorld::getMap() const {
    return map.get();
}

Team* GameWorld::addTeam(const std::string &name, Script *script) {
    const auto team_id = last_team_id++;
    TeamPtr team = std::make_shared<Team>(team_id, name, script);
    teams[team_id] = team;
    teams_v.push_back(team.get());
    return team.get();
}

GameRobot* GameWorld::addRobot(Team *team, const Coord &coord) {
    const auto robot_id = last_robot_id++;
    GameRobotPtr robot = std::make_shared<GameRobot>(robot_id, team->getRobotConfig());
    robot->setTeam(team);
    robot->setCoord(coord);
    getMap()->setObject(robot.get(), coord);
    robots[robot_id] = robot;
    robots_v.push_back(robot.get());
    return robot.get();
}

GameBase* GameWorld::addBase(Team *team, const Coord &coord) {
    const auto base_id = last_base_id++;
    auto config = team ? team->getBaseConfig() : getDefaultBaseConfig();
    GameBasePtr base = std::make_shared<GameBase>(base_id, config);
    base->setTeam(team);
    base->setCoord(coord);
    getMap()->setObject(base.get(), coord);
    bases[base_id] = base;
    bases_v.push_back(base.get());
    return base.get();
}

GameBase* GameWorld::addNeutralBase(const Coord &coord) {
    return addBase(nullptr, coord);
}

void GameWorld::setDefaultBaseConfig(const BaseConfigPtr &config) {
    default_base_config = config;
}

BaseConfig* GameWorld::getDefaultBaseConfig() const {
    return default_base_config.get();
}

void GameWorld::spawnRobot(GameBase *producer) {
    auto *cell = getMap()->getEmptyCellAround(producer->getCoord());
    if (!cell) {
        return;
    }
    addRobot(producer->getTeam(), cell->getCoord());
}

void GameWorld::moveRobot(GameRobot *robot, const Coord &coord) {
    getMap()->moveObject(robot, coord);
}

void GameWorld::removeRobot(GameRobot *robot) {
    getMap()->removeObject(robot);
    robots_v.erase(std::find(robots_v.begin(), robots_v.end(), robot));    
    robots.erase(robot->getId());
}

void GameWorld::removeBase(GameBase *base) {
    getMap()->removeObject(base);
    bases_v.erase(std::find(bases_v.begin(), bases_v.end(), base));
    bases.erase(base->getId());
}

/*void GameWorld::setTeamForBase(GameBase *base, Team *team) {
    base->setTeam(team);
    base->setConfig(team->getBaseConfig());
}*/

GameRobot* GameWorld::getRobotById(int id) {
    return getItemByKey(robots, id);
}

GameBase* GameWorld::getBaseById(int id) {
    return getItemByKey(bases, id);
}

Team* GameWorld::getTeamById(int id) {
    return getItemByKey(teams, id);
}

int GameWorld::getNumOfRobots() const {
    return robots_v.size();
}

int GameWorld::getNumOfBases() const {
    return bases_v.size();
}

int GameWorld::getNumOfTeams() const {
    return teams_v.size();
}

GameRobot* GameWorld::getRobot(int index) {
    return getItemByIndex(robots_v, index);
}

GameBase* GameWorld::getBase(int index) {
    return getItemByIndex(bases_v, index);
}

Team* GameWorld::getTeam(int index) {
    return getItemByIndex(teams_v, index);
}

const std::vector<GameRobot*>& GameWorld::getRobots() const {
    return robots_v;
}

const std::vector<GameBase*>& GameWorld::getBases() const {
    return bases_v;
}

const std::vector<Team*>& GameWorld::getTeams() const {
    return teams_v;
}

void GameWorld::generateRandom(int num_of_bases, int num_of_neutral_bases) {
    srand((unsigned)time(0));
    for (int i = 0; i < num_of_bases; i++) {
        int team_num = i % getNumOfTeams();
        addBase(getTeam(team_num), getRandomFreeBaseCoord(5, 2));
    }
    for (int i = 0; i < num_of_neutral_bases; i++) {
        addNeutralBase(getRandomFreeBaseCoord(5, 2));
    }
}

Coord GameWorld::getRandomFreeBaseCoord(int free_range, int border_range) {
    int num_of_tries = 50;
    while (num_of_tries > 0) {
        const auto coord = getMap()->getRandomFreeCell()->getCoord();
        if (isOutOfRangeFromOtherBases(coord, free_range) &&
            isAwayFromBorder(coord, border_range)) {
            return coord;
        }
        num_of_tries--;
    }
    return getMap()->getRandomFreeCell()->getCoord();
}

bool GameWorld::isOutOfRangeFromOtherBases(const Coord &coord, int range) const {
    for (auto *base: getBases()) {
        if (coord.distanceTo(base->getCoord()) < range) {
            return false;
        }
    }
    return true;
}

bool GameWorld::isAwayFromBorder(const Coord &coord, int range) const {
    return getMap()->hasAllCells(coord, range);
}

int GameWorld::getNumOfRobotsForTeam(int team_id) const {
    int num = 0;
    for (auto *robot: getRobots()) {
        if (robot->getTeam()->getId() == team_id) {
            num++;
        }
    }
    return num;
}

Team* GameWorld::getWinner() const {
    Team *winner = nullptr;
    for (auto *team: getTeams()) {
        if (getNumOfRobotsForTeam(team->getId()) > 0) {
            if (!winner) {
                winner = team;
            } else {
                return nullptr;
            }
        }
    }
    return winner;
}

}
