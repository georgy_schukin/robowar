#include "direction.h"

#include <map>
#include <algorithm>

namespace robowar {

namespace {
    const std::vector<Direction> around_directions = {DIR_NORTH_EAST,
                                 DIR_EAST,
                                 DIR_SOUTH_EAST,
                                 DIR_SOUTH_WEST,
                                 DIR_WEST,
                                 DIR_NORTH_WEST};

    const std::map<std::string, Direction> direction_names = {
        {"none", DIR_NONE},
        {"ne", DIR_NORTH_EAST},
        {"e", DIR_EAST},
        {"se", DIR_SOUTH_EAST},
        {"sw", DIR_SOUTH_WEST},
        {"w", DIR_WEST},
        {"nw", DIR_NORTH_WEST}
    };
}

Direction directionFromString(const std::string &dir_str) {
    auto dir = dir_str;
    std::transform(dir.begin(), dir.end(), dir.begin(), ::tolower);
    auto it = direction_names.find(dir);
    return (it != direction_names.end() ? it->second : DIR_NONE);
}

const std::vector<Direction>& getAroundDirections() {
    return around_directions;
}

}
