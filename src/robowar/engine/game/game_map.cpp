#include "game_map.h"
#include "engine/game/objects/game_cell.h"
#include "engine/game/objects/game_object.h"

#include <cstdlib>

namespace robowar {

void GameMap::addCell(const Coord &coord) {
    if (hasCell(coord)) {
        return;
    }
    cells[coord] = std::make_shared<GameCell>(coord);
}

void GameMap::removeCell(const Coord &coord) {
    cells.erase(coord);
}

GameCell* GameMap::getCell(const Coord &coord) const {
    auto it = cells.find(coord);
    return (it != cells.end() ? it->second.get() : nullptr);
}

bool GameMap::hasCell(const Coord &coord) const {
    return (getCell(coord) != nullptr);
}

void GameMap::setObject(GameObject *object, const Coord &coord) {
    auto cell = getCell(coord);
    if (cell) {
        cell->setObject(object);
    }
}

void GameMap::moveObject(GameObject *object, const Coord &new_coord) {
    auto prev_cell = getCell(object->getCoord());
    if (prev_cell) {
        if (prev_cell->getObject() == object) {
            prev_cell->setEmpty();
        }
    }
    setObject(object, new_coord);
}

void GameMap::removeObject(GameObject *object) {
    auto cell = getCell(object->getCoord());
    if (cell) {
        cell->setEmpty();
    }
}

std::vector<Coord> GameMap::getCellCoords() const {
    std::vector<Coord> result;
    for (const auto &p: cells) {
        result.push_back(p.first);
    }
    return result;
}

GameCell* GameMap::getEmptyCellAround(const Coord &coord) {
    for (auto direction: getAroundDirections()) {
        const auto near_coord = coord.atDirection(direction);
        auto cell = getCell(near_coord);
        if (cell && cell->isEmpty()) {
            return cell;
        }
    }
    return nullptr;
}

std::vector<GameCell*> GameMap::getCellsInRange(const Coord &coord, int range) {
    const auto coords = coord.getCoordsInRange(range);
    std::vector<GameCell*> result;
    for (auto &cell_coord: coords) {
        auto cell = getCell(cell_coord);
        if (cell) {
            result.push_back(cell);
        }
    }
    return result;
}

bool GameMap::hasAllCells(const Coord &coord, int range) const {
    const auto coords = coord.getCoordsInRange(range);
    for (const auto &cell_coord: coords) {
        if (!hasCell(cell_coord)) {
            return false;
        }
    }
    return true;
}

int GameMap::getNumOfCells() const {
    return cells.size();
}

GameCell* GameMap::getRandomFreeCell() {
    int num_of_tries = cells.size();
    while (num_of_tries > 0) {
        const int cell_index = rand() % getNumOfCells();
        auto it = cells.begin();
        std::advance(it, cell_index);
        auto *cell = it->second.get();
        if (cell->isEmpty()) {
            return cell;
        }
        num_of_tries--;
    }
    throw std::runtime_error("Can't find free cell");
    return nullptr;
}

}

