#include "script_robot.h"
#include "engine/game/objects.h"

namespace robowar {

ScriptRobot::ScriptRobot(GameRobot *robot, GameRobot *viewer) :
    ScriptObject(robot, viewer),
    robot(robot) {
}

int ScriptRobot::getHitpoints() const {
    return getRobot()->getHitpoints();
}

int ScriptRobot::getWeaponCooldown() const {
    return getRobot()->getCooldown();
}

int ScriptRobot::getMaxHitpoints() const {
    return getRobot()->getMaxHitpoints();
}

int ScriptRobot::getMaxWeaponCooldown() const {
    return getRobot()->getMaxCooldown();
}

int ScriptRobot::getViewDistance() const {
    return getRobot()->getViewDistance();
}

int ScriptRobot::getShootDistance() const {
    return getRobot()->getShootDistance();
}

int ScriptRobot::getTransmitDistance() const {
    return getRobot()->getTransmitDistance();
}

bool ScriptRobot::canShoot() const {
    return getRobot()->canShoot();
}

GameRobot* ScriptRobot::getRobot() const {
    return robot;
}

}
