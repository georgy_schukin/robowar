#include "script_robot_controller.h"
#include "engine/script/implementation.h"
#include "engine/game/objects.h"
#include "engine/game/game_map.h"

namespace robowar {

ScriptRobotController::ScriptRobotController(GameRobot *robot, GameMap *map) :
    ScriptRobot(robot, robot),
    map(map)
{
    for (int i = 0; i < robot->getNumOfRobotsInView(); i++) {
        robots_in_view.push_back(std::make_shared<ScriptRobot>(robot->getRobotInView(i), robot));
    }
    for (int i = 0; i < robot->getNumOfBasesInView(); i++) {
        bases_in_view.push_back(std::make_shared<ScriptBase>(robot->getBaseInView(i), robot));
    }
}

int ScriptRobotController::getNumOfRobotsInView() const {
    return robots_in_view.size();
}

int ScriptRobotController::getNumOfBasesInView() const {
    return bases_in_view.size();
}

Robot* ScriptRobotController::getRobotInView(int index) const {    
    return (index < getNumOfRobotsInView() ? robots_in_view.at(index).get() : nullptr);
}

Base* ScriptRobotController::getBaseInView(int index) const {
    return (index < getNumOfBasesInView() ? bases_in_view.at(index).get() : nullptr);
}

int ScriptRobotController::getNumOfMessages() const {
    return getRobot()->getNumOfIncomingMessages();
}

Message* ScriptRobotController::getMessage(int index) const {
    return getRobot()->getIncomingMessage(index);
}

Message* ScriptRobotController::getOwnMessage() const {
    return getRobot()->getOwnMessage();
}

Message* ScriptRobotController::getMemory() const {
    return getRobot()->getMemory();
}

bool ScriptRobotController::isShooting() const {
    return getRobot()->isShooting();
}

bool ScriptRobotController::isMoving() const {
    return getRobot()->isMoving();
}

bool ScriptRobotController::isTransmitting() const {
    return getRobot()->isTransmitting();
}

bool ScriptRobotController::canMoveAt(int direction) const {
    const auto dest_coord = getCoord().atDirection(static_cast<Direction>(direction));
    auto dest_cell = getMap()->getCell(dest_coord);
    if (!dest_cell || dest_cell->isBaseHere()) {
        return false;
    }    
    if (!getRobot()->canMoveAt(dest_coord)) {
        return false;
    }
    return true;
}

/*bool ScriptRobotController::canMoveAt(const std::string &direction) const {
    return canMoveAt(directionFromString(direction));
}*/

bool ScriptRobotController::canShootAt(const Coord &coord) const {
    if (!getMap()->hasCell(coord)) {
        return false;
    }
    if (!getRobot()->canShootAt(coord)) {
        return false;
    }
    return true;
}

void ScriptRobotController::moveAt(int direction) {
    if (!canMoveAt(direction)) {
        cancelMove();
        return;
    }
    getRobot()->setMoveCoord(getCoord().atDirection(static_cast<Direction>(direction)));
}

/*void ScriptRobotController::moveAt(const std::string &direction) {
    moveAt(directionFromString(direction));
}*/

void ScriptRobotController::moveTo(const Coord &coord) {
    moveAt(getCoord().directionTo(coord));
}

void ScriptRobotController::shootAt(const Coord &coord) {
    if (!canShootAt(coord)) {
        cancelShoot();
        return;
    }
    getRobot()->setShootCoord(coord);
}

void ScriptRobotController::transmit() {
    getRobot()->transmit();
}

void ScriptRobotController::cancelMove() {
    getRobot()->cancelMove();
}

void ScriptRobotController::cancelShoot() {
    getRobot()->cancelShoot();
}

void ScriptRobotController::cancelTransmit() {
    getRobot()->cancelTransmit();
}

bool ScriptRobotController::cellExists(const Coord &coord) const {
    if (getRobot()->getCoord().distanceTo(coord) > getViewDistance()) {
        return false;
    }
    return getMap()->hasCell(coord);
}

Cell* ScriptRobotController::getCell(const Coord &coord) {
    if (!cellExists(coord)) {
        return nullptr;
    }
    auto it = cells.find(coord);
    if (it == cells.end()) {
        auto cell = std::make_shared<ScriptCell>(getMap()->getCell(coord), getViewer());
        cells[coord] = cell;
        return cell.get();
    } else {
        return it->second.get();
    }
}

GameMap* ScriptRobotController::getMap() const {
    return map;
}

}
