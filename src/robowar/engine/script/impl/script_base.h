#pragma once

#include "engine/script/interfaces/base.h"
#include "script_object.h"

namespace robowar {

class GameBase;
class GameRobot;

class ScriptBase : public ScriptObject, virtual public Base {
public:
    ScriptBase(GameBase *base, GameRobot *viewer);

    virtual bool isActive() const;

    virtual int getHitpoints() const;
    virtual int getConstructionCooldown() const;

    virtual int getMaxHitpoints() const;
    virtual int getMaxConstructionCooldown() const;

private:
    GameBase* getBase() const;

private:
    GameBase *base;
};

}
