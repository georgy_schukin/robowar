#include "script_message.h"

namespace robowar {

ScriptMessage::ScriptMessage() {
}

ScriptMessage::ScriptMessage(const size_t &size) :
    data(size, 0) {
}

int ScriptMessage::getSize() const {
    return static_cast<int>(data.size());
}

void ScriptMessage::write(int index, int value) {
    if (index < getSize()) {
        data[index] = value;
    }
}

int ScriptMessage::read(int index) const {
    return (index < getSize() ? data[index] : 0);
}

void ScriptMessage::clear() {
    std::fill(data.begin(), data.end(), 0);
}

void ScriptMessage::copy(Message *msg) {
    const int size = std::min(getSize(), msg->getSize());
    clear();
    for (int i = 0; i < size; i++) {
        write(i, msg->read(i));
    }
}

}
