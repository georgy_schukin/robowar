#include "script_cell.h"
#include "script_robot.h"
#include "script_base.h"
#include "engine/game/objects.h"

namespace robowar {

ScriptCell::ScriptCell(GameCell *cell, GameRobot *viewer) :
    cell(cell), viewer(viewer) {
}

Coord ScriptCell::getCoord() const {
    return cell->getCoord();
}

bool ScriptCell::isEmpty() const {
    return cell->isEmpty();
}

bool ScriptCell::isRobotHere() const {
    return cell->isRobotHere();
}

bool ScriptCell::isBaseHere() const {
    return cell->isBaseHere();
}

Robot* ScriptCell::getRobot() const {
    if (!isRobotHere()) {
        return nullptr;
    }
    if (!object) {
        object = std::make_shared<ScriptRobot>(cell->getRobot(), viewer);
    }
    return dynamic_cast<Robot*>(object.get());
}

Base* ScriptCell::getBase() const {
    if (!isBaseHere()) {
        return nullptr;
    }
    if (!object) {
        object = std::make_shared<ScriptBase>(cell->getBase(), viewer);
    }
    return dynamic_cast<Base*>(object.get());
}

}
