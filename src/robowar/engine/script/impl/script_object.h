#pragma once

#include "engine/script/interfaces/object.h"

namespace robowar {

class GameObject;
class GameRobot;

class ScriptObject : virtual public Object {
public:
    ScriptObject(GameObject *object, GameRobot *viewer);

    virtual Coord getCoord() const;
    virtual int getTeamId() const;

    virtual bool isEnemy() const;
    virtual bool isNeutral() const;

    virtual int getDistanceTo() const;

    virtual bool isInViewRange() const;
    virtual bool isInShootRange() const;
    virtual bool isInTransmitRange() const;

protected:
    GameObject* getObject() const;
    GameRobot* getViewer() const;

private:
    GameObject *object;
    GameRobot *viewer;
    int distance;
};

}
