#include "engine/script/interfaces/cell.h"

#include <memory>

namespace robowar {

class GameRobot;
class GameCell;
class ScriptObject;
class Object;

class ScriptCell : public Cell {
public:
    ScriptCell(GameCell *cell, GameRobot *viewer);

    virtual Coord getCoord() const;

    virtual bool isEmpty() const;
    virtual bool isRobotHere() const;
    virtual bool isBaseHere() const;

    virtual Robot* getRobot() const;
    virtual Base* getBase() const;

private:
    GameCell *cell;
    GameRobot *viewer;
    mutable std::shared_ptr<Object> object;
};

}
