#pragma once

#include "engine/script/interfaces/message.h"

#include <vector>
#include <cstddef>

namespace robowar {

class ScriptMessage : public Message {
public:
    ScriptMessage();
    ScriptMessage(const size_t &size);

    virtual int getSize() const;

    virtual void write(int index, int value);

    virtual int read(int index) const;

    virtual void clear();

    virtual void copy(Message *msg);

private:
    std::vector<int> data;
};

}
