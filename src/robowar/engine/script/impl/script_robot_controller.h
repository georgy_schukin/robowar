#pragma once

#include "engine/script/interfaces/robot_controller.h"
#include "script_robot.h"

#include <vector>
#include <memory>
#include <map>

namespace robowar {

class GameRobot;
class GameMap;

class ScriptRobotController : public RobotController, public ScriptRobot {
public:
    ScriptRobotController(GameRobot *robot, GameMap *map);

    virtual int getNumOfRobotsInView() const;
    virtual int getNumOfBasesInView() const;

    virtual Robot* getRobotInView(int index) const;
    virtual Base* getBaseInView(int index) const;

    virtual int getNumOfMessages() const;
    virtual Message* getMessage(int index) const;

    virtual Message* getOwnMessage() const;
    virtual Message* getMemory() const;

    virtual bool isShooting() const;
    virtual bool isMoving() const;
    virtual bool isTransmitting() const;

    virtual bool canMoveAt(int direction) const;
    //virtual bool canMoveAt(const std::string &direction) const;
    virtual bool canShootAt(const Coord &coord) const;

    virtual void moveAt(int direction);
    //virtual void moveAt(const std::string &direction);
    virtual void moveTo(const Coord &coord);
    virtual void shootAt(const Coord &coord);
    virtual void transmit();

    virtual void cancelMove();
    virtual void cancelShoot();
    virtual void cancelTransmit();

    virtual bool cellExists(const Coord &coord) const;
    virtual Cell* getCell(const Coord &coord);

private:
    GameMap* getMap() const;       

private:    
    GameMap *map;
    std::vector<std::shared_ptr<Robot>> robots_in_view;
    std::vector<std::shared_ptr<Base>> bases_in_view;
    std::map<Coord, std::shared_ptr<Cell>> cells;
};

}
