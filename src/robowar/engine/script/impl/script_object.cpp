#include "script_object.h"
#include "engine/game/objects/game_object.h"
#include "engine/game/objects/game_robot.h"
#include "engine/game/objects/team.h"

namespace robowar {

ScriptObject::ScriptObject(GameObject *object, GameRobot *viewer) :
    object(object), viewer(viewer)
{
    distance = object->getCoord().distanceTo(viewer->getCoord());
}

Coord ScriptObject::getCoord() const {
    return object->getCoord();
}

int ScriptObject::getTeamId() const {
    if (!object->hasTeam()) {
        return -1;
    }
    return object->getTeam()->getId();
}

bool ScriptObject::isEnemy() const {
    return object->isEnemyWith(viewer);
}

bool ScriptObject::isNeutral() const {
    return !object->hasTeam();
}

int ScriptObject::getDistanceTo() const {
    return distance;
}

bool ScriptObject::isInViewRange() const {
    return (getDistanceTo() <= getViewer()->getViewDistance());
}

bool ScriptObject::isInShootRange() const {
    return (getDistanceTo() <= getViewer()->getShootDistance());
}

bool ScriptObject::isInTransmitRange() const {
    return (getDistanceTo() <= getViewer()->getTransmitDistance());
}

GameObject* ScriptObject::getObject() const {
    return object;
}

GameRobot* ScriptObject::getViewer() const {
    return viewer;
}

}
