#pragma once

#include "engine/script/interfaces/robot.h"
#include "script_object.h"

namespace robowar {

class GameRobot;

class ScriptRobot : public ScriptObject, virtual public Robot {
public:
    ScriptRobot(GameRobot *robot, GameRobot *viewer);

    virtual int getHitpoints() const;
    virtual int getWeaponCooldown() const;

    virtual int getMaxHitpoints() const;
    virtual int getMaxWeaponCooldown() const;

    virtual int getViewDistance() const;
    virtual int getShootDistance() const;
    virtual int getTransmitDistance() const;

    virtual bool canShoot() const;

protected:
    GameRobot* getRobot() const;

private:
    GameRobot *robot;
};

}
