#include "script_base.h"
#include "engine/game/objects/game_base.h"

namespace robowar {

ScriptBase::ScriptBase(GameBase *base, GameRobot *viewer) :
    ScriptObject(base, viewer),
    base(base) {
}

bool ScriptBase::isActive() const {
    return getBase()->isActive();
}

int ScriptBase::getHitpoints() const {
    return getBase()->getHitpoints();
}

int ScriptBase::getMaxHitpoints() const {
    return getBase()->getHitpoints();
}

int ScriptBase::getConstructionCooldown() const {
    return getBase()->getCooldown();
}

int ScriptBase::getMaxConstructionCooldown() const {
    return getBase()->getMaxCooldown();
}

GameBase* ScriptBase::getBase() const {
    return base;
}

}
