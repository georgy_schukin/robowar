#include "script_library.h"
#include "scripts.h"

#include <memory>
#include <stdexcept>

namespace robowar {

ScriptLibrary::ScriptLibrary() {
}

//const std::string ROBOT_MAIN_FUNCTION = "execute";

/*void ScriptLibrary::addLocalScript(const std::string &name, RobotFunction action) {
    auto script = std::make_shared<LocalScript>(action);
    addScript(name, script);
}

void ScriptLibrary::addDLLScript(const std::string &name, const std::string &filename) {
    #ifdef _WIN32
        const std::string &lib_file = prefix + filename + std::string(".dll"); // Windows .dll
    #else
        const std::string &lib_file = prefix + std::string("lib") + filename + std::string(".so"); // Linux .so
    #endif
    auto script = std::make_shared<DLLScript>(lib_file, ROBOT_MAIN_FUNCTION);
    addScript(name, script);
}

void ScriptLibrary::addAngelScript(const std::string &name, const std::string &filename) {
    auto script = std::make_shared<AngelScript>(filename);
    addScript(name, script);
}*/

Script* ScriptLibrary::addScript(const std::string &path) {
    auto script = getScript(path);
    if (script) {
        return script;
    }
    if (path.find(".as") != std::string::npos) {
        return addScript(path, AngelScript::newInstance(path));
    } else if (path.find(".dll") != std::string::npos || path.find(".so") != std::string::npos) {
        return addScript(path, DLLScript::newInstance(path));
    } else {
        throw std::runtime_error(std::string("Failed to add script ") + path);
    }
    return nullptr;
}

Script* ScriptLibrary::addScript(const std::string &name, const ScriptPtr &script) {
    std::unique_lock<std::mutex> lock(mutex);
    if (!script) {
        throw std::runtime_error("Null script " + name);
    }
    if (!script->isValid()) {
        throw std::runtime_error("Not valid script " + name);
    }
    scripts[name] = script;
    return script.get();
}

Script* ScriptLibrary::getScript(const std::string &name) const {
    std::unique_lock<std::mutex> lock(mutex);
    auto it = scripts.find(name);
    return (it != scripts.end() ? it->second.get() : nullptr);
}

bool ScriptLibrary::isLoaded(const std::string &name) const {
    std::unique_lock<std::mutex> lock(mutex);
    return (scripts.find(name) != scripts.end());
}

}

