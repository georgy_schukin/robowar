#pragma once

#include "engine/script/script.h"

#include <string>
#include <map>
#include <mutex>

namespace robowar {

/**
 * Collection of robot scripts (loaded from external libraries).
 */
class ScriptLibrary {
public:
    ScriptLibrary();
    ~ScriptLibrary() {}    

    // Identify script type by path.
    Script* addScript(const std::string &path);
    Script* addScript(const std::string &name, const ScriptPtr &script);

    Script* getScript(const std::string &name) const;

    bool isLoaded(const std::string &name) const;        

private:
    typedef std::map<std::string, ScriptPtr> ScriptMap;

private:
    ScriptMap scripts;
    mutable std::mutex mutex;
};

}
