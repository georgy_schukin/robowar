#pragma once

#include "object.h"

namespace robowar {

/**
 * @brief Base interface.
 *
 * Interface for interaction with a robot base.
 */
class Base : virtual public Object {
public:
    /// Check if the base is active, i.e. produce robots.
    virtual bool isActive() const = 0;    

    /// Get current robot construction cooldown of the base.
    /// Zero cooldown means that the base can produce a robot.
    virtual int getConstructionCooldown() const = 0;    

    /// Get maximum robot construction cooldown of the base.
    virtual int getMaxConstructionCooldown() const = 0;
};

}
