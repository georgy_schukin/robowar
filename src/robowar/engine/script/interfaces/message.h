#pragma once

namespace robowar {

/**
 * @brief Message interface.
 *
 * Interface for a message in robot interactions.
 */
class Message {
public:
    virtual ~Message() {}

    /// Get message size.
    /// @returns message size in integers.
    virtual int getSize() const = 0;

    /// Write integer value by given index.
    virtual void write(int index, int value) = 0;

    /// Read integer value by given index.
    /// @returns value or zero if index is out of range.
    virtual int read(int index) const = 0;

    /// Init message by zeroes.
    virtual void clear() = 0;

    /// Copy given message.
    virtual void copy(Message *msg) = 0;
};

}
