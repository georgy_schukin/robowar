#pragma once

#include "engine/game/coords.h"

namespace robowar {

/**
 * @brief Object interface.
 *
 * Interface for a general map object (robot, base, etc.).
 */
class Object {
public:
    virtual ~Object() {}

    /// Get coordinate of the object.
    virtual Coord getCoord() const = 0;

    /// Get id of the object's team.
    /// @returns team's id or -1 if object has no team.
    virtual int getTeamId() const = 0;    

    /// Check if the object is an enemy (belongs to another team).
    virtual bool isEnemy() const = 0;

    /// Check if the object is neutral (has no team).
    virtual bool isNeutral() const = 0;

    /// Get distance to the object.
    virtual int getDistanceTo() const = 0;

    /// Check if the object is in view range.
    virtual bool isInViewRange() const = 0;

    /// Check if the object is in shoot range.
    virtual bool isInShootRange() const = 0;

    /// Check if the object is in transmit range.
    virtual bool isInTransmitRange() const = 0;

    /// Get current hitpoints of the object.
    virtual int getHitpoints() const = 0;

    /// Get maximum hitpoints of the object.
    virtual int getMaxHitpoints() const = 0;
};

}
