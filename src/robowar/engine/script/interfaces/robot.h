#pragma once

#include "object.h"

namespace robowar {

/**
 * @brief Robot interface.
 *
 * Interface for interaction with a robot.
 */
class Robot : virtual public Object {
public:    
    /// Get current weapon cooldown of the robot.
    /// Zero cooldown means that the robot can shoot.
    virtual int getWeaponCooldown() const = 0;    

    /// Get maximum weapon cooldown of the robot.
    virtual int getMaxWeaponCooldown() const = 0;

    /// Get view range of the robot.
    virtual int getViewDistance() const = 0;
    /// Get shoot range of the robot.
    virtual int getShootDistance() const = 0;
    /// Get transmit range of the robot.
    virtual int getTransmitDistance() const = 0;

    /// Check if the robot can shoot now.
    virtual bool canShoot() const = 0;
};

}
