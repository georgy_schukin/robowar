#pragma once

#include "engine/game/coords.h"

namespace robowar {

class Robot;
class Base;

/**
 * @brief Cell interface.
 *
 * Interface for a (hexagonal) cell on a game map.
 */
class Cell {
public:
    virtual ~Cell() {}

    /// Get coordinate of the cell.
    virtual Coord getCoord() const = 0;

    /// Check if the cell is empty (has no object on it).
    virtual bool isEmpty() const = 0;

    /// Check if there is a robot on the cell.
    virtual bool isRobotHere() const = 0;

    /// Check if there is a base on the cell.
    virtual bool isBaseHere() const = 0;

    /// Get robot on the cell.
    /// @returns robot or null if there is no robot.
    virtual Robot* getRobot() const = 0;

    /// Get base on the cell.
    /// @returns base or null if there is no base.
    virtual Base* getBase() const = 0;
};

}
