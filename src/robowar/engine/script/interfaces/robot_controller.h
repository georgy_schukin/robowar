#pragma once

#include "robot.h"

#include <string>

namespace robowar {

class Message;
class Base;
class Cell;

/**
 * @brief Robot controller interface.
 *
 * Interface to control a robot in a robot script.
 */
class RobotController : virtual public Robot {
public:
    /// Get number of other robots in this robot's view range.
    virtual int getNumOfRobotsInView() const = 0;

    /// Get number of bases in this robot's view range.
    virtual int getNumOfBasesInView() const = 0;

    /// Get robot in view by given index.
    /// @returns robot or null if index is out of range.
    virtual Robot* getRobotInView(int index) const = 0;

    /// Get base in view by given index.
    /// @returns base or null if index is out of range.
    virtual Base* getBaseInView(int index) const = 0;

    /// Get number of incoming messages.
    virtual int getNumOfMessages() const = 0;

    /// Get incoming message by index.
    /// @returns message of null if index is out of range.
    virtual Message* getMessage(int index) const = 0;

    /// Get own message.
    virtual Message* getOwnMessage() const = 0;
    /// Get own memory.
    virtual Message* getMemory() const = 0;

    /// Check if robot will attempt to shoot.
    virtual bool isShooting() const = 0;
    /// Check if robot will attempt to move.
    virtual bool isMoving() const = 0;
    /// Check if robot will attempt to transmit.
    virtual bool isTransmitting() const = 0;

    /// Check if robot can move to neighbor cell in given direction.
    /// @returns true if movement is possible, false otherwise.
    /// Movement cannot be made if there is no cell in given direction or the cell is occupied by a base.
    /// If there is a robot on the neighbor cell, movement is possible, assuming that the robot will free the cell.
    virtual bool canMoveAt(int direction) const = 0;

    //virtual bool canMoveAt(const std::string &direction) const = 0;

    /// Check if robot can shoot at the cell with the given coord.
    /// @returns true if shooting is possible, false otherwise.
    /// Shooting cannot be made if there is no cell with given coordinate.
    virtual bool canShootAt(const Coord &coord) const = 0;

    /// Attempt to move in given direction.
    /// If movement isn't possible, it will be cancelled.
    virtual void moveAt(int direction) = 0;

    //virtual void moveAt(const std::string &direction) = 0;

    /// Attempt to move in direction to given coordinate.
    /// If movement isn't possible, it will be cancelled.
    virtual void moveTo(const Coord &coord) = 0;

    /// Attempt to shoot at the cell with given coordinate.
    /// If shooting isn't possible, it will be cancelled.
    virtual void shootAt(const Coord &coord) = 0;

    /// Start transmission.
    /// The content of own message will be transmitted.
    /// Message will be transmitted to ally robots in transmit range and become available to them on the next iteration.
    virtual void transmit() = 0;

    /// Cancel move attempt.
    virtual void cancelMove() = 0;
    /// Cancel ahoot attempt.
    virtual void cancelShoot() = 0;
    /// Cancel transmit attempt.
    virtual void cancelTransmit() = 0;

    /// Check if cell with given coordinate exists.
    /// @returns true if cell is in view range and exists, false otherwise.
    /// Only cells in this robot's view range can be checked for existence.
    virtual bool cellExists(const Coord &coord) const = 0;

    /// Get cell in view range.
    /// @returns cell or null if cell doesn't exist or out of view range.
    virtual Cell* getCell(const Coord &coord) = 0;
};

}
