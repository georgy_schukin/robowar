#pragma once

#include <memory>

namespace robowar {

class RobotController;

/**
 * Script to control robots.
 */
class Script {
public:    
    virtual ~Script() {}

    /// Check that script is OK.
    virtual bool isValid() const = 0;

    /// Call robot action.
    virtual void run(RobotController *robot) = 0;
};

typedef std::shared_ptr<Script> ScriptPtr;

}
