#pragma once

#include "engine/script/script.h"

#include <string>

class asIScriptEngine;
class asIScriptFunction;
class asIScriptModule;

namespace robowar {

class AngelScript : public Script {
public:    
    AngelScript(asIScriptEngine *engine, const std::string &filename);

    virtual bool isValid() const;

    virtual void run(RobotController *robot);

    static ScriptPtr newInstance(const std::string &filename);

private:
    asIScriptModule* loadModuleFromFile(const std::string &filename);

    static std::string newModuleName();

private:
    asIScriptEngine *engine;
    asIScriptModule *module;
    asIScriptFunction *function;
};

}
