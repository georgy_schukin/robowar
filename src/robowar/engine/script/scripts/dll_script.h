#pragma once

#include "local_script.h"

#ifdef _WIN32
#include <Windows.h>
#endif

namespace robowar {

/**
 * Script is loaded from external C/C++ dll/shared library.
 */
class DLLScript : public LocalScript {
public:
    DLLScript(const std::string &library, const std::string &func_name);

    virtual ~DLLScript();

    static ScriptPtr newInstance(const std::string &filename, const std::string &func_name="execute");

private:
    int open(const std::string &library, const std::string &func);
    void close();

private:
#ifdef _WIN32
    typedef HMODULE LibHandle;
#else
    typedef void* LibHandle;
#endif

private:
    LibHandle lib_handle;
};

}
