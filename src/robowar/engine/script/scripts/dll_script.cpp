#include "dll_script.h"

#include <iostream>
#include <string>

#ifdef _WIN32
#include <Windows.h>
#else
#include <dlfcn.h>
#endif

namespace robowar {

typedef void(*ROBOT_FUNCTION)(RobotController*);

DLLScript::DLLScript(const std::string &library, const std::string &func_name) :
    LocalScript(NULL), lib_handle(NULL)
{
    open(library, func_name);
}

DLLScript::~DLLScript() {
    close();
}

int DLLScript::open(const std::string &library, const std::string &func_name) {
#ifdef _WIN32    
    const std::wstring library_w(library.begin(), library.end()); // LoadLibrary requires wchar string

    lib_handle = LoadLibraryEx(library_w.c_str(), NULL, 0);
    if (lib_handle == NULL) {
        std::cerr << "ERROR: cannot load library " << library << ": error code: " << GetLastError() << std::endl;        
        return -1;
    }

    auto action = (ROBOT_FUNCTION)GetProcAddress(lib_handle, func_name.c_str());
    if (action == NULL) {
        std::cerr << "ERROR: cannot load function " << func_name << " from " << library << ": error code " << GetLastError() << std::endl;
        std::cerr << "Robot main function must be 'void " << func_name << "(RobotController*)'" << std::endl;
        return -1;
    }    
#else    
    lib_handle = dlopen(library.c_str(), RTLD_LAZY | RTLD_GLOBAL);
    if (lib_handle == NULL) {
        std::cerr << "ERROR: cannot load library " << library << ": " << dlerror() << std::endl;
        return -1;
    }

    auto action = (ROBOT_FUNCTION)dlsym(lib_handle, func_name.c_str());
    if (action == NULL) {
        std::cerr << "ERROR: cannot load function " << func_name << " from " << library << ": " << dlerror() << std::endl;
        std::cerr << "Robot main function must be 'void " << func_name << "(RobotController*)'" << std::endl;
        return -1;
    }
#endif
    setFunction(action);
    return 0;
}

void DLLScript::close() {
    if (lib_handle != NULL) {
    #ifdef _WIN32
        FreeLibrary(lib_handle);
    #else
        dlclose(lib_handle);
    #endif
        lib_handle = NULL;
    }
}

ScriptPtr DLLScript::newInstance(const std::string &filename, const std::string &func_name) {
    return std::make_shared<DLLScript>(filename, func_name);
}

}

