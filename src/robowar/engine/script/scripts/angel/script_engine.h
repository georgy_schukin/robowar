#pragma once

class asIScriptEngine;
class asSMessageInfo;

namespace robowar {

class ScriptEngine {
public:
    ScriptEngine();
    ~ScriptEngine();

    static ScriptEngine* getInstance();

    asIScriptEngine* getEngine() {
        return engine;
    }

private:
    void initEngine();

    void messageCallback(const asSMessageInfo *msg);

    void registerCommonFunctions();
    void registerCoordType();
    void registerInterfaces();

private:
    asIScriptEngine *engine;
};

}
