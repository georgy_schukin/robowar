#pragma once

#include "engine/game/coords.h"

namespace robowar {

class Robot;
class Base;
class Message;
class Cell;

#define WRAP(FUNC) template <class T> void wrap_##FUNC(T *obj) {obj->FUNC();}
#define WRAP1P(FUNC,P1) template <class T> void wrap_##FUNC(P1 a, T *obj) {obj->FUNC(a);}
#define WRAPR(RET,FUNC) template <class T> RET wrap_##FUNC(T *obj) {return obj->FUNC();}
#define WRAPR1P(RET,FUNC,P1) template <class T> RET wrap_##FUNC(P1 a, T *obj) {return obj->FUNC(a);}
#define WRAPR2P(RET,FUNC,P1,P2) template <class T> RET wrap_##FUNC(P1 a, P2 b, T *obj) {return obj->FUNC(a, b);}

// Wrappers for Object methods.
WRAPR(Coord,getCoord)
WRAPR(int,getTeamId)
WRAPR(bool,isEnemy)
WRAPR(bool,isNeutral)
WRAPR(int,getDistanceTo)
WRAPR(bool,isInViewRange)
WRAPR(bool,isInShootRange)
WRAPR(bool,isInTransmitRange)
WRAPR(int,getHitpoints)
WRAPR(int,getMaxHitpoints)

// Wrappers for Robot methods.
WRAPR(int,getWeaponCooldown)
WRAPR(int,getMaxWeaponCooldown)
WRAPR(int,getViewDistance)
WRAPR(int,getShootDistance)
WRAPR(int,getTransmitDistance)

// Wrappers for Base methods.
WRAPR(int,getConstructionCooldown)
WRAPR(int,getMaxConstructionCooldown)
WRAPR(bool,isActive)

// Wrappers for RobotController methods.
WRAPR(int,getNumOfRobotsInView)
WRAPR(int,getNumOfBasesInView)
WRAPR1P(Robot*,getRobotInView,int)
WRAPR1P(Base*,getBaseInView,int)
WRAPR(int,getNumOfMessages)
WRAPR1P(Message*,getMessage,int)
WRAPR(Message*,getMemory)
WRAPR(Message*,getOwnMessage)
WRAPR(bool,isMoving)
WRAPR(bool,isShooting)
WRAPR(bool,isTransmitting)
WRAPR(bool,canShoot)
WRAPR1P(bool,canMoveAt,int)
WRAPR1P(bool,canShootAt,const Coord&)
WRAP1P(moveAt,int)
WRAP1P(moveTo,const Coord&)
WRAP1P(shootAt,const Coord&)
WRAP(transmit)
WRAP(cancelMove)
WRAP(cancelShoot)
WRAP(cancelTransmit)
WRAPR1P(bool,cellExists,const Coord&)
WRAPR1P(Cell*,getCell,const Coord&)

}
