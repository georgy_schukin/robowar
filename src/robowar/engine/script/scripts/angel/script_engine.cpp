#include "script_engine.h"
#include "engine/game/coords.h"
#include "engine/script/interface.h"
#include "wrappers.h"

#include <angelscript/angelscript.h>
#include <angelscript/scriptstdstring/scriptstdstring.h>
#include <angelscript/scriptarray/scriptarray.h>
#include <mutex>
#include <memory>
#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <cstdlib>

namespace robowar {

namespace {

    void print(const std::string &str) {
        std::cout << str << std::endl;
    }

    void coordConstructorDefault(void *memory) {
        new (memory) Coord();
    }

    void coordConstructor(void *memory, int col, int row) {
        new (memory) Coord(col, row);
    }

    void coordDestructor(void *memory) {
        ((Coord*)memory)->~Coord();
    }

    // Example opCast behaviour
    template<class A, class B>
    B* referenceCast(A *a) {
        if (!a) return nullptr;
        return dynamic_cast<B*>(a);
    }

    template <class Base, class Derived>
    void registerInheritance(asIScriptEngine *engine, const char *base, const char *derived) {
        engine->RegisterObjectMethod(base, (std::string(derived) + "@ opCast()").c_str(), asFUNCTION((referenceCast<Base, Derived>)), asCALL_CDECL_OBJLAST);
        engine->RegisterObjectMethod(derived, (std::string(base) + "@ opImplCast()").c_str(), asFUNCTION((referenceCast<Derived, Base>)), asCALL_CDECL_OBJLAST);
        engine->RegisterObjectMethod(base, (std::string("const ") + derived + "@ opCast() const").c_str(), asFUNCTION((referenceCast<Base, Derived>)), asCALL_CDECL_OBJLAST);
        engine->RegisterObjectMethod(derived, (std::string("const ") + base + "@ opImplCast() const").c_str(), asFUNCTION((referenceCast<Derived, Base>)), asCALL_CDECL_OBJLAST);
    }

#define REGW(DEF,FUNC) engine->RegisterObjectMethod(class_name, DEF, asFUNCTION(wrap_##FUNC<T>), asCALL_CDECL_OBJLAST)
#define REGM(DEF,FUNC) engine->RegisterObjectMethod(class_name, DEF, asMETHOD(T,FUNC), asCALL_THISCALL)

    template <class T>
    void registerObjectMethods(asIScriptEngine *engine, const char *class_name) {
        REGW("Coord getCoord() const",getCoord);
        REGW("int getTeamId() const",getTeamId);
        REGW("bool isEnemy() const",isEnemy);
        REGW("bool isNeutral() const",isNeutral);
        REGW("int getDistanceTo() const",getDistanceTo);
        REGW("bool isInViewRange() const",isInViewRange);
        REGW("bool isInShootRange() const",isInShootRange);
        REGW("bool isInTransmitRange() const",isInTransmitRange);
        REGW("int getHitpoints() const",getHitpoints);
        REGW("int getMaxHitpoints() const",getMaxHitpoints);
    }

    template <class T>
    void registerRobotMethods(asIScriptEngine *engine, const char *class_name) {
        registerObjectMethods<T>(engine, class_name);        
        REGW("int getWeaponCooldown() const",getWeaponCooldown);
        REGW("int getMaxWeaponCooldown() const",getMaxWeaponCooldown);
        REGW("int getViewDistance() const",getViewDistance);
        REGW("int getShootDistance() const",getShootDistance);
        REGW("int getTransmitDistance() const",getTransmitDistance);
        REGW("bool canShoot() const",canShoot);
    }

    template <class T>
    void registerBaseMethods(asIScriptEngine *engine, const char *class_name) {
        registerObjectMethods<T>(engine, class_name);        
        REGW("int getConstructionCooldown() const",getConstructionCooldown);
        REGW("int getMaxConstructionCooldown() const",getMaxConstructionCooldown);
        REGW("bool isActive() const",isActive);
    }

    template <class T>
    void registerRobotControllerMethods(asIScriptEngine *engine, const char *class_name) {
        registerRobotMethods<T>(engine, class_name);
        REGW("int getNumOfRobotsInView() const",getNumOfRobotsInView);
        REGW("int getNumOfBasesInView() const",getNumOfBasesInView);
        REGW("Robot& getRobotInView(int) const",getRobotInView);
        REGW("Base& getBaseInView(int) const",getBaseInView);
        REGW("int getNumOfMessages() const",getNumOfMessages);
        REGW("Message& getMessage(int) const",getMessage);
        REGW("Message& getMemory() const",getMemory);
        REGW("Message& getOwnMessage() const",getOwnMessage);
        REGW("bool isShooting() const",isShooting);
        REGW("bool isMoving() const",isMoving);
        REGW("bool isTransmitting() const",isTransmitting);
        REGW("bool canMoveAt(int) const",canMoveAt);
        REGW("bool canShootAt(const Coord &in) const",canShootAt);
        REGW("void moveAt(int)",moveAt);
        REGW("void moveTo(const Coord &in)",moveTo);
        REGW("void shootAt(const Coord &in)",shootAt);
        REGW("void transmit()",transmit);
        REGW("void cancelMove()",cancelMove);
        REGW("void cancelShoot()",cancelShoot);
        REGW("void cancelTransmit()",cancelTransmit);
        REGW("bool cellExists(const Coord &in) const",cellExists);
        REGW("Cell& getCell(const Coord &in) const",getCell);
    }

    template <class T>
    void registerMessageMethods(asIScriptEngine *engine, const char *class_name) {
         REGM("int getSize() const",getSize);
         REGM("int read(int) const",read);
         REGM("void write(int, int)",write);
         REGM("void clear()",clear);
         REGM("void copy(const Message &in)",copy);
    }

    template <class T>
    void registerCellMethods(asIScriptEngine *engine, const char *class_name) {
         REGM("Coord getCoord() const",getCoord);
         REGM("bool isEmpty() const",isEmpty);
         REGM("bool isRobotHere() const",isRobotHere);
         REGM("bool isBaseHere() const",isBaseHere);
         REGM("Robot& getRobot() const",getRobot);
         REGM("Base& getBase() const",getBase);
    }

    template <class T>
    void registerReferenceType(asIScriptEngine *engine, const char *class_name) {
         engine->RegisterObjectType(class_name, 0, asOBJ_REF | asOBJ_NOCOUNT);
    }

    template <class T>
    void registerCoordMethods(asIScriptEngine *engine, const char *class_name) {
        REGM("int getX() const",getX);
        REGM("int getY() const",getY);
        REGM("int getRow() const",getRow);
        REGM("int getCol() const",getCol);
        REGM("Coord plus(const Coord &in) const",plus);
        REGM("Coord atDirection(int) const",atDirection);
        REGM("int distanceTo(const Coord &in) const",distanceTo);
        REGM("int directionTo(const Coord &in) const",directionTo);
    }

}

ScriptEngine::ScriptEngine() :
    engine(nullptr)
{
    engine = asCreateScriptEngine();
    if (!engine) {
        throw std::runtime_error("Failed to create script engine");
    }
    initEngine();
}

ScriptEngine::~ScriptEngine() {
    if (engine) {
        engine->ShutDownAndRelease();
    }
}

void ScriptEngine::initEngine() {
    engine->SetMessageCallback(asMETHOD(ScriptEngine, messageCallback), this, asCALL_THISCALL);

    engine->SetEngineProperty(asEP_DISALLOW_GLOBAL_VARS, true);
    engine->SetEngineProperty(asEP_DISALLOW_VALUE_ASSIGN_FOR_REF_TYPE, true);

    RegisterStdString(engine);
    RegisterScriptArray(engine, true);
    RegisterStdStringUtils(engine);

    registerCommonFunctions();
    registerCoordType();
    registerInterfaces();
}

void ScriptEngine::registerCommonFunctions() {
    engine->RegisterGlobalFunction("int rand()", asFUNCTION(rand), asCALL_CDECL);
    engine->RegisterGlobalFunction("void print(const string &in)", asFUNCTION(print), asCALL_CDECL);
}

void ScriptEngine::registerCoordType() {
    engine->RegisterObjectType("Coord", sizeof(Coord), asOBJ_VALUE | asGetTypeTraits<Coord>() | asOBJ_APP_CLASS_ALLINTS);
    engine->RegisterObjectBehaviour("Coord", asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(coordConstructorDefault), asCALL_CDECL_OBJLAST);
    engine->RegisterObjectBehaviour("Coord", asBEHAVE_CONSTRUCT, "void f(int, int)", asFUNCTION(coordConstructor), asCALL_CDECL_OBJLAST);
    engine->RegisterObjectBehaviour("Coord", asBEHAVE_DESTRUCT, "void f()", asFUNCTION(coordDestructor), asCALL_CDECL_OBJLAST);
    engine->RegisterObjectMethod("Coord", "Coord& opAssign(const Coord &in)", asMETHODPR(Coord, operator=, (const Coord&), Coord&), asCALL_THISCALL);
    registerCoordMethods<Coord>(engine, "Coord");

}

void ScriptEngine::registerInterfaces() {
    registerReferenceType<Object>(engine, "Object");
    registerReferenceType<Robot>(engine, "Robot");
    registerReferenceType<Base>(engine, "Base");    
    registerReferenceType<Message>(engine, "Message");
    registerReferenceType<Cell>(engine, "Cell");
    registerReferenceType<RobotController>(engine, "RobotController");

    registerInheritance<Object, Robot>(engine, "Object", "Robot");
    registerInheritance<Object, Base>(engine, "Object", "Base");
    registerInheritance<Robot, RobotController>(engine, "Robot", "RobotController");

    registerObjectMethods<Object>(engine, "Object");
    registerRobotMethods<Robot>(engine, "Robot");
    registerBaseMethods<Base>(engine, "Base");    
    registerMessageMethods<Message>(engine, "Message");
    registerCellMethods<Cell>(engine, "Cell");
    registerRobotControllerMethods<RobotController>(engine, "RobotController");
}

void ScriptEngine::messageCallback(const asSMessageInfo *msg) {
    std::string type = "ERR ";
    if (msg->type == asMSGTYPE_WARNING) {
        type = "WARN";
    } else if (msg->type == asMSGTYPE_INFORMATION) {
       type = "INFO";
    }
    std::ostringstream out;
    out << msg->section << "(" << msg->row << ", " << msg->col << ") : "
        << type << " : " << msg->message << "\n";
    std::cerr << out.str();
}

ScriptEngine* ScriptEngine::getInstance() {
    static std::mutex mutex;
    static std::unique_ptr<ScriptEngine> engine;

    std::unique_lock<std::mutex> lock(mutex);
    if (!engine) {
        engine = std::make_unique<ScriptEngine>();
    }
    return engine.get();
}

}
