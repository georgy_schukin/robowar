#include "angel_script.h"
#include "angel/script_engine.h"

#include <angelscript/angelscript.h>
#include <angelscript/scriptbuilder/scriptbuilder.h>
#include <stdexcept>
#include <iostream>

namespace robowar {

AngelScript::AngelScript(asIScriptEngine *engine, const std::string &filename) :
    engine(engine), module(nullptr), function(nullptr)
{
    module = loadModuleFromFile(filename);
    if (!module) {
        throw std::runtime_error("Failed to load module for " + filename);
    }
    function = module->GetFunctionByDecl("void execute(RobotController &inout)");
    if (!function) {
        throw std::runtime_error("Failed to find function 'void execute(RobotController&)' in " + filename);
    }
}

bool AngelScript::isValid() const {
    if (!function) {
        return false;
    }
    return true;
}

void AngelScript::run(RobotController *robot) {
    asIScriptContext *context = engine->CreateContext();
    if (!context) {
        throw std::runtime_error("Failed to create script context");
    }
    context->Prepare(function);
    context->SetArgObject(0, robot);
    try {
        auto result = context->Execute();
        if (result == asEXECUTION_EXCEPTION) {
            std::cerr << "Exception in a script: " << context->GetExceptionString()
                      << ", at line " << context->GetExceptionLineNumber() << std::endl;
        }
    }
    catch (std::exception &e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }

    context->Release();
}

asIScriptModule *AngelScript::loadModuleFromFile(const std::string &filename) {
    CScriptBuilder builder;
    auto mod_name = newModuleName();
    auto result = builder.StartNewModule(engine, mod_name.c_str());
    if (result < 0) {
        throw std::runtime_error("Failed to start new module for " + filename);
    }
    result = builder.AddSectionFromFile(filename.c_str());
    if (result < 0) {
        throw std::runtime_error("Failed to load script " + filename);
    }
    result = builder.BuildModule();
    if (result < 0) {
        throw std::runtime_error("Failed to build module for " + filename);
    }
    return builder.GetModule();
}

ScriptPtr AngelScript::newInstance(const std::string &filename) {
    auto *script_engine = ScriptEngine::getInstance();
    return std::make_shared<AngelScript>(script_engine->getEngine(), filename);
}

std::string AngelScript::newModuleName() {
    static int module_id = 0;
    return "module_" + std::to_string(module_id++);
}

}
