#include "local_script.h"

namespace robowar {

LocalScript::LocalScript(RobotFunction func) :
    function(func) {
}

void LocalScript::setFunction(RobotFunction func) {
    function = func;
}

bool LocalScript::isValid() const {
    return (function != nullptr);
}

void LocalScript::run(RobotController *robot) {
    if (function) {
        function(robot);
    }
}

ScriptPtr LocalScript::newInstance(RobotFunction func) {
    return std::make_shared<LocalScript>(func);
}

}

