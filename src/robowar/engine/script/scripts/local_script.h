#pragma once

#include "engine/script/script.h"

#include <functional>

namespace robowar {

typedef std::function<void(RobotController*)> RobotFunction;

/**
 * Script uses ptr to function in local memory.
 */
class LocalScript : public Script {
public:    
    LocalScript(RobotFunction func);

    void setFunction(RobotFunction func);

    virtual bool isValid() const;

    virtual void run(RobotController *robot);

    static ScriptPtr newInstance(RobotFunction func);

private:
    RobotFunction function;
};

}
