#pragma once

namespace robowar {

class GameState;
class GameInitState;

class GameListener {
public:
    virtual ~GameListener() {}

    virtual void handleInit(const GameInitState &init) = 0;
    virtual void handleUpdate(const GameState &state) = 0;
    virtual void handleFinish() = 0;
};

}
