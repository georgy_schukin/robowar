#pragma once

#include <string>

namespace robowar {

class Team;

class TeamState {
public:
    TeamState() {}
    TeamState(Team *team);

    int getId() const {
        return id;
    }

    const std::string& getName() const {
        return name;
    }

private:
    int id;
    std::string name;
};

}
