#include "robot_state.h"
#include "engine/game/objects/game_robot.h"

namespace robowar {

RobotState::RobotState(GameRobot *robot) :
    ActiveGameObjectState(robot)
{
    is_moving = robot->isMoving();
    is_shooting = robot->isShooting();
    is_transmitting = robot->isTransmitting();
    shoot_coord = robot->getShootCoord();
    move_coord = robot->getMoveCoord();
    view_distance = robot->getViewDistance();
    shoot_distance = robot->getShootDistance();
    transmit_distance = robot->getTransmitDistance();    
}

}
