#include "active_game_object_state.h"
#include "engine/game/objects/active_game_object.h"
#include "engine/game/objects/team.h"

namespace robowar {

ActiveGameObjectState::ActiveGameObjectState() :
    team_id(-1) {
}

ActiveGameObjectState::ActiveGameObjectState(ActiveGameObject *obj) {
    id = obj->getId();
    team_id = (obj->getTeam() ?  obj->getTeam()->getId() : -1);
    hitpoints = obj->getHitpoints();
    cooldown = obj->getCooldown();
    coord = obj->getCoord();
    damage = obj->getDamage();
    max_hitpoints = obj->getMaxHitpoints();
    max_cooldown = obj->getMaxCooldown();
}

bool ActiveGameObjectState::hasTeam() const {
    return (team_id != -1);
}

}
