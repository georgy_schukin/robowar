#include "game_state.h"
#include "engine/game/game_world.h"

namespace robowar {

GameState::GameState(GameWorld *world, int iter) :
    iteration(iter)
{
    for (auto *robot: world->getRobots()) {
        robots.push_back(RobotState(robot));
    }
    for (auto *base: world->getBases()) {
        bases.push_back(BaseState(base));
    }
}

}
