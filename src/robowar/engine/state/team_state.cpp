#include "team_state.h"
#include "engine/game/objects/team.h"

namespace robowar {

TeamState::TeamState(Team *team) {
    id = team->getId();
    name = team->getName();
}

}
