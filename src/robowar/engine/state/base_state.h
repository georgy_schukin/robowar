#pragma once

#include "active_game_object_state.h"

namespace robowar {

class GameBase;

class BaseState : public ActiveGameObjectState {
public:
    BaseState();
    BaseState(GameBase *base);

    int getAttackTeamId() const {
        return attack_team_id;
    }

private:    
    int attack_team_id;
};

}
