#include "game_init_state.h"
#include "engine/game/game_world.h"
#include "engine/game/game_map.h"

namespace robowar {

GameInitState::GameInitState(GameWorld *world) {
    cells = world->getMap()->getCellCoords();
    for (auto *team: world->getTeams()) {
        teams.push_back(TeamState(team));
    }
}

GameInitState::GameInitState(const GameInitState &state) :
    cells(state.cells), teams(state.teams) {
}

}
