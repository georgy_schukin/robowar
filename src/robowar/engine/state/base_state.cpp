#include "base_state.h"
#include "engine/game/objects.h"

namespace robowar {

BaseState::BaseState() :
    attack_team_id(-1) {
}

BaseState::BaseState(GameBase *base) :
    ActiveGameObjectState(base)
{
    attack_team_id = (base->getAttackTeam() ? base->getAttackTeam()->getId() : -1);
}

}
