#pragma once

#include "active_game_object_state.h"

namespace robowar {

class GameRobot;

class RobotState : public ActiveGameObjectState {
public:
    RobotState() {}
    RobotState(GameRobot *robot);

    bool isMoving() const {
        return is_moving;
    }

    bool isShooting() const {
        return is_shooting;
    }

    bool isTransmitting() const {
        return is_transmitting;
    }

    const Coord& getShootCoord() const {
        return shoot_coord;
    }

    const Coord& getMoveCoord() const {
        return move_coord;
    }

    int getViewDistance() const {
        return view_distance;
    }

    int getShootDistance() const {
        return shoot_distance;
    }

    int getTransmitDistance() const {
        return transmit_distance;
    }    

private:
    bool is_moving;
    bool is_shooting;
    bool is_transmitting;
    Coord shoot_coord;
    Coord move_coord;
    int view_distance;
    int shoot_distance;
    int transmit_distance;    
};

}
