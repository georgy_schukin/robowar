#pragma once

#include "team_state.h"
#include "engine/game/coords.h"

#include <vector>

namespace robowar {

class GameWorld;

class GameInitState {
public:
    GameInitState() {}
    GameInitState(GameWorld *world);
    GameInitState(const GameInitState &state);

    const std::vector<Coord>& getCells() const {
        return cells;
    }

    const std::vector<TeamState>& getTeams() const {
        return teams;
    }

private:
    std::vector<Coord> cells;
    std::vector<TeamState> teams;
};

}
