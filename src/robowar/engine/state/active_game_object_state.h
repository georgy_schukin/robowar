#pragma once

#include "engine/game/coords.h"

#include <string>

namespace robowar {

class ActiveGameObject;

class ActiveGameObjectState {
public:
    ActiveGameObjectState();
    ActiveGameObjectState(ActiveGameObject *obj);

    virtual ~ActiveGameObjectState() {}

    int getId() const {
        return id;
    }

    const Coord& getCoord() const {
        return coord;
    }

    int getTeamId() const {
        return team_id;
    }

    int getHitpoints() const {
        return hitpoints;
    }

    int getCooldown() const {
        return cooldown;
    }

    int getDamage() const {
        return damage;
    }

    int getMaxHitpoints() const {
        return max_hitpoints;
    }

    int getMaxCooldown() const {
        return max_cooldown;
    }

    bool hasTeam() const;       

private:
    int id;
    int team_id;
    int hitpoints;
    int damage;
    int cooldown;
    Coord coord;
    int max_hitpoints;
    int max_cooldown;
};

}
