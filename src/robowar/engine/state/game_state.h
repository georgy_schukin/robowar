#pragma once

#include "robot_state.h"
#include "base_state.h"

#include <vector>

namespace robowar {

class GameWorld;

class GameState {
public:
    GameState() {}
    GameState(GameWorld *world, int iter);

    const std::vector<RobotState>& getRobots() const {
        return robots;
    }

    const std::vector<BaseState>& getBases() const {
        return bases;
    }

    int getIteration() const {
        return iteration;
    }

private:
    std::vector<RobotState> robots;
    std::vector<BaseState> bases;
    int iteration;
};

}
