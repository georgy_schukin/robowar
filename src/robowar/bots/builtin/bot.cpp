#include "bot.h"

#include <cstdlib>

using namespace robowar;

namespace bots {

namespace {
    void attack(RobotController *robot, Object *obj) {
        if (obj->isInShootRange()) {
            robot->shootAt(obj->getCoord());
        } else {
            robot->moveTo(obj->getCoord());
        }
    }
}

void bot(RobotController *robot) {
    robot->moveAt(rand() % 7);

    for (int i = 0; i < robot->getNumOfBasesInView(); i++) {
        auto *base = robot->getBaseInView(i);
        if (base->isNeutral() || base->isEnemy()) {
            attack(robot, base);
            return;
        }
    }

    for (int i = 0; i < robot->getNumOfRobotsInView(); i++) {
        auto *bot = robot->getRobotInView(i);
        if (bot->isEnemy()) {
            attack(robot, bot);
            return;
        }
    }    
}

}
