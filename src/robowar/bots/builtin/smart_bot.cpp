#include "smart_bot.h"
#include <cstdlib>

using namespace robowar;

namespace bots {

namespace {
    void attack(RobotController *robot, Object *obj) {
        if (obj->isInShootRange()) {
            robot->shootAt(obj->getCoord());
        } else {
            robot->moveTo(obj->getCoord());
        }
    }

    void transmitCoord(RobotController *robot, const Coord &coord) {
        auto *msg = robot->getOwnMessage();
        msg->write(0, coord.getX()); // transmit robot coords to others
        msg->write(1, coord.getY());
        robot->transmit();
    }
}

int getNumOfFriends(RobotController *robot) {
    int num = 0;
    for (int i = 0; i < robot->getNumOfRobotsInView(); i++) {
        if (!robot->getRobotInView(i)->isEnemy()) {
            num++;
        }
    }
    return num;
}

int getNumOfEnemies(RobotController *robot) {
    int num = 0;
    for (int i = 0; i < robot->getNumOfRobotsInView(); i++) {
        if (robot->getRobotInView(i)->isEnemy()) {
            num++;
        }
    }
    return num;
}

void smartbot(RobotController *robot) {
    const auto friends_num = getNumOfFriends(robot); // num of friends
    const auto enemies_num = getNumOfEnemies(robot);

    robot->moveAt(rand() % 7);

    for (int i = 0; i < robot->getNumOfRobotsInView(); i++) { // for each robot in view
        auto *bot = robot->getRobotInView(i);
        if (bot->isEnemy()) { // robot is not in our team
            const auto bot_coord = bot->getCoord();
            attack(robot, bot);
            if (friends_num > enemies_num) {
                robot->moveTo(bot_coord); // move to robot
            } else if (enemies_num > friends_num) {
                robot->moveAt(bot_coord.directionTo(robot->getCoord())); // move away from enemy
            }
            transmitCoord(robot, bot_coord);
        }
    }    

    for (int i = 0; i < robot->getNumOfBasesInView(); i++) { // for each base in view
        auto *base = robot->getBaseInView(i);
        if (base->isEnemy() || base->isNeutral()) {// base is not in our team
            const auto base_coord = base->getCoord();
            attack(robot, base);
            if (friends_num > 1) {
                robot->moveTo(base_coord); // move to base
            }
            transmitCoord(robot, base_coord);
        }
    }

    if (!robot->isShooting() && (robot->getNumOfMessages() > 0)) { // if is idle and have income messages
        Message *msg = robot->getMessage(0); // first income message
        int tx = msg->read(0);
        int ty = msg->read(1);        
        robot->moveTo(Coord(tx, ty));
        if (!robot->isTransmitting()) {
            robot->getOwnMessage()->copy(msg);
            robot->transmit();
        }
    }
}

}
