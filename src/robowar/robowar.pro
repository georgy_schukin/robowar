TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++14
CONFIG -= app_bundle
CONFIG -= qt

TARGET = robowar

QMAKE_CXXFLAGS_RELEASE += -O2

exists(../Local.pri) {
    include(../Local.pri)
}

unix {        
    LIBS += -ldl    
    LIBS += -lpthread
    LIBS += -langelscript
}

win32 {        
    LIBS += -langelscript
}

SOURCES += \ 
    bots/builtin/bot.cpp \
    bots/builtin/smart_bot.cpp \
    engine/game/objects/team.cpp \
    engine/game/game_engine.cpp \
    engine/game/game_logic.cpp \
    engine/game/game_map.cpp \
    engine/script/script_library.cpp \
    engine/game/conf/robot_config.cpp \
    engine/script/impl/script_object.cpp \
    engine/script/impl/script_robot.cpp \
    engine/script/impl/script_robot_controller.cpp \
    engine/script/scripts/angel_script.cpp \
    engine/script/scripts/dll_script.cpp \
    engine/script/scripts/local_script.cpp \
    engine/script/scripts/angel/script_engine.cpp \
    engine/game/objects/game_base.cpp \
    engine/game/objects/game_robot.cpp \
    engine/game/objects/game_object.cpp \
    engine/script/impl/script_message.cpp \
    engine/game/conf/base_config.cpp \
    engine/game/objects/active_game_object.cpp \
    engine/script/impl/script_base.cpp \
    engine/game/coords.cpp \
    engine/game/game_world.cpp \
    engine/game/maps/rectangular_game_map.cpp \
    engine/state/game_init_state.cpp \
    engine/state/game_state.cpp \
    engine/game/game_session.cpp \
    engine/game/direction.cpp \   
    engine/state/base_state.cpp \
    engine/state/robot_state.cpp \
    engine/state/team_state.cpp \
    engine/state/active_game_object_state.cpp \
    engine/game/objects/game_cell.cpp \
    engine/script/impl/script_cell.cpp

HEADERS += \ 
    bots/builtin/bot.h \
    bots/builtin/bots.h \
    bots/builtin/smart_bot.h \
    engine/game/objects/active_game_object.h \
    engine/game/objects/game_object.h \
    engine/game/objects/team.h \
    engine/game/game_engine.h \
    engine/game/game_logic.h \
    engine/script/script.h \
    engine/script/script_library.h \         
    robowar.h \    
    engine/game/game_session.h \
    engine/game/conf/robot_config.h \
    engine/script/interfaces/base.h \
    engine/script/interfaces/robot.h \
    engine/script/interfaces/object.h \
    engine/script/interfaces/robot_controller.h \
    engine/script/interfaces/message.h \
    engine/script/impl/script_object.h \
    engine/script/impl/script_robot.h \
    engine/script/impl/script_robot_controller.h \
    engine/script/impl/script_base.h \
    engine/game/objects/game_robot.h \
    engine/game/objects/game_base.h \
    engine/script/scripts/angel_script.h \
    engine/script/scripts/dll_script.h \
    engine/script/scripts/local_script.h \
    engine/script/scripts/angel/script_engine.h \
    engine/game/coords.h \
    engine/script/impl/script_message.h \
    engine/game/conf/base_config.h \
    engine/game/game_map.h \
    engine/game/maps/rectangular_game_map.h \
    engine/game/game_world.h \
    engine/script/scripts.h \
    engine/game/objects.h \
    engine/state/game_state.h \
    engine/state/game_init_state.h \
    engine/state/game_listener.h \
    engine/script/interface.h \
    engine/script/implementation.h \
    engine/game/direction.h \  
    engine/state/states.h \
    engine/script/scripts/angel/wrappers.h \
    engine/state/base_state.h \
    engine/state/robot_state.h \
    engine/state/team_state.h \
    engine/state/active_game_object_state.h \
    engine/game/objects/game_cell.h \
    engine/script/interfaces/cell.h \
    engine/script/impl/script_cell.h


