#pragma once

#include "engine/script/interface.h"

#ifdef _WIN32
#ifdef ROBO_EXPORT
#define ROBO_FUNC __declspec(dllexport)
#else
#define ROBO_FUNC __declspec(dllimport)
#endif
#else
#define ROBO_FUNC
#endif
