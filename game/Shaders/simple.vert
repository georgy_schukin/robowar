#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in mat4 ModelMatrix;

//out vec3 color;

uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

void main()
{    
    vec4 pos = vec4(position, 1);
    vec4 worldPos = ModelMatrix*pos;
    //mat4 MVP = ProjectionMatrix*ViewMatrix*ModelMatrix;
    gl_Position = ProjectionMatrix*ViewMatrix*worldPos;
    //color = vec3(0.3, 0, 1);
    //color = vec3(clamp(worldPos.x/10, 0, 1), 0, clamp(worldPos.y/10, 0, 1));
}
