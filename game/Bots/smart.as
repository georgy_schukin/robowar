void attack(RobotController& robot, Object@ obj) {
    if (robot.canShootAt(obj.getCoord())) {
        robot.shootAt(obj.getCoord());
    } else {
        robot.moveTo(obj.getCoord());
    }
}

void transmitCoord(RobotController& robot, Coord coord) {
    Message@ msg = robot.getOwnMessage();
    msg.write(0, coord.getX()); 
    msg.write(1, coord.getY());
    robot.transmit();
}

int getNumOfFriends(RobotController& robot) {
    int num = 0;
    for (int i = 0; i < robot.getNumOfRobotsInView(); i++) {
        if (!robot.getRobotInView(i).isEnemy()) {
            num++;
        }
    }
    return num;
}

int getNumOfEnemies(RobotController& robot) {
    int num = 0;
    for (int i = 0; i < robot.getNumOfRobotsInView(); i++) {
        if (robot.getRobotInView(i).isEnemy()) {
            num++;
        }
    }
    return num;
}

void execute(RobotController& robot) {
    const int friends_num = getNumOfFriends(robot);
    const int enemies_num = getNumOfEnemies(robot);

    robot.moveAt(rand() % 7);

    for (int i = 0; i < robot.getNumOfRobotsInView(); i++) { // for each robot in view
        Robot@ bot = robot.getRobotInView(i);
        if (bot.isEnemy()) { // robot is not in our team
            const Coord bot_coord = bot.getCoord();
            attack(robot, bot);
            if (friends_num > enemies_num) {
                robot.moveTo(bot_coord); // move to robot
            } else if (enemies_num > friends_num) {
                robot.moveAt(bot_coord.directionTo(robot.getCoord())); // move away from enemy
            }
            transmitCoord(robot, bot_coord); // transmit bot coord to others
        }
    }    

    for (int i = 0; i < robot.getNumOfBasesInView(); i++) { // for each base in view
        Base@ base = robot.getBaseInView(i);
        if (base.isEnemy() || base.isNeutral()) { // base is not in our team
            const Coord base_coord = base.getCoord();
            attack(robot, base);
            if (friends_num > 1) {
                robot.moveTo(base_coord); // move to base
            }
            transmitCoord(robot, base_coord); // transmit base coord to others
        }
    }

    if (robot.getNumOfMessages() > 0) { // if have incoming messages
        Message@ msg = robot.getMessage(0); // first incoming message
        if (!robot.isShooting()) { // is idle        
            int tx = msg.read(0);
            int ty = msg.read(1);        
            robot.moveTo(Coord(tx, ty));
        }
        if (!robot.isTransmitting()) {
            robot.getOwnMessage().copy(msg);
            robot.transmit();
        }
    }
}
