// Simple bot

void attack(RobotController& robot, Object@ obj) {
        if (obj.isInShootRange()) {
            robot.shootAt(obj.getCoord());
        } else {
            robot.moveTo(obj.getCoord());
        }
    }

void execute(RobotController& robot) {
    robot.moveAt(rand() % 7);

    for (int i = 0; i < robot.getNumOfRobotsInView(); i++) {
        Robot@ bot = robot.getRobotInView(i);
        if (bot.isEnemy()) {
            attack(robot, bot);
            return;
        }
    }    

    for (int i = 0; i < robot.getNumOfBasesInView(); i++) {
        Base@ base = robot.getBaseInView(i);
        if (base.isNeutral() || base.isEnemy()) {
            attack(robot, base);
            return;
        }
    }
}
