#pragma once

#include "engine/model/robot_function.h"

#ifdef _WIN32
#ifdef ROBO_EXPORT
#define ROBO_FUNC __declspec(dllexport)
#else
#define ROBO_FUNC __declspec(dllimport)
#endif
#else
#define ROBO_FUNC
#endif
