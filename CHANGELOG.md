# Changelog

## [2.0.3] - 2017-07-12
### Fixed
- Fix robot transmissions
- Fix placement of bases for random world generation

## [2.0.2] - 2017-07-11
### Added
- Setting robot and base config from file.

## [2.0.1] - 2017-07-06
### Added
- Restart and stop game menu options.

### Fixed
- Fix robot movement resolution and dead robots elimination.
- Fix scene update when moving frame slider.

## [2.0.0] - 2017-07-05
### Added
- New interfaces.
- 2D visualizer.
- AngelScript support.
